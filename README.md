# Larawind - Laravel 8.0+ Jetstream and Tailwind CSS Admin Theme

This project is created with [Laravel Jetstream](https://jetstream.laravel.com/1.x/introduction.html) Framework and [Tailwind CSS](https://tailwindcss.com), the admin environment is desing by [Windmill Dashboard](https://windmill-dashboard.vercel.app/).

## Requirements

- Laravel installer
- Composer
- Npm installer

## Installation

```
# Clone the repository from GitHub and open the directory:
git clone https://github.com/miten5/larawind.git

# cd into your project directory
cd larawind

#install composer and npm packages
composer install
npm install && npm run dev

# Start prepare the environment:
cp .env.example .env // setup database credentials
php artisan key:generate
php artisan migrate
php artisan storage:link

# Run your server
php artisan serve

# Run migrations
1: php artisan migrate
2: php artisan db:seed --class=SucursalesSeeder
3: php artisan db:seed --class=HorariosImagenologicosSeeder
4: php artisan db:seed --class=SucursalesHorariosSeeder 
5: php artisan db:seed --class=EstudioSucursalesSeeder 
7: php artisan db:seed --class=CitasImagenologicosSeeder


# Para montar en producción primero tenemos que dropear dos tablas :
1: Horarios_imagenologicos
2: citas_imagenologicos

Corremos estos comandos: 
1: Composer install
1: php artisan migrate
2: Correr estos seeders en este orden:
    * php artisan db:seed --class=SucursalesSeeder
    * php artisan db:seed --class=HorariosImagenologicosSeeder
    * php artisan db:seed --class=SucursalesHorariosSeeder 
    * php artisan db:seed --class=EstudioSucursalesSeeder 
    * php artisan db:seed --class=CitasImagenologicosSeeder
3: Cambiar la liga de las apis que estan en imagenologia
4: Cambiar los correos de destino en la api


```
If you like my work [Buy me a coffee](https://www.buymeacoffee.com/miten5)

### Project made possible thanks to:

- [Laravel Jetstream](https://jetstream.laravel.com/1.x/introduction.html)
- [Tailwind CSS](https://tailwindcss.com/)
- [Windmill Dashboard](https://windmill-dashboard.vercel.app/)
