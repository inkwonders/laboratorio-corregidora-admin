<?php

namespace Database\Factories;

use App\Models\Sintoma;
use Illuminate\Database\Eloquent\Factories\Factory;

class SintomaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sintoma::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
