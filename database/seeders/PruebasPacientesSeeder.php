<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;

class PruebasPacientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paciente = Paciente::find(1);
        $paciente->pruebas()->attach(1);
        $paciente->pruebas()->attach(5);
        $paciente->pruebas()->attach(4);

        $paciente = Paciente::find(2);
        $paciente->pruebas()->attach(3);
        $paciente->pruebas()->attach(2);
        $paciente->pruebas()->attach(5);

        $paciente = Paciente::find(3);
        $paciente->pruebas()->attach(2);
        $paciente->pruebas()->attach(3);
        $paciente->pruebas()->attach(4);

        $paciente = Paciente::find(4);
        $paciente->pruebas()->attach(5);
        $paciente->pruebas()->attach(2);
        $paciente->pruebas()->attach(1);
    }
}
