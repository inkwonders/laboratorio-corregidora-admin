<?php

namespace Database\Seeders;

use App\Models\Sucursales;
use Illuminate\Database\Seeder;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sucursales::insert([
            ['sucursal' => 'Centro', 'ubicacion' => 'Corregidora 75-101 Col. Centro Querétaro, Qro.', 'estatus' =>'1'],
            ['sucursal' => 'Juriquilla', 'ubicacion' => 'Boulevard Universitario 560 Centro Comercial Juriquilla Col. Jurica Acueducto', 'estatus' => '1'],
            ['sucursal' => 'Tecnológico', 'ubicacion' => 'Av. Tecnológico Sur No. 2 local 103 planta baja Col. Niños Héroes. ', 'estatus' =>'0'],
            ['sucursal' => 'Marqués', 'ubicacion' => 'Avenida Prolongación Constituyentes No. 32 Oriente Calle Avenida Marqués de la Villa del Villar del Aguila, Altos del Marques, 76140 Santiago de Querétaro, Qro.', 'estatus' =>'1'],
            ['sucursal' => 'Pueblo nuevo', 'ubicacion' => 'Av. Rufino Tamayo No. 37 Planta baja interior 1 Col. Pueblo Nuevo Corregidora, Qro.', 'estatus' =>'0'],
            ['sucursal' => 'Milenio III', 'ubicacion' => 'Camino Real de Carretas 416 local 105 Plaza Ubika Fracc. Milenio III', 'estatus' =>'0'],
            ['sucursal' => 'El Refugio', 'ubicacion' => 'Anillo Fray Junípero Serra Plaza Ubika El Refugio, Local. 124 El Refugio, C.P. 76148, Querétaro, Qro.', 'estatus' =>'0'],
        ]);


    }
}
