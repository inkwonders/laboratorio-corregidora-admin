<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class AnalisisImagenologicosRadiografiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('analisis_imagenologicos')->insert([
            [
                'nombre' => 'Abdomen 1 proyeccion',
                'descripcion' => 'AP - Bipedestacion o decubito',
                'clave' => 'RX-001',
                'indicaciones' => 'Aseo',
                'costo' => 430.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'abdomen-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Abdomen 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-002',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'abdomen-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Tele de Torax (Pulmones)',
                'descripcion' => 'AP',
                'clave' => 'RX-003',
                'indicaciones' => 'Aseo',
                'costo' => 430.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'tele-de-torax',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Tele de Torax 2  (Pulmones) proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-004',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'tele-de-torax-2',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Torax osea 1 proyeccion (Hueso)',
                'descripcion' => 'PA',
                'clave' => 'RX-005',
                'indicaciones' => 'Aseo',
                'costo' => 430.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'torax-osea-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Antebrazo 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-006',
                'indicaciones' => 'Aseo',
                'costo' => 380.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'antebrazo-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Antebrazo 2 proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-007',
                'indicaciones' => 'Aseo',
                'costo' => 650.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'antebrazo-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Brazo 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-008',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'brazo-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Codo 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-009',
                'indicaciones' => 'Aseo',
                'costo' => 380.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'codo-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Codo 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-010',
                'indicaciones' => 'Aseo',
                'costo' => 650.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'codo-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Hombro 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-011',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'hombro-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Hombro 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-012',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'hombro-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Hombro comparativo 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-013',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'hombro-comparativo-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Hombro comparativo 4 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-014',
                'indicaciones' => 'Aseo',
                'costo' => 1400.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'hombro-comparativo-4-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Mano 1 proyecion',
                'descripcion' => 'AP, PA, Lateral u oblicua',
                'clave' => 'RX-015',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'mano-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Mano 2 proyeciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-016',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'mano-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Muñeca 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-017',
                'indicaciones' => 'Aseo',
                'costo' => 4450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'muneca-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Muñeca 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-018',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'muneca-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Muñeca 3 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-019',
                'indicaciones' => 'Aseo',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'muneca-3-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Muñeca 4 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-020',
                'indicaciones' => 'Aseo',
                'costo' => 1100.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'muneca-4-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Cervical 1 proyeccion ',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-021',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'cervical-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Cervical 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-022',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'cervical-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Cervical 4 proyecciones. Dinamicas',
                'descripcion' => 'AP, Lateral, flexion y extension ',
                'clave' => 'RX-023',
                'indicaciones' => 'Aseo',
                'costo' => 1100.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'cervical-4-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Dorsal 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-024',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'dorsal-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Dorsal 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-025',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'dorsal-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Dorsolumbar 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-026',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'dorsolumbar-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Lumbar 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-027',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'lumbar-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Lumbar 2 proyecciones',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-028',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'lumbar-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Lumbar 4 proyecciones. Dinamicas',
                'descripcion' => 'AP, Lateral, flexion, extension u oblicua',
                'clave' => 'RX-029',
                'indicaciones' => 'Aseo',
                'costo' => 1100.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'lumbar-4-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Lumbosacra 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-030',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'lumbosacra-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Lumbosacra 2 proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-031',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'lumbosacra-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Sacrococcigea 1 proyeccion (ultimo hueso de espalda)',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-032',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'sacrococcigea-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Sacrococcigea 2 proyecciones (ultimo hueso de espalda)',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-033',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'sacrococcigea-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pelvis adulto 1 proyeccion',
                'descripcion' => 'AP',
                'clave' => 'RX-034',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pelvis-adulto-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pelvis adulto 2 proyecciones',
                'descripcion' => 'AP y Posicion de ran',
                'clave' => 'RX-035',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pelvis-adulto-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Craneo 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-036',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'craneo-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Craneo 2 proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-037',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'craneo-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Craneo 3 proyecciones',
                'descripcion' => 'AP, PA y Lateral',
                'clave' => 'RX-038',
                'indicaciones' => 'Aseo',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'craneo-3-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Senos paranasales 3 proyecciones',
                'descripcion' => 'Waters, cadwell y lateral',
                'clave' => 'RX-039',
                'indicaciones' => 'Aseo',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'senos-paranasales-3-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Perfilograma 1 proyeccion',
                'descripcion' => 'AP o  Lateral',
                'clave' => 'RX-040',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'perfilograma-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Perfilograma 2 proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-041',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'perfilograma-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Femur 1 proyeccion',
                'descripcion' => 'AP o Lateral',
                'clave' => 'RX-042',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'femur-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Femur 2 proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-043',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'femur-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pie 1 proyeccion',
                'descripcion' => 'AP o Lateral u oblicua',
                'clave' => 'RX-044',
                'indicaciones' => 'Aseo',
                'costo' => 380.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pie-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pie 2 proyecciones',
                'descripcion' => 'AP o Lateral u oblicua',
                'clave' => 'RX-045',
                'indicaciones' => 'Aseo',
                'costo' => 650.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pie-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pie 3 proyecciones',
                'descripcion' => 'AP, Lateral y oblicua',
                'clave' => 'RX-046',
                'indicaciones' => 'Aseo',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pie-3-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pie comparativos 2 proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-047',
                'indicaciones' => 'Aseo',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pie-comparativos-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pie comparativos 3 proyecciones',
                'descripcion' => 'AP, Lateral y Oblicua',
                'clave' => 'RX-048',
                'indicaciones' => 'Aseo',
                'costo' => 1100.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pie-comparativos-3-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Rodilla 1 proyeccion',
                'descripcion' => 'AP',
                'clave' => 'RX-049',
                'indicaciones' => 'Aseo',
                'costo' => 450.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'rodilla-1-proyeccion',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Rodilla 2 proyecciones',
                'descripcion' => 'AP y Lateral',
                'clave' => 'RX-050',
                'indicaciones' => 'Aseo',
                'costo' => 700.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'rodilla-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Rodilla axiliares 30,60,90 g (merchant)',
                'descripcion' => 'Dinamicas',
                'clave' => 'RX-051',
                'indicaciones' => 'Aseo',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'rodilla-axiliares-30-60-90-g-merchant',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Rodilla comparativa 2 proyecciones',
                'descripcion' => 'AP  y Lateral ',
                'clave' => 'RX-052',
                'indicaciones' => 'Aseo',
                'costo' => 1100.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'rodilla-comparativa-2-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Rodilla comparativa 3 proyecciones',
                'descripcion' => 'AP, Lateral y Dinamicas',
                'clave' => 'RX-053',
                'indicaciones' => 'Aseo',
                'costo' => 1400.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'rodilla-comparativa-3-proyecciones',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Tobillo 1 proyeccion (Partes blandas)',
                'descripcion' => 'Lateral',
                'clave' => 'RX-054',
                'indicaciones' => 'Aseo',
                'costo' => 380.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'tobillo-1-proyeccion-partes-blandas',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Tobillo 2 proyeccion (Partes blandas)',
                'descripcion' => 'Ap y Lateral',
                'clave' => 'RX-055',
                'indicaciones' => 'Aseo',
                'costo' => 650.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
                La radiografia impresa tiene un costo adicional.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'tobillo-2-proyecciones-partes-blandas',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Serie Esofagogastroduodenal (SEGD)',
                'descripcion' => 'Estudio dinámico con medio de contraste balitado',
                'clave' => 'CRX-001',
                'indicaciones' => 'Ayuno de 8 hrs. Aseo. Acudir con ropa comoda',
                'costo' => 2000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
La radiografia impresa tiene un costo adicional.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'serie-esofagogastroduodenal-SEGD',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Transito intestinal',
                'descripcion' => 'Estudio dinámico con medio de contraste balitado',
                'clave' => 'CRX-002',
                'indicaciones' => 'Ayuno de 8 hrs. Aseo. Acudir con ropa comoda',
                'costo' => 2000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
La radiografia impresa tiene un costo adicional.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'transito-intestinal',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Colon por enema',
                'descripcion' => 'Estudio dinámico con medio de contraste balitado',
                'clave' => 'CRX-003',
                'indicaciones' => 'Dieta blanda el día previo. Aplicar 2 enemas (fleet). El primero 12 hrs antes del estudio. El segundo 2 hrs antes del estudio con la finalidad de “limpiar” el recto',
                'costo' => 2000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
La radiografia impresa tiene un costo adicional.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'colon-por-enema',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pielografia',
                'descripcion' => 'Estudio dinámico con medio de contraste hidrosoluble',
                'clave' => 'CRX-004',
                'indicaciones' => 'Ayuno de 8 hrs. Aseo. Acudir con ropa comoda',
                'costo' => 2000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
La radiografia impresa tiene un costo adicional.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pielografia',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Cistografia miccional',
                'descripcion' => 'Estudio dinámico con medio de contraste hidrosoluble',
                'clave' => 'CRX-005',
                'indicaciones' => 'Aseo. Acudir con ropa comoda',
                'costo' => 2000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
La radiografia impresa tiene un costo adicional.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'cistografia-miccional',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Histerosalpingografia',
                'descripcion' => 'Estudio dinámico con medio de contraste hidrosoluble',
                'clave' => 'CRX-006',
                'indicaciones' => 'Aseo. Acudir con ropa comoda',
                'costo' => 2000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
La radiografia impresa tiene un costo adicional.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'histerosalpingografia',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Trago de bario',
                'descripcion' => 'Estudio dinámico con medio de contraste balitado',
                'clave' => 'CRX-007',
                'indicaciones' => 'Ayuno de 8 hrs. Aseo. Acudir con ropa comoda',
                'costo' => 2000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea. Si requiere interpretacion se le entrega al dia siguiente de su estudio apartir de las 16:00 hrs.
La radiografia impresa tiene un costo adicional.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => null,
                'contacto' => 'Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'trago-de-bario',
                'tipoTipoestudioImagelologicos_id' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
