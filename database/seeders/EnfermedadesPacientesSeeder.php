<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;

class EnfermedadesPacientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paciente = Paciente::find(1);
        $paciente->enfermedades()->attach(6);
        $paciente->enfermedades()->attach(5);
        $paciente->enfermedades()->attach(4);

        $paciente = Paciente::find(2);
        $paciente->enfermedades()->attach(3);
        $paciente->enfermedades()->attach(2);
        $paciente->enfermedades()->attach(8);

        $paciente = Paciente::find(3);
        $paciente->enfermedades()->attach(2);
        $paciente->enfermedades()->attach(3);
        $paciente->enfermedades()->attach(7);

        $paciente = Paciente::find(4);
        $paciente->enfermedades()->attach(5);
        $paciente->enfermedades()->attach(2);
        $paciente->enfermedades()->attach(6);
    }
}
