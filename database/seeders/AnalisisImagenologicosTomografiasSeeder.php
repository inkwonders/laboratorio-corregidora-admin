<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Sucursales;

class AnalisisImagenologicosTomografiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipoestudio_images')->insert([

                [
                    'nombre' => 'Tomografía',
                    'activo' => '1',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                ],
            ]);

            $sucursal = Sucursales::find(4);
            $sucursal->sucursalesEstudio()->attach(5);

        DB::table('analisis_imagenologicos')->insert([
            [
                'nombre' => 'ABDOMEN Y PELVIS',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran los órganos del abdomen y la pelvis como son: hígado, páncreas, bazo, vesícula biliar, estomago, duodeno, yeyuno, ileon, ciego, colon, estructuras óseas, vejiga, etc.',
            'clave'=>'TS-001',
            'indicaciones' => null,
            'costo' => 2200.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-abdomen-y-pelvis',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],


            [
                'nombre' => 'TORAX',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran los órganos del tórax como son: pulmones, corazón, traquea, bronquios, esófago, costillas, estructuras óseas, etc.',
            'clave' =>'TS-002',
            'indicaciones' => null,
            'costo' => 2500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-torax',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'TORAX DE ALTA RESOLUCIÓN (TACAR)',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran los órganos del tórax enfocado principalmente a pulmones, traquea y bronquios.',
            'clave' =>'TS-003',
            'indicaciones' => null,
            'costo' => 3500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-torax-de-alta-resolucion-tacar',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'CRANEO',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras de la cabeza como son: huesos del craneo, encéfalo (cerebro) y cerebelo.',
            'clave' =>'TS-004',
            'indicaciones' => null,
            'costo' => 2000.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-craneo',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'CUELLO',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras del cuello como son: ganglios, tiroides, tranquea, columna cervical, glándulas salivales y cuerdas vocales',
            'clave' =>'TS-005',
            'indicaciones' => null,
            'costo' => 2200.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-cuello',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'EXTREMIDADES (PIERNAS O BRAZOS)',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones y piel) de las piernas o brazos.',
            'clave' =>'TS-006',
            'indicaciones' => null,
            'costo' => 2500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-extremidades-piernas-o-brazos',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'HOMBRO',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones y piel) del hombro.',
            'clave' =>'TS-007',
            'indicaciones' => null,
            'costo' => 2500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-hombro',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'HOMBROS',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones y piel) de ambos hombros.',
            'clave' =>'TS-008',
            'indicaciones' => null,
            'costo' => 4000.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-hombros',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'MANOS',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones y piel) de ambas manos.',
            'clave' =>'TS-009',
            'indicaciones' => null,
            'costo' => 4000.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-manos',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'PIES',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones y piel) de ambos pies.',
            'clave' =>'TS-010',
            'indicaciones' => null,
            'costo' => 3000.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-pies',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA CERVICAL',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna cervical.',
            'clave' =>'TS-011',
            'indicaciones' => null,
            'costo' => 2900.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-cervical',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA DORSAL (TORACICA)',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna dorsal.',
            'clave' =>'TS-012',
            'indicaciones' => null,
            'costo' => 2900.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-dorsal-toracica',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA LUMBAR',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna lumbar.',
            'clave' =>'TS-013',
            'indicaciones' => null,
            'costo' => 2900.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-lumbar',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA CERVICODORSAL',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna cervical y dorsal',
            'clave' =>'TS-014',
            'indicaciones' => null,
            'costo' => 4600.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-cervicodorsal',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA DORSOLUMBAR',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna dorsal y lumbar.',
            'clave' =>'TS-015',
            'indicaciones' => null,
            'costo' => 4600.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-dorsolumbar',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA COMPLETA',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de toda la columna (Cervical, dorsal y lumbar).',
            'clave' =>'TS-016',
            'indicaciones' => null,
            'costo' => 5900.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-completa',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'SENOS PARANASALES',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran los senos paranasales.',
            'clave' =>'TS-017',
            'indicaciones' => null,
            'costo' => 2200.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-senos-paranasales',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'OIDO',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran los huesos del oido y peñascos.',
            'clave' =>'TS-018',
            'indicaciones' => null,
            'costo' => 2600.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-oido',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'ORBITAS',
            'descripcion' =>'Tomografía simple (sin medio de contraste) en donde se valoran los globos oculares y los tejidos blandos (músculos periféricos y nervio óptico)',
            'clave' =>'TS-019',
            'indicaciones' => null,
            'costo' => 2600.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-orbitas',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'ABDOMEN Y PELVIS CON CONTRASTE INTRAVENOSO (IV)',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste vía venosa) en donde se valoran los órganos del abdomen y la pelvis como son: hígado, páncreas, bazo, vesícula biliar, estomago, duodeno, yeyuno, ileon, ciego, colon, estructuras óseas, vejiga, etc.',
            'clave' =>'TCON-001',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4200.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-abdomen-y-pelvis-con-contraste-intravenoso-iv',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'ABDOMEN Y PELVIS CON CONTRASTE ORAL ',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste tomado (vía oral)) en donde se valoran los órganos del abdomen y la pelvis como son: hígado, páncreas, bazo, vesícula biliar, estomago, duodeno, yeyuno, ileon, ciego, colon, estructuras óseas, vejiga, etc.',
            'clave' =>'TCON-002',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones. 4.- Debe tomar un liquido especial (medio de contraste oral) 3 horas antes del estudio, por lo que:   4.1 Puede recoger dicho liquido uno o dos dias previos a su cita para así empezar a tomarlo en su casa 3 horas antes de su estudio y presentarse a la hora acordada para realizar su tomografia.   4.2 Puede presentarse temprano en la sucursal “El Marques” y empezar a tomar dicho liquido ahí mismo, pasado el tiempo de transito intestinal (aproximadamente 3 hrs) se realizará su estudio.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-abdomen-y-pelvis-con-contraste-oral',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'ABDOMEN Y PELVIS CON DOBLE CONTRASTE (ORAL E INTRAVENOSO)',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste tomado (vía oral) y via venosa) en donde se valoran los órganos del abdomen y la pelvis como son: hígado, páncreas, bazo, vesícula biliar, estomago, duodeno, yeyuno, ileon, ciego, colon, estructuras óseas, vejiga, etc.',
            'clave' =>'TCON-003',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 - 12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones. 4.- Debe tomar un liquido especial (medio de contraste oral) 3 horas antes del estudio, por lo que:   4.1 Puede recoger dicho liquido uno o dos dias previos a su cita para así empezar a tomarlo en su casa 3 horas antes de su estudio y presentarse a la hora acordada para realizar su tomografia.   4.2 Puede presentarse temprano en la sucursal “El Marques” y empezar a tomar dicho liquido ahí mismo, pasado el tiempo de transito intestinal (aproximadamente 3 hrs) se realizará su estudio.',
            'costo' => 5500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-abdomen-y-pelvis-con-doble-contraste-oral-e-intravenoso',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'TORAX',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran los órganos del tórax como son: pulmones, corazón, traquea, bronquios, esófago, costillas, estructuras óseas, etc.',
            'clave' =>'TCON-004',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-torax-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'CRANEO',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras de la cabeza como son: huesos del craneo, encéfalo (cerebro) y cerebelo.',
            'clave' =>'TCON-005',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 2800.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-craneo-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'CUELLO',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste) en donde se valoran estructuras del cuello como son: ganglios, tiroides, tranquea, columna cervical, glándulas salivales y cuerdas vocales',
            'clave' =>'TCON-006',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 3500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-cuello-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'TOMOGRAFIA DE EXTREMIDADES (PIERNAS O BRAZOS)',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones y piel) de las piernas o brazos.',
            'clave' =>'TCON-007',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-tomografia-de-extremidades-piernas-o-brazos',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'ANGIOTOMOGRAFIA DE EXTREMIDADES (PIERNAS O BRAZOS)',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran las estructuras vasculares (arterias y venas) de las piernas o brazos. Asi como parcialmente valoración de tejidos blandos.',
            'clave' =>'TCON-008',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 6500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-angiotomografia-de-extremidades-piernas-o-brazos',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'ANGIOTOMOGRAFIA DE AORTA TORÁCICA Y TRONCOS SUPRA-AORTICOS.',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valora la aorta ascendente, cayado aórtico y segmento torácico de la aorta descendente así como la salida de los troncos supra-aorticos (arteria carótida izquierda, subclavia izquierda y tronco braquiocefálico derecho).',
            'clave' =>'TCON-009',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 6500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-angiotomografia-de-aorta-torácica-y-troncos-supra-aorticos.',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'ANGIOTOMOGRAFIA DE AORTA ABDOMINAL Y SUS RAMAS.',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valora la aorta descendente, arterias iliacas comunes y ramas de la aorta abdominal (tronco celiaco, arteria mesentérica superior, arteria mesentérica inferior y arterias renales).',
            'clave' =>'TCON-010',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 6500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-angiotomografia-de-aorta-abdominal-y-sus-ramas.',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'MANOS CON MEDIO DE CONTRASTE',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras vasculares (arterias y venas) así como óseas y tejidos blandos (músculo, partes valorable de tendones y piel) de ambas manos.',
            'clave' =>'TCON-011',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-manos-con-medio-de-contraste',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'PIES CON MEDIO DE CONTRASTE',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras vasculares (arterias y venas) así como óseas y tejidos blandos (músculo, partes valorable de tendones y piel) de ambos pies.',
            'clave' =>'TCON-012',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-pies-con-medio-de-contraste',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA CERVICAL CONTRASTADA',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna cervical.',
            'clave' =>'TCON-013',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-cervical-contrastada',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA DORSAL (TORACICA)',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna dorsal.',
            'clave' =>'TCON-014',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-dorsal-toracica-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA CERVICODORSAL',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna cervical.',
            'clave' =>'TCON-015',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 4500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-cervicodorsal-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA LUMBAR',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna cervical y dorsal.',
            'clave' =>'TCON-016',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 5500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-lumbar-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA DORSOLUMBAR',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de la columna dorsal y lumbar.',
            'clave' =>'TCON-017',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 5500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-dorsolumbar-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],



            [
                'nombre' => 'COLUMNA COMPLETA',
            'descripcion' =>'Tomografía simple y contrastada (con medio de contraste intravenoso) en donde se valoran estructuras óseas y tejidos blandos (músculo, partes valorable de tendones, piel y raíces nerviosas) de toda la columna (Cervical, dorsal y lumbar).',
            'clave' =>'TCON-018',
            'indicaciones' => '1.- Se requiere presentar reporte de creatinina en sangre previo al estudio (es un estudio de laboratorio en donde se valora la función renal, no requiere cita ni receta para realizarlo). Idealmente realizar dicho estudio de 1 a 7 días antes de la tomografía. 2.- Debe presentarse en ayuno de 8 -12 hrs 3.- Si es alérgico a algún medicamento o alimento, mencionarlo al momento de hacer la cita para que se le den indicaciones previas al estudio.  Si ya se le ha realizado algún estudio contrastado y presentó reacción alérgica, mencionarlo al medico radiólogo previo a la cita para que se le den indicaciones.',
            'costo' => 6500.00,
            'estimado_costo' => null,
            'tiempo_de_entrega' => 'Inmediatamente se le entrega un disco con sus imágenes, y de igual forma puede consultarlas en nuestro visualizador en linea y por WhatsApp. La interpretación se le envía al día siguiente de su estudio a partir de las 17:00 hrs.
            En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo día. * La tomografía impresa tiene un costo adicional.',
            'nombres_alternativos' => null,
            'contacto' => 'Requiere agendar cita. En horario de Lunes a Viernes de 7:00 a 18:00hrs, sábado de 7:00 a 13.30hrs. Para dudas o aclaraciones favor de llamar al 442-212-1052 opción 3, o mandar mensaje de WhatsApp al 442-474-9162',
            'slug' => 'tomografia-columna-completa-2',
            'tipoestudioimagelologicos_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],






        ]);
    }
}
