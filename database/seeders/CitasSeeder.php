<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cita;
use DateTime;
class CitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Cita::insert([
            [
                'horario_id' => '1',
                'paciente_id' => '1',
                'fecha_seleccionada' => '2021-05-27',
                'no_confirmacion' => '0afa2a65',
                'status_asistencia' => '1',
                'costo' => '2600',
                'tipo_prueba' => '1',
                'tipo_pago' => '1',
                'pagado' => '1',
                'tipo_tarjeta' => 'CREDITO/BBVA/Visa',
                'digitos' => '2964'
            ],
            [
                'horario_id' => '16',
                'paciente_id' => '4',
                'fecha_seleccionada' => date('Y-m-d'),
                'no_confirmacion' => '0bcca434',
                'status_asistencia' => '0',
                'costo' => '750',
                'tipo_prueba' => '2',
                'tipo_pago' => '0',
                'pagado' => '0',
                'tipo_tarjeta' => '',
                'digitos' => ''
            ],
            [
                'horario_id' => '1',
                'paciente_id' => '3',
                'fecha_seleccionada' => date("Y-m-d", strtotime(date('Y-m-d') . "+ 1 days")),
                'no_confirmacion' => '0ca5955d',
                'status_asistencia' => '0',
                'costo' => '750',
                'tipo_prueba' => '2',
                'tipo_pago' => '0',
                'pagado' => '0',
                'tipo_tarjeta' => '',
                'digitos' => ''
            ],
            [
                'horario_id' => '2',
                'paciente_id' => '2',
                'fecha_seleccionada' => '2021-12-26',
                'no_confirmacion' => '1708f7c5',
                'status_asistencia' => '0',
                'costo' => '750',
                'tipo_prueba' => '2',
                'tipo_pago' => '0',
                'pagado' => '0',
                'tipo_tarjeta' => '',
                'digitos' => ''
            ],
            [
                'horario_id' => '4',
                'paciente_id' => '1',
                'fecha_seleccionada' => '2021-06-01',
                'no_confirmacion' => '1402f7c5',
                'status_asistencia' => '0',
                'costo' => '2600',
                'tipo_prueba' => '1',
                'tipo_pago' => '0',
                'pagado' => '0',
                'tipo_tarjeta' => '',
                'digitos' => ''
            ]
        ]);
    }
}
