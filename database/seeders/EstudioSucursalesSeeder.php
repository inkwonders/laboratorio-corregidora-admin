<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sucursales;
use App\Models\TipoestudioImage;

class EstudioSucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sucursal = Sucursales::find(1);
        $sucursal->sucursalesEstudio()->attach(3);

        $sucursal = Sucursales::find(2);
        $sucursal->sucursalesEstudio()->attach(3);


        $sucursal = Sucursales::find(4);
        $sucursal->sucursalesEstudio()->attach(2);
        $sucursal->sucursalesEstudio()->attach(3);
        $sucursal->sucursalesEstudio()->attach(4);
        $sucursal->sucursalesEstudio()->attach(1);

    }
}
