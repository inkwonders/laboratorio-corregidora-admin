<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            ['name' => 'alain', 'email' => 'alain@inkwonders.com', 'password' => Hash::make('inkwonders')],
            // ['name' => 'GuilloMtzA','email' => 'guillo@inkwonders.com', 'password' => Hash::make('inkwonders')],
            // ['name' => 'Manuel Mendez Ramirez', 'email' => 'alainttlm1@gmail.com', 'password' => ''],
            // ['name' => 'Miguel Perea Resendis', 'email' => 'alainttlm2@gmail.com', 'password' => ''],
            // ['name' => 'Marco Perez Perea', 'email' => 'alainttlm3@gmail.com', 'password' => ''],
            // ['name' => 'Manuel Mendez Ramirez', 'email' => 'alainttlm4@gmail.com', 'password' => ''],
            // ['name' => 'Miguel Perea Resendis', 'email' => 'alainttlm5@gmail.com', 'password' => ''],
            // ['name' => 'Marco Perez Perea', 'email' => 'alainttlm6@gmail.com', 'password' => ''],
            // ['name' => 'Manuel Mendez Ramirez', 'email' => 'alainttlm7@gmail.com', 'password' => ''],
            // ['name' => 'Miguel Perea Resendis', 'email' => 'alainttlm8@gmail.com', 'password' => ''],
            // ['name' => 'Marco Perez Perea', 'email' => 'alainttlm9@gmail.com', 'password' => '']
        ]);
    }
}
