<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsuariosSeeder::class);
        $this->call(SintomasSeeder::class);
        $this->call(PruebasSeeder::class);
        $this->call(EnfermedadesSeeder::class);
        $this->call(HorariosSeeder::class);
        $this->call(UbicacionesSeeder::class);

        // $this->call(PacientesSeeder::class);
        // $this->call(CitasSeeder::class);
        // $this->call(SintomasPacientesSeeder::class);
        // $this->call(EnfermedadesPacientesSeeder::class);
        // $this->call(PruebasPacientesSeeder::class);

        $this->call(PermisosSeeder::class);
        $this->call(CategoriasSeeder::class);
        $this->call(AnalisisSeeder::class);
        // $this->call(EncriptacionSeeder::class);
        $this->call(TipoestudioImageSeeder::class);
        $this->call(AnalisisImagenologicosSeeder::class);
        $this->call(AnalisisImagenologicosRadiografiasSeeder::class);
        $this->call(HorariosImagenologicosSeeder::class);
    }
}
