<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AESCrypto;
use App\Models\ResponseTarjeta;

class TransferenciaResponseOrigen extends Seeder
{
    private $contador = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {

        DB::beginTransaction();

        try {

            $response_origen = DB::connection('mysql_origen')->table('response_14tg51ds5t')->get();
            $this->command->info("Se encontraron {$response_origen->count()} pagos");

            $response_origen->each(function($cadena, $key) {

                // Decodificar
                $key = 'DD33CEF1A4A0DDF130996A2BD052486D';
                $decryptedString = AESCrypto::desencriptar($cadena->strResponse, $key);

                //Cargar string desencriptado
                $xml = simplexml_load_string($decryptedString);

                $tipo_pago = isset($xml->datos_adicionales->data[5]->value) ? $xml->datos_adicionales->data[5]->value : 1;

                // dd(gettype($tipo_pago), $tipo_pago);

                if ($tipo_pago != '2') {
                    $tipo_compra = 1;
                } else {
                    $tipo_compra = 2;
                }

                $fecha_guardar = null;

                if( isset($xml->date) ){
                    $fecha_directa = explode("/",$xml->date);

                    if( isset( $fecha_directa[1])) {
                        $fecha_armada = $fecha_directa[0]."-".$fecha_directa[1]."-".$fecha_directa[2];
                        $fecha_guardar = date('Y/m/d', strtotime($fecha_armada));
                    }
                }

                // $fecha_directa = isset($xml->date) ? explode("/",$xml->date) : '';
                // $fecha_armada = $fecha_directa[0]."-".$fecha_directa[1]."-".$fecha_directa[2];
                // $fecha_guardar = date('Y/m/d', strtotime($fecha_armada));

                if(isset($xml->cc_type)){
                    $datos_tarjeta_explode = explode("/", $xml->cc_type);
                    $tipo_tarjeta_guardar = $datos_tarjeta_explode[0];
                    $banco_tarjeta_guardar = isset($datos_tarjeta_explode[1]) ? $datos_tarjeta_explode[1] : null;
                    $banco_emisor_tarjeta_guardar = isset($datos_tarjeta_explode[2]) ? $datos_tarjeta_explode[2] : null;
                }else{
                    $datos_tarjeta_explode = null;
                    $tipo_tarjeta_guardar = null;
                    $banco_tarjeta_guardar = null;
                    $banco_emisor_tarjeta_guardar = null;
                }

                // $datos_tarjeta_explode = explode("/", $xml->cc_type);
                // $tipo_tarjeta_guardar = $datos_tarjeta_explode[0];
                // $banco_tarjeta_guardar = $datos_tarjeta_explode[1];
                // $banco_emisor_tarjeta_guardar = $datos_tarjeta_explode[2];

                $tipo = $tipo_compra;
                $numero_confirmacion = isset($xml->reference) ? $xml->reference : '';
                $tipo_pago_cadena = 'Contado';
                $tarjeta_digitos = isset($xml->cc_mask) ? (string)$xml->cc_mask : null;

                $autorizacion = null;

                if(isset($xml->auth) && (string)$xml->auth != '') {
                    $autorizacion = $xml->auth;
                }

                $tipo_transaccion = 'E-COMMERCE';
                $importe = isset($xml->amount) ? (string)$xml->amount : null;
                $fecha = $fecha_guardar;
                $hora = isset($xml->time) ? (string)$xml->time : null;
                $tipo_tarjeta = $banco_tarjeta_guardar;
                $tarjeta = $tipo_tarjeta_guardar;
                $banco_emisor = $banco_emisor_tarjeta_guardar;
                $estatus = isset($xml->response) ? $xml->response : null;

                $nuevo_pago = ResponseTarjeta::create([
                    'response'                  => $cadena->strResponse,
                    'tipo'                      => $tipo,
                    'numero_confirmacion'       => $numero_confirmacion,
                    'tipo_pago'                 => $tipo_pago_cadena,
                    'tarjeta_digitos'           => $tarjeta_digitos,
                    'autorizacion'              => $autorizacion,
                    'tipo_transaccion'          => $tipo_transaccion,
                    'importe'                   => $importe,
                    'fecha'                     => $fecha,
                    'hora'                      => $hora,
                    'tipo_tarjeta'              => $tipo_tarjeta,
                    'tarjeta'                   => $tarjeta,
                    'banco_emisor'              => $banco_emisor,
                    'estatus'                   => $estatus,
                ]);

                $this->command->info("Se creó el pago {$nuevo_pago->id}");
                $this->contador ++;

            });

        } catch (\Throwable $th) {
            DB::rollback();
            $this->command->error("Ocurrió un error al migrar información: {$th->getMessage()} en la línea {$th->getLine()}");
        }

        DB::commit();
        $this->command->info("Se insertaron {$this->contador} registros");
    }
}
