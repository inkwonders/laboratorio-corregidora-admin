<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ListaCorreoEncuesta;

class ListaCorreoEncuestasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ListaCorreoEncuesta::insert([
            ['nombre' => 'mario',  'correo'=>'mario@inkwonders.com', 'estatus' => '1'],
            ['nombre' => 'guillo',  'correo'=>'guillo@inkwonders.com', 'estatus' => '1'],
            ['nombre' => 'yetzin',  'correo'=>'yetzin@inkwonders.com', 'estatus' => '1'],

        ]);
    }
}
