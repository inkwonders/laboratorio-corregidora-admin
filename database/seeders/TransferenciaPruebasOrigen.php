<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class TransferenciaPruebasOrigen extends Seeder
{
    private $contador = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $usuarios = User::with('pacientes')->has('pacientes')
            ->get()
            ->unique()
            ->mapWithKeys(function($user) {
                return [
                    strtolower($user->email) =>
                        (Object) [
                            'user_id' => $user->id,
                            'paciente_id' => $user->pacientes->first()->id
                        ]
                ];
            });

        try {

            $pruebas_paciente_origen = DB::connection('mysql_origen')
                ->table('pruebas_paciente_75d90786')
                ->select(
                    DB::raw('pruebas_paciente_75d90786.fk_prueba'),
                    'pacientes_b7db6bae.paciente_email'
                )
                ->distinct()
                ->leftjoin('pacientes_b7db6bae', 'pacientes_b7db6bae.sk_paciente', '=', 'pruebas_paciente_75d90786.fk_paciente')
                ->get();

            $this->command->info("Se encontraron {$pruebas_paciente_origen->count()} citas");

            $pruebas_paciente_origen->each(function($pruebas_paciente, $key) use ($usuarios) {

                $usuario_nuevo = $usuarios[ strtolower( $pruebas_paciente->paciente_email ) ];

                $prueba_id = '';

                switch($pruebas_paciente->fk_prueba){
                    case '96a2eb97-4475-11eb-95a6-04d4c4afee7c':
                        $prueba_id = 1;
                        break;
                    case '96a2f1fb-4475-11eb-95a6-04d4c4afee7c':
                        $prueba_id = 2;
                        break;
                    case '96a2f795-4475-11eb-95a6-04d4c4afee7c':
                        $prueba_id = 3;
                        break;
                    case '96a2fd30-4475-11eb-95a6-04d4c4afee7c':
                        $prueba_id = 4;
                        break;
                    case '96a302cc-4475-11eb-95a6-04d4c4afee7c':
                        $prueba_id = 5;
                        break;
                }

                $paciente = Paciente::find($usuario_nuevo->paciente_id);

                $paciente->pruebas()->attach($prueba_id);

                $this->command->info("Se asoció la enfermedad {$prueba_id} con el paciente {$usuario_nuevo->paciente_id}");
                $this->contador ++;

            });

        }catch (\Throwable $th){
            DB::rollback();
            $this->command->error("Ocurrió un error al migrar información: {$th->getMessage()} en la línea {$th->getLine()}");
        }

        DB::commit();
        $this->command->info("Se insertaron {$this->contador} registros");
    }
}
