<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class AnalisisImagenologicosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('analisis_imagenologicos')->insert([
            [
                'nombre' => 'Hígado y Vías Biliares',
                'descripcion' => 'Hígado
                Vía biliar intra y extrahepática
                Conducto colédoco
                Vesícula biliar
                PáncreasBazo',
                'clave' => 'US-001',
                'indicaciones' => 'Ayuno mínimo de 6 horas, idealmente 8 horas',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Ultrasonido hepatobiliar',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'higado-y-vias-biliares',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Renal',
                'descripcion' => 'Riñones y vejiga',
                'clave' => 'US-002',
                'indicaciones' => 'Ayuno mínimo de 4 horas, idealmente 8 horas',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Renal bilateral',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'renal',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Abdomen Superior',
                'descripcion' => 'Hígado
                Vía biliar intra y extrahepática
                Conducto colédoco
                Vesícula biliar
                Páncreas
                Bazo
                Riñones',
                'clave' => 'US-003',
                'indicaciones' => 'Ayuno mínimo de 6 horas, idealmente 8 horas',
                'costo' => 1100.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Abdomen total',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'abdomen-superior',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Abdomen Completo',
                'descripcion' => 'Hígado
                Vía biliar intra y extrahepática
                Conducto colédoco
                Vesícula biliar
                Páncreas
                Bazo
                Riñones
                Vejiga
                Útero y ovarios (mujeres)
                Próstata (hombres)',
                'clave' => 'US-004',
                'indicaciones' => 'Ayuno mínimo de 6 horas, idealmente 8 horas',
                'costo' => 1300.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Hepatorrenal Hígado y riñones',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'abdomen-completo',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Ultrasonido Apendicular',
                'descripcion' => 'Apéndice cecal (para descartar apendicitis',
                'clave' => 'US-005',
                'indicaciones' => 'De preferencia acudir con ganas de orinar (vejiga llena)',
                'costo' => 1000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Ultrasonido de fosa iliaca derecha',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'ultrasonido-apendicular',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'F.A.S.T.',
                'descripcion' => 'Líquido libre en abdomen',
                'clave' => 'US-006',
                'indicaciones' => 'Ninguna',
                'costo' => 1000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Búsqueda de colecciones',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'fast',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Vesico-prostático Con Orina Residual',
                'descripcion' => 'Vejiga Prostata',
                'clave' => 'US-007',
                'indicaciones' => 'Tener la vejiga llena, tomar 1 litro de agua natural 1 hora antes del estudio, (en caso de que el paciente tenga sonda, hay que pinzarla y esperar el tiempo requerido para llenar la vejiga)',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Prostático',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'vesico-prostatico-con-orina-residual',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Ginecológico (pélvico)',
                'descripcion' => 'Útero Endometrio Cervix Ovarios',
                'clave' => 'US-008',
                'indicaciones' => 'Tener la vejiga llena, tomar 1 litro de agua natural 1 hora antes del estudio, (en caso de que el paciente tenga sonda, pinzarla y esperar el tiempo requerido para llenar la vejiga)',
                'costo' => 900.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Pélvico',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'ginecologico',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Doppler Portal',
                'descripcion' => 'Hígado
                Vía biliar intra y extrahepática
                Conducto colédoco
                Vesícula biliar
                Páncreas Bazo Valoración vascular (Doppler) de la vena porta y de los vasos colaterales',
                'clave' => 'US-009',
                'indicaciones' => 'Dieta blanda el día previo al estudio y ese día antes del estudio',
                'costo' => 1600.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.',
                'nombres_alternativos' => 'Hígado y vías biliares Doppler',
                'contacto' => 'Se requiere agendar cita Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'ginecologico',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Renal Doppler',
                'descripcion' => 'Riñones y su vascularidad',
                'clave' => 'US-010',
                'indicaciones' => 'Dieta blanda el día previo al estudio y ese día antes del estudio',
                'costo' => 1800.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales. ',
                'nombres_alternativos' => 'Doppler renal bilateral',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'renal-doppler',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Doppler de Vena Cava',
                'descripcion' => 'Permeabilidad y vascularidad de la vena cava',
                'clave' => 'US-011',
                'indicaciones' => 'Dieta blanda el día previo al estudio y ese día antes del estudio',
                'costo' => 1600.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Valoración filtro de vena cava',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'doppler-de-vena-cava',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Tronco Celíaco y Mesentérico',
                'descripcion' => 'Permeabilidad de tronco celíaco y mesentéricas, su anatomía, trayectos, flujos. ',
                'clave' => 'US-012',
                'indicaciones' => 'Dieta blanda el día previo al estudio y ese día antes del estudio',
                'costo' => 1600.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Doppler de ramas de aorta abdominal',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'tronco-celiaco-y-mesenterico',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Ginecológico Endovaginal',
                'descripcion' => 'Útero
                Cervix uterino
                Endometrio
                Ovarios',
                'clave' => 'US-013',
                'indicaciones' => 'Aseo de región genital',
                'costo' => 1000.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Transvaginal ginecológico',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'ginecologico-endovaginal',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Prostático Transrectal',
                'descripcion' => 'Próstata',
                'clave' => 'US-014',
                'indicaciones' => 'Dieta blanda el día previo
                Aplicar 2 enemas (fleet)
                El primero 12 hrs antes del estudio
                El segundo 2 hrs antes del estudio
                con la finalidad de “limpiar” el recto',
                'costo' => 1250.00,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Prostático transrectal',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'prostatico-transrectal',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Venoso - Extremidad Superior',
                'descripcion' => 'Venas del brazo; desde axila hasta muñeca',
                'clave' => 'US-015',
                'indicaciones' => 'Aseo del área',
                'costo' => 1550,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Doppler venoso miembro superior o miembro torácico',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'venoso-extremidad-superior',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Arterial - Extremidad Superior',
                'descripcion' => 'Arterias del brazo; desde axila hasta muñeca',
                'clave' => 'US-016',
                'indicaciones' => 'Aseo del área',
                'costo' => 1550,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Doppler arterial miembro superior o torácico',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'arterial-extremidad-superior',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Venoso - Extremidad Inferior',
                'descripcion' => 'Venas de la pierna; desde ingle hasta tobillo',
                'clave' => 'US-017',
                'indicaciones' => 'Aseo del área',
                'costo' => 1550,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Doppler venoso miembro pélvico',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. .',
                'slug' => 'venoso-extremidad-inferior',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Arterial - Extremidad Inferior',
                'descripcion' => 'Arterias de la pierna; desde ingle hasta tobillo',
                'clave' => 'US-018',
                'indicaciones' => 'Aseo del área',
                'costo' => 1550,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
                En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia.  ',
                'nombres_alternativos' => 'Doppler arterial miembro pélvico',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'arterial-extremidad-inferior',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Carotídeo y Vertebral',
                'descripcion' => 'En ambos lados: Arteria carótida común, Arteria carótida interna y externa, Arteria vertebral',
                'clave' => 'US-019',
                'indicaciones' => 'Aseo del área',
                'costo' => 1900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Doppler carotídeo bilateral',
                'contacto' => 'Se requiere agendar cita
                Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'carotideo-y-vertebral',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Hombro',
                'descripcion' => 'Tendones del manguito rotador',
                'clave' => 'US-020',
                'indicaciones' => 'Aseo del área',
                'costo' => 1800,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido de manguito rotador',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'hombro',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Codo',
                'descripcion' => 'Tendones, ligamentos y músculos del codo',
                'clave' => 'US-021',
                'indicaciones' => 'Aseo del área',
                'costo' => 1800,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido articular de codo',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'codo',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Muñeca',
                'descripcion' => 'Tendones, ligamentos y músculos de la muñeca',
                'clave' => 'US-022',
                'indicaciones' => 'Aseo del área',
                'costo' => 1800,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido articular de muñeca',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'muneca',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Rodilla',
                'descripcion' => 'Tendones, ligamentos y músculos de la rodilla, bursas',
                'clave' => 'US-023',
                'indicaciones' => 'Aseo del área',
                'costo' => 1800,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido articular de rodilla',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'rodilla',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Tobillo',
                'descripcion' => 'Tendones, ligamentos y músculos del tobillo',
                'clave' => 'US-024',
                'indicaciones' => 'Aseo del área',
                'costo' => 1800,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido articular de tobillo',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'tobillo',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Mama',
                'descripcion' => 'Glándulas mamarias',
                'clave' => 'US-025',
                'indicaciones' => 'Aseo del área',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Mamario - ultrasonido de glándula mamaria',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'mama',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Testicular',
                'descripcion' => 'Testículos Epididimos',
                'clave' => 'US-026',
                'indicaciones' => 'Aseo de la región genital',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido escrotal',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'testicular',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Testicular Doppler',
                'descripcion' => 'Testículos, Epididimos, Plexos venosos',
                'clave' => 'US-027',
                'indicaciones' => 'Aseo de la región genital',
                'costo' => 1600,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Escrotal Doppler',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'testicular-doppler',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pene',
                'descripcion' => 'Cuerpos cavernosos',
                'clave' => 'US-028',
                'indicaciones' => 'Aseo de la región genital',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido de pene',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pene',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pene Doppler con Vasoactivo',
                'descripcion' => 'Cuerpos cavernosos. Valoración vascular post-inyección de medicamento vasoactivo. ',
                'clave' => 'US-029',
                'indicaciones' => 'Aseo de la región genital',
                'costo' => 4500,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido de pene con Caverjet',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pene-doppler-con-vasoactivo',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Tiroideo Doppler',
                'descripcion' => 'Glándula tiroides y su vascularidad',
                'clave' => 'US-030',
                'indicaciones' => 'Aseo cuello',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido tiroideo',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio.',
                'slug' => 'tiroideo-doppler',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Cuello',
                'descripcion' => 'Glándula tiroides, glándulas submandibulares, glândulas parotidas, cadenas ganglionares.',
                'clave' => 'US-031',
                'indicaciones' => 'Aseo cuello',
                'costo' => 1100,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido de cuello',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'cuello',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Protocolo Evaluación, Artritis Reumatoide',
                'descripcion' => 'Articulación de ambas muñecas para valoración de artritis',
                'clave' => 'US-032',
                'indicaciones' => 'Aseo manos',
                'costo' => 1900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido para artritis',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'protocolo-evaluación-artritis-reumatoide',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],

            [
                'nombre' => 'Ultrasonido Primer Trimestre (endocavitario Doppler)',
                'descripcion' => 'Embarazo',
                'clave' => 'US-034',
                'indicaciones' => 'Aseo de la región genital',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido endocavitario o endovaginal para valoración de embarazo temprano',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'ultrasonido-primer-trimestre-endocavitario-doppler',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Ultrasonido control 2do y 3er Trimestre',
                'descripcion' => 'Valoración general de condiciones del embarazo, crecimiento fetal , líquido amniótico, placenta, etc.',
                'clave' => 'US-035',
                'indicaciones' => 'Aseo',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido obstétrico 2do o 3er trimestre',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'ultrasonido-control-2do-y-3er-trimestre',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
             [
                'nombre' => 'Inguinal Unilateral',
                'descripcion' => 'Región inguinal',
                'clave' => 'US-036',
                'indicaciones' => 'Aseo región inguinal',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido inguinal. Valoración hernia inguinal',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'inguinal-unilateral',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Inguinal Bilateral',
                'descripcion' => 'Ambas regiones inguinales',
                'clave' => 'US-037',
                'indicaciones' => 'Aseo región inguinal',
                'costo' => 1400,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Ultrasonido inguinal. Valoración hernia inguinal',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'inguinal-bilateral',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Pared Abdominal',
                'descripcion' => 'Músculos y fascias de pared abdominal',
                'clave' => 'US-038',
                'indicaciones' => 'Aseo región a explorar',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Partes blandas pared abdominal. Valoración hernia pared abdominal',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'pared-abdominal',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Hernia',
                'descripcion' => 'Tejidos blandos en búsqueda de hernia en otra región',
                'clave' => 'US-039',
                'indicaciones' => 'Aseo región a explorar',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Hernia',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'hernia',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
             [
                'nombre' => 'Tejidos Blandos',
                'descripcion' => 'Lesión en tejidos blandos',
                'clave' => 'US-040',
                'indicaciones' => 'Aseo región a explorar',
                'costo' => 900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Partes blandas. Lesiones superficiales',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'tejidos-blandos',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
             [
                'nombre' => 'Transfrontanelar',
                'descripcion' => 'Estructura y probable lesion cerebral en niños recien nacidos',
                'clave' => 'US-041',
                'indicaciones' => 'Aseo región a explorar',
                'costo' => 1900,
                'estimado_costo' => null,
                'tiempo_de_entrega' => 'Al dia siguiente después de las 17:00hrs por mail o impreso recogiendolo en matriz, ó a los siguientes 2 dias si se recoge en cualquiera de nuestras sucursales.
En caso de requerir su estudio con urgencia, favor de comentarlo y se entrega el mismo dia. ',
                'nombres_alternativos' => 'Partes blandas. Lesiones superficiales',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'transfrontanelar',
                'tipoTipoestudioImagelologicos_id' => 3,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            # // biopsias
            [
                'nombre' => 'Biopsia de Tiroides guiada por ultrasonido',
                'descripcion' => 'Biopsia de nodulo tiroideo',
                'clave' => 'BX-001',
                'indicaciones' => null,
                'costo' => null,
                'estimado_costo' => '$4500 - $5900 dependiendo el tipo de biopsia que se realice y el numero de muestras que se requieran. Esto solo se puede determinar al momento de la biopsia. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-de-tiroides-guiada-por-ultrasonido',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Biopsia de mama guiada por ultrasonido',
                'descripcion' => 'Biopsia de nodulo mamario',
                'clave' => 'BX-002',
                'indicaciones' => null,
                'costo' => null,
                'estimado_costo' => '$4500 - $5900 dependiendo el tipo de biopsia que se realice y el numero de muestras que se requieran. Esto solo se puede determinar al momento de la biopsia. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-de-mama-guiada-por-ultrasonido',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Biopsia de mama guiada por ultrasonido',
                'descripcion' => 'Biopsia de nodulo mamario',
                'clave' => 'BX-003',
                'indicaciones' => null,
                'costo' => null,
                'estimado_costo' => '$4500 - $5900 dependiendo el tipo de biopsia que se realice y el numero de muestras que se requieran. Esto solo se puede determinar al momento de la biopsia. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-de-mama-guiada-por-ultrasonido',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                 'nombre' => 'Biopsia de tejidos blandos guiada por ultrasonido',
                'descripcion' => 'Biopsia de tumor en tejidos blandos',
                'clave' => 'BX-004',
                'indicaciones' => null,
                'costo' => null,
                'estimado_costo' => '$4500 - $5900 dependiendo el tipo de biopsia que se realice y el numero de muestras que se requieran. Esto solo se puede determinar al momento de la biopsia. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-de-tejidos-blandos-guiada-por-ultrasonido',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Biopsia Hepática',
                'descripcion' => 'Biopsia de tumor en tejidos blandos',
                'clave' => 'BX-005',
                'indicaciones' => 'Requiere cita con medico radiólogo para valoración del paciente, de estudios con lo que cuente, indicaciones, comentarios sobre riesgos, beneficios, etc. Favor de agendar cita al teléfono 442-474-9162',
                'costo' => null,
                'estimado_costo' => 'Se determina en consulta valorando particularmente el caso. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-hepatica',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Biopsia Pulmonar',
                'descripcion' => 'Biopsia de tumor, lesión o nódulo en pulmón',
                'clave' => 'BX-006',
                'indicaciones' => 'Requiere cita con medico radiólogo para valoración del paciente, de estudios con lo que cuente, indicaciones, comentarios sobre riesgos, beneficios, etc. Favor de agendar cita al teléfono 442-474-9162',
                'costo' => null,
                'estimado_costo' => 'Se determina en consulta valorando particularmente el caso. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-pulmonar',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                 'nombre' => 'Biopsia Mediastinal',
                'descripcion' => 'Biopsia de lesion en mediastino',
                'clave' => 'BX-007',
                'indicaciones' => 'Requiere cita con medico radiólogo para valoración del paciente, de estudios con lo que cuente, indicaciones, comentarios sobre riesgos, beneficios, etc. Favor de agendar cita al teléfono 442-474-9162',
                'costo' => null,
                'estimado_costo' => 'Se determina en consulta valorando particularmente el caso. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-mediastinal',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'nombre' => 'Biopsia Renal',
                'descripcion' => 'Biopsia de lesión renal. Biopsia para valoración de injerto renal. Biopsia para valoración de enfermedad autoinmune.',
                'clave' => 'BX-008',
                'indicaciones' => 'Requiere cita con medico radiólogo para valoración del paciente, de estudios con lo que cuente, indicaciones, comentarios sobre riesgos, beneficios, etc. Favor de agendar cita al teléfono 442-474-9162',
                'costo' => null,
                'estimado_costo' => 'Se determina en consulta valorando particularmente el caso. ',
                'tiempo_de_entrega' => null,
                'nombres_alternativos' => '',
                'contacto' => 'Se requiere agendar cita
Favor de llamar al 442-212-1052 opcion 3, o mandar mensaje de WhatsApp al 442-474-9162 para solicitar cotización de su estudio. ',
                'slug' => 'biopsia-renal',
                'tipoTipoestudioImagelologicos_id' => 4,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),  'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
