<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sucursales;
// use Doctrine\DBAL\Schema\Schema;

class SucursalesHorariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Schema::disableForeignKeyConstraints();

        // Sucursales::truncate();

        // CENTRO ULTRASONIDO
        $sucursal = Sucursales::find(1);
        $sucursal->horarios()->attach(8, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(9, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(10, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(11, ['id_tipo_estudio' => 3]);

        //JURIQUILLA ULTRASONIDO
        $sucursal = Sucursales::find(2);
        $sucursal->horarios()->attach(12, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(13, ['id_tipo_estudio' => 3]);

        // MARQUEZ ULTRASONIDO (ENTRE SEMANA)
        $sucursal = Sucursales::find(4);
        // $sucursal->horarios()->attach(1, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(2, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(3, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(4, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(5, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(6, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(7, ['id_tipo_estudio' => 3]);
        // dia de la semana 6 Ultrasonido Lunes y jueves
        $sucursal->horarios()->attach(25, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(26, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(27, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(28, ['id_tipo_estudio' => 3]);
        // dia de la semana 7 Ultrasonido martes y viernes
        $sucursal->horarios()->attach(29, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(30, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(31, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(32, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(33, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(34, ['id_tipo_estudio' => 3]);
        $sucursal->horarios()->attach(35, ['id_tipo_estudio' => 3]);
         // dia de la semana 7 Ultrasonido miercoles
         $sucursal->horarios()->attach(36, ['id_tipo_estudio' => 3]);
         $sucursal->horarios()->attach(37, ['id_tipo_estudio' => 3]);
         $sucursal->horarios()->attach(38, ['id_tipo_estudio' => 3]);
         $sucursal->horarios()->attach(39, ['id_tipo_estudio' => 3]);
         // dia de la semana 7 Ultrasonido sabado
         $sucursal->horarios()->attach(40, ['id_tipo_estudio' => 3]);
         $sucursal->horarios()->attach(41, ['id_tipo_estudio' => 3]);
         $sucursal->horarios()->attach(42, ['id_tipo_estudio' => 3]);


        // MARQUEZ ULTRASONIDO (SABADO)
        // $sucursal = Sucursales::find(4);
        // $sucursal->horarios()->attach(14, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(15, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(16, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(17, ['id_tipo_estudio' => 3]);
        // $sucursal->horarios()->attach(18, ['id_tipo_estudio' => 3]);

        // MARQUEZ RADIOGRAFIA
        $sucursal = Sucursales::find(4);
        $sucursal->horarios()->attach(19, ['id_tipo_estudio' => 1]);
        $sucursal->horarios()->attach(20, ['id_tipo_estudio' => 1]);
        $sucursal->horarios()->attach(21, ['id_tipo_estudio' => 1]);
        $sucursal->horarios()->attach(22, ['id_tipo_estudio' => 1]);
        $sucursal->horarios()->attach(23, ['id_tipo_estudio' => 1]);
        // $sucursal->horarios()->attach(24, ['id_tipo_estudio' => 1]);

        // MARQUEZ RADIOLOGIA
        $sucursal = Sucursales::find(4);
        $sucursal->horarios()->attach(19, ['id_tipo_estudio' => 2]);
        $sucursal->horarios()->attach(20, ['id_tipo_estudio' => 2]);
        $sucursal->horarios()->attach(21, ['id_tipo_estudio' => 2]);
        $sucursal->horarios()->attach(22, ['id_tipo_estudio' => 2]);
        $sucursal->horarios()->attach(23, ['id_tipo_estudio' => 2]);
        // $sucursal->horarios()->attach(24, ['id_tipo_estudio' => 2]);

        // MARQUEZ BIOPSIAS
        $sucursal = Sucursales::find(4);
        $sucursal->horarios()->attach(19, ['id_tipo_estudio' => 4]);
        $sucursal->horarios()->attach(20, ['id_tipo_estudio' => 4]);
        $sucursal->horarios()->attach(21, ['id_tipo_estudio' => 4]);
        $sucursal->horarios()->attach(22, ['id_tipo_estudio' => 4]);
        $sucursal->horarios()->attach(23, ['id_tipo_estudio' => 4]);
        // $sucursal->horarios()->attach(24, ['id_tipo_estudio' => 4]);


        // Schema::enableForeignKeyConstraints();


    }
}
