<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class TransferenciaEnfermedadesOrigen extends Seeder
{

    private $contador = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $usuarios = User::with('pacientes')->has('pacientes')
            ->get()
            ->unique()
            ->mapWithKeys(function($user) {
                return [
                    strtolower($user->email) =>
                        (Object) [
                            'user_id' => $user->id,
                            'paciente_id' => $user->pacientes->first()->id
                        ]
                ];
            });

        try {

            $enfermedades_paciente = DB::connection('mysql_origen')
                ->table('enfermedades_paciente_0276a999')
                ->select(
                    DB::raw('enfermedades_paciente_0276a999.fk_enferemdad'),
                    'pacientes_b7db6bae.paciente_email'
                )
                ->distinct()
                ->leftjoin('pacientes_b7db6bae', 'pacientes_b7db6bae.sk_paciente', '=', 'enfermedades_paciente_0276a999.fk_paciente')
                ->get();

            $this->command->info("Se encontraron {$enfermedades_paciente->count()} citas");

            $enfermedades_paciente->each(function($enfermedades_paciente, $key) use ($usuarios) {
                $this->contador ++;

                $usuario_nuevo = $usuarios[ strtolower( $enfermedades_paciente->paciente_email ) ];

                $enfermedad_id = '';

                switch($enfermedades_paciente->fk_enferemdad){
                    case '5e64d9ed-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 1;
                        break;
                    case '5e64e124-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 2;
                        break;
                    case '5e64e6e2-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 3;
                        break;
                    case '5e64ec90-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 4;
                        break;
                    case '5e64f23a-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 5;
                        break;
                    case '5e64f7e5-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 6;
                        break;
                    case '5e64fd8c-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 7;
                        break;
                    case '5e650347-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 8;
                        break;
                    case '5e6508ea-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 9;
                        break;
                    case '5e650e91-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 10;
                        break;
                    case '5e651458-43b0-11eb-a442-04d4c4afee7c':
                        $enfermedad_id = 11;
                        break;
                }

                $paciente = Paciente::find($usuario_nuevo->paciente_id);

                $paciente->enfermedades()->syncWithoutDetaching($enfermedad_id);

                $this->command->info("Se asoció la enfermedad {$enfermedad_id} con el paciente {$usuario_nuevo->paciente_id}");
            });

        }catch (\Throwable $th){
            DB::rollback();
            $this->command->error("Ocurrió un error al migrar información: {$th->getMessage()} en la línea {$th->getLine()}");
        }

        DB::commit();

        $this->command->info("Se insertaron {$this->contador} elementos");
    }
}
