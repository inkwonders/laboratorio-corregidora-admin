<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::insert([
            ['nombre' => 'PRUEBAS FRECUENTES'],
            ['nombre' => 'HEMATOLOGÍA'],
            ['nombre' => 'COAGULACIÓN'],
            ['nombre' => 'COPROPARASITOSCOPIA'],
            ['nombre' => 'UROANÁLISIS'],
            ['nombre' => 'QUÍMICA CLÍNICA'],
            ['nombre' => 'QUÍMICA URINARIA'],
            ['nombre' => 'HORMONAS'],
            ['nombre' => 'ALERGIAS'],
            ['nombre' => 'ANTICUERPOS'],
            ['nombre' => 'CULTIVOS'],
            ['nombre' => 'BIOLOGÍA MOLECULAR'],
            ['nombre' => 'PERFILES'],
            ['nombre' => 'MARCADORES TUMORALES'],
            ['nombre' => 'GENERAL']
        ]);
    }
}
