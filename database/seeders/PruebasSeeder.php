<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class PruebasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pruebas')->insert([
            ['nombre' => 'Porque voy a viajar', 'orden' => '1'],
            ['nombre' => 'Me van a operar', 'orden' => '2'],
            ['nombre' => 'Tengo síntomas de COVID-19', 'orden' => '3'],
            ['nombre' => 'Estuve en contacto con personas COVID-19 	', 'orden' => '4'],
            ['nombre' => 'Seguimiento', 'orden' => '5']
        ]);
    }
}
