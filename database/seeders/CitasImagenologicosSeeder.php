<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\CitaImagenologico;
use App\Models\AnalisisImagenologico;
use App\Models\Scheduleimage;


class CitasImagenologicosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $analisis = AnalisisImagenologico::find(3);
        // $horario = Scheduleimage::find(1);
        CitaImagenologico::insert([
            [
                'id'=>'2',
                'nombre'=>'Patricio Hurtado Garcia',
                'telefono'=>'4423798532',
                'correo'=>'francisco.hurtado@itwpolymex.com',
                'fecha_nacimiento'=>'2008-12-02',
                'fecha_cita'=>'2022-03-16',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'6',
                'created_at'=>'2022-03-10 14:45:19',
                'updated_at'=>'2022-03-10 14:45:19',
                'sucursal'=>'4',
            ],
            [
                'id'=>'3',
                'nombre'=>'Aldo Pablo Mayorga Fernandez',
                'telefono'=>'4421153027',
                'correo'=>'aldomayor@hotmail.com',
                'fecha_nacimiento'=>'1981-02-16',
                'fecha_cita'=>'2022-03-16',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'1',
                'created_at'=>'2022-03-15 12:47:10',
                'updated_at'=>'2022-03-15 12:47:10',
                'sucursal'=>'4',
            ],
            [
                'id'=>'4',
                'nombre'=>'Fortino Fernando Hernández Martínez',
                'telefono'=>'4191913811',
                'correo'=>'espinocabrera@hotmail.com',
                'fecha_nacimiento'=>'1970-11-12',
                'fecha_cita'=>'2022-03-21',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'1',
                'created_at'=>'2022-03-20 08:30:13',
                'updated_at'=>'2022-03-20 08:30:13',
                'sucursal'=>'4',
            ],
            [
                'id'=>'5',
                'nombre'=>'Fortino Fernando Hernández Martínez',
                'telefono'=>'4428199287',
                'correo'=>'espinocabrera@hotmail.com',
                'fecha_nacimiento'=>'1970-11-12',
                'fecha_cita'=>'2022-03-22',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'1',
                'created_at'=>'2022-03-20 09:57:43',
                'updated_at'=>'2022-03-20 09:57:43',
                'sucursal'=>'4',
            ],
            [
                'id'=>'6',
                'nombre'=>'PruebaInkwonders',
                'telefono'=>'4424347581',
                'correo'=>'oswaldoferral@gmail.com',
                'fecha_nacimiento'=>'2022-03-25',
                'fecha_cita'=>'2022-03-26',
                'analisisimagelologico_id'=>'1',
                'scheduleimage_id'=>'9',
                'created_at'=>'2022-03-22 14:03:28',
                'updated_at'=>'2022-03-22 14:03:28',
                'sucursal'=>'4'
            ],
            [
                'id'=>'7',
                'nombre'=>'María Eugenia Peralta Arce',
                'telefono'=>'4421541265',
                'correo'=>'maruperalta18@gmail.com',
                'fecha_nacimiento'=>'1967-09-18',
                'fecha_cita'=>'2022-03-23',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'1',
                'created_at'=>'2022-03-22 21:27:33',
                'updated_at'=>'2022-03-22 21:27:33',
                'sucursal'=>'4'
            ],
            [
                'id'=>'8',
                'nombre'=>'Prueba Eddy',
                'telefono'=>'1111111111',
                'correo'=>'eddy.hb29@gmail.com',
                'fecha_nacimiento'=>'2022-03-22',
                'fecha_cita'=>'2022-03-24',
                'analisisimagelologico_id'=>'1',
                'scheduleimage_id'=>'1',
                'created_at'=>'2022-03-23 09:06:10',
                'updated_at'=>'2022-03-23 09:06:10',
                'sucursal'=>'4'
            ],
            [
                'id'=>'9',
                'nombre'=>'Arantza sanchez reyes',
                'telefono'=>'5519191947',
                'correo'=>'araarss@hotmail.com',
                'fecha_nacimiento'=>'1996-06-09',
                'fecha_cita'=>'2022-03-24',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'5',
                'created_at'=>'2022-03-24 07:24:44',
                'updated_at'=>'2022-03-24 07:24:44',
                'sucursal'=>'4'
            ],
            [
                'id'=>'10',
                'nombre'=>'REGINA LILIAN ARIAS ESPARZA',
                'telefono'=>'4421488922',
                'correo'=>'eduardoariaskanemoto@gmail.com',
                'fecha_nacimiento'=>'2006-05-20',
                'fecha_cita'=>'2022-04-02',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'9',
                'created_at'=>'2022-03-28 18:16:21',
                'updated_at'=>'2022-03-28 18:16:21',
                'sucursal'=>'4'
            ],
            [
                'id'=>'11',
                'nombre'=>'Araceli caballero aguilar',
                'telefono'=>'4421468950',
                'correo'=>'acaballeroconferencista@gmail.com',
                'fecha_nacimiento'=>'2022-04-02',
                'fecha_cita'=>'2022-04-02',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'19',
                'created_at'=>'2022-04-01 20:48:53',
                'updated_at'=>'2022-04-01 20:48:53',
                'sucursal'=>'4'
            ],
            [
                'id'=>'12',
                'nombre'=>'Araceli caballero aguilar',
                'telefono'=>'4421468950',
                'correo'=>'acaballeroconferencista@gmail.com',
                'fecha_nacimiento'=>'1966-02-02',
                'fecha_cita'=>'2022-04-02',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'19',
                'created_at'=>'2022-04-01 20:51:12',
                'updated_at'=>'2022-04-01 20:51:12',
                'sucursal'=>'4'
            ],
            [
                'id'=>'13',
                'nombre'=>'IVAN DE JESUS HERNANDEZ DIAZ',
                'telefono'=>'4423052935',
                'correo'=>'miguelhhburgos@gmail.com',
                'fecha_nacimiento'=>'1986-12-29',
                'fecha_cita'=>'2022-04-04',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'19',
                'created_at'=>'2022-04-03 18:43:33',
                'updated_at'=>'2022-04-03 18:43:33',
                'sucursal'=>'4'
            ],
            [
                'id'=>'14',
                'nombre'=>'Diego Salazar',
                'telefono'=>'4422580932',
                'correo'=>'carbajalqueta@gmail.com',
                'fecha_nacimiento'=>'2009-04-29',
                'fecha_cita'=>'2022-04-04',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'19',
                'created_at'=>'2022-04-03 18:43:33',
                'updated_at'=>'2022-04-03 18:43:33',
                'sucursal'=>'4'
            ],
            [
                'id'=>'15',
                'nombre'=>'Pablo Salazar Carbajal',
                'telefono'=>'4422580932',
                'correo'=>'carbajalqueta@gmail.com',
                'fecha_nacimiento'=>'2011-01-22',
                'fecha_cita'=>'2022-04-04',
                'analisisimagelologico_id'=>'1',
                'scheduleimage_id'=>'19',
                'created_at'=>'2022-04-03 23:49:11',
                'updated_at'=>'2022-04-03 23:49:11',
                'sucursal'=>'4'
            ],
            [
                'id'=>'16',
                'nombre'=>'Irma Mireya Jiménez Rubio',
                'telefono'=>'4421495787',
                'correo'=>'mireyajr@hotmail.com',
                'fecha_nacimiento'=>'1971-06-28',
                'fecha_cita'=>'2022-04-09',
                'analisisimagelologico_id'=>'3',
                'scheduleimage_id'=>'19',
                'created_at'=>'2022-04-08 16:51:33',
                'updated_at'=>'2022-04-08 16:51:33',
                'sucursal'=>'4'
            ],
            [
                'id'=>'17',
                'nombre'=>'Bertha Veronica Angeles Yreta',
                'telefono'=>'4421753030',
                'correo'=>'angelesyreta@gmail.com',
                'fecha_nacimiento'=>'1970-12-16',
                'fecha_cita'=>'2022-04-09',
                'analisisimagelologico_id'=>'1',
                'scheduleimage_id'=>'4',
                'created_at'=>'2022-04-09 10:53:44',
                'updated_at'=>'2022-04-09 10:53:44',
                'sucursal'=>'4'
            ],
        ]);

        // $citaImagen=CitaImagenologico::find(1);
        // $citaImagen->horarios()->attach(1);
        // $citaImagen->analisImg()->attach(3);

    }
}
