<?php

namespace Database\Seeders;

use App\Models\Sintoma;
use Illuminate\Database\Seeder;

class SintomasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sintoma::insert([
            ['nombre' => 'Inicio súbito de los síntomas', 'orden' => 1],
            ['nombre' => 'Fiebre', 'orden' => 2],
            ['nombre' => 'Tos', 'orden' => 3],
            ['nombre' => 'Cefalea', 'orden' => 4],
            ['nombre' => 'Disnea', 'orden' => 5],
            ['nombre' => 'Irritabilidad', 'orden' => 6],
            ['nombre' => 'Diarrea', 'orden' => 7],
            ['nombre' => 'Dolor torácico', 'orden' => 8],
            ['nombre' => 'Escalofríos', 'orden' => 9],
            ['nombre' => 'Odinofagia (pérdida del olfato y gusto)', 'orden' => 10],
            ['nombre' => 'Mialgias (dolor muscular)', 'orden' => 11],
            ['nombre' => 'Artralgias (dolor de huesos)', 'orden' => 12],
            ['nombre' => 'Ataque al estado general', 'orden' => 13],
            ['nombre' => 'Rinorrea (nariz congestionada o tapada)', 'orden' => 14],
            ['nombre' => 'Polipnea (respiración rápida)', 'orden' => 15],
            ['nombre' => 'Vómito', 'orden' => 16],
            ['nombre' => 'Dolor abdominal', 'orden' => 17],
            ['nombre' => 'Conjuntivitis', 'orden' => 18],
            ['nombre' => 'Cianosis (coloración azulada de la piel)', 'orden' => 19]
        ]);
    }
}
