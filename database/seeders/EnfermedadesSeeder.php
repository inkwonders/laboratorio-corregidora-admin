<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;


class EnfermedadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enfermedades')->insert([
            ['nombre' => 'Ninguna', 'orden' => '1'],
            ['nombre' => 'Diabetes', 'orden' => '2'],
            ['nombre' => 'EPOC (Enfermedad Pulmonar Obstructiva Crónica)', 'orden' => '3'],
            ['nombre' => 'Asma', 'orden' => '4'],
            ['nombre' => 'Inmunosupresión', 'orden' => '5'],
            ['nombre' => 'Hipertensión', 'orden' => '6'],
            ['nombre' => 'VIH/SIDA', 'orden' => '7'],
            ['nombre' => 'Enfermedad cardiovascular', 'orden' => '8'],
            ['nombre' => 'Obesidad', 'orden' => '9'],
            ['nombre' => 'Insuficiencia renal crónica', 'orden' => '10'],
            ['nombre' => 'Tabaquismo', 'orden' => '11'],
        ]);
    }
}
