<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Scheduleimage;
use DB;

class HorariosImagenologicosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * Explicacion de horario:
     *
     * 1: Lunes, Martes, Miércoles, Jueves y Viernes 8:00am - 6:00pm
     * 2: Lunes y jueves 8:00am - 1:00pm
     * 3: Martes y Viernes 9:00am - 11:00am
     * 4: Sabado 8:00am - 01:00pm
     * 5: Lunes a Sábado 7:00am -1:00pm
     *
     * @return void
     */
    public function run()
    {
        // $borrarDatos = Scheduleimage::all();
        // dd($borrarDatos);
        // $borrarDatos->first()->delete();
        DB::table('schedule_images')->insert([
            ['horario_inicial' => '08:00:00', 'horario_final' => '09:00:00','numero_pacientes' => '1000','dia_semana' => '1','orden' =>'1'],
            ['horario_inicial' => '09:00:00', 'horario_final' => '10:00:00','numero_pacientes' => '1000','dia_semana' => '1','orden' =>'2'],
            ['horario_inicial' => '10:00:00', 'horario_final' => '11:00:00','numero_pacientes' => '1000','dia_semana' => '1','orden' =>'3'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '1','orden' =>'4'],
            ['horario_inicial' => '13:00:00', 'horario_final' => '14:00:00','numero_pacientes' => '1000','dia_semana' => '1','orden' =>'5'],
            ['horario_inicial' => '16:00:00', 'horario_final' => '17:00:00','numero_pacientes' => '1000','dia_semana' => '1','orden' =>'6'],
            ['horario_inicial' => '17:00:00', 'horario_final' => '18:00:00','numero_pacientes' => '1000','dia_semana' => '1','orden' =>'7'],
            ['horario_inicial' => '08:00:00', 'horario_final' => '09:00:00','numero_pacientes' => '1000','dia_semana' => '2','orden' =>'8'],
            ['horario_inicial' => '09:00:00', 'horario_final' => '10:00:00','numero_pacientes' => '1000','dia_semana' => '2','orden' =>'9'],
            ['horario_inicial' => '10:00:00', 'horario_final' => '11:00:00','numero_pacientes' => '1000','dia_semana' => '2','orden' =>'10'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '2','orden' =>'11'],
            ['horario_inicial' => '09:00:00', 'horario_final' => '10:00:00','numero_pacientes' => '1000','dia_semana' => '3','orden' =>'12'],
            ['horario_inicial' => '10:00:00', 'horario_final' => '11:00:00','numero_pacientes' => '1000','dia_semana' => '3','orden' =>'13'],
            ['horario_inicial' => '08:00:00', 'horario_final' => '09:00:00','numero_pacientes' => '1000','dia_semana' => '4','orden' =>'14'],
            ['horario_inicial' => '09:00:00', 'horario_final' => '10:00:00','numero_pacientes' => '1000','dia_semana' => '4','orden' =>'15'],
            ['horario_inicial' => '10:00:00', 'horario_final' => '11:00:00','numero_pacientes' => '1000','dia_semana' => '4','orden' =>'16'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '4','orden' =>'17'],
            ['horario_inicial' => '13:00:00', 'horario_final' => '14:00:00','numero_pacientes' => '1000','dia_semana' => '4','orden' =>'18'],
            ['horario_inicial' => '07:00:00', 'horario_final' => '08:00:00','numero_pacientes' => '1000','dia_semana' => '5','orden' =>'19'],
            ['horario_inicial' => '08:00:00', 'horario_final' => '09:00:00','numero_pacientes' => '1000','dia_semana' => '5','orden' =>'20'],
            ['horario_inicial' => '09:00:00', 'horario_final' => '10:00:00','numero_pacientes' => '1000','dia_semana' => '5','orden' =>'21'],
            ['horario_inicial' => '10:00:00', 'horario_final' => '11:00:00','numero_pacientes' => '1000','dia_semana' => '5','orden' =>'22'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '5','orden' =>'23'],
            ['horario_inicial' => '00:00:00', 'horario_final' => '00:00:00','numero_pacientes' => '10000','dia_semana' => '5','orden' =>'24'],
            //dia de la semana 6, ultrasonido marques Lunes y jueves
            ['horario_inicial' => '11:00:00', 'horario_final' => '12:00:00','numero_pacientes' => '1000','dia_semana' => '6','orden' =>'25'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '6','orden' =>'26'],
            ['horario_inicial' => '13:00:00', 'horario_final' => '14:00:00','numero_pacientes' => '1000','dia_semana' => '6','orden' =>'27'],
            ['horario_inicial' => '17:00:00', 'horario_final' => '18:00:00','numero_pacientes' => '1000','dia_semana' => '6','orden' =>'28'],
            //dia de la semana 7, ultrasonido marques martes y viernes
            ['horario_inicial' => '8:00:00', 'horario_final' => '9:00:00','numero_pacientes' => '1000','dia_semana' => '7','orden' =>'29'],
            ['horario_inicial' => '9:00:00', 'horario_final' => '10:00:00','numero_pacientes' => '1000','dia_semana' => '7','orden' =>'30'],
            ['horario_inicial' => '10:00:00', 'horario_final' => '11:00:00','numero_pacientes' => '1000','dia_semana' => '7','orden' =>'31'],
            ['horario_inicial' => '11:00:00', 'horario_final' => '12:00:00','numero_pacientes' => '1000','dia_semana' => '7','orden' =>'32'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '7','orden' =>'33'],
            ['horario_inicial' => '13:00:00', 'horario_final' => '14:00:00','numero_pacientes' => '1000','dia_semana' => '7','orden' =>'34'],
            ['horario_inicial' => '17:00:00', 'horario_final' => '18:00:00','numero_pacientes' => '1000','dia_semana' => '7','orden' =>'35'],
            //dia de la semana 8, ultrasonido marques miercoles
            ['horario_inicial' => '8:00:00', 'horario_final' => '9:00:00','numero_pacientes' => '1000','dia_semana' => '8','orden' =>'36'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '8','orden' =>'37'],
            ['horario_inicial' => '13:00:00', 'horario_final' => '14:00:00','numero_pacientes' => '1000','dia_semana' => '8','orden' =>'38'],
            ['horario_inicial' => '14:00:00', 'horario_final' => '15:00:00','numero_pacientes' => '1000','dia_semana' => '8','orden' =>'39'],
            //dia de la semana 9, ultrasonido marques sabado
            ['horario_inicial' => '10:00:00', 'horario_final' => '11:00:00','numero_pacientes' => '1000','dia_semana' => '9','orden' =>'40'],
            ['horario_inicial' => '11:00:00', 'horario_final' => '12:00:00','numero_pacientes' => '1000','dia_semana' => '9','orden' =>'41'],
            ['horario_inicial' => '12:00:00', 'horario_final' => '13:00:00','numero_pacientes' => '1000','dia_semana' => '9','orden' =>'42'],

        ]);
    }
}
