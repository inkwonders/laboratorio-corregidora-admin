<?php

namespace Database\Seeders;

use App\Models\Cita;
use App\Models\Paciente;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class TransferenciaCitasOrigen extends Seeder
{
    private $contador = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $usuarios = User::with('pacientes')->has('pacientes')
            ->get()
            ->unique()
            ->mapWithKeys(function($user) {
                return [
                    strtolower($user->email) =>
                        (Object) [
                            'user_id' => $user->id,
                            'paciente_id' => $user->pacientes->first()->id
                        ]
                ];
            });

        try {
            $citas_origen = DB::connection('mysql_origen')
                ->table('citas_f731d7ac')
                ->select(
                    DB::raw('citas_f731d7ac.*'),
                    'pacientes_b7db6bae.paciente_email'
                )
                ->leftjoin('pacientes_b7db6bae', 'pacientes_b7db6bae.sk_paciente', '=', 'citas_f731d7ac.fk_paciente')->get();

            $this->command->info("Se encontraron {$citas_origen->count()} citas");

            $citas_origen->each(function($citas_origen, $key) use ($usuarios) {

                //$id_viejo = Paciente::where('id_viejo','=',$citas_origen->fk_paciente)->first();
                $usuario_nuevo = $usuarios[ strtolower( $citas_origen->paciente_email ) ];

                $horario_id = '';

                switch($citas_origen->fk_horario){
                    case '83fae73b-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 1;
                        break;
                    case '83faee5d-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 2;
                        break;
                    case '83faf302-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 3;
                        break;
                    case '83faf816-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 4;
                        break;
                    case '83fafc05-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 5;
                        break;
                    case '83fb03f9-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 6;
                        break;
                    case '83fb0ab8-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 7;
                        break;
                    case '83fb10c6-43e4-11eb-a442-04d4c4afee7c':
                        $horario_id = 8;
                        break;
                    case 'ac416b09-b8cb-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 9;
                        break;
                    case 'ac4171bf-b8cb-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 10;
                        break;
                    case 'ac4177b9-b8cb-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 11;
                        break;
                    case 'ac417da2-b8cb-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 12;
                        break;
                    case 'ac4183a0-b8cb-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 13;
                        break;
                    case 'ac418988-b8cb-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 14;
                        break;
                    case 'b2c2b03a-b8c9-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 15;
                        break;
                    case 'b2c2b6e5-b8c9-11eb-a3c0-04d4c4afee7c':
                        $horario_id = 16;
                        break;
                }

                $nueva_cita = Cita::create([
                    'horario_id'            => $horario_id,
                    'paciente_id'           => $usuario_nuevo->paciente_id,
                    'no_confirmacion'       => $citas_origen->no_confirmacion,
                    'status_asistencia'     => $citas_origen->status_asistencia,
                    'fecha_seleccionada'    => $citas_origen->fecha_seleccionada,
                    'tipo_pago'             => $citas_origen->tipo_pago == null ? 1 : $citas_origen->tipo_pago,
                    'pagado'                => $citas_origen->pagado,
                    'response'              => $citas_origen->response,
                    'tipo_tarjeta'          => $citas_origen->tipo_tarjeta,
                    'digitos'               => $citas_origen->digitos,
                    'cadena'                => $citas_origen->cadena,
                    'costo'                 => $citas_origen->costo == null ? 0 : $citas_origen->costo,
                    'tipo_prueba'           => $citas_origen->tipo_prueba == 0 ? 1 : $citas_origen->tipo_prueba,
                ]);

                $this->command->info("Se creó la cita {$nueva_cita->no_confirmacion} con el id {$nueva_cita->id} -- {$key}");

                $this->contador ++;

            });

        }catch (\Throwable $th){
            DB::rollback();
            $this->command->error("Ocurrió un error al migrar información: {$th->getMessage()} en la línea {$th->getLine()}");
            Log::error($th);
        }

        DB::commit();
        $this->command->info("Se insertaron {$this->contador} citas");
    }
}
