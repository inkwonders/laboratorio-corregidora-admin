<?php

namespace Database\Seeders;

use App\Models\Paciente;
use App\Models\User;
use App\Models\Cita;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransferenciaBaseOrigen extends Seeder
{
    private $contador = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {
            $pacientes_origen = DB::connection('mysql_origen')->table('pacientes_b7db6bae')->get();

            $this->command->info("Se encontraron {$pacientes_origen->count()} pacientes");

            $pacientes_origen->each(function($paciente_origen, $key) {
                $nuevo_usuario = User::whereEmail($paciente_origen->paciente_email)->first();

                if( ! $nuevo_usuario ) {
                    $nuevo_usuario = User::create([
                        'email' => strtolower( $paciente_origen->paciente_email ),
                        'name'  => "{$paciente_origen->nombre} {$paciente_origen->apellido_paterno} {$paciente_origen->apellido_materno}"
                    ]);
                }

                $nuevo_paciente = Paciente::create([
                    'user_id'                    => $nuevo_usuario->id,
                    'ubicacion_id'               => $paciente_origen->fk_ubicacion,
                    'clave'                      => $paciente_origen->clave,
                    'nombre'                     => $paciente_origen->nombre,
                    'apellido_paterno'           => $paciente_origen->apellido_paterno,
                    'apellido_materno'           => $paciente_origen->apellido_materno,
                    'nombre_completo'            => "{$paciente_origen->nombre} {$paciente_origen->apellido_paterno} {$paciente_origen->apellido_materno}",
                    'genero'                     => $paciente_origen->genero,
                    'fecha_nacimiento'           => $paciente_origen->fecha_nacimiento,
                    'calle'                      => $paciente_origen->calle,
                    'colonia'                    => $paciente_origen->colonia,
                    'no_exterior'                => $paciente_origen->no_exterior,
                    'no_interior'                => $paciente_origen->no_interior,
                    'cp'                         => $paciente_origen->cp,
                    'tel_celular'                => $paciente_origen->tel_celular,
                    'tel_casa'                   => $paciente_origen->tel_casa,
                    'paciente_email'             => $paciente_origen->paciente_email,
                    'medico_nombre'              => $paciente_origen->medico_nombre,
                    'medico_email'               => $paciente_origen->medico_email,
                    'razon_social'               => $paciente_origen->razon_social,
                    'rfc'                        => $paciente_origen->rfc,
                    'domicilio_fiscal'           => $paciente_origen->domicilio_fiscal,
                    'otro_sintoma'               => $paciente_origen->otro_sintoma,
                    'otra_enfermedad'            => $paciente_origen->otra_enfermedad,
                    'contacto_persona_positivo'  => $paciente_origen->contacto_persona_positivo,
                    'antiviral'                  => $paciente_origen->antiviral,
                    'influenza'                  => $paciente_origen->influenza,
                    'otro_prueba'                => $paciente_origen->otro_prueba,
                    'fecha_alta'                 => $paciente_origen->fecha_alta,
                    'pasaporte'                  => $paciente_origen->pasaporte,
                    'es'                         => $paciente_origen->es,
                    'en'                         => $paciente_origen->en,
                ]);

                $this->contador ++;

                $this->command->info("Se creó el paciente {$nuevo_usuario->email} con el id {$nuevo_paciente->id}: {$nuevo_paciente->nombre_completo} -- {$key}");
            });
        } catch (\Throwable $th) {
            DB::rollback();
            $this->command->error("Ocurrió un error al migrar información: {$th->getMessage()} en la línea {$th->getLine()}");
        }

        DB::commit();

        $this->command->info("Se insertaron {$this->contador} registros");
    }
}
