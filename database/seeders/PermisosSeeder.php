<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Facades\Hash;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions(); //reseteamos los roles y permisos

        $role_admin = Role::create(['name' => 'Administrador']);
        $role_medic = Role::create(['name' => 'Medico']);
        $role_invited = Role::create(['name' => 'Invitado']);

        $permission_update_citas = Permission::create(['name' => 'update citas']);
        $permission_list_citas = Permission::create(['name' => 'list citas']);
        $permission_show_citas = Permission::create(['name' => 'show citas']);
        $permission_estadisticas = Permission::create(['name' => 'estadisticas']);
        $permission_todas_las_citas = Permission::create(['name' => 'todas las citas']);
        $permission_citas_del_dia = Permission::create(['name' => 'citas del dia']);
        $permission_citas_de_manana = Permission::create(['name' => 'citas de manana']);
        $permission_listado_de_analisis = Permission::create(['name' => 'listado de analisis']);
        $permission_ordenes_de_analisis = Permission::create(['name' => 'ordenes de analisis']);
        $permission_pagos_con_tarjeta = Permission::create(['name' => 'pagos con tarjeta']);
        $permission_usuarios = Permission::create(['name' => 'usuarios']);
        $permission_encuestas = Permission::create(['name' => 'encuestas']);

        // IMAGENOLIGIA PERMISOS
        $permission_todas_las_citas_imagenologia = Permission::create(['name' => 'todas las citas imagenologia']);
        $permission_citas_del_dia_imagenologia = Permission::create(['name' => 'citas del dia imagenologia']);
        $permission_citas_de_manana_imagenologia = Permission::create(['name' => 'citas de manana imagenologia']);

        $permission_lista_analisis_imagenologia = Permission::create(['name' => 'lista analisis imagenologia']);


        $role_admin->givePermissionTo($permission_update_citas);
        $role_admin->givePermissionTo($permission_list_citas);
        $role_admin->givePermissionTo($permission_show_citas);
        $role_admin->givePermissionTo($permission_estadisticas);
        $role_admin->givePermissionTo($permission_todas_las_citas);
        $role_admin->givePermissionTo($permission_citas_del_dia);
        $role_admin->givePermissionTo($permission_citas_de_manana);
        $role_admin->givePermissionTo($permission_listado_de_analisis);
        $role_admin->givePermissionTo($permission_ordenes_de_analisis);
        $role_admin->givePermissionTo($permission_pagos_con_tarjeta);
        $role_admin->givePermissionTo($permission_usuarios);
        $role_admin->givePermissionTo($permission_encuestas);

        $role_invited->givePermissionTo($permission_list_citas);
        $role_invited->givePermissionTo($permission_show_citas);
        $role_invited->givePermissionTo($permission_update_citas);

        // asignado permisos a admin de imagenologia
        $role_invited->givePermissionTo($permission_todas_las_citas_imagenologia);
        $role_invited->givePermissionTo($permission_citas_del_dia_imagenologia);
        $role_invited->givePermissionTo($permission_citas_de_manana_imagenologia);

        // creando usarios demos con los permisos y roles

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Example User Invited',
        //     'email' => 'invited@example.com',
        //     'password' => Hash::make('inkwonders')
        // ]);
        // $user->assignRole($role_invited);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Example Admin User',
        //     'email' => 'admin@example.com',
        //     'password' => Hash::make('inkwonders')
        // ]);
        // $user->assignRole($role_admin);

        // $user = \App\Models\User::factory()->create([
        //     'name' => 'Example Medic User',
        //     'email' => 'medic@example.com',
        //     'password' => Hash::make('inkwonders')
        // ]);
        // $user->assignRole($role_medic);

        $alain = User::find(1);
        $alain->assignRole('Administrador');

        $alain->givePermissionTo($permission_update_citas);
        $alain->givePermissionTo($permission_list_citas);
        $alain->givePermissionTo($permission_show_citas);
        $alain->givePermissionTo($permission_estadisticas);
        $alain->givePermissionTo($permission_todas_las_citas);
        $alain->givePermissionTo($permission_citas_del_dia);
        $alain->givePermissionTo($permission_citas_de_manana);
        $alain->givePermissionTo($permission_listado_de_analisis);
        $alain->givePermissionTo($permission_ordenes_de_analisis);
        $alain->givePermissionTo($permission_pagos_con_tarjeta);
        $alain->givePermissionTo($permission_usuarios);
        $alain->givePermissionTo($permission_encuestas);

        $alain->givePermissionTo($permission_todas_las_citas_imagenologia);
        $alain->givePermissionTo($permission_citas_del_dia_imagenologia);
        $alain->givePermissionTo($permission_citas_de_manana_imagenologia);
        $alain->givePermissionTo($permission_lista_analisis_imagenologia);

        // $guillo = User::find(2);
        // $guillo->assignRole('Administrador');

    }
}
