<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;

class SintomasPacientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paciente = Paciente::find(1);
        $paciente->sintomas()->attach(6);
        $paciente->sintomas()->attach(5);
        $paciente->sintomas()->attach(4);

        $paciente = Paciente::find(2);
        $paciente->sintomas()->attach(3);
        $paciente->sintomas()->attach(2);
        $paciente->sintomas()->attach(1);

        $paciente = Paciente::find(3);
        $paciente->sintomas()->attach(2);
        $paciente->sintomas()->attach(3);
        $paciente->sintomas()->attach(1);

        $paciente = Paciente::find(4);
        $paciente->sintomas()->attach(5);
        $paciente->sintomas()->attach(2);
        $paciente->sintomas()->attach(6);
    }
}
