<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sucursales;

class AttachSucursalTomografiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $sucursal = Sucursales::find(4);
        $sucursal->sucursalesEstudio()->attach(5);
    }
}
