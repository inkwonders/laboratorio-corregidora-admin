<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;


class HorariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('horarios_citas')->insert([
            ['hora_inicial' => '07:30:00', 'hora_final' => '08:30:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'1'],
            ['hora_inicial' => '08:31:00', 'hora_final' => '09:30:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'2'],
            ['hora_inicial' => '09:31:00', 'hora_final' => '10:30:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'3'],
            ['hora_inicial' => '10:31:00', 'hora_final' => '11:30:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'4'],
            ['hora_inicial' => '11:31:00', 'hora_final' => '12:30:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'5'],
            ['hora_inicial' => '12:31:00', 'hora_final' => '13:30:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'6'],
            ['hora_inicial' => '13:31:00', 'hora_final' => '14:30:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'7'],
            ['hora_inicial' => '14:31:00', 'hora_final' => '15:00:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'8'],
            ['hora_inicial' => '07:30:00', 'hora_final' => '08:30:00','no_pacientes' => '1000','dia_seman' => '2','orden' =>'1'],
            ['hora_inicial' => '08:31:00', 'hora_final' => '09:30:00','no_pacientes' => '1000','dia_seman' => '2','orden' =>'2'],
            ['hora_inicial' => '09:31:00', 'hora_final' => '10:30:00','no_pacientes' => '1000','dia_seman' => '2','orden' =>'3'],
            ['hora_inicial' => '10:31:00', 'hora_final' => '11:30:00','no_pacientes' => '1000','dia_seman' => '2','orden' =>'4'],
            ['hora_inicial' => '11:31:00', 'hora_final' => '12:30:00','no_pacientes' => '1000','dia_seman' => '2','orden' =>'5'],
            ['hora_inicial' => '12:31:00', 'hora_final' => '13:00:00','no_pacientes' => '1000','dia_seman' => '2','orden' =>'6'],
            ['hora_inicial' => '16:00:00', 'hora_final' => '17:00:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'9'],
            ['hora_inicial' => '17:01:00', 'hora_final' => '18:00:00','no_pacientes' => '1000','dia_seman' => '1','orden' =>'10'],
        ]);
    }
}
