<?php

namespace Database\Seeders;

use App\Models\Encriptacion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EncriptacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('encriptacion')->insert([
            ['id_company' => 'QPBA', 'semilla_aes' => 'DD33CEF1A4A0DDF130996A2BD052486D', 'url' => 'https://bc.mitec.com.mx/p/gen', 'cadena' => '9265655295', 'id_branch' => '008','user' => 'QPBASIUS8','pwd' => '6VS2RP5IU','moneda' =>'MXN', 'canal' => 'W', 'notif' => 1, 'correo' => 1],
        ]);
    }
}
