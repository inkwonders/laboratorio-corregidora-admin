<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class TransferenciaSintomasOrigen extends Seeder
{
    private $contador = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $usuarios = User::with('pacientes')->has('pacientes')
            ->get()
            ->unique()
            ->mapWithKeys(function($user) {
                return [
                    strtolower($user->email) =>
                        (Object) [
                            'user_id' => $user->id,
                            'paciente_id' => $user->pacientes->first()->id
                        ]
                ];
            });

        try {

            $sintomas_paciente_origen = DB::connection('mysql_origen')
                ->table('sintomas_paciente_f30a2728')
                ->select(
                    DB::raw('sintomas_paciente_f30a2728.fk_sintoma'),
                    'pacientes_b7db6bae.paciente_email'
                )
                ->leftjoin('pacientes_b7db6bae', 'pacientes_b7db6bae.sk_paciente', '=', 'sintomas_paciente_f30a2728.fk_paciente')
                ->distinct()
                ->get();

            $this->command->info("Se encontraron {$sintomas_paciente_origen->count()} citas");

            $sintomas_paciente_origen->each(function($sintomas_paciente) use ($usuarios) {

                $usuario_nuevo = $usuarios[ strtolower( $sintomas_paciente->paciente_email ) ];

                $prueba_id = '';

                switch($sintomas_paciente->fk_sintoma){
                    case '13bb41ea-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 1;
                        break;
                    case '13bb4b89-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 2;
                        break;
                    case '13bb4fe2-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 3;
                        break;
                    case '13bb53eb-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 4;
                        break;
                    case '13bb5801-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 5;
                        break;
                    case '13bb5bf4-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 6;
                        break;
                    case '13bb5fe7-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 7;
                        break;
                    case '13bb63da-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 8;
                        break;
                    case '13bb67af-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 9;
                        break;
                    case '13bb6b8a-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 10;
                        break;
                    case '13bb6e77-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 11;
                        break;
                    case '13bb7161-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 12;
                        break;
                    case '13bb74c7-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 13;
                        break;
                    case '13bb77f5-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 14;
                        break;
                    case '13bb7aef-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 15;
                        break;
                    case '13bb7dd5-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 16;
                        break;
                    case '13bb80b7-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 17;
                        break;
                    case '13bb839c-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 18;
                        break;
                    case '13bb8680-43ac-11eb-a442-04d4c4afee7c':
                        $prueba_id = 19;
                        break;
                }

                $paciente = Paciente::find($usuario_nuevo->paciente_id);

                $paciente->sintomas()->attach($prueba_id);

                $this->command->info("Se asoció el sintomas {$prueba_id} con el paciente {$usuario_nuevo->paciente_id}");

                $this->contador ++;
            });

        }catch (\Throwable $th){
            DB::rollback();
            $this->command->error("Ocurrió un error al migrar información: {$th->getMessage()} en la línea {$th->getLine()}");
        }

        DB::commit();

        $this->command->info("Se insertaron {$this->contador} sintomas");
    }
}
