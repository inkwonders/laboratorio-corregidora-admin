<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalisisImagenologicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analisis_imagenologicos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('descripcion', 1000);
            $table->string('clave');
            $table->string('indicaciones', 1000)->nullable();
            $table->float('costo')->nullable();
            $table->string('estimado_costo')->nullable();
            $table->string('tiempo_de_entrega', 1000)->nullable();
            $table->string('nombres_alternativos', 1000)->nullable();
            $table->string('contacto', 1000);
            $table->string('slug', 200);
            $table->foreignId('tipoTipoestudioImagelologicos_id')->constrained('tipoestudio_images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analisis_imagenologicos');
    }
}
