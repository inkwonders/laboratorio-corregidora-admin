<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoestudioImagesTable extends Migration
{
    /**
     * Run the migrations.
     *TipoestudioImage
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_estudio_imagenologicos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipoestudio_images');
    }
}
