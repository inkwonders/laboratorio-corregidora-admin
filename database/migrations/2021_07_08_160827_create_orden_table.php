<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('no_confirmacion',25);
            $table->string('nombre_paciente',100);
            $table->string('tel_paciente',10);
            $table->string('email_paciente');
            $table->double('total');
            $table->string('estatus',1)->nullable();
            $table->string('response',100)->nullable();
            $table->string('cadena',5000)->nullable();
            $table->string('tipo_tarjeta',150)->nullable();
            $table->string('digitos',4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden');
    }
}
