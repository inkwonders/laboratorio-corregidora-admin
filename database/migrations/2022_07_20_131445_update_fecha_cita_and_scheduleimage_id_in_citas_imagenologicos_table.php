<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFechaCitaAndScheduleimageIdInCitasImagenologicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('citas_imagenologicos', function (Blueprint $table) {
            //
            $table->date('fecha_cita')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citas_imagenologicos', function (Blueprint $table) {
            //
        });
    }
}
