<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSucursalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sucursales', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id()->autoIncrement();
            $table->string('sucursal', 200);
            $table->string('ubicacion', 200)->nullable();
            $table->integer('estatus')->nullable()->default(1)->comment('En caso de ser 0 no se tomara en cuenta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop table sucursales
        Schema::dropIfExists('sucursales');
    }
}
