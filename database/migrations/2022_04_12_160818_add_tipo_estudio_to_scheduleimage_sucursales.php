<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipoEstudioToScheduleimageSucursales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scheduleimage_sucursales', function($table) {
            $table->string('id_tipo_estudio')->nullable()->default('NULL')->after('scheduleimage_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheduleimage_sucursales', function($table) {
            $table->dropColumn('id_tipo_estudio');
        });
    }
}
