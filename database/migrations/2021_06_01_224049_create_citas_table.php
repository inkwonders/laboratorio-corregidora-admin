<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id();
            $table->foreignId('horario_id')->constrained('horarios_citas');
            $table->foreignId('paciente_id')->constrained('pacientes');
            $table->date('fecha_seleccionada');
            $table->string('no_confirmacion',12);
            $table->integer('status_asistencia');
            $table->integer('tipo_pago');
            $table->integer('pagado')->default('0')->nullable();
            $table->string('response',50)->nullable();
            $table->text('cadena')->nullable();
            $table->string('tipo_tarjeta',150)->nullable();
            $table->string('digitos',4)->nullable();
            $table->double('costo');
            $table->string('tipo_prueba',150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
}
