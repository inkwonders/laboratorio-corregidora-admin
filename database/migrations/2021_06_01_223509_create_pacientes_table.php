<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->string('clave',10)->nullable();
            $table->string('nombre',100);
            $table->string('apellido_paterno',100);
            $table->string('apellido_materno',100);
            $table->string('nombre_completo',100)->nullable();
            $table->integer('genero');
            $table->date('fecha_nacimiento');
            $table->string('calle',300);
            $table->string('colonia',200);
            $table->string('no_exterior',50);
            $table->string('no_interior',50);
            $table->foreignId('ubicacion_id')->constrained('ubicaciones');
            $table->string('cp',5);
            $table->string('tel_celular',10);
            $table->string('tel_casa',10)->nullable();
            $table->string('paciente_email',100);
            $table->string('medico_nombre',300)->nullable();
            $table->string('medico_email',100)->nullable();
            $table->string('razon_social',100)->nullable();
            $table->string('rfc',15)->nullable();
            $table->string('domicilio_fiscal',400)->nullable();
            $table->string('otro_sintoma',500)->nullable();
            $table->string('otra_enfermedad',500)->nullable();
            $table->integer('contacto_persona_positivo');
            $table->integer('antiviral');
            $table->integer('influenza');
            $table->string('otro_prueba',500)->nullable();
            $table->datetime('fecha_alta');
            $table->string('id_viejo')->nullable();
            $table->string('pasaporte',100)->nullable();
            $table->integer('es')->nullable();
            $table->integer('en')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
