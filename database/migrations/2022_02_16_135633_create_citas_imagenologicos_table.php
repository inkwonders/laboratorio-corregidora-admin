<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitasImagenologicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas_imagenologicos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('telefono');
            $table->string('correo');
            $table->date('fecha_nacimiento');
            $table->date('fecha_cita');
            $table->foreignId('analisisimagelologico_id')->constrained('analisis_imagenologicos');
            $table->foreignId('scheduleimage_id')->constrained('schedule_images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas_imagenologicos');
    }
}
