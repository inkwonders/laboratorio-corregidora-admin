<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEncriptacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('encriptacion', function (Blueprint $table) {
            $table->id();
            $table->string('id_company');
            $table->string('semilla_aes');
            $table->string('url');
            $table->string('cadena');
            $table->string('id_branch',10);
            $table->string('user',10);
            $table->string('pwd',15);
            $table->string('moneda',5);
            $table->string('canal',5);
            $table->string('notif',5);
            $table->string('correo',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('encriptacion');
    }
}
