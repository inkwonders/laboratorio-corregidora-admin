<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponseTarjetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('response_tarjeta', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->id();
            $table->string('response',5000);
            $table->integer('tipo');
            $table->string('numero_confirmacion');
            $table->string('tipo_pago')->nullable();
            $table->string('tarjeta_digitos')->nullable();
            $table->integer('autorizacion')->nullable();
            $table->string('tipo_transaccion')->nullable();
            $table->string('importe')->nullable();
            $table->date('fecha')->nullable();
            $table->string('hora')->nullable();
            $table->string('tipo_tarjeta')->nullable();
            $table->string('tarjeta')->nullable();
            $table->string('banco_emisor')->nullable();
            $table->string('foliocpagos')->nullable();
            $table->string('estatus')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('response_tarjeta');
    }
}
