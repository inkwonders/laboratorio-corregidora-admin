<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTipoEstudioImagenologicosToTipoestudioImages extends Migration
{
    /**
     * Run the migrations.
     *tipoestudio_images
     * @return void
     */
    public function up()
    {
        Schema::rename('tipo_estudio_imagenologicos','tipoestudio_images');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('tipoestudio_images','tipo_estudio_imagenologicos');
    }
}
