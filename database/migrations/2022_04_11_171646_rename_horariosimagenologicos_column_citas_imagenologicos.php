<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameHorariosimagenologicosColumnCitasImagenologicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('citas_imagenologicos', function(Blueprint $table) {
            $table->renameColumn('horariosimagelologico_id', 'scheduleimage_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citas_imagenologicos', function(Blueprint $table) {
            $table->renameColumn('scheduleimage_id', 'horariosimagelologico_id');
        });
    }
}
