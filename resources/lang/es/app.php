<?php

return [
    'login.titulo' => 'Iniciar sesión',
    'login.recuerdame' => 'Recuerdame',
    'login.boton' => 'Iniciar Sesión',
    'login.olvidaste' => '¿Olvidaste tu contraseña?',
    'navi-dropdown.perfil' => 'Perfil',
    'navi-dropdown.salir' => 'Salir'
];
