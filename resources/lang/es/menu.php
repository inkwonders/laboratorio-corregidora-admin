<?php

return [
    'menu.panel' => 'Estadísticas',
    'menu.historico' => 'Todas las citas',
    'menu.citasHoy' => 'Citas del día',
    'menu.citasMañana' => 'Citas del día de mañana',
    'menu.index' => 'Analisis',
    'menu.historicoMedico' => 'Historico Analisis',
    'menu.usuarios' => 'Usuarios',
    'menu.analisis' => 'Listado Análisis',
    'menu.tarjetas' => 'Pagos con Tarjeta',
    'menu.carrito' => 'Ordenes Analisis',
    'menu.encuestas' => 'Encuestas'
];
