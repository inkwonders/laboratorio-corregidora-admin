<div class="relative inline-block w-10 mr-2 align-middle transition duration-200 ease-in select-none">
    <input type="checkbox" name="toggle" id="toggle{{ $id }}" class="absolute block w-6 h-6 bg-white border-4 rounded-full appearance-none cursor-pointer toggle-checkbox"  wire:click="$emit('modalCambioEstatus', {{ $id }})"  @if($estatus) checked @endif/>
    <label for="toggle{{ $id }}" class="block h-6 overflow-hidden bg-gray-300 rounded-full cursor-pointer toggle-label"></label>
</div>
