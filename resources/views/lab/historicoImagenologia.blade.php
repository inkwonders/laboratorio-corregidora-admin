<x-app-layout title="Historico">
    <div class="grid px-6 mx-auto">

        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Historico Imagenología
        </h2>

        <div class="w-full overflow-hidden rounded-lg">
            <livewire:citas-historico-imagenologia-table />
        </div>

    </div>

    {{-- <livewire:modal-informacion-cita /> --}}

</x-app-layout>
