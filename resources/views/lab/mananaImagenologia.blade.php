<x-app-layout title="Historico">
    <div class="grid px-6 mx-auto">

        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Citas del día de mañana
        </h2>

        <div class="w-full overflow-hidden rounded-lg">
            <livewire:citas-manana-imagenologia-table />
        </div>

    </div>

</x-app-layout>
