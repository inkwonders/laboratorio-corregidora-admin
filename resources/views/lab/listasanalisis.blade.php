<x-app-layout title="Listas de Analísis impresos por Médicos">

    <div class="container grid px-6 mx-auto">

        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Listas de Analísis impresas por Médicos
        </h2>

        <div class="w-full my-8 font-bold text-black dark:text-gray-200" aria-label="Breadcrumb">
            <ol class="inline-flex float-right p-0 list-none">
                <li class="flex items-center">
                    <a href="{{ route('panel') }}" class="text-blue-800 dark:text-blue-300">Estadísticas</a>
                    <svg class="w-3 h-3 mx-3 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/></svg>
                </li>
                <li class="flex items-center">
                    <p>Médicos</p>
                </li>
            </ol>
        </div>

        <h4 class="my-6 font-semibold text-gray-700 text-1xl dark:text-gray-200">Medicos General</h4>

        <div class="grid gap-6 mb-8 md:grid-cols-2">
            <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

                <div class="flex">
                    <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-lg dark:text-green-100 dark:bg-green-400">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
                        </svg>
                    </div>
                    <div>
                        <p class="text-gray-600 dark:text-gray-400">
                            Total medicos registrados:
                        </p>
                        <p class="text-lg font-bold text-gray-700 dark:text-gray-200">
                            {{ $total_medicos }}
                        </p>
                    </div>

                </div>
            </div>
            <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

                <div class="flex">
                    <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-lg dark:text-green-100 dark:bg-green-400">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                            <path d="M8 3a1 1 0 011-1h2a1 1 0 110 2H9a1 1 0 01-1-1z" />
                            <path
                                d="M6 3a2 2 0 00-2 2v11a2 2 0 002 2h8a2 2 0 002-2V5a2 2 0 00-2-2 3 3 0 01-3 3H9a3 3 0 01-3-3z" />
                        </svg>
                    </div>
                    <div>
                        <p class="text-gray-600 dark:text-gray-400">
                            Total listas de estudios para pacientes impresas:
                        </p>
                        <p class="text-lg font-bold text-gray-700 dark:text-gray-200">
                            {{ $notas_impresas }}
                        </p>
                    </div>

                </div>
            </div>

        </div>

    </div>

</x-app-layout>
