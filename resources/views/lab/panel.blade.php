<x-app-layout title="Panel">

    @role('Administrador')
    @if (auth()->user()->hasDirectPermission('estadisticas'))

        <div class="container grid mx-auto h-auto md:p-12">

            <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200 h-1/6">
                Estadísticas
            </h2>

            <div class="flex justify-center w-full items-start">

                <div class="w-1/3  p-5 flex flex-col">
                    <h4 class="w-full my-6 font-semibold text-gray-700 text-1xl dark:text-gray-200">COVID</h4>
                    <a class="py-5 text-sm font-semibold transition-colors duration-150" href="{{ route('covid-general') }}">
                        <div class="min-w-0 p-4 text-white bg-blue-500 rounded-lg shadow-xs hover:bg-blue-800">
                            <div class="flex ">
                                <div class="flex flex-col w-2/3 ">
                                    <h4 class="mb-4 font-semibold">
                                        Covid (SARS-Cov-2)
                                    </h4>
                                    <p>
                                        Estadísticas de citas general
                                    </p>
                                </div>
                                <div class="flex justify-center w-1/3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-20 h-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a class="py-5 text-sm font-semibold transition-colors duration-150" href="{{ route('covid-hoy') }}">
                        <div class="min-w-0 p-4 text-white bg-blue-500 rounded-lg shadow-xs hover:bg-blue-800">
                            <div class="flex ">
                                <div class="flex flex-col w-2/3 ">
                                    <h4 class="mb-4 font-semibold">
                                        Covid (SARS-Cov-2)
                                    </h4>
                                    <p>
                                        Estadísticas de citas del día
                                    </p>
                                </div>
                                <div class="flex justify-center w-1/3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-20 h-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>

                    <a class="py-5 text-sm font-semibold transition-colors duration-150" href="{{ route('covid-tomorrow') }}">
                        <div class="min-w-0 p-4 text-white bg-blue-500 rounded-lg shadow-xs hover:bg-blue-800">
                            <div class="flex ">
                                <div class="flex flex-col w-2/3 ">
                                    <h4 class="mb-4 font-semibold">
                                        Covid (SARS-Cov-2)
                                    </h4>
                                    <p>
                                        Estadísticas de citas del día de mañana
                                    </p>
                                </div>
                                <div class="flex justify-center w-1/3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-20 h-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5l7 7-7 7M5 5l7 7-7 7" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>

                </div>
                <div class="w-1/3  p-5 flex flex-col ">
                    <h4 class="w-full my-6 font-semibold text-gray-700 text-1xl dark:text-gray-200">ANALÍSIS</h4>
                    <a class="py-5 text-sm font-semibold transition-colors duration-150" href="{{ route('analisis-general') }}">
                        <div class="min-w-0 p-4 text-white bg-purple-700 rounded-lg shadow-xs hover:bg-purple-800">
                            <div class="flex ">
                                <div class="flex flex-col w-2/3 ">
                                    <h4 class="mb-4 font-semibold">
                                        Analísis
                                    </h4>
                                    <p>
                                        Estadísticas de ordenes de Analísis
                                    </p>
                                </div>
                                <div class="flex justify-center w-1/3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-20 h-20" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="w-1/3 p-5 flex flex-col">
                    <h4 class="w-full my-6 font-semibold text-gray-700 text-1xl dark:text-gray-200">MÉDICOS</h4>
                    <a class="py-5 text-sm font-semibold transition-colors duration-150" href="{{ route('listas-analisis') }}">
                        <div class="min-w-0 p-4 text-white bg-pink-700 rounded-lg shadow-xs hover:bg-pink-800">
                            <div class="flex ">
                                <div class="flex flex-col w-2/3 ">
                                    <h4 class="mb-4 font-semibold">
                                        Listas de Anlísis
                                    </h4>
                                    <p>
                                        Impresiones de listas de Analísis
                                    </p>
                                </div>
                                <div class="flex justify-center w-1/3">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-20 h-20" viewBox="0 0 20 20"
                                        fill="currentColor">
                                        <path fill-rule="evenodd"
                                            d="M10.293 15.707a1 1 0 010-1.414L14.586 10l-4.293-4.293a1 1 0 111.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z"
                                            clip-rule="evenodd" />
                                        <path fill-rule="evenodd"
                                            d="M4.293 15.707a1 1 0 010-1.414L8.586 10 4.293 5.707a1 1 0 011.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z"
                                            clip-rule="evenodd" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

            </div>

        </div>
        @else
        <div class="container grid mx-auto h-auto md:p-12 content-center">

            {{-- <div class="md:grid md:grid-cols-12 md:gap-6"> --}}
                {{-- <div class="flex"> --}}
                    {{-- <div class="px-96 py-5 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800 ">
                        <h1 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200 h-1/6  uppercase">
                            Bienvenido {{ auth()->user()->name }}
                        </h1>
                        <h2 class="my-6 text-xl font-semibold text-gray-700 dark:text-gray-200 h-1/6">
                            {{ auth()->user()->email }}<br>
                            {{ date("F j, Y, g:i a") }}<br>
                        </h2>
                        <img aria-hidden="true" class="object-cover w-full h-full" src="{{ asset('img/logo-nuevo-oscuro.svg') }}" alt="Laboratorio Corregidora" />
                    </div> --}}
                {{-- </div> --}}
            {{-- </div> --}}
            <div class="flex flex-col w-full p-1 mx-auto lg:w-full sm:w-full lg:mr-1 lg:mx-0 lg:pr-10 sm:pr-1 text-center">

                <div class="w-full mt-32 md:mt-40 lg:flex lg:items-center lg:justify-center">

                    <div class="visita_text">
                        <div class="relative w-full">
                            <h1 class="dark:text-gray-200 pt-0 text-4xl font-normal text-left uppercase font-bebasRegular text-BlueCont 2xl:text-6xl lg:text-5xl sm:text-left md:text-left">
                                Bienvenido {{ auth()->user()->name }}</h1>
                            <div class="pt-4">
                                <p class=" dark:text-gray-200 text-base text-justify lg:w-auto text-GreyDosCont font-robotoConReg lg:text-2xl lg:text-justify">
                                    {{ auth()->user()->email }}<br>
                                    {{ date("F j, Y, g:i a") }}<br>
                                </p>
                            </div>

                        </div>
                    </div>
                    <img src="{{ asset('img/logo-nuevo-oscuro.svg') }}" class="randomImg w-2/3" id="randomImg" alt="random">
                </div>
            </div>



        </div>

        @endif


    @endrole

</x-app-layout>

