
    <style>

        table, th, td, tr{
            border: 1px solid black;
            font-size: 10px;
        }
        tr{
            height: 50px;
        }
        th{
            top: 2px;
        }

        table{
            position: relative;
            padding: 10px;
            width: 95%;
            left: 2.5%;
        }

        @media print {
           .boton {
              visibility: hidden;
           }
        }
    </style>

    <div>

        <div>

            <div style="padding: 10px;">

                <div>

                    <button type="button" onclick="window.print()" class="boton">
                        Imprimir Cita
                    </button>

                </div>

                <table style="border-collapse: collapse;">

                    <thead  style="border-collapse: collapse;">
                        <td style="font-weight: bold;">No. confirmacion</td>
                        <td style="font-weight: bold;">Nombre</td>
                        <td style="font-weight: bold;">Email</td>
                        <td style="font-weight: bold;">Teléfono</td>
                        <td style="font-weight: bold;">Fecha de cita</td>
                        <td style="font-weight: bold;">Pagado</td>
                        <td style="font-weight: bold;">Datos pago</td>
                    </thead>

                @foreach ($respuestas as $cita)

                    <tr>
                        <td>{{ $cita["no_confirmacion"] }}</td>
                        <td>{{ $cita["nombre"] }} {{ $cita["apellido_paterno"] }} {{ $cita["apellido_materno"] }}</td>
                        <td>{{ $cita["paciente_email"] }}</td>
                        <td>{{ $cita["tel_celular"] }}</td>
                        <td>{{ $cita["fecha_seleccionada"] }}</td>
                        <td>${{ number_format($cita["costo"],2) }}</td>
                        <td>{{ $cita["tipo_tarjeta"] }} / {{ $cita["digitos"] }}</td>
                    </tr>

                @endforeach

                </table>

            </div>

        </div>



    </div>

   <script>

    window.onload = function() {
        window.print();
        // window.close();
    };

    window.addEventListener("afterprint", function(event) {
        window.close();
    });

    </script>


