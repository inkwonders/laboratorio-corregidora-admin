
    <style>

        table, th, td, tr{
            border: 1px solid black;
            font-size: 10px;
        }
        tr{
            height: 50px;
        }
        th{
            top: 2px;
        }

        table{
            position: relative;
            padding: 10px;
            width: 95%;
            left: 2.5%;
        }

        @media print {
           .boton {
              visibility: hidden;
           }
        }
    </style>

    <div>

        <div>

            <div style="padding: 10px;">

                <div>

                    <button type="button" onclick="window.print()" class="boton">
                        Imprimir Cita
                    </button>

                </div>

                <table style="border-collapse: collapse;">

                    <thead  style="border-collapse: collapse;">
                        <td style="font-weight: bold;">Tipo Analisis</td>
                        <td style="font-weight: bold;">Nombre</td>
                        <td style="font-weight: bold;">Teléfono</td>
                        <td style="font-weight: bold;">Email</td>
                        <td style="font-weight: bold;">Fecha de cita</td>
                        <td style="font-weight: bold;">Horario Cita</td>
                        <td style="font-weight: bold;">Fecha de registro</td>
                    </thead>

                @foreach ($respuestas as $cita)

                    <tr>
                        <td>{{ $cita["tipo"] }}</td>
                        <td>{{ $cita["nombre"] }} </td>
                        <td>{{ $cita["telefono"] }}</td>
                        <td>{{ $cita["correo"] }}</td>
                        <td>{{ $cita["fecha_cita"] }}</td>
                        <td>{{ $cita["horario_inicial"] }} - {{ $cita['horario_final'] }}</td>
                        <td>{{ $cita["created_at"] }} </td>
                    </tr>

                @endforeach

                </table>

            </div>

        </div>



    </div>

   <script>

    window.onload = function() {
        window.print();
        // window.close();
    };

    window.addEventListener("afterprint", function(event) {
        window.close();
    });

    </script>


