<x-app-layout title="Analísis General">

    <div class="container grid px-6 mx-auto">

        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Analísis General
        </h2>

        <div class="w-full my-8 font-bold text-black dark:text-gray-200" aria-label="Breadcrumb">
            <ol class="inline-flex float-right p-0 list-none">
                <li class="flex items-center">
                    <a href="{{ route('panel') }}" class="text-blue-800 dark:text-blue-300">Estadísticas</a>
                    <svg class="w-3 h-3 mx-3 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"/></svg>
                </li>
                <li class="flex items-center">
                    <p>Analísis General</p>
                </li>
            </ol>
        </div>

        <h4 class="my-6 font-semibold text-gray-700 text-1xl dark:text-gray-200">General Análisis</h4>

        <div class="grid gap-6 mb-8 md:grid-cols-2">

            @livewire('grafica-pie', ['config' => $grafica_pruebas_pie ], key(uniqid()) )

            @livewire('grafica-line', ['config' => $grafica_pruebas_line ] )


        </div>

        <h4 class="my-6 font-semibold text-gray-700 text-1xl dark:text-gray-200">Ordenes Analisis General</h4>
        <div class="grid gap-6 mb-8 md:grid-cols-3">
            <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

                <div class="flex">
                    <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-lg dark:text-green-100 dark:bg-green-400">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 20 20" fill="currentColor">
                            <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
                        </svg>
                    </div>
                    <div>
                        <p class="text-gray-600 dark:text-gray-400">
                            Ingreso Análisis:
                        </p>
                        <p class="text-lg font-bold text-gray-700 dark:text-gray-200">
                        $  {{ number_format($ingreso_orden_total,2) }}
                        </p>
                    </div>

                </div>
            </div>

            <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

                <div class="flex">
                    <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-lg dark:text-green-100 dark:bg-green-400">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7 2a1 1 0 00-.707 1.707L7 4.414v3.758a1 1 0 01-.293.707l-4 4C.817 14.769 2.156 18 4.828 18h10.343c2.673 0 4.012-3.231 2.122-5.121l-4-4A1 1 0 0113 8.172V4.414l.707-.707A1 1 0 0013 2H7zm2 6.172V4h2v4.172a3 3 0 00.879 2.12l1.027 1.028a4 4 0 00-2.171.102l-.47.156a4 4 0 01-2.53 0l-.563-.187a1.993 1.993 0 00-.114-.035l1.063-1.063A3 3 0 009 8.172z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div>
                        <p class="text-gray-600 dark:text-gray-400">
                            Total Análisis vendidos:
                        </p>
                        <p class="text-lg font-bold text-gray-700 dark:text-gray-200">
                            {{ $orden }}
                        </p>
                    </div>

                </div>
            </div>

            <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

                <div class="flex">
                    <div class="p-3 mr-4 text-red-500 bg-red-100 rounded-lg dark:text-red-100 dark:bg-red-400">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                    </div>
                    <div>
                        <p class="text-gray-600 dark:text-gray-400">
                            Total Análisis no pagados:
                        </p>
                        <p class="text-lg font-bold text-gray-700 dark:text-gray-200">
                            {{ $orden_no }}
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <h4 class="my-6 font-semibold text-gray-700 text-1xl dark:text-gray-200">Hoy (Orden analisis):
            {{ date('d M Y', strtotime(date('Y-m-d') . '+ 1 days')) }}</h4>
        <div class="grid gap-6 mb-8 md:grid-cols-2">

            <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

                <div class="flex">
                    <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-lg dark:text-green-100 dark:bg-green-400">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7 2a1 1 0 00-.707 1.707L7 4.414v3.758a1 1 0 01-.293.707l-4 4C.817 14.769 2.156 18 4.828 18h10.343c2.673 0 4.012-3.231 2.122-5.121l-4-4A1 1 0 0113 8.172V4.414l.707-.707A1 1 0 0013 2H7zm2 6.172V4h2v4.172a3 3 0 00.879 2.12l1.027 1.028a4 4 0 00-2.171.102l-.47.156a4 4 0 01-2.53 0l-.563-.187a1.993 1.993 0 00-.114-.035l1.063-1.063A3 3 0 009 8.172z" clip-rule="evenodd" />
                        </svg>
                    </div>
                    <div>
                        <p class="text-gray-600 dark:text-gray-400">
                            Analisis Vendidos:
                        </p>
                        <p class="text-lg font-bold text-gray-700 dark:text-gray-200">
                        {{ $analisis_vendidos_hoy }}
                        </p>
                    </div>

                </div>
            </div>
            <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

                <div class="flex">
                    <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-lg dark:text-green-100 dark:bg-green-400">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </div>
                    <div>
                        <p class="text-gray-600 dark:text-gray-400">
                            Ingresos totales del día:
                        </p>
                        <p class="text-lg font-bold text-gray-700 dark:text-gray-200">
                        $ {{ number_format($total_analisis_vendidos_hoy,2) }}
                        </p>
                    </div>

                </div>
            </div>
        </div>

    </div>

</x-app-layout>
