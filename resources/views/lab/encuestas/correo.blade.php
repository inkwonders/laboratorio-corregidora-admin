<html>
<head>
    <meta charset="utf-8">
    <title>Se agendó la cita</title>
    <style media="screen">
        .contenedor * {
            box-sizing: border-box;
            font-family: arial;
        }

        .contenedor {
            box-sizing: border-box;
            width: 600px;
            max-width: 100%;
            background-color: #e6e6e6;
            margin-top: 50px;
            padding: 17px;
        }

        .mensaje {
            width: 100%;
            background-color: #f2f2f2;
            display: block;
            text-align: center;
            font-size: 24px;
            margin: 0;
            padding: 40px;
            color: #194271;
        }

        .logo_head {
            width: 90%;
            height: auto;
            margin: 20px 0;
        }

        .texto_msg,
        .manifiesto_txt {
            margin-top: 40px;
            display: block;
        }

        .texto_confirmacion {
            font-size: 30px;
        }

        .texto_confirmacion {
            margin-top: 40px;
        }

        .link_aviso {
            text-decoration: none;
            margin-top: 30px;
            font-size: 17px;
            color: #194271;
        }

        .manifiesto_txt {
            color: #848484;
            font-size: 16px;
        }

        .btn_aviso {
            background-color: #ccc;
            color: white;
            text-decoration: none;
            margin-top: 20px;
            padding: 10px 20px;
            border-radius: 10px;
            border: 2px #ccc solid;
        }

        .btn_aviso:hover{
            color: #848484;
            background-color: transparent;
        }

    </style>
</head>

<body>
    <div class="contenedor">
        <div class="mensaje">
            <img class="logo_head" src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png">
            <span class="texto_msg">Gracias por confiar en LABORATORIO CORREGIDORA. Te agradeceríamos que nos dieras 5 minutos o menos para responder una breve encuesta y compartirnos cómo fue tu experiencia con nosotros.</span>
            <span class="texto_msg"> </span>
            <a target="_blank" class="btn_aviso" href="/responder_encuesta/{{ $encuesta }}">Clic aqui </a>
            <span class="manifiesto_txt">Su información está protegida y no será utilizada para fines publicitarios u otros.</span>
            <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf">AVISO DE PRIVACIDAD</a>
        </div>
    </div>
</body>

</html>
