<div>
    <!DOCTYPE html>
    <html :class="{ 'theme-dark': dark }" x-data="data()" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
        <link rel="manifest" href="img/favicon/site.webmanifest">
        <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#cbf202">
        <meta name="msapplication-TileColor" content="#000000">
        <meta name="theme-color" content="#ffffff">
        <!-- favicon -->
        <title> Encuestas </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap"
            rel="stylesheet" />

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        {{-- <link rel="stylesheet" href="{{ asset('css/tailwind.output.css') }}" /> --}}
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
        <script src="{{ asset('js/init-alpine.js') }}" defer></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" defer></script>
        {{-- <script src="{{asset('js/charts-lines.js')}}" defer></script> --}}
        {{-- <script src="{{asset('js/charts-pie.js')}}" defer></script> --}}
        <script src="{{ asset('js/charts-bars.js') }}" defer></script>

        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        @livewireStyles

        <script>
            import Turbolinks from 'turbolinks';
            Turbolinks.start()
        </script>

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.js" defer></script>
    </head>
    <body>

    <section id="Encuestas">
        <div class="container mx-auto ">

            <div class="flex justify-center w-full p-16 ">
                <img class="w-full md:w-1/2" src="../img/logo_imprimir.svg" alt="Laboratorios Corregidora">
            </div>
            <article class="flex flex-col items-center content-center w-full mb-16 azul_textos">
                <span class="w-7/12 text-3xl"><br>  </span>
                <h2 class="w-11/12 mt-5 mb-5 text-xl font-bold text-center md:text-4xl" style="color:#194271">- MUCHAS GRACIAS, LA ENCUESTA YA NO ESTA DISPONIBLE -</h2><br><br>
                <a href="https://www.laboratoriocorregidora.com.mx" title="Laboratorios Corregidora" class="p-3 text-xl text-white bg-blue-500 border border-blue-500 rounded-lg md:p-5 md:text-2xl mt-9 hover:bg-white hover:text-blue-500"> Entendido. </a>
            </article>



        </div>

    </section>
    </body>

    </html>

</div>
