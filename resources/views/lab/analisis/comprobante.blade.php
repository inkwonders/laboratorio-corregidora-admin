<html>

<head>
    <meta charset="utf-8">
    <title>Se registro el pago</title>
    <style media="screen">
        .contenedor * {
            box-sizing: border-box;
            font-family: arial;
        }

        .contenedor {
            box-sizing: border-box;
            width: 600px;
            max-width: 100%;
            background-color: #e6e6e6;
            margin-top: 50px;
            padding: 17px;
        }


        .mensaje {
            width: 100%;
            background-color: #f2f2f2;
            display: inline-flex;
            flex-direction: column;
            align-items: center;
            text-align: center;
            font-size: 18px;
            margin: 0;
            padding: 5%;
            color: #194271;
        }

        .logo_head {
            width: 90%;
            height: auto;
        }

        .texto_msg,
        .manifiesto_txt {
            margin-top: 80px;
        }

        .texto_confirmacion {
            font-size: 20px;
        }

        .texto_confirmacion,
        .texto_numero {
            margin-top: 40px;
        }

        .link_aviso {
            text-decoration: none;
            margin-top: 30px;
            font-size: 16px;
            color: #194271;
        }

        .numero_confirmacion {
            color: #007af6;
            font-weight: bold;
            font-size: 20px;
        }

        .manifiesto_txt {
            color: #848484;
            font-size: 13px;
        }

        .numero_tel {
            font-weight: bold;
        }

        .numero_tel a {
            text-decoration: none;
            color: #194271;
        }

        span {
            overflow-wrap: anywhere;
        }

        .border {
            border: solid 1px;
            text-align: left;
            padding: 10px;
        }

    </style>
</head>

<body>
    <div class="contenedor">
        <table class="mensaje">
            <tr>
                <td>
                    <img class="logo_head"
                        src="https://www.laboratoriocorregidora.com.mx/img/logo-nuevo.png"><br><br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="texto_msg">Se realizo el comprobante de los siguientes estudios:</span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td>

                    <br>
                </td>
            </tr>

            @foreach ($analisis as $prueba)
                @foreach ($prueba as $item)
                    <tr>
                        <td class="border">
                            <p>
                                Cantidad: <span class="numero_confirmacion">  {{ $item->cantidad }} </span>
                            </p>
                            <p>
                                Nombre: <span class="numero_confirmacion">  {{ $item->nombre }} </span>
                            </p>
                            <p>
                                Precio: <span class="numero_confirmacion"> $ {{ $item->precio }} </span>
                            </p>
                        </td>
                    </tr>
                @endforeach
            @endforeach

            <tr>
                <td>
                    <span class="texto_confirmacion"><br>Número de Confirmación: <span class="numero_confirmacion">
                            {{ $confirmacion }} </span></span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="texto_confirmacion">Total: <span
                            class="numero_confirmacion">${{ number_format($total, 2) }}</span></span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="texto_numero">Para mayores informes favor de llamar al: <span class="numero_tel"><a
                                href="tel:4422121052">4422121052</a></span> / WhatsApp: <span class="numero_tel"><a
                                href="https://api.whatsapp.com/send?phone=+524421206215" target="_blank">442
                                1206215</a></span></span>
                    <br><br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="manifiesto_txt">Este es un correo de confirmación, en caso de estar enterado de la
                        información haga caso omiso. Su información está protegida y no será utilizada para fines
                        publicitarios u otros.</span>
                    <br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <a class="link_aviso" href="https://laboratoriocorregidora.com.mx/pdf/AVISODEPRIVACIDAD.pdf"
                        target="_blank">AVISO DE PRIVACIDAD</a>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>
