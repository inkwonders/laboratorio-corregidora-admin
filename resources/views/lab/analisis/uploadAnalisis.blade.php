<div>

    {{-- @if ($agregar_masiva) --}}
        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none "
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="absolute w-5/12 text-center items-center">
                <form action="{{ route('importAnalisis') }}" method="get" class="relative p-10 m-8 mb-6 bg-white rounded shadow-xl dark:bg-gray-700 text-center items-center">
                @csrf
                    <div class="absolute cursor-pointer right-6 top-4 hover:text-red-600 dark:text-white"
                        wire:click="$emit('cerrarModal')">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </div>
                    <div class="flex justify-between mt-2">
                        <p class="mb-4 text-2xl font-bold text-center dark:text-white"> Carga masiva de listado de Análisis
                        </p>
                    </div>
                    <div class="flex w-full mt-2">

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Selecciona un archivo de excel</label>
                            <label
                                class=" w-64 flex flex-col items-center px-4 py-6 bg-white rounded-md shadow-md tracking-wide uppercase border border-blue cursor-pointer hover:bg-purple-600 hover:text-white text-purple-600 ease-linear transition-all duration-150">
                                <i class="fas fa-cloud-upload-alt fa-3x"></i>
                                <span class="mt-2 text-base leading-normal">Seleccionar excel</span>
                                <input  name="archivo[]" id="archivo[]" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" type="file" class="hidden" />
                            </label>
                            @error('clave')
                                <p class="mb-2 text-red-600 error_clave">{{ $message }}</p>
                            @enderror
                        </div>
                    
                    </div>

                    <br>
                    <span class="text-center text-gray-400 dark:text-white"> Todos los campos son obligatorios </span>

                    <!--footer-->
                    <div class="flex items-center justify-center p-3 mt-2 space-x-4 text-center">
                        <div class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500"
                            wire:click="$emit('cerrarModal')">
                            Cerrar
                        </div>
                        <button 
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600" type="submit">
                            Agregar
                        </button>
                    </div>

                </form>

            </div>

        </div>
    {{-- @endif --}}


</div>
