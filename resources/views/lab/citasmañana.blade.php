<x-app-layout title="Citas del día de mañana">
    <div class="grid px-6 mx-auto">
        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Citas del día de mañana
        </h2>

        {{-- <div class="flex justify-end w-full mt-2 mb-4">

            <a href="{{ route('export_manana') }}"
                class="flex w-auto px-4 py-4 font-bold text-center text-white bg-green-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-green-500 focus:outline-none focus:ring focus:border-transparent">
                Exportar
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </svg>
            </a>
        </div> --}}

        <div class="w-full overflow-hidden rounded-lg">

            <livewire:tomorrow-table />

        </div>

    </div>

    @livewire('modal-informacion-cita')

</x-app-layout>
