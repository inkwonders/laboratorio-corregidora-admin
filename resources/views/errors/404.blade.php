@extends('errors::minimal')

@section('title', __('404 | Laboratorio Corregidora'))
@section('code', '404')
@section('message', __('Página no encontrada'))
