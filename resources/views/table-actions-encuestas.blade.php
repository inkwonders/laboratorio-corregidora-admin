<div class="flex justify-around space-x-1">

    <button type="button" wire:click="$emit('abrirModal',{{ $id }})" title="Ver"
        class="p-1 text-blue-600 rounded hover:bg-teal-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M8 16l2.879-2.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242zM21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
    </button>

    @if ($enviado != 1)

        <button type="button" wire:click="$emit('abrirModalEnviar',{{ $id }})" title="Enviar"
            class="p-1 text-blue-600 rounded hover:bg-teal-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
            </svg>
        </button>
    @else
        <button type="button" wire:click="$emit('abrirModalEstadisticaEncuesta',{{ $id }})"
            title="Estadisticas"
            class="p-1 text-blue-600 rounded hover:bg-teal-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z" />
            </svg>
        </button>
        <button type="button" wire:click="$emit('abrirModalEnviados',{{ $id }})" title="Enviar"
        class="p-1 text-blue-600 rounded hover:bg-teal-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
        </svg>
    </button>
    @endif

    <button type="button" wire:click="$emit('editar', {{ $id }})" title="Editar"
        class="p-1 text-blue-600 rounded hover:bg-teal-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
        </svg>
    </button>

    <button type="button" wire:click="$emit('eliminar', {{ $id }})" title="Eliminar"
        class="p-1 text-red-600 rounded hover:bg-teal-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd"
                d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                clip-rule="evenodd" />
        </svg>
    </button>

</div>
