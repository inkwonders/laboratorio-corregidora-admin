<div class="py-4 text-gray-500 dark:text-gray-400">

    @role('Medico')
        @php
            $home = "/analisis";
        @endphp
    @endrole
    @role('Administrador')
        @php
            $home = "/panel";
        @endphp
    @endrole
    <a class="flex items-center justify-center text-lg font-bold text-gray-800 dark:text-gray-200" href="{{ $home }}">
        <template x-if="dark">
            <img class="relative top-0 w-40" src="{{ asset('img/logo-nuevo-oscuro.svg') }}" alt="Laboratorio Corregidora">
        </template>
        <template x-if="!dark">
            <img class="relative top-0 w-40" src="{{ asset('img/logo-nuevo.svg') }}" alt="Laboratorio Corregidora">
        </template>
    </a>

    @role('Medico')

        <ul class="mt-6">

            <li class="relative px-6 py-3">

                {!! request()->route()->uri == 'analisis' ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('analisis') ? 'text-blue-800 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold text-gray-800 transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200 dark:text-gray-100" href="{{route('analisis.panel')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.index') }}</span>
                </a>
            </li>

            <li class="relative px-6 py-3">
                {!! request()->route()->uri == 'historico-medico' ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('historico-medico') ? 'text-blue-800 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold text-gray-800 transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200 dark:text-gray-100" href="{{route('analisis.historicomedico')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.historicoMedico') }}</span>
                </a>
            </li>

        </ul>

    @endrole

    @role('Administrador')
        <ul class="mt-6">
            @if (auth()->user()->hasDirectPermission('estadisticas'))
            <li class="relative px-6 py-3">
                {!! request()->routeIs('panel') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('panel') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200 dark:text-gray-100" href="/panel">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 3.055A9.001 9.001 0 1020.945 13H11V3.055z" />
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.488 9H15V3.512A9.025 9.025 0 0120.488 9z" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.panel') }}</span>
                </a>
            </li>
            @endif

            @if (auth()->user()->hasDirectPermission('todas las citas'))
            <hr class="w-10/12 ml-5">
            <br>
            <li class="ml-6">CITAS COVID</li>
            <br>
            <li class="relative px-6 py-3">
                {!! request()->routeIs('historico') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('historico') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('historico')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.historico') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('citas del dia'))
            <li class="relative px-6 py-3">
                {!! request()->routeIs('citashoy') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('citashoy') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('citashoy')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.citasHoy') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('citas de manana'))
            <li class="relative px-6 py-3">
                {!! request()->routeIs('citasmañana') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('citasmañana') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('citasmañana')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5l7 7-7 7M5 5l7 7-7 7" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.citasMañana') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('listado de analisis'))
            <hr class="w-10/12 ml-5">
            <li class="relative px-6 py-3">
                {!! request()->routeIs('pruebas') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('pruebas') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="/pruebas">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M7 2a1 1 0 00-.707 1.707L7 4.414v3.758a1 1 0 01-.293.707l-4 4C.817 14.769 2.156 18 4.828 18h10.343c2.673 0 4.012-3.231 2.122-5.121l-4-4A1 1 0 0113 8.172V4.414l.707-.707A1 1 0 0013 2H7zm2 6.172V4h2v4.172a3 3 0 00.879 2.12l1.027 1.028a4 4 0 00-2.171.102l-.47.156a4 4 0 01-2.53 0l-.563-.187a1.993 1.993 0 00-.114-.035l1.063-1.063A3 3 0 009 8.172z" clip-rule="evenodd" />
                      </svg>
                    <span class="ml-4">{{ __('menu.menu.analisis') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('ordenes de analisis'))
            <li class="relative px-6 py-3">
                {!! request()->routeIs('ordenes') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('ordenes') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="/ordenes">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.carrito') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('pagos con tarjeta'))
            <hr class="w-10/12 ml-5">
            <li class="relative px-6 py-3">
                {!! request()->route()->uri == 'tarjetas' ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->route()->uri == 'tarjetas' ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="/tarjetas">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M4 4a2 2 0 00-2 2v1h16V6a2 2 0 00-2-2H4z" />
                        <path fill-rule="evenodd" d="M18 9H2v5a2 2 0 002 2h12a2 2 0 002-2V9zM4 13a1 1 0 011-1h1a1 1 0 110 2H5a1 1 0 01-1-1zm5-1a1 1 0 100 2h1a1 1 0 100-2H9z" clip-rule="evenodd" />
                      </svg>
                    <span class="ml-4">{{ __('menu.menu.tarjetas') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('usuarios'))
            <hr class="w-10/12 ml-5">
            <li class="relative px-6 py-3">
                {!! request()->routeIs('usuarios') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('usuarios') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('usuarios')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.usuarios') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('encuestas'))
            <hr class="w-10/12 ml-5">
            <li class="relative px-6 py-3">
                {!! request()->routeIs('encuestas') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('encuestas') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('encuestas')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.encuestas') }}</span>
                </a>
            </li>
            @endif


            {{-- citas imagenologia --}}
            @if (auth()->user()->hasDirectPermission('todas las citas imagenologia'))
            <hr class="w-10/12 ml-5">
            <br>
            <li class="ml-6">IMAGENOLOGÍA</li>
            <br>
            <li class="relative px-6 py-3">
                {!! request()->routeIs('historicoImagenologia') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('historicoImagenologia') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('historicoImagenologia')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.historico') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('citas del dia imagenologia'))
            <li class="relative px-6 py-3">
                {!! request()->routeIs('hoyImagenologia') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('hoyImagenologia') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('hoyImagenologia')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.citasHoy') }}</span>
                </a>
            </li>
            @endif
            @if (auth()->user()->hasDirectPermission('citas de manana imagenologia'))
            <li class="relative px-6 py-3">
                {!! request()->routeIs('mananaImagenologia') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('mañanaImagenologia') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('mananaImagenologia')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5l7 7-7 7M5 5l7 7-7 7" />
                    </svg>
                    <span class="ml-4">{{ __('menu.menu.citasMañana') }}</span>
                </a>
            </li>
            @endif

            {{-- menu de lista analisis imagenologicos --}}

            @if (auth()->user()->hasDirectPermission('lista analisis imagenologia'))
            <li class="relative px-6 py-3">
                {!! request()->routeIs('analisisImagenologia') ? '<span class="absolute inset-y-0 left-0 w-1 bg-blue-600 rounded-tr-lg rounded-br-lg" aria-hidden="true"></span>' : '' !!}
                <a class="{!! request()->routeIs('analisisImagenologia') ? 'text-blue-800 dark:text-blue-300 font-bold' : '' !!} inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-blue-800 dark:hover:text-gray-200" href="{{route('analsis-imagenologia')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M7 2a1 1 0 00-.707 1.707L7 4.414v3.758a1 1 0 01-.293.707l-4 4C.817 14.769 2.156 18 4.828 18h10.343c2.673 0 4.012-3.231 2.122-5.121l-4-4A1 1 0 0113 8.172V4.414l.707-.707A1 1 0 0013 2H7zm2 6.172V4h2v4.172a3 3 0 00.879 2.12l1.027 1.028a4 4 0 00-2.171.102l-.47.156a4 4 0 01-2.53 0l-.563-.187a1.993 1.993 0 00-.114-.035l1.063-1.063A3 3 0 009 8.172z" clip-rule="evenodd" />
                      </svg>
                    <span class="ml-4">{{ __('menu.menu.analisis') }}</span>
                </a>
            </li>
            @endif

        </ul>

    @endrole

</div>
