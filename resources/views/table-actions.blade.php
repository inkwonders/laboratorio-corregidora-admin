<div class="flex justify-around space-x-1">

    @if ($ruta_actual == 'historico-medico')

        <button title="Ver cita" type="button" wire:click="$emit('mostrarLista', {{ $id }})" class="p-1 text-teal-600 rounded hover:bg-teal-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path><path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd"></path></svg>
        </button>

    @endif

    @if ($ruta_actual == 'historico' || $ruta_actual == 'citashoy')

        <button title="Ver cita" type="button" wire:click="$emit('mostrarCita', {{ $id }})" class="p-1 text-blue-600 rounded hover:bg-blue-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path><path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd"></path></svg>
        </button>

        @if (strtotime(date('Y-m-d')) >= strtotime($fecha_seleccionada))

            @if($status_asistencia == 0)

                <button title="Marcar asistencia" type="button" wire:click="$emit('asistenciaCita', {{ $id }})" class="p-1 text-green-600 rounded hover:bg-green-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z" />
                        <path fill-rule="evenodd" d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm9.707 5.707a1 1 0 00-1.414-1.414L9 12.586l-1.293-1.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                    </svg>
                </button>

            @else

                <button title="Quitar asistencia" type="button" wire:click="$emit('noAsistencia', {{ $id }})" class="p-1 text-red-600 rounded hover:bg-red-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </button>

            @endif

        @endif

    @endif

    @if($ruta_actual == 'citasmañana')

        <button title="Ver cita" type="button" wire:click="$emit('mostrarCita', {{ $id }})" class="p-1 text-blue-600 rounded hover:bg-blue-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10 12a2 2 0 100-4 2 2 0 000 4z"></path><path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd"></path></svg>
        </button>

        @if($status_asistencia == 0)

            <button title="Marcar asistencia" type="button" wire:click="$emit('asistenciaCita', {{ $id }})" class="p-1 text-green-600 rounded hover:bg-green-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z" />
                    <path fill-rule="evenodd" d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm9.707 5.707a1 1 0 00-1.414-1.414L9 12.586l-1.293-1.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                </svg>
            </button>

        @else

            <button title="Quitar asistencia" type="button" wire:click="$emit('noAsistencia', {{ $id }})" class="p-1 text-red-600 rounded hover:bg-red-600 hover:text-white focus:outline-none focus:ring focus:border-transparent">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </button>

        @endif

    @endif

</div>
