<div>

    <div class="grid px-6 mx-auto">

        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Encuestas
        </h2>

        <div class="flex items-center justify-end w-full pb-8">
            <button wire:click="modalEncuesta" class="flex w-auto px-4 py-4 mr-2 font-bold text-center text-white bg-blue-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-blue-500 focus:outline-none focus:ring focus:border-transparent">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z" />
                </svg>
                NUEVA ENCUESTA
            </button>
            <button
                class="flex w-auto px-4 py-4 mr-2 font-bold text-center text-white bg-purple-500 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-purple-600 focus:outline-none focus:ring focus:border-transparent"
                wire:click="uploadMasivaModalCorreos">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 pr-1" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </svg>
                IMPORTACION MASIVA DE CORREOS
            </button>
        </div>

        <div class="flex w-full lg:flex-row md:flex-col-reverse ">

            <div class="w-full px-5">

                <livewire:encuestas-table />

            </div>

        </div>

    </div>

    @if ($modal_encuesta)

        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none min-w-screen animated fadeIn faster focus:outline-none"  style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="w-2/4 pt-0 leading-loose">

                <div class="bg-white rounded shadow-xl w-4/4 ">

                    <div class="container relative mx-auto">

                        <div class="absolute z-50 cursor-pointer dark:text-white right-2 top-2 hover:text-red-600 print:hidden" wire:click="$emit('cerrarModal')">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>

                        <div class="relative w-full px-5 py-4 bg-white rounded shadow-xl dark:bg-gray-700">

                            <div class="flex justify-between mt-2">
                                <p class="mb-4 text-2xl font-bold text-center dark:text-white ">Datos encuesta</p>
                            </div>

                            <div class="flex w-full">

                                <div class="w-full p-2">
                                    <label class="block text-xl text-gray-600 dark:text-white">Título</label>
                                    <input class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text" wire:model="titulo">
                                    @error('titulo')
                                        <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>

                            <div class="flex w-full">

                                <div class="w-full p-2">
                                    <label class="block text-xl text-gray-600 dark:text-white">Pregunta 1</label>
                                    <input class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text" wire:model="pregunta1">
                                    @error('pregunta1')
                                        <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>

                            <div class="flex w-full">

                                <div class="w-full p-2">
                                    <label class="block text-xl text-gray-600 dark:text-white">Pregunta 2</label>
                                    <input class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text" wire:model="pregunta2">
                                    @error('pregunta2')
                                        <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>

                            <div class="flex w-full">

                                <div class="w-full p-2">
                                    <label class="block text-xl text-gray-600 dark:text-white">Pregunta 3</label>
                                    <input class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text" wire:model="pregunta3">
                                    @error('pregunta3')
                                        <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>

                            <div class="flex w-full">

                                <div class="w-full p-2">
                                    <label class="block text-xl text-gray-600 dark:text-white">Pregunta 4</label>
                                    <input class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text" wire:model="pregunta4">
                                    @error('pregunta4')
                                        <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>

                            <div class="flex w-full">

                                <div class="w-full p-2">
                                    <label class="block text-xl text-gray-600 dark:text-white">Pregunta 5</label>
                                    <input class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text" wire:model="pregunta5">
                                    @error('pregunta5')
                                        <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>

                            <br>
                            <span class="text-center text-gray-400 dark:text-white"> Todos los campos son obligatorios </span>

                            <!--footer-->
                            <div class="flex items-center justify-center p-3 mt-2 space-x-4 text-center">

                                @if ($operacion)

                                    <div wire:click="create" class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600">
                                        Agregar
                                    </div>

                                @else

                                    <div wire:click="update" class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600">
                                        Actualizar
                                    </div>

                                @endif

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    @endif
    @if ($modal_carga_masiva_correos)
        <livewire:modal-upload-masiva-correos />
    @endif

    @if ($modal)

        <livewire:modal-encuesta :encuesta="$form" :preguntas="$asks"/>

    @endif

    @if ($modalEnviar)

        <livewire:modal-enviar-encuesta />

    @endif

    @if ($eliminar)

        <livewire:modal-eliminar-encuesta :encuesta="$id_eliminar"/>

    @endif

    @if ($modalEstadisticaEncuesta)

        <livewire:modal-estadistica-encuesta :encuesta="$id_encuesta"/>

    @endif


    @if ($modalEnviados)

        <livewire:correos-enviados :encuesta="$id_encuesta"/>

    @endif


</div>

@push('scripts')

<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function () {

        Livewire.on('drawChart', function(data) {

            console.log(data)
            let interno = data.dataset;
            console.log(interno)

            const pieConfig = {
                type: 'doughnut',
                data: {
                    datasets: data.dataset,
                    labels: ['Pregunta 1','Pregunta 2','Pregunta 3','Pregunta 4','Pregunta 5']
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 0,
                    legend: {
                        display: false,
                    },
                },
            }

            // change this to the id of your chart element in HMTL
            const pieCtx = document.getElementById('grafica_encuesta')
            window.myPie = new Chart(pieCtx, pieConfig)

        });
    });

</script>

@endpush
