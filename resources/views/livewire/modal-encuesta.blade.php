<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen outline-none min-w-screen animated fadeIn faster focus:outline-none"  style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="w-2/4 h-screen pt-0 overflow-y-auto leading-loose ">

            <div class="bg-white rounded shadow-xl w-4/4">

                <style>
                    .clasificacion {
                        direction: rtl;
                        unicode-bidi: bidi-override;
                        font-size: 16px;
                    }

                    .label_estrella:hover {
                        color: orange;
                        cursor: pointer;
                    }

                    .label_estrella:hover~.label_estrella {
                        color: orange;
                    }

                    input[type="radio"]:checked~.label_estrella {
                        color: orange;
                    }

                </style>

                <section id="Encuestas">
                    <div class="container relative mx-auto dark:bg-gray-700">

                        <div class="absolute cursor-pointer right-2 top-2 hover:text-red-600 print:hidden dark:text-white" wire:click="$emit('cerrarModal')">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </div>

                        <div class="flex justify-center w-full p-10 ">
                            <img class="w-1/2" src="img/logo_imprimir.svg" alt="Laboratorios Corregidora">
                        </div>
                        <article class="flex flex-col items-center content-center w-full mb-8 azul_textos">
                            <h2 class="mt-1 mb-2 font-bold lg:text-2xl md:text-sm" style="color:#194271">- ENCUESTA DE SATISFACCIÓN -</h2>
                            <span class="w-10/12 leading-normal text-center lg:text-2xl md:text-sm">{{ $encuesta->nombre}}</span>
                        </article>


                        <div type="post" class="flex flex-col items-center content-center ">

                            <div class="flex flex-col items-center content-center mb-4">
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($preguntas as $pregunta )

                                    @php
                                        $random = rand(1, 5000);
                                        $random2 = rand(1, 5000);
                                        $random3 = rand(1, 5000);
                                        $random4 = rand(1, 5000);
                                        $random5 = rand(1, 5000);
                                    @endphp

                                    <p class="w-5/6 font-semibold leading-normal text-center text-gray-600 dark:text-white lg:text-2xl md:text-sm">
                                        {{ $i }}.-{{ $pregunta->descripcion }}
                                    </p>

                                    <p class="clasificacion">
                                        <input class="hidden" id="radio{{ $random }}" type="radio"
                                            name="estrellas{{ $i }}" value="5">
                                        <label for="radio{{ $random }}" class="dark:text-white lg:text-3xl md:text-sm label_estrella">★</label>
                                        <input class="hidden" id="radio{{ $random2 }}" type="radio"
                                            name="estrellas{{ $i }}" value="4">
                                        <label for="radio{{ $random2 }}" class="dark:text-white lg:text-3xl md:text-sm label_estrella">★</label>
                                        <input class="hidden" id="radio{{ $random3 }}" type="radio"
                                            name="estrellas{{ $i }}" value="3">
                                        <label for="radio{{ $random3 }}" class="dark:text-white lg:text-3xl md:text-sm label_estrella">★</label>
                                        <input class="hidden" id="radio{{ $random4 }}" type="radio"
                                            name="estrellas{{ $i }}" value="2">
                                        <label for="radio{{ $random4 }}" class="dark:text-white lg:text-3xl md:text-sm label_estrella">★</label>
                                        <input class="hidden" id="radio{{ $random5 }}" type="radio"
                                            name="estrellas{{ $i }}" value="1">
                                        <label for="radio{{ $random5 }}" class="dark:text-white lg:text-3xl md:text-sm label_estrella">★</label>
                                    </p>

                                    @php
                                        $i++;
                                    @endphp

                                @endforeach

                            </div>
                            <div>
                                <button
                                    class="px-8 py-2 mt-4 mb-4 font-medium text-center text-white transition-colors duration-150 bg-blue-600 border border-blue-600 lg:text-2xl md:text-sm hover:bg-white hover:text-blue-600 focus:outline-none focus:shadow-outline-blue">RESPONDER</button>
                            </div>
                        </div>


                    </div>

                </section>

            </div>
        </div>

    </div>

</div>
