<div>

    @if ($cita)

        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none"
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="absolute top-0 w-11/12 print:w-full print:h-full">

                <div
                    class="relative p-10 m-4 mb-6 bg-white rounded dark:bg-gray-700 print:p-0 print:bg-transparent print:m-0 print:mb-0">

                    <div class="absolute cursor-pointer right-2 top-2 hover:text-red-600 print:hidden dark:text-white"
                        wire:click="$emit('cerrarModal')">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </div>

                    <div class="flex justify-between mt-2 print:mt-0">
                        <p class="text-lg font-bold text-gray-800 dark:text-white" >Datos del paciente</p>
                        <p class="block text-lg font-bold text-gray-00 dark:text-white">No. Confirmación: <span
                                class="px-2 py-1 text-lg font-bold text-blue-500 bg-gray-200 rounded">{{ $cita->no_confirmacion }}</span><br><span
                                class="block text-sm text-right">Fecha de cita:
                                {{ date('d M Y', strtotime($cita->fecha_seleccionada)) }}</span>
                                @if ($cita->updated_at)
                                    <span class="block text-sm text-right"> Fecha de compra:
                                    {{ date('d M Y', strtotime($cita->updated_at )) }}   {{ date('h:i A', strtotime($cita->updated_at)) }}
                                </span>
                                @endif
                                <span class="block text-sm text-right">Pago: {{ $cita->tipo_tarjeta }} {{ $cita->digitos }}</span>
                                <span class="block text-sm text-right">Costo: ${{ number_format($cita->costo, 2) }}</span>
                        </p>
                    </div>

                    <div class="flex w-full mt-1 print:flex print:flex-wrap">

                        <div class="inline-block w-1/5 p-2 print:w-full print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Nombre Paciente</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text"
                                value="{{ $cita->paciente->nombre }} {{ $cita->paciente->apellido_paterno }} {{ $cita->paciente->apellido_materno }}">
                        </div>

                        <div class="inline-block w-1/5 p-2 print:w-1/4 print:flex print:flex-col print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Género</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="@if ($cita->paciente->genero) {{ 'Hombre' }} @else {{ 'Mujer' }} @endif">
                        </div>

                        <div class="inline-block w-1/5 p-2 print:w-1/4 print:flex print:flex-col print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Fecha de nacimiento</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ date('d M Y', strtotime($cita->paciente->fecha_nacimiento)) }}">
                        </div>

                        <div class="inline-block w-1/5 p-2 print:w-1/4 print:flex print:flex-col print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Teléfono casa</label>
                            <input class="w-full px-2 py-1 text-left text-gray-700 bg-gray-200 rounded print:border-2"
                                disabled type="text" value="{{ $cita->paciente->tel_casa }}">
                        </div>

                        <div class="inline-block w-1/5 p-2 print:w-1/4 print:flex print:flex-col print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Teléfono celular</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->tel_celular }}">
                        </div>

                    </div>

                    <div class="flex w-full mt-1">

                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">EMAIL PACIENTE</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->paciente_email }}">
                        </div>

                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">PASAPORTE PACIENTE</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->pasaporte }}">
                        </div>

                    </div>


                    <div class="flex justify-between mt-1">
                        <p class="text-lg font-bold text-gray-800 dark:text-white">Dirección</p>
                    </div>

                    <div class="flex w-full mt-1 print:flex-wrap">

                        <div class="inline-block w-1/5 p-2 print:w-full print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Calle</label>
                            <input class="w-full px-2 py-1 text-left text-gray-700 bg-gray-200 rounded print:border-2"
                                disabled type="text" value="{{ $cita->paciente->calle }}">
                        </div>

                        <div class="inline-block w-1/5 p-2 print:flex print:flex-col print:w-1/4 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Colonia</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->colonia }}">
                        </div>

                        <div class="inline-block w-1/5 p-2 print:flex print:flex-col print:w-1/4 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">No. exterior</label>
                            <input class="w-full px-2 py-1 text-left text-gray-700 bg-gray-200 rounded print:border-2"
                                disabled type="text" value="{{ $cita->paciente->no_exterior }}">
                        </div>
                        <div class="inline-block w-1/5 p-2 print:flex print:flex-col print:w-1/4 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">No. interior</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->no_interior }}">
                        </div>

                        <div class="inline-block w-1/5 p-2 print:flex print:flex-col print:w-1/4 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Código Postal</label>
                            <input class="w-full px-2 py-1 text-left text-gray-700 bg-gray-200 rounded print:border-2"
                                disabled type="text" value="{{ $cita->paciente->cp }}">
                        </div>

                    </div>


                    <div class="flex justify-between mt-1">
                        <p class="text-lg font-bold text-gray-800 dark:text-white">Datos del Médico</p>
                    </div>

                    <div class="flex w-full mt-1">

                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Nombre Médico</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->medico_nombre }}">
                        </div>

                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Email Médico</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->medico_email }}">
                        </div>

                    </div>


                    <div class="flex justify-between mt-1">
                        <p class="text-lg font-bold text-gray-800 dark:text-white">Datos Fiscales del Paciente</p>
                    </div>

                    <div class="flex w-full mt-1">

                        <div class="w-1/3 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Razón Social</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->razon_social }}">
                        </div>
                        <div class="w-1/3 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">RFC</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->rfc }}">
                        </div>
                        <div class="w-1/3 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Domicilio Fiscal</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="{{ $cita->paciente->domicilio_fiscal }}">
                        </div>

                    </div>

                    <div class="flex w-full mt-1">

                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Uso del cfdi</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="@switch($cita->paciente->cfdis_id) @case(1)G01 - Adquisición de mercancias @break @case(2)G03 - Gastos en general @break @case(3)P01 - Por definir @break @endswitch ">
                        </div>
                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Regimen Fiscal</label>
                            <input class="w-full px-5 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled
                                type="text" value="@switch($cita->paciente->regimen_fiscal_id) @case(1)601 - General de Ley Personas Morales @break @case(2)603 - Personas Morales con Fines no Lucrativos @break @case(3) 	605 - Sueldos y Salarios e Ingresos Asimilados a Salarios @break @endswitch">
                        </div>

                    </div>


                    <div class="flex justify-between mt-1">
                        <p class="text-lg font-bold text-gray-800 dark:text-white">Antecedentes</p>
                    </div>


                    <div class="flex w-full mt-1">

                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Sintomas:</label>
                            <span class="dark:text-white">{{ $cita->paciente->lista_sintomas }}</span>
                        </div>
                        <div class="w-1/2 p-2 print:p-1">
                            <label class="block text-sm text-gray-600 dark:text-white">Enfermedades:</label>
                            <span class="dark:text-white">{{ $cita->paciente->lista_enfermedades }}</span>
                        </div>

                    </div>

                    <div class="flex w-full mt-1">

                        <div class="inline-block w-4/12 p-2 print:p-1 dark:text-white">
                            <span>
                                <label class="text-sm text-gray-600 dark:text-white">Contacto con persona positiva: </label>
                                {{ $cita->paciente->contacto_persona_positivo }}
                            </span>
                        </div>
                        <div class="inline-block w-4/12 p-2 print:p-1 dark:text-white">
                            <span>
                                <label class="text-sm text-gray-600 dark:text-white">Toma algun Antiviral:</label>
                                {{ $cita->paciente->antiviral }}
                            </span>
                        </div>
                        <div class="w-4/12 p-2 print:p-1 dark:text-white">
                            <label class="text-sm text-gray-600 dark:text-white">Se vacuno contra la Influenza:</label>
                            {{ $cita->paciente->influenza }}
                        </div>

                    </div>

                    <div class="flex w-full mt-1">

                        <div class="w-1/2 p-2 print:p-1 dark:text-white">
                            <label class="block text-sm text-gray-600 dark:text-white">Pruebas:</label>
                            <span>{{ $cita->paciente->lista_pruebas }}</span>
                        </div>
                        <div class="w-1/2 p-2 print:p-1 dark:text-white">
                            <label class="block text-sm text-gray-600 dark:text-white">Iidoma de estudios:</label>
                            @if ($cita->paciente->es == null && $cita->paciente->es == null)
                                <p>Español</p>
                            @else

                                @if ($cita->paciente->es)
                                    <p>Español</p>
                                @endif

                                @if ($cita->paciente->en)
                                    <p>Inglés</p>
                                @endif

                            @endif

                        </div>

                    </div>


                    <!--footer-->
                    <div class="flex justify-center p-3 mt-2 space-x-4 text-center print:hidden">

                        <button type="button" onclick="window.print()"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-500 focus:outline-none focus:ring focus:border-transparent">
                            Imprimir Cita
                        </button>

                        <button type="button" wire:click="$emit('cerrarModal')"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500 focus:outline-none focus:ring focus:border-transparent">
                            Cerrar
                        </button>

                    </div>

                </div>

            </div>

        </div>

    @endif

    @if ($asistencia)

        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none min-w-screen animated fadeIn faster focus:outline-none"
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="leading-loose">
                <div class="max-w-xl p-10 m-4 bg-white rounded shadow-xl dark:bg-gray-700">

                    <div class="flex justify-between mt-2">
                        <p class="text-2xl font-bold text-gray-800 dark:text-white">¿Desea marcar la Asistencia de esta
                            cita?</p>
                    </div>

                    <!--footer-->
                    <div class="p-3 mt-2 space-x-4 text-center md:flex">
                        <button wire:click="confirmarAsistencia({{ $asistencia->id }})"
                            class="w-full p-2 font-bold text-center text-white bg-blue-500 rounded-lg text-1xl md:text-2xl hover:bg-blue-300 focus:outline-none focus:ring focus:border-transparent">
                            Si
                        </button>
                        <button wire:click="cerrarModal"
                            class="w-full p-2 font-bold text-center text-white bg-red-500 rounded-lg text-1xl md:text-2xl hover:bg-red-300 focus:outline-none focus:ring focus:border-transparent">
                            No
                        </button>
                    </div>

                </div>

            </div>

        </div>

    @endif

    @if ($noAsistencia)

        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none min-w-screen animated fadeIn faster focus:outline-none"
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="leading-loose">
                <form class="max-w-xl p-10 m-4 bg-white rounded shadow-xl dark:bg-gray-700">

                    <div class="flex justify-between mt-2">
                        <p class="text-2xl font-bold text-gray-800 dark:text-white">¿Desea quitar la Asistencia de esta
                            cita?</p>
                    </div>

                    <!--footer-->
                    <div class="p-3 mt-2 space-x-4 text-center md:flex">
                        <button wire:click="quitarAsistencia({{ $noAsistencia->id }})"
                            class="w-full p-2 font-bold text-center text-white bg-blue-500 rounded-lg text-1xl md:text-2xl hover:bg-blue-300 focus:outline-none focus:ring focus:border-transparent">
                            Si
                        </button>
                        <button wire:click="cerrarModal"
                            class="w-full p-2 font-bold text-center text-white bg-red-500 rounded-lg text-1xl md:text-2xl hover:bg-red-300 focus:outline-none focus:ring focus:border-transparent">
                            No
                        </button>
                    </div>

                </form>

            </div>

        </div>

    @endif

</div>
