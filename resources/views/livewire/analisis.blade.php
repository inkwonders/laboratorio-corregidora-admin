<div>
    <div class="grid px-6 mx-auto">
        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Análisis
        </h2>

        <div class="flex items-center justify-end w-full">
             <button
                class="flex w-auto px-4 py-4 mr-2 font-bold text-center text-white bg-purple-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-purple-500 focus:outline-none focus:ring focus:border-transparent"
                wire:click="uploadMasivaModal">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 pr-1" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </svg>
                IMPORTACION MASIVA
            </button>
            {{-- <a href="{{ route('showUpdateAnalisis') }}"
                class="flex w-auto px-4 py-4 font-bold text-center text-white bg-green-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-green-500 focus:outline-none focus:ring focus:border-transparent">
                Exportar
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </svg>
            </a> --}}


            <button
                class="flex w-auto px-4 py-4 mr-2 font-bold text-center text-white bg-blue-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-blue-500 focus:outline-none focus:ring focus:border-transparent"
                wire:click="agregarModal">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 pr-1" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </svg>
                REGISTRAR ANÁLISIS
            </button>
            <a href="{{ route('export_analisis') }}"
                class="flex w-auto px-4 py-4 font-bold text-center text-white bg-green-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-green-500 focus:outline-none focus:ring focus:border-transparent">
                Exportar
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </svg>
            </a>

        </div>

        <div class="w-full pt-4 overflow-hidden rounded-lg ">
            <livewire:analisis-table />
        </div>

    </div>

    @if ($modal)
        <livewire:modal-analisis />
    @endif

    @if ($modal_carga_masiva)
        <livewire:modal-upload-masiva-analisis />
    @endif



</div>
