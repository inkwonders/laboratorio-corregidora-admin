<div>

    <div class="container grid px-6 mx-auto">

        <section id="home_tipos_analisis">

            <div class="container mx-auto ">

                <div class="flex-row pt-12">
                    <div class="flex justify-center bg-no-repeat place-content-end"
                        style="background-image:url('img/analisis_clinicos.svg'); background-position:center;">
                        <h2 class="pt-10 mt-16 text-5xl azul_textos dark:text-blue-500 "><b> Análisis </b> <br /> Medicos</h2>
                        <div class="pruebas"></div>
                    </div>
                    <div class="flex flex-col mx-auto w-4/4 lg:grid-cols-2 md:grid-cols-2 md:flex-row lg:flex-row">
                        <input
                            class="w-full p-3 px-3 text-sm font-bold text-gray-800 rounded md:col-span-2 lg:col-span-2 md:m-1 md:flex md:w-10/12 field"
                            type="text" placeholder="Buscador..." wire:model="search">
                        <button
                            class="p-3 m-1 font-bold text-center text-gray-200 bg-blue-500 rounded md:px-5 md:flex md:w-1/12 hover:opacity-75 focus:outline-none focus:ring focus:border-transparent">Buscar</button>

                        <select wire:model="tipo" wire:change="seleccionarCategoria({{ $tipo }})"
                            class="flex p-4 px-5 overflow-hidden font-bold text-center text-gray-200 truncate bg-blue-500 rounded md:m-1 sm:w-full md:w-3/12 lg:w-3/12"
                            required="required" name="" id="tipo_estudio">
                            @if (is_null($categorias))
                                <option disabled value="0">No existen categorias registradas.</option>
                            @else
                                <option disabled value="0">Tipo</option>

                                @foreach ($categorias as $categoria)

                                    <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>

                                @endforeach
                            @endif

                        </select>

                    </div>

                </div>

            </div>

            <div class="container mx-auto">

                <div class="grid justify-center">
                    <h2 id="estudio_titulo" class="p-10 text-2xl font-bold md:text-3xl azul_textos dark:text-blue-500">PRUEBAS</h2>
                </div>



                @if (sizeof($analisis) >= 1)

                    <div class="grid grid-cols-1 gap-4 mb-8 md:grid-cols-3" id="contenedor_estudios">

                        @foreach ($analisis as $estudio)

                            <div
                                class="grid content-end w-11/12 mx-auto overflow-hidden bg-white rounded-lg material-card dark:bg-gray-700">
                                <div class="px-6 py-4">
                                    <div class="text-xl font-bold tracking-wide azul_textos dark:text-white">
                                        {{ $estudio->nombre }}
                                    </div>
                                    <div class="m-3 text-gray-500">
                                        <hr />
                                    </div>
                                    <div class="grid justify-center text-lg text-center">
                                        @if ($estudio->precio != 0)
                                            <p class="text-center azul_textos dark:text-white">PRECIO</p>
                                            <p class="font-bold text-green-500">$
                                                {{ number_format($estudio->precio, 2) }}</p>
                                        @endif
                                        @if ($estudio->precio == 0)
                                            <p class="font-bold text-green-500">Favor de llamar al teléfono 442 212 10
                                                52 para solicitar el precio de este estudio.
                                        @endif


                                        </p>
                                    </div>

                                </div>

                                <div class="grid text-center md:grid-cols-2">
                                    <a href="analisis/{{ $estudio->id }}"
                                        class="cursor-pointer bg-blue-800 tracking-wider uppercase font-bold text-gray-100 hover:bg-blue-700  p-2.5 ">VER
                                        ESTUDIO</a>
                                    <a wire:click="$emit('agregarCarrito', {{ $estudio->id }})"
                                        class="cursor-pointer bg-blue-600 tracking-wider uppercase font-bold text-gray-100 hover:bg-blue-700  p-2.5 ">+
                                        AGREGAR</a>
                                </div>
                            </div>

                        @endforeach

                    </div>

                @else

                    <div class="flex items-center justify-center w-full realtive h-3/4">
                        <p class="w-full text-xl font-bold tracking-wide text-center azul_textos">No existen ningún
                            registro coincidente con la busqueda...</p>
                    </div>

                @endif

            </div>

        </section>

    </div>

</div>
