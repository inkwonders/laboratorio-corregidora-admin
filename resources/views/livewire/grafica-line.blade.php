<div wire:init="loadChartLine">
    <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
    <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300">
      {{ $titulo }}
    </h4>
    {{-- <canvas id="line" style="display: block; width: 572px; height: 286px;" class="chartjs-render-monitor" width="572" height="286"></canvas> --}}
    <canvas id="{{ $id_grafica }}" style="display: block; width: 572px; height: 286px;" class="chartjs-render-monitor" width="572" height="286"></canvas>
    <div class="flex justify-center mt-4 space-x-3 text-sm text-gray-600 dark:text-gray-400">
      <!-- Chart legend -->
      @foreach ($etiquetas as $etiqueta)
        <div class="flex items-center">
            <span class="inline-block w-3 h-3 mr-1 {{ $etiqueta['clase'] }} rounded-full"></span>
            <span>{{ $etiqueta['texto'] }}</span>
        </div>
      @endforeach

    </div>
  </div>
</div>


@push('scripts')

<script>

    Livewire.on('drawChartLine', function(id_grafica, data) {
        /**
        * For usage, visit Chart.js docs https://www.chartjs.org/docs/latest/
        */

        const lineConfig = {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                legend: {
                display: false,
                },
                tooltips: {
                mode: 'index',
                intersect: false,
                },
                hover: {
                mode: 'nearest',
                intersect: true,
                },
                scales: {
                x: {
                    display: true,
                    scaleLabel: {
                    display: true,
                    labelString: 'Month',
                    },
                },
                y: {
                    display: true,
                    scaleLabel: {
                    display: true,
                    labelString: 'Value',
                    },
                },
                },
            },
        }

        // change this to the id of your chart element in HMTL
        const lineCtx = document.getElementById(id_grafica)
        window.myLine = new Chart(lineCtx, lineConfig)

    });

</script>

@endpush
