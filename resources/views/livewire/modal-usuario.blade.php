<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none "
        style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="absolute w-6/12 ">
            <form class="relative w-full p-10 bg-white rounded shadow-xl dark:bg-gray-700">
                <div class="absolute cursor-pointer right-6 top-4 hover:text-red-600 dark:text-white" wire:click="$emit('cerrarModal')">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div class="flex justify-between mt-2">
                    <p class="mb-4 text-2xl font-bold text-center dark:text-white">
                        @if ($editar)
                            Editar usuario
                        @else
                            Agregar usuario
                        @endif
                    </p>
                </div>

                <div class="flex w-full">

                    <div class="w-1/3 p-2">
                        <label class="block text-xl text-gray-600 dark:text-white">Nombre</label>
                        <input
                            class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                            type="text" value="" wire:model="nombre">
                        @error('nombre')
                            <p class="mb-2 text-red-600 error_nombre">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="w-1/3 p-2">
                        <label class="block text-xl text-gray-600 dark:text-white">Email</label>
                        <input
                            class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                            type="text" value="" wire:model="email">
                        @error('email')
                            <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="w-1/3 p-2">
                        <label class="block text-xl text-gray-600 dark:text-white">Tipo usuario</label>
                        <select
                            class="block w-full p-2 border rounded-lg cursor-pointer h-13 bg-grey-lighter text-grey-darker border-grey-lighter md:w-full"
                            name="rol_usuario" wire:model="rol_usuario" wire:change="change" >
                            <option value="">Tipo usuario</option>
                            <option value="1">Administrador</option>
                            <option value="2">Medico</option>
                            {{-- <option value="3">Invitado</option> --}}
                        </select>
                        @error('tipo')
                            <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                        @enderror
                    </div>



                </div>
                  @if ($mostrar_admin)
                  <div class="flex w-full">
                    <span class="block text-xl text-gray-600 dark:text-white">PERMISOS</span>
                    <div class="w-full p-2">
                        <div class="grid grid-cols-2 auto-rows-min">
                        @foreach ($permisos_usuario as $id => $permiso)
                            <label class="relative self-start" for="">
                                    <input class="inline-block rounded-sm my-2" type="checkbox" wire:model.defer="permisos_usuario.{{ $id }}.activo" @if($permiso['heredado']) title="Permiso heredado del un rol" disabled @endif>
                                    <span class="w-auto m-4 dark:text-white uppercase font-rob-light">{{ $permiso['nombre'] }}</span>
                            </label>
                        @endforeach
                    </div>
                    </div>
                </div>
                @endif
                @if ($mostrar_medico)
                @endif
                <div class="w-full mt-2">

                    <div class="1/2">
                        <label class="block text-xl text-gray-600 dark:text-white">Contraseña</label>
                        <input maxlength="10"
                            class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                            type="password" value="" wire:model="password1">
                        @error('password1')
                            <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                        @enderror
                    </div>

                    <div class="1/2">
                        <label class="block text-xl text-gray-600 dark:text-white">Confirmar Contraseña</label>
                        <input maxlength="10"
                            class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                            type="password" value="" wire:model="password2">
                        @error('password2')
                            <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
                        @enderror
                    </div>

                </div>


                <br>
                <span class="text-center text-gray-400 dark:text-white"> Todos los campos son obligatorios </span>

                <!--footer-->
                <div class="flex items-center justify-center p-3 mt-2 space-x-4 text-center">
                    <div wire:click="$emit('cerrarModal')"
                        class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500">
                        Cerrar
                    </div>
                    @if ($editar)


                        <div wire:click="update"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600">
                            Editar
                        </div>
                    @else
                        <div wire:click="create"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600">
                            Agregar
                        </div>
                    @endif

                </div>

            </form>

        </div>

    </div>

</div>
