<div x-data class="flex flex-row pb-10">
    <div class="relative flex items-center w-full">
        <label for="" class="dark:text-white" >Fecha inicio: </label>
        <input wire:model="fecha_start" x-ref="start" class="flex-grow pt-1 pr-6 m-1 text-sm form-input" type="date"
            wire:change="doDateFilterStart('{{ $index }}', $event.target.value)" style="padding-bottom: 5px" />
        <div class="absolute inset-y-0 right-0 flex items-center pr-2">
            {{-- <button x-on:click="$refs.start.value=''" wire:click="doDateFilterStart('{{ $index }}', '')" class="inline-flex text-gray-400 hover:text-red-600 focus:outline-none" tabindex="-1">
                <x-icons.x-circle class="w-3 h-3 stroke-current" />
            </button> --}}
        </div>
    </div>
    <div class="relative flex items-center w-full pl-10">
        <label for="" class="dark:text-white" >Fecha final: </label>
        <input wire:model="fecha_end" x-ref="end" class="flex-grow pt-1 pr-6 m-1 text-sm form-input" type="date"
            wire:change="doDateFilterEnd('{{ $index }}', $event.target.value)" style="padding-bottom: 5px" />
            <div class="absolute inset-y-0 right-0 flex items-center pr-2">
            {{-- <button x-on:click="$refs.end.value=''" wire:click="doDateFilterEnd('{{ $index }}', '')" class="inline-flex text-gray-400 hover:text-red-600 focus:outline-none" tabindex="-1">
                <x-icons.x-circle class="w-3 h-3 stroke-current" />
            </button> --}}
        </div>
    </div>
</div>
