<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none"
        style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="absolute w-6/12 print:w-full print:h-full">

            <div
                class="relative p-10 m-4 mb-6 bg-white rounded print:p-0 print:bg-transparent print:m-0 print:mb-0 dark:bg-gray-700">

                <div class="absolute cursor-pointer right-2 top-2 hover:text-red-600 print:hidden dark:text-white"
                    wire:click="$emit('cerrarModal')">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div class="flex justify-between mt-2">
                    <p class="mb-4 text-2xl font-bold text-center dark:text-white">Enviar encuesta</p>
                </div>

                <div class="flex w-full pb-4">

                    <label class="block pr-10 text-xl text-gray-600 dark:text-white">¿A quién deseas enviar la
                        encuesta?</label>

                    <label class="flex items-start justify-start pr-4">
                        <div
                            class="flex items-center justify-center flex-shrink-0 w-6 h-6 mr-2 bg-white border-2 border-gray-400 rounded dark:text-white focus-within:border-blue-500">
                            <input type="checkbox" class="absolute opacity-0" wire:model="citas">
                            <svg class="hidden w-4 h-4 text-green-500 pointer-events-none fill-current"
                                viewBox="0 0 20 20">
                                <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
                            </svg>
                        </div>
                        <div class="select-none dark:text-white">Citas Covid</div>
                    </label>

                    <label class="flex items-start justify-start pr-4">
                        <div
                            class="flex items-center justify-center flex-shrink-0 w-6 h-6 mr-2 bg-white border-2 border-gray-400 rounded focus-within:border-blue-500">
                            <input type="checkbox" class="absolute opacity-0" wire:model="analisis">
                            <svg class="hidden w-4 h-4 text-green-500 pointer-events-none fill-current"
                                viewBox="0 0 20 20">
                                <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
                            </svg>
                        </div>
                        <div class="select-none dark:text-white">Analisis</div>
                    </label>

                    <label class="flex items-start justify-start pr-4">
                        <div
                            class="flex items-center justify-center flex-shrink-0 w-6 h-6 mr-2 bg-white border-2 border-gray-400 rounded focus-within:border-blue-500">
                            <input type="checkbox" class="absolute opacity-0" wire:model="lista_de_correos">
                            <svg class="hidden w-4 h-4 text-green-500 pointer-events-none fill-current"
                                viewBox="0 0 20 20">
                                <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
                            </svg>
                        </div>
                        <div class="select-none dark:text-white">Lista de correos</div>
                    </label>

                    <style>
                        input:checked+svg {
                            display: block;
                        }

                    </style>

                </div>

                <div class="flex w-full">

                    <div class="w-full p-2">

                        <div class="w-full pb-2 realtive">
                            <label class="block text-xl text-gray-600 dark:text-white">Buscador de pacientes</label>
                            <input placeholder="Escribe el nombre o email del paciente"
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" wire:model="paciente">

                            <ul class="w-full p-2 text-left bg-white realtive dark:bg-gray-700 dark:text-white">
                                @foreach ($consulta as $resultado)
                                    <li><button
                                            wire:click="agregar('{{ $resultado->email }}','{{ $resultado->id }}')">{{ $resultado->name }}
                                            - {{ $resultado->email }}</button></li>
                                @endforeach
                            </ul>
                            <div>
                                @if (count($pacientes_encuesta) > 0)
                                    <p class="text-sm text-right dark:text-white"> Cantidad de correos:
                                        {{ count($pacientes_encuesta) }} </p>
                                @endif
                            </div>
                        </div>
                        <div class="flex flex-col items-start justify-start w-full p-2 overflow-y-scroll bg-gray-300 border border-solid dark:bg-white"
                            style="height: 300px;">
                            @foreach ($pacientes_encuesta as $id => $correo)
                                <span
                                    class="flex items-center justify-between w-full px-2 mb-2 text-xs font-bold text-white bg-blue-500 rounded-lg rounded-min "
                                    style="height: 30px;">
                                    {{ $correo['email'] }}
                                    <svg xmlns="http://www.w3.org/2000/svg" wire:click="quitar({{ $id }})"
                                        class="w-4 h-4 ml-2 cursor-pointer hover:text-red-600" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                </span>
                            @endforeach
                        </div>
                        <div class="w-full mt-2 ">
                            <label for="agregar_correo" class="dark:text-white ">Agregar correo</label>
                            <input wire:model="agregar_correo" type="text" name="agregar_correo" id="agregar_correo"
                                class="w-7/12 p-2 m-2 ml-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400">
                            <button wire:click="agregar_correos"
                                class="w-auto px-4 py-2 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer hover:bg-blue-400 focus:outline-none focus:ring focus:border-transparent justify-evenly text-1xl md:text-1xl">Agregar</button>
                            @if ($correo_repetido)
                                <p class="mb-2 text-center text-red-600">{{ $correo_repetido }}</p>
                            @endif
                            @error('agregar_correo')
                                <p class="mb-2 text-red-600 error_nombre">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="flex justify-center w-full mt-4 ">
                            <button wire:click="enviar_correo"
                                class="@if (!$validado) pointer-events-none cursor-not-allowed text-gray-300 bg-gray-500 @else text-white bg-blue-500 cursor-pointer hover:bg-blue-400 focus:outline-none focus:ring focus:border-transparent @endif flex w-auto px-4 py-4 font-bold text-center  rounded-lg  justify-evenly text-1xl md:text-1xl ">
                                Enviar
                            </button>
                            {{-- <button wire:model="validado" :attr="disabled" wire:click="enviar_correo"
                                class="flex w-auto px-4 py-4 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer justify-evenly text-1xl md:text-1xl hover:bg-blue-400 focus:outline-none focus:ring focus:border-transparent">
                                Enviar
                        </button> --}}
                        </div>
                    </div>




                    {{-- <div class="w-6/12 p-2">

                        <div class="w-full pb-2">
                            <label class="block text-xl text-gray-600">Excluir pacientes</label>
                            <input class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text">
                        </div>

                        <div class="flex flex-wrap w-full p-2 bg-gray-500 border border-solid" style="height: 300px;">
                            <span class="flex justify-between w-auto px-2 py-1 mr-3 text-xs font-bold text-white bg-blue-500 rounded-lg rounded-min" style="height: 25px;">
                                Citas Covid
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 ml-2 cursor-pointer hover:text-red-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                            </span>
                            <span class="flex justify-between w-auto px-2 py-1 mr-3 text-xs font-bold text-white bg-blue-500 rounded-lg rounded-min" style="height: 25px;">
                                Orden Analisis
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 ml-2 cursor-pointer hover:text-red-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                            </span>
                        </div>

                    </div> --}}

                </div>

            </div>

        </div>

    </div>

</div>
