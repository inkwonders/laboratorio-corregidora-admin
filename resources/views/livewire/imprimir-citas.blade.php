<div>

    @if ($modal)

        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none"
        style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="absolute top-0 w-8/12">

                <div class="relative p-16 m-4 mb-6 bg-white rounded dark:bg-gray-700 print:p-0 print:bg-transparent print:m-0 print:mb-0">

                    <div class="absolute cursor-pointer right-2 top-2 hover:text-red-600 print:hidden dark:text-white"
                        wire:click="$emit('cerrarModalImprimir')">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </div>

                    <div class="relative w-full h-full ">
                        @foreach ($respuestas as $cita)
                            <div class="flex items-center w-full p-8 py-2 text-xs border-2 border-black border-dashed justify-evenly realtive">
                                <p>No. confirmación: {{ $cita["no_confirmacion"] }}</p>
                                <p>Nombre completo: {{ $cita["nombre"] }} {{ $cita["apellido_paterno"] }} {{ $cita["apellido_materno"] }}</p>
                                <p>Correo paciente: {{ $cita["paciente_email"] }}</p>
                                <p>Teléfono paciente: {{ $cita["tel_celular"] }}</p> <br>
                                <p>Fecha cita: {{ $cita["fecha_seleccionada"] }}</p>
                                <p>Total pagado: ${{ number_format($cita["costo"],2) }}</p>
                                <p>Forma de pago: {{ $cita["tipo_tarjeta"] }} <br> {{ $cita["digitos"] }}</p>
                            </div>
                        @endforeach
                    </div>

                    <div class="flex justify-center p-3 mt-2 space-x-4 text-center print:hidden">

                        <button type="button" onclick="window.print()" class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-500 focus:outline-none focus:ring focus:border-transparent">
                            Imprimir Cita
                        </button>

                        <button type="button" wire:click="$emit('cerrarModalImprimir')" class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500 focus:outline-none focus:ring focus:border-transparent">
                            Cerrar
                        </button>

                    </div>

                </div>



            </div>

        </div>

    @endif

</div>
