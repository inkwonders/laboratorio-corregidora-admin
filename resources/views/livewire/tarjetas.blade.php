<div>

    <div class="grid px-6 mx-auto">
        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Pagos con tarjeta
        </h2>

        <div class="w-full pt-4 overflow-hidden rounded-lg">

            <div class="flex justify-end w-full mt-2 mb-4">
                <a href="{{ route('export_tarjetas',['fs' => $fecha_start, 'fe' => $fecha_end ]) }}"
                    class="flex w-auto px-4 py-4 font-bold text-center text-white bg-green-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-green-500 focus:outline-none focus:ring focus:border-transparent">
                    Exportar
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                    </svg>
                </a>


            </div>

            <livewire:tarjetas-table />

            <br>
            <br><br>

            {{-- <div class="flex items-center w-9/12 p-4 mt-4 text-center bg-white rounded-lg shadow-xs md:w-3/12 xl:w-2/12 dark:bg-gray-800">
                <div class="p-3 mr-4 text-green-500 bg-green-100 rounded-lg dark:text-green-100 dark:bg-green-500">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                        Ingresos Totales
                    </p>
                    <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                        $ {{ number_format($total,2) }}
                    </p>
                </div>
            </div> --}}

        </div>

    </div>

    @if ($modal)
        <livewire:modal-tarjetas />
    @endif



</div>
