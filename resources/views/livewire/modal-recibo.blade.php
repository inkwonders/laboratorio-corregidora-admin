<div>

    @if($modal)

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none min-w-screen animated fadeIn faster focus:outline-none"  style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="w-3/4 leading-loose">

            <div class="bg-white rounded shadow-xl w-4/4 dark:bg-gray-700">

                <div class="flex flex-col w-full p-8 rounded no-print azul_textos">
                    <h2 class="mb-4 text-2xl font-bold text-center ">RESUMEN</h2>

                    @foreach ($carrito->analisis as $estudio)

                        <div class="flex flex-col w-full p-4 text-base text-gray-900 md:flex-row dark:text-white ">
                            <div class="flex justify-center w-full font-bold md:justify-start md:w-1/12">
                                {{ $estudio->pivot->cantidad }}
                            </div>
                            <div class="flex justify-center w-full md:justify-start md:w-10/12">
                                {{ $estudio->nombre }}
                            </div>
                            <div class="flex justify-center w-full text-green-500 dark:text-green-400 md:justify-end md:w-1/12">
                            $ {{ number_format($estudio->precio,2) }}
                            </div>
                        </div>

                    @endforeach

                    <div class="flex justify-start w-full p-4 font-bold border-t-2 border-b-2 md:justify-end">
                        <span class="ml-4 text-xl textos_azules dark:text-blue-300">
                            TOTAL  &nbsp
                        </span>
                        <span class="text-xl text-green-500">
                            $ {{ number_format($carrito->analisis->sum('precio'),2) }}
                        </span>
                    </div class="flex w-full ">
                        <div class="flex flex-wrap p-4 mr-2 text-xl ">
                            <p class="dark:text-blue-300">NOMBRE:&nbsp&nbsp&nbsp&nbsp&nbsp</p> <p class="text-gray-900 dark:text-white"> {{ $carrito->nombre_paciente }} </p>
                        </div>
                        <div class="flex flex-wrap p-4 mr-2 text-xl ">
                            <p class="dark:text-blue-300">CORREO:&nbsp&nbsp&nbsp&nbsp &nbsp</p> <p class="text-gray-900 dark:text-white"> {{ $carrito->email_paciente }} </p>
                        </div>
                        <div class="flex flex-wrap p-4 mr-2 text-xl ">
                            <p class="dark:text-blue-300">TELÉFONO:&nbsp&nbsp&nbsp </p> <p class="text-gray-900 dark:text-white"> {{ $carrito->telefono_paciente }} </p>
                        </div>
                    <div>
                    </div>
                </div>

                <div class="flex justify-center w-full py-10">

                    <div class="flex flex-col justify-center w-full md:flex-row no-print">

                        <div class="flex items-center justify-center mb-2 xs:w-full md:m-2 md:w-2/12">
                            <a wire:click="cerrarModalRecibo" class="w-full p-2 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500">CERRAR</a>
                        </div>

                        <div class="flex items-center justify-center mb-2 xs:w-full md:m-2 md:w-2/12">
                            <button wire:click="$emit('imprimir',  {{$carrito->id}})" class="w-full p-2 font-bold text-center text-white bg-blue-500 rounded-lg focus:outline-none focus:ring focus:border-blue-400 text-1xl md:text-2xl no-print text- hover:bg-blue-600">IMPRIMIR</button>
                        </div>

                    </div>

                </div>

                <div id="div_imprimir" class="hidden bg-white">

                    <div class="container flex flex-col w-full mx-auto bg-white">

                        <!-- logo -->
                        <div class="flex justify-center w-full p-8 ">
                            <img class="h-20" src="../img/logo_imprimir.svg" alt="">
                        </div>

                        <!-- nombre -->
                        <div class="w-full p-4 text-2xl font-bold text-center border-b-2 border-gray-300 azul_textos">
                            <h2>RESUMEN Y PAGO</h2>
                        </div>

                        <!-- info paciente -->
                        <div class="w-full border-b-2 border-gray-300 ">
                            <h2 class="p-2 text-xl font-bold azul_textos">INFORMACIÓN DEL PACIENTE</h2>
                            <div class="flex p-2 text-xl">
                                <p class="w-3/12 azul_textos ">
                                    NOMBRE:
                                </p>
                                <p class="w-9/12">
                                    {{ $carrito->nombre_paciente }}
                                </p>
                            </div>
                            <div class="flex p-2 text-xl">
                                <p class="w-3/12 azul_textos ">
                                    CORREO:
                                </p>
                                <p class="w-9/12">
                                    {{ $carrito->email_paciente }}
                                </p>
                            </div>
                            <div class="flex p-2 text-xl">
                                <p class="w-3/12 azul_textos ">
                                    TELÉFONO:
                                </p>
                                <p class="w-9/12">
                                    {{ $carrito->telefono_paciente }}
                                </p>
                            </div>
                        </div>

                        <!-- listado -->
                        <div class="w-full border-b-2 border-gray-300 ">

                            <h2 class="p-2 text-xl font-bold azul_textos">LISTADO DE ANALISÍS</h2>

                            @foreach ($carrito->analisis as $estudio)

                                <div class="flex items-center w-full p-2 text-xl">
                                    <span class="w-1/12 font-bold">{{ $estudio->pivot->cantidad }}</span>
                                    <span class="w-8/12">{{ $estudio->nombre }}</span>
                                    <span class="w-2/12 text-right text-green-500">$ {{ number_format($estudio->precio,2) }} </span>
                                </div>

                            @endforeach

                            <div class="flex justify-end p-2 mt-4 text-xl font-bold border-t-2 border-gray-300 w-12/12">
                                <div class="mr-4">
                                    <span class="mr-4 azul_textos">TOTAL</span> <span class="text-green-500">$ {{ number_format($carrito->analisis->sum('precio'),2) }} </span>
                                </div>
                            </div>

                        </div>

                        <!-- logo_grande -->
                        <div class="flex justify-center w-full p-6 border-b-2 border-gray-300">
                            <img class="h-36" src="../img/logo_imprimir_dos.svg" alt="">
                        </div>

                        <!-- pagina -->
                        <div class="flex justify-center w-full p-2">
                            <span>www.laboratoriocorregidora.com.mx</span>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endif

</div>
