<div>

    <div class="flex flex-col justify-between w-full p-10 space-y-4 md:flex-row md:space-y-0 md:space-x-8">
        <div class="flex flex-col w-full p-3 bg-white rounded-lg md:w-8/12 md:p-7 dark:bg-gray-700">
            <h2 class="mb-2 text-xl font-semibold text-gray-400 md:text-2xl dark:text-white"> CLAVE {{ $analisis->clave }} </h2>
            <h2 class="w-full text-xl font-bold md:text-4xl azul_textos dark:text-blue-300"> {{ $analisis->nombre }} </h2>
        </div>
        <div class="flex flex-col justify-center w-full bg-white rounded-lg md:w-4/12 p-7 dark:bg-gray-700">
            <p class="text-base font-bold md:text-lg azul_textos dark:text-blue-300">ESTUDIOS CONTENIDOS</p>
            <pre class="mb-2 font-sans whitespace-pre-line md:text-lg dark:text-white">{{ $analisis->estudios }}</pre>
            <p class="text-base font-bold md:text-lg azul_textos dark:text-blue-300">AYUNO</p>
            <p class="mb-2 text-base md:text-lg dark:text-white">{{ $analisis->ayuno }}</p>
            <p class="text-base font-bold md:text-lg azul_textos dark:text-blue-300">TIEMPOS DE ENTREGA</p>
            <p class="mb-2 text-base md:text-lg dark:text-white">{{ $analisis->entrega }}</p>
        </div>
    </div>
    <div class="flex flex-col justify-between w-full pb-10 pl-10 pr-10 space-y-4 md:flex-row md:space-y-0 md:space-x-8">
        <div class="flex flex-col justify-center w-full bg-white rounded-lg md:w-8/12 p-7 dark:bg-gray-700">
            <h2 class="text-lg font-semibold azul_textos dark:text-blue-300">INDICACIONES PARA EL PACIENTE</h2>
            <pre class="font-sans text-lg whitespace-pre-line dark:text-white"> {{ $analisis->indicaciones }}</pre>
        </div>
        <div class="flex flex-col justify-end w-full overflow-hidden text-center bg-white rounded-lg md:w-4/12 dark:bg-gray-700">
            @if ($analisis->precio != 0)

                <p class="text-2xl font-semibold azul_textos dark:text-blue-300">Precio</p>
                <p class="text-3xl font-bold text-green-500">${{ number_format($analisis->precio, 2) }} <br></p>

            @endif

            @if ($analisis->precio == 0)

                <p class="pt-2 text-2xl font-bold text-green-500">Favor de llamar al teléfono 442 212 10 52 para solicitar el precio
                    de este estudio.</p>


            @endif

            <a wire:click="$emit('agregarCarrito', {{ $analisis->id }})"
                class="cursor-pointer mt-6 bg-blue-500 text-2xl tracking-wider uppercase font-bold text-gray-100 hover:bg-blue-700  p-2.5 ">+
                AGREGAR</a>
        </div>
    </div>
    <div class="flex justify-center w-full pb-8">
        <br><br>
        <a href="{{ url()->previous() }}"
            class="p-3 px-4 text-white bg-gray-500 rounded-lg md:mt-8 focus:outline-none modal-close hover:bg-gray-400">Regresar</a>
    </div>

</div>
