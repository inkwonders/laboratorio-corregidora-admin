<html :class="{ 'theme-dark': dark }">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="../img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon/favicon-16x16.png">
    <link rel="manifest" href="../img/favicon/site.webmanifest">
    <link rel="mask-icon" href="../img/favicon/safari-pinned-tab.svg" color="#cbf202">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">
    <!-- favicon -->
    <title> Encuestas </title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&display=swap"
        rel="stylesheet" />

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/tailwind.output.css') }}" /> --}}
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <script src="{{ asset('js/init-alpine.js') }}" defer></script>

    {{-- <script src="{{asset('js/charts-lines.js')}}" defer></script> --}}
    {{-- <script src="{{asset('js/charts-pie.js')}}" defer></script> --}}

    <style>
        .clasificacion {
            direction: rtl;
            unicode-bidi: bidi-override;
            font-size: 16px;
        }

        .label_estrella:hover {
            color: orange;
            cursor: pointer;
        }

        .label_estrella:hover~.label_estrella {
            color: orange;
        }

        input[type="radio"]:checked~.label_estrella {
            color: orange;
        }

    </style>
    @livewireScripts()
    @livewireStyles



    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.js" defer></script>
</head>

<body>
    <section id="Encuestas">
        <div class="container mx-auto ">

            <div class="flex justify-center w-full p-16 ">
                <img class="w-full md:w-1/2" src="../img/logo_imprimir.svg" alt="Laboratorios Corregidora">
            </div>
            <article
                class="flex flex-col items-center content-center justify-center w-full mb-16 text-center azul_textos">
                <h2 class="mt-5 mb-5 text-4xl font-bold" style="color:#194271">- ENCUESTA DE SATISFACCIÓN -</h2>
                @if ($contestado != 'ok')


                    <span class="w-full text-3xl md:w-7/12">{{ $titulo_encuesta }} </span>
                @endif

            </article>

            @if ($contestado != 'ok')
                <div>
                    <div class="flex flex-col items-center content-center ">

                        <div class="flex flex-col items-center content-center mb-6">
                            @php
                                $i = 1;

                            @endphp
                            @foreach ($preguntas as $pregunta => $value)
                                @php
                                    $random = rand(1, 5000);
                                    $random2 = rand(1, 5000);
                                    $random3 = rand(1, 5000);
                                    $random4 = rand(1, 5000);
                                    $random5 = rand(1, 5000);
                                @endphp

                                <p class="w-5/6 text-3xl font-semibold text-center text-gray-600">
                                    {{ $i }}.- {{ $value['descripcion_preguntas'] }}
                                </p>
                                <p class="clasificacion">


                                    <input wire:key='{{ $loop->index }}' class="hidden" id="radio{{ $random }}"
                                        type="radio" name="estrellas{{ $i }}" value="5">
                                    <label for="radio{{ $random }}" class="text-5xl label_estrella">★</label>
                                    <input wire:key='{{ $loop->index }}' class="hidden"
                                        id="radio{{ $random2 }}" type="radio" name="estrellas{{ $i }}"
                                        value="4">
                                    <label for="radio{{ $random2 }}" class="text-5xl label_estrella">★</label>
                                    <input wire:key='{{ $loop->index }}' class="hidden"
                                        id="radio{{ $random3 }}" type="radio" name="estrellas{{ $i }}"
                                        value="3">
                                    <label for="radio{{ $random3 }}" class="text-5xl label_estrella">★</label>
                                    <input wire:key='{{ $loop->index }}' class="hidden"
                                        id="radio{{ $random4 }}" type="radio" name="estrellas{{ $i }}"
                                        value="2">
                                    <label for="radio{{ $random4 }}" class="text-5xl label_estrella">★</label>
                                    <input wire:key='{{ $loop->index }}' class="hidden"
                                        id="radio{{ $random5 }}" type="radio" name="estrellas{{ $i }}"
                                        value="1">
                                    <label for="radio{{ $random5 }}" class="text-5xl label_estrella">★</label>
                                    <input id="cero" class="hidden" type="radio" name="estrellas{{ $i }}"
                                        value="0" checked>

                                </p>
                                @php
                                    $i = $i + 1;
                                @endphp
                            @endforeach

                        </div>
                        <div class="flex flex-col">
                            <span id="error" class="text-base text-red-500">{{ $error }} </span>
                            <button onclick="validar()"
                                class="px-8 py-4 mt-8 mb-8 text-3xl font-medium text-center text-white transition-colors duration-150 bg-blue-600 border border-blue-600 hover:bg-white hover:text-blue-600 focus:outline-none focus:shadow-outline-blue">RESPONDER
                            </button>

                        </div>

                    </div>
                </div>
            @else
                <div>
                    <div class="flex flex-col items-center content-center justify-center w-full p-4 text-2xl text-center"
                        style="color:#194271">
                        Muchas gracias por su participación en nuestra
                        encuesta de satisfacción. Sus respuestas han sido enviadas correctamente.

                        <a href="https://www.laboratoriocorregidora.com.mx" target="_blank"
                            class="w-8/12 px-8 py-4 mt-8 mb-8 text-3xl font-medium text-center text-white transition-colors duration-150 bg-blue-600 border border-blue-600 md:w-3/12 hover:bg-white hover:text-blue-600 focus:outline-none focus:shadow-outline-blue">ENTENDIDO
                        </a>
                    </div>
                </div>
            @endif
        </div>

    </section>



</body>

<script>
    function validar() {
        if (document.getElementsByName("estrellas1") != '0') {
            valor_encuesta1 = document.querySelector("input[name=estrellas1]:checked").value;
        }
        if (document.getElementsByName("estrellas2") != '0') {
            valor_encuesta2 = document.querySelector("input[name=estrellas2]:checked").value;
        }
        if (document.getElementsByName("estrellas3") != '0') {
            valor_encuesta3 = document.querySelector("input[name=estrellas3]:checked").value;
        }
        if (document.getElementsByName("estrellas4") != '0') {
            valor_encuesta4 = document.querySelector("input[name=estrellas4]:checked").value;
        }
        if (document.getElementsByName("estrellas5") != '0') {
            valor_encuesta5 = document.querySelector("input[name=estrellas5]:checked").value;
        }

        if (valor_encuesta1 != '0' && valor_encuesta2 != '0' && valor_encuesta3 != '0' && valor_encuesta4 != '0' &&
            valor_encuesta5 != '0') {
            datos_enviar = [valor_encuesta1, valor_encuesta2, valor_encuesta3, valor_encuesta4, valor_encuesta5];
            encuesta(datos_enviar);
        } else {
            document.getElementById('error').innerHTML = 'Todos los campos son obligatorios'
        }


    }

    function encuesta($arreglo) {

        Livewire.emit('enviar_encuesta', $arreglo)

    }
</script>

</html>
