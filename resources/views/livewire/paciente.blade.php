<div>

    <div class="p-10">



        <div class="flex flex-row justify-center mb-2 space-x-4 md:justify-end">
            <div class="w-3 h-3 mb-4 bg-blue-600 rounded-full" ></div>
            <div class="w-3 h-3 mb-4 border border-blue-600 rounded-full" ></div>
        </div>

        <div class="flex flex-col w-full p-8 bg-white rounded azul_textos dark:bg-gray-700">
            <h2 class="mb-4 text-2xl font-bold text-center text-blue-300 ">DATOS DEL PACIENTE</h2>
            <label class="text-xl dark:text-blue-300 " for="txt_nombre">NOMBRE COMPLETO*</label>
            <input maxlength="120" class="p-2 mb-4 border rounded-lg border-grey-200 focus:ring-2 focus:outline-none focus:ring focus:border-blue-400" type="text" id="txt_nombre" name="nombre" wire:model="nombre">
            @error('nombre')
                <p class="mb-2 text-red-600 error_nombre">{{ $message }}</p>
            @enderror
            <label class="mb-2 text-xl dark:text-blue-300" for="txt_correo">CORREO*</label>
            <input maxlength="120" class="p-2 mb-4 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="text" id="txt_correo" name="email" wire:model="email">
            @error('email')
                <p class="mb-2 text-red-600 error_correo">{{ $message }}</p>
            @enderror
            <label class="mb-2 text-xl dark:text-blue-300" for="txt_telefono">TELÉFONO*</label>
            <input maxlength="10" class="p-2 mb-4 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400" type="tel" id="txt_telefono" name="telefono" wire:model="telefono">
            @error('telefono')
                <p class="mb-2 text-red-600 error_telefono">{{ $message }}</p>
            @enderror
            <span class="text-center text-gray-400" > Los campos con * son obligatorios </span>
        </div>

        <div class="flex justify-center pt-6 space-x-4">

            <a href="{{ route("carrito") }}" class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg text-1xl md:text-2xl hover:bg-gray-500">
                REGRESAR
            </a>
            <button wire:click="update()" id="siguiente_btn"  class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg focus:outline-none focus:ring focus:border-blue-400 text-1xl md:text-2xl hover:bg-blue-600">
                SIGUIENTE
            </button>
        </div>

    </div>

</div>
