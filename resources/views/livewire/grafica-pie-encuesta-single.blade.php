<div>

    @if (count($respuestas) == 0)
        <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300">Aun no hay respuestas en esta encuesta.</h4>
    @else
        <h4 class="mb-4 text-2xl font-semibold text-gray-800 dark:text-gray-300" id="titulo_encuesta_pie">
            {{ $titulo }}: {{ $nombre }}
        </h4>
        <div>
            <h2 class="text-2xl text-blue-500">Datos Generales</h2>
        </div>

        <div>
            <h2 class="mt-3 text-xl text-blue-500"> Preguntas Realizadas</h2>
            @for ($i = 0; $i < 5; $i++)
                <ul>
                    <li class="mt-2 text-base text-black dark:text-white"> {{ $i + 1 }}.- {{ $preguntas[$i] }}
                    </li>
                </ul>
            @endfor
        </div>

        <h2 class="mt-3 text-xl text-blue-500"> Porcentaje de Respuestas</h2>
        <div class="grid gap-6 mt-4 md:grid-cols-2 xl:grid-cols-5">
            @for ($i = 0; $i < 5; $i++)

                <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                    <div class="p-3 mr-4 text-blue-600 bg-purple-100 rounded-lg dark:text-purple-100 dark:bg-blue-600">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                        </svg>
                    </div>
                    <div>
                        <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                            Pregunta {{ $i + 1 }}

                        </p>
                        <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                            {{ number_format($respuestas[$i], 2) }}%
                        </p>
                    </div>
                </div>
            @endfor
        </div>
        <div>
            <h2 class="mt-3 text-2xl text-blue-500"> Calificación Individual</h2>
        </div>
        <div>
            <h2 class="mt-3 text-xl text-blue-400"> {{ $preguntas[0] }} </h2>
        </div>
        <div class="grid gap-6 mt-4 md:grid-cols-2 xl:grid-cols-5">
            @foreach ($respuestas_preguntas_uno as $respuestas)
                <div class="flex items-center p-3 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                    <div
                        class="p-2 mr-3 text-yellow-300 bg-yellow-100 rounded-lg dark:text-gray-100 dark:bg-yellow-300">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                        </svg>
                    </div>
                    <div>
                        <p class="mb-1 text-sm font-medium text-gray-600 dark:text-gray-400">
                            {{ $loop->iteration }} estrellas
                        </p>
                        <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                            {{ $respuestas }}

                        </p>
                    </div>
                </div>
            @endforeach
        </div>

        <div>
            <h2 class="mt-3 text-xl text-blue-400"> {{ $preguntas[1] }} </h2>
        </div>
        <div class="grid gap-6 mt-4 md:grid-cols-2 xl:grid-cols-5">
            @foreach ($respuestas_preguntas_dos as $respuestas)
                <div class="flex items-center p-3 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                    <div
                        class="p-2 mr-3 text-yellow-300 bg-yellow-100 rounded-lg dark:text-gray-100 dark:bg-yellow-300">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                        </svg>
                    </div>
                    <div>
                        <p class="mb-1 text-sm font-medium text-gray-600 dark:text-gray-400">
                            {{ $loop->iteration }} estrellas
                        </p>
                        <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                            {{ $respuestas }}

                        </p>
                    </div>
                </div>
            @endforeach
        </div>

        <div>
            <h2 class="mt-3 text-xl text-blue-400"> {{ $preguntas[2] }} </h2>
        </div>
        <div class="grid gap-6 mt-4 md:grid-cols-2 xl:grid-cols-5">
            @foreach ($respuestas_preguntas_tres as $respuestas)
                <div class="flex items-center p-3 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                    <div
                        class="p-2 mr-3 text-yellow-300 bg-yellow-100 rounded-lg dark:text-gray-100 dark:bg-yellow-300">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                        </svg>
                    </div>
                    <div>
                        <p class="mb-1 text-sm font-medium text-gray-600 dark:text-gray-400">
                            {{ $loop->iteration }} estrellas
                        </p>
                        <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                            {{ $respuestas }}

                        </p>
                    </div>
                </div>
            @endforeach
        </div>

        <div>
            <h2 class="mt-3 text-xl text-blue-400"> {{ $preguntas[3] }} </h2>
        </div>
        <div class="grid gap-6 mt-4 md:grid-cols-2 xl:grid-cols-5">
            @foreach ($respuestas_preguntas_cuatro as $respuestas)
                <div class="flex items-center p-3 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                    <div
                        class="p-2 mr-3 text-yellow-300 bg-yellow-100 rounded-lg dark:text-gray-100 dark:bg-yellow-300">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                        </svg>
                    </div>
                    <div>
                        <p class="mb-1 text-sm font-medium text-gray-600 dark:text-gray-400">
                            {{ $loop->iteration }} estrellas
                        </p>
                        <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                            {{ $respuestas }}
                        </p>
                    </div>
                </div>
            @endforeach
        </div>

        <div>
            <h2 class="mt-3 text-xl text-blue-400"> {{ $preguntas[4] }} </h2>
        </div>
        <div class="grid gap-6 mt-2 md:grid-cols-2 xl:grid-cols-5">
            @foreach ($respuestas_preguntas_cinco as $respuestas)
                <div class="flex items-center p-3 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                    <div
                        class="p-2 mr-3 text-yellow-300 bg-yellow-100 rounded-lg dark:text-gray-100 dark:bg-yellow-300">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                        </svg>
                    </div>
                    <div>
                        <p class="mb-1 text-sm font-medium text-gray-600 dark:text-gray-400">
                            {{ $loop->iteration }} estrellas
                        </p>
                        <p class="text-sm font-semibold text-gray-700 dark:text-gray-200">
                            {{ $respuestas }}

                        </p>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="mt-4">
            <canvas id="grafica_encuesta" wire:init="loadChart" style="display: block; width: 572px; height: 286px;"
                class="mt-4 chartjs-render-monitor" width="572" height="286"></canvas>
            <div class="flex justify-center mt-2 space-x-3 text-sm text-gray-600 dark:text-gray-400">
                @foreach ($config['etiquetas'] as $etiqueta)
                    <div class="flex items-center">
                        <span class="inline-block w-3 h-3 mr-1 {{ $etiqueta['clase'] }} rounded-full"></span>
                        <span>{{ $etiqueta['texto'] }}</span>
                    </div>
                @endforeach
            </div>
        </div>

        <h2 class="mt-3 text-xl text-blue-500"> Datos de Envio</h2>

        <div class="grid gap-6 mt-4 md:grid-cols-3 xl:grid-cols-3">
            <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="p-3 mr-4 text-orange-600 bg-orange-100 rounded-lg dark:text-purple-100 dark:bg-orange-600">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                    </svg>
                </div>
                <div>
                    <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                        Encuestas Enviadas

                    </p>
                    <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                        {{ $total_enviados }}

                    </p>
                </div>
            </div>
            <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="p-3 mr-4 text-green-600 bg-green-100 rounded-lg dark:text-purple-100 dark:bg-green-600">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                        Encuestas Contestadas
                    </p>
                    <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                        {{ $contestado }}

                    </p>
                </div>
            </div>

            <div class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
                <div class="p-3 mr-4 text-red-600 bg-red-100 rounded-lg dark:text-purple-100 dark:bg-red-600">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                        Encuestas No Contestadas
                    </p>
                    <p class="text-lg font-semibold text-gray-700 dark:text-gray-200">
                        {{ $total_enviados - $contestado }}

                    </p>
                </div>
            </div>
        </div>



    @endif
</div>
