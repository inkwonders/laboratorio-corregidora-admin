<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none"
        style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="absolute w-6/12 print:w-full print:h-full">

            <div
                class="relative p-10 m-4 mb-6 bg-white rounded print:p-0 print:bg-transparent print:m-0 print:mb-0 dark:bg-gray-700">

                <div class="absolute cursor-pointer right-2 top-2 hover:text-red-600 print:hidden dark:text-white"
                    wire:click="$emit('cerrarModal')">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div class="flex flex-col w-full mt-1 print:flex print:flex-wrap">
                    <div>
                        <h2 class="text-xl dark:text-blue-400">Correos Enviados </h2>
                    </div>
                    <div>
                        <input
                            class="w-full p-2 m-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                            placeholder="Buscador..." type="text" wire:model="correos_buscador">
                    </div>
                    <ul class="w-full p-2 m-2 overflow-y-auto border border-gray-200 dark:border-gray-50 max-h-80">
                        @if (count($correos) < 1)
                        <li class="pl-2 mt-2 text-xl text-center text-white bg-blue-500 rounded-md">No se encontraron resultados para su búsqueda. </li>
                        @endif
                        @foreach ($correos as $correo)
                            <li class="pl-2 mt-2 text-xl text-white bg-blue-500 rounded-md">{{ $correo->paciente_email }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>

    </div>

</div>
