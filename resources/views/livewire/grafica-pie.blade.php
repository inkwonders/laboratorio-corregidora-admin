 <div>

    <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">

        <div class="chartjs-size-monitor">
            <div class="chartjs-size-monitor-expand">
                <div class=""></div>
            </div>
            <div class="chartjs-size-monitor-shrink">
                <div class=""></div>
            </div>
        </div>

        <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300" >
            {{ $titulo }}
        </h4>

        <canvas id="{{ $id_grafica }}" style="display: block; width: 572px; height: 286px;" class="chartjs-render-monitor" width="572" height="286"></canvas>

        <div class="flex justify-center mt-4 space-x-3 text-sm text-gray-600 dark:text-gray-400">

            <!-- Chart legend -->
            @foreach ($etiquetas as $etiqueta)
                <div class="flex items-center">
                    <span class="inline-block w-3 h-3 mr-1 {{ $etiqueta['clase'] }} rounded-full"></span>
                    <span>{{ $etiqueta['texto'] }}</span>
                </div>
            @endforeach
        </div>

    </div>

    @push('scripts')

        <script>

            Livewire.hook('component.initialized', (component) => {
                Livewire.emit('loadChart');
            });

            Livewire.on('drawChart', function(data) {
                // console.log(data)
                if(data['id'] == '{{ $id_grafica }}')
                {
                    /**
                    * For usage, visit Chart.js docs https://www.chartjs.org/docs/latest/
                    */
                    const pieConfig = {
                        type: 'doughnut',
                        data: {
                            datasets: data.dataset,
                            labels: {!! $etiquetas->pluck('texto') !!},
                        },
                        options: {
                            responsive: true,
                            cutoutPercentage: 80,
                            /**
                            * Default legends are ugly and impossible to style.
                            * See examples in charts.html to add your own legends
                            *  */
                            legend: {
                                display: false,
                            },
                        },
                    }

                    // change this to the id of your chart element in HMTL
                    const pieCtx = document.getElementById('{{ $id_grafica }}')
                    window.myPie = new Chart(pieCtx, pieConfig)
                }
            });

        </script>

    @endpush

</div>
