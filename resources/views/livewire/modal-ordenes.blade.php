<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none "
        style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="absolute w-8/12 print:w-full print:h-full">

            <div class="relative max-w-full p-10 m-4 bg-white rounded print:p-0 print:m-0 dark:bg-gray-700">

                <div class="absolute cursor-pointer right-2 top-2 hover:text-red-600 print:hidden dark:text-white" wire:click="$emit('cerrarModal')">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div class="flex flex-col justify-between mt-2">
                    <p class="text-lg font-bold text-gray-800 dark:text-white">Datos de la orden</p>
                    <p class="block pt-10 text-lg font-bold text-gray-00 dark:text-white">
                        No. Confirmación:
                        <span
                            class="px-2 py-1 text-lg font-bold text-blue-500 bg-gray-200 rounded dark:bg-white">{{ $orden->no_confirmacion }}</span><br><span
                            class="block text-sm text-right">
                            Fecha de compra: {{ date('d M Y h:i A', strtotime($orden->updated_at)) }}
                        </span>
                        <span class="float-right px-2 py-1 text-lg font-bold text-green-500 bg-gray-200 rounded">
                            Total ${{ number_format($orden->total, 2) }}
                        </span>
                    </p>
                </div>

                <div class="flex w-full mt-2">


                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">Nombre Paciente</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded" disabled type="text"
                            value="{{ $orden->nombre_paciente }}">
                    </div>

                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">Teléfono</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded" disabled type="text"
                            value="{{ $orden->tel_paciente }}">
                    </div>

                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">EMAIL PACIENTE</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded" disabled type="text"
                            value="{{ $orden->email_paciente }}">
                    </div>

                </div>

                <br>

                <div class="mt-2">
                    <label class="block text-sm text-gray-600 dark:text-white">ESTUDIOS</label>

                </div>
                <div class="h-auto overflow-y-auto">
                    @foreach ($orden->analisis as $analisis)

                        <hr>
                        <div class="flex w-full mt-2">
                            <div class="w-1/3">
                                <label class="block text-sm text-gray-600 dark:text-white">Clave: <span
                                        class="text-lg font-bold text-blue-500">{{ $analisis->clave }}</span> -
                                    {{ $analisis->nombre }}</label>
                            </div>
                            <div class="flex items-center justify-center w-1/3 pl-1 -mx-1">
                                <label class="block text-sm text-gray-600 dark:text-white">Precio: <span
                                        class="font-bold">${{ number_format($analisis->precio, 2) }}</span></label>
                            </div>
                            <div class="flex items-center justify-center w-1/3 pl-1 -mx-1">
                                <label class="block text-sm text-right text-gray-600 dark:text-white">Cantidad:
                                    {{ $analisis->pivot->cantidad }}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!--footer-->
                <div class="flex justify-center p-3 mt-2 space-x-4 text-center print:hidden">
                    <button type="button" onclick="window.print()"
                        class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-400 rounded-lg cursor-pointer focus:outline-none focus:ring focus:border-transparent text-1xl md:text-2xl hover:bg-blue-500">
                        Imprimir Orden
                    </button>
                    <button wire:click="$emit('cerrarModal')"
                        class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer focus:outline-none focus:ring focus:border-transparent text-1xl md:text-2xl hover:bg-gray-500">
                        Cerrar
                    </button>
                </div>

            </div>

        </div>

    </div>

</div>
