<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-auto outline-none animated fadeIn faster focus:outline-none " style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="relative w-auto print:w-full print:h-full" >

            <div class="relative w-full max-h-screen p-10 m-4 mx-auto mb-10 overflow-y-auto bg-white rounded print:p-0 print:bg-transparent print:m-0 print:mb-0 dark:bg-gray-700">

                <div class="absolute cursor-pointer dark:text-white right-4 top-4 hover:text-red-600 print:hidden"
                    wire:click="$emit('cerrarModal')">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div class="flex w-full mt-1 print:flex print:flex-wrap">

                    @livewire('grafica-pie-encuesta-single', ['encuesta_id' => $encuesta], key(uniqid()) )

                </div>

            </div>
        </div>

        <div class="h-10"></div>
    </div>

</div>
