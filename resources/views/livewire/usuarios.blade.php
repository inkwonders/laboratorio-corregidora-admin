<div>

    <div class="grid px-6 mx-auto">
        <h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
            Usuarios
        </h2>

        <div class="flex items-center justify-end w-full">
            <button wire:click="abrirModal"
                class="flex w-auto px-4 py-4 mr-2 font-bold text-center text-white bg-blue-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-blue-500">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6 pr-1" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M18 9v3m0 0v3m0-3h3m-3 0h-3m-2-5a4 4 0 11-8 0 4 4 0 018 0zM3 20a6 6 0 0112 0v1H3v-1z" />
                </svg>
                REGISTRAR USUARIO
            </button>
            <a href="{{ route('export_usuarios') }}"
                class="flex w-auto px-4 py-4 font-bold text-center text-white bg-green-400 rounded-lg justify-evenly text-1xl md:text-1xl hover:bg-green-500 focus:outline-none focus:ring focus:border-transparent">
                Exportar
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                </svg>
            </a>
        </div>

        <div class="w-full pt-4 overflow-hidden rounded-lg">

            <livewire:usuarios-table />

        </div>

    </div>

    @if ($modal)
        <livewire:modal-usuario />
    @endif

    @if ($eliminar)

        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none min-w-screen animated fadeIn faster focus:outline-none"
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="leading-loose">
                <form class="max-w-xl p-10 m-4 bg-white rounded shadow-xl dark:bg-gray-700">

                    <div class="flex justify-between mt-2">
                        <p class="text-2xl font-medium font-bold text-gray-800 dark:text-white">¿Desea eliminar el
                            usuario?</p>
                    </div>

                    <!--footer-->
                    <div class="p-3 mt-2 space-x-4 text-center md:flex">

                        <button wire:click="eliminarUsuario"
                            class="w-full p-2 font-bold text-center text-white bg-blue-500 rounded-lg text-1xl md:text-2xl hover:bg-blue-300">
                            Si
                        </button>
                        <button wire:click="cerrarModal"
                            class="w-full p-2 font-bold text-center text-white bg-red-500 rounded-lg text-1xl md:text-2xl hover:bg-red-300">
                            No
                        </button>
                    </div>

                </form>

            </div>

        </div>

    @endif

</div>
