<div>

    @if ($agregar)
        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none "
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="absolute w-11/12 ">
                <form class="relative p-10 m-8 mb-6 bg-white rounded shadow-xl dark:bg-gray-700">
                    <div class="absolute cursor-pointer right-6 top-4 hover:text-red-600 dark:text-white"
                        wire:click="$emit('cerrarModal')">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </div>
                    <div class="flex justify-between mt-2">
                        <p class="mb-4 text-2xl font-bold text-center dark:text-white"> Registrar Análisis
                        </p>
                    </div>
                    <div class="flex w-full mt-2">

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Clave</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="number" value="" wire:model="clave">
                            @error('clave')
                                <p class="mb-2 text-red-600 error_clave">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Precio</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="number" value="" wire:model="precio">
                            @error('precio')
                                <p class="mb-2 text-red-600 error_precio">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Nombre</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="nombre">
                            @error('nombre')
                                <p class="mb-2 text-red-600 error_nombre">{{ $message }}</p>
                            @enderror
                        </div>

                    </div>

                    <div class="flex w-full mt-2">

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Ayuno</label>
                            <select name="" id="{{ $identificador }}"
                                class="block w-full p-2 border rounded-lg cursor-pointer h-13 bg-grey-lighter text-grey-darker border-grey-lighter md:w-full"
                                wire:model="ayuno">
                                <option selected>SELECCIONAR AYUNO</option>
                                <option value="4 horas">4 horas</option>
                                <option value="8 horas">8 horas</option>
                                <option value="De 8 a 10 horas">De 8 a 10 horas</option>
                                <option value="De 8 a 12 horas">De 8 a 12 horas</option>
                                <option value="8 a 12 *para pediatrico antes de su siguiente alimento.">8 a 12 *para
                                    pediatrico
                                    antes de su siguiente alimento.</option>
                                <option value="De 9 a 12 horas">De 9 a 12 horas</option>
                                <option value="4 días">4 días</option>
                                <option value="Favor de llamar para solicitar esta información">Favor de llamar para
                                    solicitar
                                    esta información</option>
                                <option value="NO SE REQUIERE">NO SE REQUIERE</option>
                                <option value="NO APLICA">NO APLICA</option>
                            </select>
                            @error('ayuno')
                                <p class="mb-2 text-red-600 error_ayuno">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Categorias</label>
                            <select name="" id="{{ $identificador }}"
                                class="block w-full p-2 border rounded-lg cursor-pointer h-13 bg-grey-lighter text-grey-darker border-grey-lighter md:w-full"
                                wire:model="categoria">
                                <option value="" selected>SELECCIONAR CATEGORIA</option>
                                @foreach ($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                @endforeach

                            </select>
                            @error('categoria')
                                <p class="mb-2 text-red-600 error_categorias">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Entrega</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="entrega">
                            @error('entrega')
                                <p class="mb-2 text-red-600 error_entrega">{{ $message }}</p>
                            @enderror
                        </div>

                    </div>

                    <div class="flex w-full mt-2">

                        <div class="w-1/2 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Estudios</label>
                            <textarea
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="estudios">
                        </textarea>
                            @error('estudios')
                                <p class="mb-2 text-red-600 error_estudios">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/2 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Indicaciones</label>
                            <textarea
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="indicaciones">
                            </textarea>
                            @error('indicaciones')
                                <p class="mb-2 text-red-600 error_indicaciones">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>



                    <br>
                    <span class="text-center text-gray-400 dark:text-white"> Todos los campos son obligatorios </span>

                    <!--footer-->
                    <div class="flex items-center justify-center p-3 mt-2 space-x-4 text-center">
                        <div class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500"
                            wire:click="$emit('cerrarModal')">
                            Cerrar
                        </div>
                        <div wire:click="create"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600">
                            Agregar
                        </div>
                    </div>

                </form>

            </div>

        </div>
    @endif

    @if ($eliminar)
        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none min-w-screen animated fadeIn faster focus:outline-none"
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="leading-loose">
                <div class="max-w-xl p-10 m-4 bg-white rounded shadow-xl dark:bg-gray-700">
                    <div class="flex justify-between mt-2">
                        <p class="text-2xl font-medium font-bold text-gray-800 dark:text-white">¿Desea eliminar el análisis?</p>
                    </div>

                    <!--footer-->
                    <div class="p-3 mt-2 space-x-4 text-center md:flex">

                        <button wire:click="delete"
                            class="w-full p-2 font-bold text-center text-white bg-blue-500 rounded-lg text-1xl md:text-2xl hover:bg-blue-300 focus:outline-none focus:ring focus:border-blue-300">
                            Si
                        </button>
                        <button wire:click="$emit('cerrarModal')"
                            class="w-full p-2 font-bold text-center text-white bg-red-500 rounded-lg text-1xl md:text-2xl hover:bg-red-300 focus:outline-none focus:ring focus:border-red-300">
                            No
                        </button>
                    </div>

                </div>

            </div>

        </div>
    @endif

    @if ($editar)
        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none "
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="absolute w-11/12 ">
                <form class="relative p-10 m-8 mb-6 bg-white rounded shadow-xl dark:bg-gray-700 ">
                    <div class="absolute cursor-pointer right-6 top-4 hover:text-red-600 dark:text-white"
                        wire:click="$emit('cerrarModal')">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </div>
                    <div class="flex justify-between mt-2">
                        <p class="mb-4 text-2xl font-bold text-center dark:text-white"> Editar Análisis </p>
                    </div>

                    <div class="flex w-full mt-2">

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Clave</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="number" value="" wire:model="clave">
                            @error('clave')
                                <p class="mb-2 text-red-600 error_clave">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Precio</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="number" value="" wire:model="precio">
                            @error('precio')
                                <p class="mb-2 text-red-600 error_precio">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Nombre</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="nombre">
                            @error('nombre')
                                <p class="mb-2 text-red-600 error_nombre">{{ $message }}</p>
                            @enderror
                        </div>

                    </div>

                    <div class="flex w-full mt-2">

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Ayuno</label>
                            <select name="" id="{{ $identificador }}"
                                class="block w-full p-2 border rounded-lg cursor-pointer h-13 bg-grey-lighter text-grey-darker border-grey-lighter md:w-full"
                                wire:model="ayuno">
                                <option selected>SELECCIONAR AYUNO</option>
                                <option value="4 horas">4 horas</option>
                                <option value="8 horas">8 horas</option>
                                <option value="De 8 a 10 horas">De 8 a 10 horas</option>
                                <option value="De 8 a 12 horas">De 8 a 12 horas</option>
                                <option value="8 a 12 *para pediatrico antes de su siguiente alimento.">8 a 12 *para
                                    pediatrico
                                    antes de su siguiente alimento.</option>
                                <option value="De 9 a 12 horas">De 9 a 12 horas</option>
                                <option value="4 días">4 días</option>
                                <option value="Favor de llamar para solicitar esta información">Favor de llamar para
                                    solicitar
                                    esta información</option>
                                <option value="NO SE REQUIERE">NO SE REQUIERE</option>
                                <option value="NO APLICA">NO APLICA</option>
                            </select>
                            @error('ayuno')
                                <p class="mb-2 text-red-600 error_ayuno">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Categorias</label>
                            <select name="categoria"
                                class="block w-full p-2 border rounded-lg cursor-pointer h-13 bg-grey-lighter text-grey-darker border-grey-lighter md:w-full"
                                wire:model="categoria">
                                <option value="0">SELECCIONAR CATEGORIA</option>

                                @foreach ($categorias as $categoria)
                                    <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
                                @endforeach

                            </select>
                            @error('categoria')
                                <p class="mb-2 text-red-600 error_categorias">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/3 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Entrega</label>
                            <input
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="entrega">
                            @error('entrega')
                                <p class="mb-2 text-red-600 error_entrega">{{ $message }}</p>
                            @enderror
                        </div>

                    </div>

                    <div class="flex w-full mt-2">


                        <div class="w-1/2 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Estudios</label>
                            <textarea
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="estudios">
                        </textarea>
                            @error('estudios')
                                <p class="mb-2 text-red-600 error_estudios">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="w-1/2 p-2">
                            <label class="block text-xl text-gray-600 dark:text-white">Indicaciones</label>
                            <textarea
                                class="w-full p-2 border rounded-lg border-grey-200 focus:outline-none focus:ring focus:border-blue-400"
                                type="text" value="" wire:model="indicaciones">
                        </textarea>
                            @error('indicaciones')
                                <p class="mb-2 text-red-600 error_indicaciones">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <br>
                    <span class="text-center text-gray-400 dark:text-white"> Todos los campos son obligatorios </span>

                    <!--footer-->
                    <div class="flex items-center justify-center p-3 mt-2 space-x-4 text-center">
                        <div class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500"
                            wire:click="$emit('cerrarModal')">
                            Cerrar
                        </div>
                        <div wire:click="update"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600">
                            Editar
                        </div>
                    </div>

                </form>

            </div>

        </div>
    @endif

</div>
