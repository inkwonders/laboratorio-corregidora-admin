<div>

    <div class="clear-both w-full h-full p-12">

        <div class="flex flex-col w-full md:flex-row">

            <div class="flex w-full md:w-6/12">

                <div class="flex items-center justify-center text-xl font-bold md:justify-start azul_textos dark:text-white dark:hidden">
                    <img src="img/lista.svg" class="h-16 m-6 "> ANALISÍS
                </div>
                <div class="items-center justify-center hidden text-xl font-bold md:justify-start azul_textos dark:text-white dark:flex">
                    <img src="img/lista_blanco.svg" class="h-16 m-6 "> ANALISÍS
                </div>

            </div>

            <div class="w-full md:w-6/12 md:flex md:items-center md:justify-end ">
                <button wire:click="$emit('vaciarCarrito')" class="w-full h-12 p-3 px-5 m-1 font-bold text-center text-white bg-blue-600 rounded-lg md:w-6/12 hover:bg-blue-500 focus:outline-none focus:ring focus:border-transparent">VACIAR LISTA</button>
            </div>

        </div>

        <div class="justify-between hidden w-full p-2 mb-4 font-bold border-b-2 border-blue-600 md:flex dark:text-gray-200">
            <div class="w-1/12 ml-6 lg:text-sm azul_textos dark:text-gray-200">
                CANTIDAD
            </div>
            <div class="w-9/12 ml-4 lg:text-sm azul_textos dark:text-gray-200">NOMBRE</div>
            <div class="w-1/12 lg:text-sm azul_textos dark:text-gray-200">PRECIO</div>
            <div class="w-1/12 lg:text-sm azul_textos dark:text-gray-200">ELIMINAR</div>
        </div>

        <div class="flex flex-col space-y-4 contenedor_carrito">

                @if($carrito->analisis->isEmpty())

                    <div class="flex flex-col items-center w-full p-3 mt-4 bg-white rounded-lg md:flex-row">
                        <p>Tu carrito esta vacio.</p>
                    </div>

                @else

                    @foreach ($carrito->analisis as $analisis)

                        <div class="flex flex-col items-center w-full p-3 mt-4 bg-white rounded-lg md:flex-row">

                            <div class="flex hidden mb-4">
                                <h2 class="text-xl font-bold azul_textos">Cantidad</h2>
                            </div>
                            <div class="flex justify-center w-full md:w-1/12">
                                <div class="text-white ml-8 {{ $analisis->pivot->cantidad < 2?'bg-gray-600 pointer-events-none cursor-not-allowed':'cursor-pointer bg-blue-600' }} rounded h-5 w-5" wire:click="$emit('decrementar', {{ $analisis->id }}, {{ $analisis->pivot->cantidad }})">
                                    <svg xmlns="http://www.w3.org/2000/svg"  fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 12H4" /></svg>
                                </div>

                                <span class="ml-4 mr-4 font-bold 2xl:text-xl azul_textos lg:text-sm">{{ $analisis->pivot->cantidad }}</span>

                                <div class="text-white cursor-pointer" wire:click="$emit('incrementar', {{ $analisis->id }}, {{ $analisis->pivot->cantidad }})">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 bg-blue-600 rounded" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" /></svg>
                                </div>
                            </div>


                            <div class="flex mt-4 mb-4 md:hidden">
                                <h2 class="text-xl font-bold azul_textos">Nombre</h2>
                            </div>

                            <div class="w-full ml-10 text-center md:text-left md:w-9/12 azul_textos lg:text-sm 2xl:text-2xl">{{ $analisis->nombre }}</div>

                            <div class="flex mt-4 mb-4 md:hidden">
                                <h2 class="text-xl font-bold azul_textos">Precio</h2>
                            </div>

                            <div class="w-full font-semibold text-center text-green-500 2xl:text-2xl md:text-left md:w-1/12 lg:text-sm">${{ number_format($analisis->precio, 2) }}</div>
                            <div class="flex justify-center w-full text-center cursor-pointer md:w-1/12" wire:click="$emit('eliminarAnalisis', {{ $analisis->id }})"><img src="img/papelera_roja.svg" class="h-7 m-7"></div>
                        </div>
                    @endforeach

                @endif

            @if(!$carrito->total == 0)

                <div class="relative flex w-full pb-4 md:justify-end">
                    <div class="flex justify-center w-full p-6 text-2xl font-bold bg-white rounded-lg md:w-3/12">
                        <span class="mr-2 azul_textos">TOTAL</span>
                        <span class="text-green-500">${{ number_format($carrito->total,2) }}</span>
                    </div>
                </div>

            @endif

        </div>

        <div class="flex justify-center w-full">
            <div class="flex flex-col justify-center w-full md:flex-row">

                <div class="flex mb-2 xs:w-full md:m-2 md:w-2/12">
                    <a href="{{ route("analisis.panel") }}" class="w-full p-2 font-bold text-center text-white bg-gray-400 rounded-lg text-1xl md:text-2xl hover:bg-gray-500">
                    REGRESAR
                    </a>
                </div>

                @if(!empty($analisis))

                    <div class="flex mb-2 xs:w-full md:m-2 md:w-2/12">
                        <a href="{{ route("carrito.paciente") }}" class="flex items-center justify-center w-full p-2 font-bold text-white bg-blue-500 rounded-lg text-1xl md:text-2xl hover:bg-blue-600">
                        SIGUIENTE
                        </a>
                    </div>

                @endif

            </div>
        </div>

    </div>

</div>
