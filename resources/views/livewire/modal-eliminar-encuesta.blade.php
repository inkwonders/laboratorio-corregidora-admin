<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none min-w-screen animated fadeIn faster focus:outline-none"
        style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="leading-loose">
            <div class="max-w-xl p-10 m-4 bg-white rounded shadow-xl dark:bg-gray-700">

                <div class="flex justify-between mt-2">
                    <p class="text-2xl font-bold text-gray-800 dark:text-white">¿Desea eliminar esta encuesta?</p>
                </div>

                <!--footer-->
                <div class="p-3 mt-2 space-x-4 text-center md:flex">
                    <button wire:click="$emit('siEliminar', {{ $encuesta }})"
                        class="w-full p-2 font-bold text-center text-white bg-blue-500 rounded-lg text-1xl md:text-2xl hover:bg-blue-300 focus:outline-none focus:ring focus:border-transparent">
                        Si
                    </button>
                    <button wire:click="$emit('noEliminar')"
                        class="w-full p-2 font-bold text-center text-white bg-red-500 rounded-lg text-1xl md:text-2xl hover:bg-red-300 focus:outline-none focus:ring focus:border-transparent">
                        No
                    </button>
                </div>

            </div>

        </div>

    </div>

</div>
