<div>

    {{-- @if ($agregar_masiva) --}}
        <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none "
            style="background: rgba(0,0,0,.2);" id="modal-id">

            <div class="absolute w-5/12 text-center items-center">
                <form method="POST"  enctype="multipart/form-data" class="relative p-10 m-8 mb-6 bg-white rounded shadow-xl dark:bg-gray-700 text-center items-center">
                @csrf
                    <div class="absolute cursor-pointer right-6 top-4 hover:text-red-600 dark:text-white"
                        wire:click="$emit('cerrarModal')">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </div>
                    <div class=" justify-between mt-2">
                        <p class="mb-4 text-2xl font-bold text-center dark:text-white"> Carga masiva de listado de correos para encuesta
                        </p>

                    </div>
                    <div class="flex w-full mt-2">

                        <div class="w-full items-center text-center">
                            <a href="{{ route('export_correos_encuesta') }}"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-green-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-green-500">
                            Descargar listado actual
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                                stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                            </svg>
                        </a>
                        <br><br>
                            <label class="block text-xl text-gray-600 dark:text-white">*Puedes descargar aquí el listado actual de correos que te servirá de plantilla y modificar o añadir sobre ésta los que se requieran para posteriormente subirlos en la siguiente línea.
                            </label>
                            <div class="w-full items-center text-center">

                                </div>
                                <br><br>
                                <div x-data="{ isUploading: false, progress: 0 }"
                                x-on:livewire-upload-start="isUploading = true"
                                x-on:livewire-upload-finish="isUploading = false"
                                x-on:livewire-upload-error="isUploading = false"
                                x-on:livewire-upload-progress="progress = $event.detail.progress">
                                <span class="text-xl mt-2 text-base leading-normal dark:text-white">Seleccionar excel</span><br>
                                <input type="file" wire:model="archivo" class="dark:text-white" >
                                <div class="mt-2" x-show="isUploading">
                                    <progress max="100" x-bind:value="progress"></progress>
                                </div>
                            </div>
                            @error('archivo')
                                <p class="mb-2 text-red-600 error_clave">{{ $message }}</p>
                            @enderror
                        </div>

                    </div>

                    <br>
                    <span class="text-center text-gray-400 dark:text-white"> *Es obligatorio subir la plantialla sin modificar orden de columnas solo el contenido de las mismas. </span>
                    {{-- {{ $archivo }} --}}
                    <!--footer-->
                    <div class="flex items-center justify-center p-3 mt-2 space-x-4 text-center">
                        <div class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500"
                            wire:click="$emit('cerrarModal')">
                            Cerrar
                        </div>
                        @if ($archivo != null)
                        <div wire:click="create"  wire:loading.class="hidden disabled"
                            class="flex items-center justify-center p-2 px-12 mt-6 font-bold text-center text-white bg-blue-500 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-600" type="submit">
                            Agregar
                        </div>
                        @endif

                    </div>

                </form>

            </div>

        </div>
    {{-- @endif --}}


</div>
