<div>

    <div class="fixed inset-0 top-0 left-0 z-50 flex items-center justify-center h-screen overflow-y-auto outline-none animated fadeIn faster focus:outline-none "
        style="background: rgba(0,0,0,.2);" id="modal-id">

        <div class="absolute w-8/12 print:w-full print:h-full">


            <div class="relative w-full p-10 mb-6 bg-white rounded print:p-0 print:mb-0 dark:bg-gray-700">

                <div class="absolute cursor-pointer right-6 top-4 hover:text-red-600 print:hidden dark:text-white" wire:click="$emit('cerrarModal')">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>

                <div class="flex justify-between mt-2 print:hidden">
                    <p class="mb-4 text-2xl font-bold text-center dark:text-white">
                        CENTRO DE PAGOS ONLINE
                    </p>
                </div>

                <div class="flex justify-between mt-2 print:mt-0">
                    <p class="text-lg font-bold text-gray-800 dark:text-white">Datos del paciente</p>
                    <p class="block text-lg font-bold text-gray-00 dark:text-white">No. Confirmación: <span class="px-2 py-1 text-lg font-bold text-blue-500 bg-gray-200 rounded">{{ $reference }}</span><br>
                        @if ( $tipo_pago == 1)
                            <span
                            class="block text-sm text-right dark:text-white">Número de pacientes:
                            {{ $numero }}</span>
                        @endif
                    </p>
                </div>

                <div class="flex">
                    {{-- <div class="w-3/12 p-2">
                        <label class="block text-sm text-gray-600">reference</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded" disabled="" type="text"
                            value="{{ $reference }}">
                    </div> --}}
                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">response</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                            value="{{ $response }}">
                    </div>
                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">time</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                            value="{{ $time }}">
                    </div>
                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">date</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                            value="{{ $date }}">
                    </div>
                </div>

                <div class="flex">
                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">foliocpagos</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                            value="{{ $foliocpagos }}">
                    </div>
                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">auth</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                            value="{{ $auth }}">
                    </div>

                    <div class="w-1/3 p-2">
                        <label class="block text-sm text-gray-600 dark:text-white">amount</label>
                        <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                            value="${{ number_format((int) $amount, 2) }}">
                    </div>
                </div>



                @if ($tipo_pago == 1)

                    <div class="flex w-full">
                        <div class="w-1/3 p-2">
                            <label class="block text-sm text-gray-600 dark:text-white">Email</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                                value="{{ $email }}">
                        </div>

                        <div class="w-1/3 p-2 ">
                            <label class="block text-sm text-gray-600 dark:text-white">Número de confirmación</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                                value="{{ $numero_confirmacion_mostrar }}">
                        </div>
                        <div class="w-1/3 p-2 ">
                            <label class="block text-sm text-gray-600 dark:text-white">Horario</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                                value="{{ $horario }}">
                        </div>
                    </div>

                    <div class="flex">
                        <div class="w-full p-2">
                            <label class="block text-sm text-gray-600 dark:text-white">Nombre de prueba</label>
                            <p class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2"> {{ $concepto }}</p>
                        </div>
                        {{-- <div class="w-3/12 p-2 ">
                            <label class="block text-sm text-gray-600">Numero de pacientes</label>
                            <p class="w-full px-2 py-1 mt-2 text-gray-700 bg-gray-200 rounded">{{ $numero }}</p>
                        </div> --}}
                    </div>


                @endif

                @if ($tipo_pago == 2)

                    <div class="flex ">

                        <div class="flex w-1/3">
                            <div class="w-full p-2">
                                <label class="block text-sm text-gray-600 dark:text-white">Email</label>
                                <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled=""
                                    type="text" value="{{ $email }}">
                            </div>
                        </div>

                        <div class="flex w-2/3">
                            <div class="w-6/12 p-2">
                                <label class="block text-sm text-gray-600 dark:text-white">Número de confirmación</label>
                                <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled=""
                                    type="text" value="{{ $numero_confirmacion_mostrar }}">
                            </div>
                            <div class="w-6/12 p-2">
                                <label class="block text-sm text-gray-600 dark:text-white">Teléfono</label>
                                <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled=""
                                    type="text" value="{{ $telefono }}">
                            </div>
                        </div>

                    </div>

                    <div class="flex w-full">
                        <div class="flex w-1/2 ">
                            <div class="w-full p-2">
                                <label class="block text-sm text-gray-600 dark:text-white">Nombre</label>
                                <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled=""
                                    type="text" value="{{ $nombre }}">
                            </div>
                        </div>
                        <div class="w-1/2 p-2">
                            <label class="block text-sm text-gray-600 dark:text-white">cc_type</label>
                            <input class="w-full px-2 py-1 text-gray-700 bg-gray-200 rounded print:border-2" disabled="" type="text"
                                value="{{ $cc_type }}">
                        </div>
                    </div>

                    <div class="flex w-full p-2">
                        <div class="w-full h-auto p-2 overflow-y-auto print:border-2">
                            <label class="block text-sm text-gray-600 dark:text-white">Estudios</label>
                            @for ($i = 0; $i < $cantidad; $i++)
                                <p class="w-full px-2 py-1 mt-2 text-gray-700 bg-gray-200 rounded">
                                    {{ $estudios_cantidad[$i] }}</p>
                            @endfor
                        </div>
                    </div>

                @endif


                <div class="flex items-center justify-center pt-5 text-center print:hidden">

                    <div onclick="window.print()"
                        class="flex items-center justify-center p-2 px-12 font-bold text-center text-white bg-blue-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-blue-500">
                        Imprimir Pago
                    </div>
                    <div class="flex items-center justify-center p-2 px-12 ml-2 font-bold text-center text-white bg-gray-400 rounded-lg cursor-pointer text-1xl md:text-2xl hover:bg-gray-500"
                        wire:click="$emit('cerrarModal')">
                        Cerrar
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
