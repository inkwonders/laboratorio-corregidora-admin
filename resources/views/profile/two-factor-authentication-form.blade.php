<x-action-section>
    <x-slot name="content">
        <h3 class="text-lg font-medium text-gray-900 dark:text-gray-300">
            @if ($this->enabled)
            {{ __('perfil.perfil.perfil-2fa-enabled') }}
            @else
            {{ __('perfil.perfil.perfil-2fa-notenabled') }}
            @endif
        </h3>

        <div class="max-w-xl mt-3 text-sm text-gray-600 dark:text-gray-400">
            <p>
                {{ __('perfil.perfil.perfil-2fa-notenabled-text') }}

            </p>
        </div>

        @if ($this->enabled)
        @if ($showingQrCode)
        <div class="max-w-xl mt-4 text-sm text-gray-600 dark:text-gray-400">
            <p class="font-semibold">
                {{ __('perfil.perfil.perfil-2fa-enabled-text') }}
            </p>
        </div>

        <div class="mt-4">
            {!! $this->user->twoFactorQrCodeSvg() !!}
        </div>
        @endif

        @if ($showingRecoveryCodes)
        <div class="max-w-xl mt-4 text-sm text-gray-600 dark:text-gray-400">
            <p class="font-semibold">
                {{ __('Store these recovery codes in a secure password manager. They can be used to recover access to your account if your two factor authentication device is lost.') }}
            </p>
        </div>

        <div class="grid max-w-xl gap-1 px-4 py-4 mt-4 font-mono text-sm bg-gray-100 rounded-lg">
            @foreach (json_decode(decrypt($this->user->two_factor_recovery_codes), true) as $code)
            <div>{{ $code }}</div>
            @endforeach
        </div>
        @endif
        @endif

        <div class="mt-5">
            @if (! $this->enabled)
            <x-button type="button" wire:click="enableTwoFactorAuthentication" wire:loading.attr="disabled">
                {{ __('perfil.perfil.perfil-2fa-boton-enabled') }}
            </x-button>
            @else
            @if ($showingRecoveryCodes)
            <x-secondary-button class="mr-3" wire:click="regenerateRecoveryCodes">
                {{ __('perfil.perfil.perfil-2fa-boton-notenabled') }}
            </x-secondary-button>
            @else
            <x-secondary-button class="mr-3" wire:click="$toggle('showingRecoveryCodes')">
                {{ __('Show Recovery Codes') }}
            </x-secondary-button>
            @endif

            <x-danger-button wire:click="disableTwoFactorAuthentication" wire:loading.attr="disabled">
                {{ __('Disable') }}
            </x-danger-button>
            @endif
        </div>
    </x-slot>
</x-action-section>