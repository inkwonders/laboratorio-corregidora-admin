<html>

<head>
    <meta charset="utf-8">
    <title>Se registro una cita para Imagenología</title>
    <style media="screen">
        .contenedor * {
            box-sizing: border-box;
            font-family: arial;
        }

        .contenedor {
            box-sizing: border-box;
            width: 600px;
            max-width: 100%;
            background-color: #e6e6e6;
            margin-top: 50px;
            padding: 17px;
        }


        .mensaje {
            width: 100%;
            background-color: #ffffff;
            display: inline-flex;
            flex-direction: column;
            align-items: center;
            text-align: center;
            font-size: 18px;
            margin: 0;
            padding: 5%;
            color: #194271;
        }

        .logo_head {
            width: 60%;
            height: auto;
            display:block;
            margin:auto;
            padding-top: 20px;
        }

        .manifiesto_txt {
            margin-top: 80px;
        }

        .texto_confirmacion {
            font-size: 20px;
        }

        .texto_confirmacion,
        .texto_numero {
            margin-top: 40px;
        }

        .link_aviso {
            text-decoration: none;
            margin-top: 30px;
            font-size: 16px;
            color: #194271;
        }

        .numero_confirmacion {
            color: #007af6;
            font-weight: bold;
            font-size: 20px;
        }

        .manifiesto_txt {
            color: #848484;
            font-size: 13px;
        }

        .numero_tel {
            font-weight: bold;
        }

        .numero_tel a {
            text-decoration: none;
            color: #194271;
        }

        span {
            overflow-wrap: anywhere;
        }

        .border {
            border: solid 1px;
            text-align: left;
            padding: 10px;
        }

        .header_correo{
            background: #194271;
            width: 100%;
            height: 120px;
        }

        .titulo{
            font-size: 1.5rem;
        }

        .link{
            color: #007AF6;
        }

        .footer-email{
            margin-left: 20px;
        }

        p{
            margin:0 ;
        }

    </style>
</head>

<body>
    <div class="contenedor">

        <div class="header_correo">
            <img class="logo_head" src="https://admin.laboratoriocorregidora.com.mx/img/logo-blanco.png">
        </div>

        <table class="mensaje">

            <tr>
                <td align="left">
                    <span class="texto_msg"><b class="titulo">CONTACTO DESDE SITIO WEB IMAGENOLOGÍA:</b></span>
                    <br><br>
                </td>
            </tr>

            <tr>
                <td align="left">
                    <p><span class="texto_msg">NOMBRE:</span> {{ $datos['nombre'] }}</p>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <p><span class="texto_msg">TELÉFONO:</span> {{ $datos['telefono'] }}</p>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <p><span class="texto_msg">CORREO:</span> {{ $datos['correo'] }}</p>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <p><span class="texto_msg">MENSAJE:</span> {{ $datos['mensaje'] }}</p>
                </td>
            </tr>

        </table>

        <div class="footer-email">
            <p>
                <br>
                Recibiste este correo como confirmación de una cita en la página de
                Laboratorios Corregidora.
            </p>
            <br>
            <p>
                Sus datos están protegidos por nuestro aviso de privacidad y no serán
                utilizados para fines comerciales.
            </p>
            <br>
            <p class="link">
                <b> © Laboratorio Corregidora</b>
            </p>
        </div>
    </div>
</body>

</html>
