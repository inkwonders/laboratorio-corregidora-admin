<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsuariosExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;

 public function collection() {
    return User::get()->map(function($user) {
        return [
            $user->name,
            $user->email,
            $user->roles->pluck('name')->join(', ', ' y ')
        ];
    });
 }

    public function headings(): array
    {
        return ['Nombre','email','Tipo Usuario'];
    }
}

