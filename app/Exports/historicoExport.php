<?php

namespace App\Exports;

use App\Models\Cita;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Http\Request;

class HistoricoExport implements FromQuery, ShouldAutoSize, WithHeadings
{
    use Exportable;
    private $request = [];

    public function __construct(Request $request)
    {
        foreach($request->toArray() as $posicion=>$id){
            array_push($this->request, $posicion);
        }
    }

    public function query()
    {

        // $consulta = cita::query()
        // ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
        // ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');
        // ->where('pagado','=',1);

        if(count($this->request) > 0){
            $consulta = cita::query()
            ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');
            foreach($this->request as $id){
                $consulta->orWhere('citas.id','=',$id);
            }
        }else{
            $consulta = cita::query()
            ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id')
            ->where('pagado','=',1);
        }

        return $consulta;

        // return cita::query()
        //     ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
        //     ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');

    }

    public function headings():array
    {
        return ["No. confirmacion", "Nombre", "Apellido Paterno", "Apellido Materno", "Fecha Seleccionada", "Tel. Celular", "Tel. Casa", "Email", "Tipo Tarjeta", "Digitos", "Costo"];
    }
}
