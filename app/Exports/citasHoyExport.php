<?php

namespace App\Exports;

use App\Models\Cita;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Http\Request;

class CitasHoyExport implements FromQuery, ShouldAutoSize, WithHeadings
{
    use Exportable;
    private $request = [];

    public function __construct(Request $request)
    {
        foreach($request->toArray() as $posicion=>$id){
            array_push($this->request, $posicion);
        }
    }

    public function query()
    {

        // $consulta = cita::query()
        // ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
        // ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id')
        // ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)');

        if(count($this->request) > 0){
            $consulta = cita::query()
            ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');
            foreach($this->request as $id){
                $consulta->orWhere('citas.id','=',$id);
            }
            $consulta->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)');
        }else{
            $consulta = cita::query()
            ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id')
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)');
        }

        return $consulta;

        // return cita::query()
        //     ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'fecha_seleccionada', 'tel_celular', 'tel_casa', 'paciente_email', 'tipo_tarjeta', 'digitos', 'costo')
        //     ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id')
        //     ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)');
    }

    public function headings(): array
    {
        return ["No. confirmacion", "Nombre", "Apellido Paterno", "Apellido Materno", "Fecha Seleccionada", "Tel. Celular", "Tel. Casa", "Email", "Tipo Tarjeta", "Digitos", "Costo"];
    }
}
