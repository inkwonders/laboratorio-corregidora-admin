<?php

namespace App\Exports;

use App\Models\Analisis;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AnalisisExport implements  FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {

        return analisis::get()->map(function($analisis) {
            return [
                $analisis->id,
                $analisis->clave,
                $analisis->nombre,
                $analisis->estudios,
                $analisis->ayuno,
                $analisis->entrega,
                $analisis->indicaciones,
                $analisis->precio,
                $analisis->categoria->nombre
            ];
        });
        }

    public function headings(): array
    {
        return ['id','clave', 'nombre', 'estudios', 'ayuno', 'entrega', 'indicaciones', 'precio', 'categorías',' ', '*IMPORTANTE: No modificar la columna id y la columna de categorías, así como no agregar mas columnas ya que no se tomaran en cuenta para el alta masiva'];
    }
}
