<?php

namespace App\Exports;

use App\Models\Orden;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdenExport implements FromQuery, ShouldAutoSize, WithHeadings
{
    use Exportable;

    public function query()
    {
        return Orden::select('no_confirmacion','nombre_paciente','tel_paciente','email_paciente','total','tipo_tarjeta','digitos' )->where('estatus', 1);
    }

    public function headings(): array
    {
        return ["No. confirmacion", "Nombre Paciente", "Tel. Celular", "Email", "Total", "Tipo Tarjeta", "Digitos"];
    }
}
