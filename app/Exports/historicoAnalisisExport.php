<?php

namespace App\Exports;

use App\Models\CarritoMedico;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class historicoAnalisisExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    use Exportable;

 public function collection() {
    return CarritoMedico::get()->map(function($carrito) {
        return [
            $carrito->no_confirmacion,
            $carrito->nombre_paciente,
            $carrito->telefono_paciente,
            $carrito->email_paciente,
            date('Y-m-d',strtotime($carrito->created_at)),
        ];
    });
 }

    public function headings(): array
    {
        return ['No. Confirmación','Nombre','Teléfono','Email','Fecha creación'];
    }
}
