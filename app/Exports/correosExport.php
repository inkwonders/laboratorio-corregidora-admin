<?php

namespace App\Exports;

use App\Models\ListaCorreoEncuesta;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class correosExport implements  FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {

        return ListaCorreoEncuesta::get()->map(function($listado) {
            return [
                $listado->id,
                $listado->nombre,
                $listado->correo
            ];
        });
        }

    public function headings(): array
    {
        return ['id','nombre', 'correo', ' ', '*IMPORTANTE: Añade un id consecutivo,                              El nombre es opcional, el correo es obligatorio. No modificar y/o agregar más columnas ya que no se tomaran en cuenta para el alta masiva'];
    }
}
