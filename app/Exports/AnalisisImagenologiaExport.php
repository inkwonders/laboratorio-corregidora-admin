<?php

namespace App\Exports;

use App\Models\AnalisisImagenologico;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AnalisisImagenologiaExport implements  FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {

        return AnalisisImagenologico::get()->map(function($analisis) {
            return [
                $analisis->id,
                $analisis->nombre,
                $analisis->descripcion,
                $analisis->clave,
                $analisis->indicaciones,
                $analisis->costo,
                $analisis->estimado_costo,
                $analisis->tiempo_de_entrega,
                $analisis->nombres_alternativos,
                $analisis->contacto,
                $analisis->slug,
                $analisis->tipoestudioimagelologicos_id
            ];
        });
        }

    public function headings(): array
    {
        return ['id', 'nombre','descripcion', 'clave', 'indicaciones', 'costo', 'estimado_costo', 'tiempo_de_entrega', 'nombres_alternativos', 'contacto', 'slug', 'tipo estudio',' ', '*IMPORTANTE: No modificar la columna id y la columna de categorías, así como no agregar mas columnas ya que no se tomaran en cuenta para el alta masiva, el tipo de estudio es de la siguiente manera: 1 - rayos x, 2 - radiologia digital, 3 - ultrasonido, 4 - biopsias '];
    }
}
