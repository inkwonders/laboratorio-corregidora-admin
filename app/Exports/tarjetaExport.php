<?php

namespace App\Exports;

use App\Models\ResponseTarjeta;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
class TarjetaExport implements FromCollection, WithHeadings
{
    use Exportable;

    public $fs = null;
    public $fe = null;
    // public $fecha_start, $fecha_end;

    public function __construct(string $fs, string $fe)
    {
        // dd($this->fs);
        if($fs == 'vacio' || $fe == 'vacio'){

        }else{
            $this->fs = $fs;
            $this->fe = $fe;
        }

    }

    public function collection()
    {
        $sucursal = "008-LABORATORIO CORREGIDORA";
        $usuario_hardcodeado = "USUARIO  SISTEMA INTEGRACIONES";
        // $transaccion = "-";
        // $nombre_cliente ="-";
        $afiliacion="8637391";
        $nombre_comercial = "SPUG MIT 250";
        $moneda="MXN";
        $operacion="VENTA";
        $banco_adq ="BANREGIO";
        $tres_d="SI";
        // dd($this->fs,$this->fe);

        if($this->fs != null && $this->fe != null){



            return responseTarjeta::
            whereDate('fecha', '>=', $this->fs)
            ->whereDate('fecha', '<=', $this->fe)->get()->map(function($tarjeta) use($sucursal,$usuario_hardcodeado,$afiliacion,$nombre_comercial,$moneda, $operacion, $banco_adq,$tres_d){
                return [
                    $tarjeta->foliocpagos,
                    $sucursal,
                    $tarjeta->numero_confirmacion,
                    $usuario_hardcodeado,
                    // $transaccion,
                    $tarjeta->tipo_pago,
                    $tarjeta->tarjeta_digitos,
                    // $nombre_cliente,
                    $tarjeta->autorizacion,
                    $tarjeta->tipo_transaccion,
                    $afiliacion,
                    $nombre_comercial,
                    $tarjeta->importe,
                    $moneda,
                    $tarjeta->fecha,
                    $tarjeta->hora,
                    $tarjeta->banco_emisor,
                    $tarjeta->tarjeta,
                    $tarjeta->tipo_tarjeta,
                    $tarjeta->estatus,
                    $operacion,
                    $banco_adq,
                    $tres_d


                    // $tarjeta->roles->pluck('name')->join(', ', ' y ')
                ];
            });


            // $consulta = responseTarjeta::select('numero_confirmacion' , 'tarjeta_digitos', 'autorizacion', 'tipo_transaccion', 'importe', 'fecha', 'hora','tipo_tarjeta', 'tarjeta', 'banco_emisor')->whereDate('fecha', '>=', $this->fs)
            // ->whereDate('fecha', '<=', $this->fe);

        }else{

            $consulta= responseTarjeta::get()->map(function($tarjeta) use($sucursal,$usuario_hardcodeado,$afiliacion,$nombre_comercial,$moneda, $operacion, $banco_adq,$tres_d){
                return [
                    $tarjeta->foliocpagos,
                    $sucursal,
                    $tarjeta->numero_confirmacion,
                    $usuario_hardcodeado,
                    // $transaccion,
                    $tarjeta->tipo_pago,
                    $tarjeta->tarjeta_digitos,
                    // $nombre_cliente,
                    $tarjeta->autorizacion,
                    $tarjeta->tipo_transaccion,
                    $afiliacion,
                    $nombre_comercial,
                    $tarjeta->importe,
                    $moneda,
                    $tarjeta->fecha,
                    $tarjeta->hora,
                    $tarjeta->banco_emisor,
                    $tarjeta->tarjeta,
                    $tarjeta->tipo_tarjeta,
                    $tarjeta->estatus,
                    $operacion,
                    $banco_adq,
                    $tres_d


                    // $tarjeta->roles->pluck('name')->join(', ', ' y ')
                ];
            });
        // return $response;
            // $consulta = responseTarjeta::select('numero_confirmacion' , 'tarjeta_digitos', 'autorizacion', 'tipo_transaccion', 'importe', 'fecha', 'hora','tipo_tarjeta', 'tarjeta', 'banco_emisor');

        }

        return $consulta;
    }

    public function headings(): array
    {
        return ['No. Operación' , 'Sucursal','REFERENCIA','Usuario','Tipo de pago','# Tarjeta','Autorización','Tipo de transacción','Afiliación','Nombre Comercial','Importe','Moneda','Fecha','Hora','Tipo de tarjeta','Tarjeta','Banco emisor','Estatus','Operación','Banco Adquirente','3DSecure'];
    }
}
