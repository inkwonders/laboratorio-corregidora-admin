<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Analisis extends Model
{
    use HasFactory;

    protected $table = 'analisis';

    protected $fillable = ['clave', 'nombre', 'estudios', 'ayuno', 'entrega', 'indicaciones', 'precio','categoria_id','estatus'
    ];

    public function carrito() {
        return $this->belongsToMany(
            CarritoMedico::class,
            'carrito_medico_analisis',
            'analisis_id',
            'carrito_medico_id'
        );
    }

    public function categoria(){
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }


}
