<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sucursales extends Model
{
    use HasFactory;

    public function horarios()
    {
        return $this->belongsToMany(Scheduleimage::class,'scheduleimage_sucursales');
    }

    public function sucursalesEstudio()
    {
        return $this->belongsToMany(TipoestudioImage::class,'sucursales_tipoestudio_image');
    }
}
