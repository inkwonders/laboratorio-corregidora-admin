<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarritoMedico extends Model
{
    use HasFactory;

    protected $table = 'carrito_medico';
    protected $primaryKey = 'id';

    protected $fillable = [
        'no_confirmacion',
        'activo',
        'nombre_paciente',
        'email_paciente',
        'telefono_paciente'
    ];

    protected static function booted()
    {
        static::creating(function ($carrito) {
            $carrito->attributes['no_confirmacion'] = uniqid();
            $carrito->attributes['activo'] = 1;
        });
    }

    public function getTotalAttribute() {
        return $this->analisis->map(function($analisis) {
            return $analisis->precio * $analisis->pivot->cantidad;
        })->sum();
    }

    public function analisis() {
        return $this->belongsToMany(
            Analisis::class,
            'carrito_medico_analisis',
            'carrito_medico_id',
            'analisis_id'
        )
        ->withPivot('cantidad')
        ->withPivot('id');
    }

    public function scopeActivo($query) {
        return $query->where('activo', 1);
    }
}
