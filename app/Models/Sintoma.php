<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sintoma extends Model
{
    use HasFactory;

    protected $table = 'sintomas';

    public function pacientes() {
        return $this->belongsToMany(
            Paciente::class,
            'sintomas_pacientes',
            'sintoma_id',
            'paciente_id'
        );
    }

}
