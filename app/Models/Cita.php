<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    use HasFactory;

    protected $table = 'citas';

    protected $fillable = [
        'horario_id',
        'paciente_id',
        'fecha_seleccionada',
        'no_confirmacion',
        'status_asistencia',
        'tipo_pago',
        'pagado',
        'response',
        'cadena',
        'tipo_tarjeta',
        'digitos',
        'costo',
        'tipo_prueba'
    ];

    public function horarios(){
        return $this->belongsTo(Horario::class, 'horario_id');
    }

    public function paciente(){
        return $this->belongsTo(Paciente::class, 'paciente_id');
    }
}
