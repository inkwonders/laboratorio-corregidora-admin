<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResponseTarjeta extends Model
{
    use HasFactory;

    protected $table = 'response_tarjeta';

    protected $fillable = [
        'response',
        'tipo',
        'numero_confirmacion',
        'tipo_pago',
        'tarjeta_digitos',
        'autorizacion',
        'tipo_transaccion',
        'importe',
        'fecha',
        'hora',
        'tipo_tarjeta',
        'tarjeta',
        'banco_emisor',
        'estatus',
    ];

}
