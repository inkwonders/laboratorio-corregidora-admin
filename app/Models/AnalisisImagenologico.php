<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalisisImagenologico extends Model
{
    public $timestamps = true;

    use HasFactory;

    protected $table = 'analisis_imagenologicos';

    protected $fillable = ['clave', 'nombre', 'descripcion', 'indicaciones', 'costo', 'estimado_costo', 'tiempo_de_entrega','nombres_alternativos','contacto','slug','tipoestudioimagelologicos_id'];

}

