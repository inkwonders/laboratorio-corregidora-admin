<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Encuesta extends Model
{
    use HasFactory;

    protected $table = 'encuestas';
    protected $fillable = ['nombre','created_at', 'updated_at', 'enviado','fecha_envio'];

    public function preguntas() {
        return $this->hasMany(Pregunta::class, 'encuesta_id');
    }

}
