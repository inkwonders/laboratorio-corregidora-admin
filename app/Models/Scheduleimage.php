<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Scheduleimage extends Model
{
    use HasFactory;

    protected $table = 'schedule_images';

    public function getHorarioCitaAttribute() {
        return "{$this->horario_inicial} - {$this->horario_final}";
    }

    public function horarios()
    {
        return $this->belongsToMany(Sucursales::class, 'scheduleimage_sucursales');
    }
}
