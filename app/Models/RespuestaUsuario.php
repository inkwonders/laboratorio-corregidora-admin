<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RespuestaUsuario extends Model
{
    use HasFactory;
    protected $fillable = ['encuesta_id','pregunta_id','users_id','respuesta','created_at','updated_at'];

    protected $table = 'respuestas_usuarios';

}
