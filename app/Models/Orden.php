<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
    use HasFactory;

    protected $table = 'orden';

    public function analisis() {
        return $this->belongsToMany(
            Analisis::class,
            'orden_analisis',
            'orden_id',
            'analisis_id'
        )
        ->withPivot('cantidad');
    }
}
