<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EncuestaUsuario extends Model
{
    use HasFactory;

    protected $table = 'encuesta_usuario';

    protected $fillable = ['encuesta_id', 'usuario_id', 'paciente_email', 'enviado', 'status'];


    public function encuesta(){
        return $this->belongsTo(Encuesta::class, 'encuesta_id');
    }

    public function usuario(){
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function preguntas() {
        return $this->hasMany(Pregunta::class, 'encuesta_id', 'encuesta_id');
    }

}
