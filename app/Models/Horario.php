<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    use HasFactory;

    protected $table = 'horarios_citas';

    public function getHorarioCitaAttribute() {
        return "{$this->hora_inicial} - {$this->hora_final}";
    }

}
