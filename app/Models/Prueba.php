<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prueba extends Model
{
    use HasFactory;

    public function pacientes() {
        return $this->belongsToMany(
            Pruebas::class,
            'pruebas_pacientes',
            'prueba_id',
            'paciente_id'
        );
    }
}
