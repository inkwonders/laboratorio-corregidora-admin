<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarritoMedicoAnalisis extends Model
{
    use HasFactory;

    protected $table = 'carrito_medico_analisis';

}
