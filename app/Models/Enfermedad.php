<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enfermedad extends Model
{
    use HasFactory;

    protected $table = 'enfermedades';

    public function pacientes() {
        return $this->belongsToMany(
            Paciente::class,
            'enfermedades_pacientes',
            'enfermedad_id',
            'paciente_id'
        );
    }
}
