<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CitaImagenologico extends Model
{
    use HasFactory;

    protected $table = 'citas_imagenologicos';

    protected $fillable = [
        "nombre",
        "telefono",
        "correo",
        "fecha_nacimiento",
        "fecha_cita",
        "analisisimagenologico_id",
        "scheduleimage_id",
        "sucursal"
    ];

    public function horarios(){
        return $this->belongsTo(Scheduleimage::class, 'Scheduleimage_id');
    }
    public function analisImg(){
        return $this->belongsTo(AnalisisImagenologico::class, 'analisisimagelologico_id');
    }

}
