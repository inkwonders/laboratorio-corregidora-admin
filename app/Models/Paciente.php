<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;

    protected $table = 'pacientes';

    protected $fillable = [
        'user_id',
        'clave',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'nombre_completo',
        'genero',
        'fecha_nacimiento',
        'calle',
        'colonia',
        'no_exterior',
        'no_interior',
        'ubicacion_id',
        'cp',
        'tel_celular',
        'tel_casa',
        'paciente_email',
        'medico_nombre',
        'medico_email',
        'razon_social',
        'rfc',
        'domicilio_fiscal',
        'otro_sintoma',
        'otra_enfermedad',
        'contacto_persona_positivo',
        'antiviral',
        'influenza',
        'otro_prueba',
        'fecha_alta',
        'id_viejo',
        'pasaporte',
        'es',
        'en',
    ];

    public function sintomas() {
        return $this->belongsToMany(
            Sintoma::class,
            'sintomas_pacientes',
            'paciente_id',
            'sintoma_id'
        );
    }

    public function enfermedades() {
        return $this->belongsToMany(
            Enfermedad::class,
            'enfermedades_pacientes',
            'paciente_id',
            'enfermedad_id'
        );
    }

    public function pruebas() {
        return $this->belongsToMany(
            Prueba::class,
            'pruebas_pacientes',
            'paciente_id',
            'prueba_id'
        );
    }

    public function getAntiviralAttribute($value) {
        return $value ? 'Si' : 'No';
    }

    public function getContactoPersonaPositivoAttribute($value) {
        return $value ? 'Si' : 'No';
    }

    public function getInfluenzaAttribute($value) {
        return $value ? 'Si' : 'No';
    }

    public function getListaEnfermedadesAttribute() { //lista de enfermedades del paciente
        $enfermedades = $this->enfermedades
            ->pluck('nombre');

        if($this->otra_enfermedad != '')
            $enfermedades = $enfermedades->push(
                $this->otra_enfermedad
            );

        if($enfermedades->isEmpty()){
            return "Sin enfermedades.";
        }else{
            return $enfermedades->join(', ', ' y ');
        }
    }

    public function getListaSintomasAttribute() { //lista de sintomas del paciente
        $sintomas = $this->sintomas
            ->pluck('nombre');

        if($this->otro_sintoma != '')
            $sintomas = $sintomas->push(
                $this->otro_sintoma
            );

        if($sintomas->isEmpty()){
            return "Sin sintomas.";
        }else{
            return $sintomas->join(', ', ' y ');
        }

    }

    public function getListaPruebasAttribute() { //lista de pruebas del paciente
        $pruebas = $this->pruebas
            ->pluck('nombre');

        if($this->otro_prueba != '')
            $pruebas = $pruebas->push(
                $this->otro_prueba
            );

        if($pruebas->isEmpty()){
            return "Sin pruebas.";
        }else{
            return $pruebas->join(', ', ' y ');
        }
    }

    public function getNombreCompletoAttribute() {
        return "{$this->nombre} {$this->apellido_paterno} {$this->apellido_materno}";
    }
}
