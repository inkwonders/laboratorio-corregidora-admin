<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ListaCorreoEncuesta extends Model
{
    use HasFactory;

    protected $table = 'lista_correo_encuestas';

    protected $fillable = ['nombre','correo','estatus'];
}
