<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    use HasFactory;
    protected $table = 'preguntas';
    protected $fillable = ['descripcion','created_at','updated_at'];

    public function encuesta() {
        return $this->belongsTo(Encuesta::class, 'encuesta_id');
    }
}
