<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoestudioImage extends Model
{
    use HasFactory;

    public function sucursalesEstudio()
    {
        return $this->belongsToMany(Sucursales::class, 'sucursales_tipoestudio_image');
    }
}
