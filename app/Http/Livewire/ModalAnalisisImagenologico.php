<?php

namespace App\Http\Livewire;

use App\Models\AnalisisImagenologico;
use App\Models\Categoria;
use App\Models\TipoestudioImage;
use App\Rules\ValidacionPrecioAnalisis;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ViewErrorBag;
use Livewire\Component;

class ModalAnalisisImagenologico extends Component
{

    // variables del formulario
    public $analisis_id, $clave, $costo, $nombre, $tipo_estudio, $slug, $estimado_costo, $nombres_alternativos, $tiempo_de_entrega, $contacto, $descripcion, $indicaciones, $tipos;

    // acciones de la modal
    public $agregar = false;
    public $eliminar = false;
    public $editar = false;

    public $newSlug;

    //listeners enviados desde liveware.estudios
    protected $listeners = ['cambioEstatus','updateAnalisis','deleteAnalisis','createAnalisis'];

    //validaciones
    protected $rules = [
        'clave' => 'required|',
        'costo' => 'sometimes|nullable|integer',
        'nombre' => 'required',
        'tipo_estudio' => 'required',
        'slug' => 'required',
        'estimado_costo' => 'required_if:costo,null',
        'nombres_alternativos' => 'required',
        'contacto' => 'required',
        'descripcion' => 'required',
        'indicaciones' => 'required',
        'tiempo_de_entrega' => 'required'
    ];

    public function mount(){

        $this->categorias = Categoria::query()
        ->get();
        $this->identificador = rand();

        $this->tipos = TipoestudioImage::query()->get();

     }

     public function crearSlug(){

        $this->newSlug = str_replace(' ', '-', $this->nombre);

        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $this->newSlug
        );

        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );

        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );

        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );

        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );

        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $string
        );

        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\\", "¨", "º", "~",
                "#", "@", "|", "!", "\"",
                "·", "$", "%", "&", "/",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "`", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ",", ":",
                "."),'', $string);

        $this->slug = mb_strtolower($string);
     }


    //create
    //funcion para mostrar las opciones para crear un analisis en la modal
     public function createAnalisis()
    {
        $this->agregar = true;
    }

    public function create(){
        $this->resetErrorBag();

        $this->validate();

        $validador = Validator::make(['costo' => $this->costo], ['costo' => new ValidacionPrecioAnalisis]);
        if($validador->fails()) {
            $this->addError('costo', $validador->errors()->first());
            return;
        }

        AnalisisImagenologico::create([
            'clave' => $this->clave,
            'costo' => $this->costo,
            'nombre' => $this->nombre,
            'tipoestudioimagelologicos_id' => $this->tipo_estudio,
            'slug' => $this->slug,
            'estimado_costo' => $this->estimado_costo,
            'nombres_alternativos' => $this->nombres_alternativos,
            'tiempo_de_entrega' => $this->tiempo_de_entrega,
            'contacto' => $this->contacto,
            'descripcion' => $this->descripcion,
            'indicaciones' => $this->indicaciones
        ]);

        $this->emit('cerrarModal');

        $this->emit('refreshLivewireDatatable');

        $this->reset(['clave','costo','nombre','tipo_estudio','slug','estimado_costo','nombres_alternativos','tiempo_de_entrega', 'contacto', 'descripcion', 'indicaciones']);

    }

    //termina create

    //delete

    public function deleteAnalisis($id){

        $this->analisis_id = $id;
        $this->eliminar = true;

    }

    public function delete(){

        AnalisisImagenologico::find($this->analisis_id)->delete();

        $this->emit('refreshLivewireDatatable');
        $this->emit('cerrarModal');

    }

    //termina delete

    //update
    public function updateAnalisis($id){
        $this->analisis_id = $id;
        $this->analisis = AnalisisImagenologico::find($id);

        $this->clave = $this->analisis->clave;
        $this->costo = $this->analisis->costo;
        $this->nombre = $this->analisis->nombre;
        $this->tipo_estudio = $this->analisis->tipoestudioimagelologicos_id;
        $this->estimado_costo = $this->analisis->estimado_costo;
        $this->nombres_alternativos = $this->analisis->nombres_alternativos;
        $this->tiempo_de_entrega = $this->analisis->tiempo_de_entrega;
        $this->contacto = $this->analisis->contacto;
        $this->descripcion = $this->analisis->descripcion;
        $this->indicaciones = $this->analisis->indicaciones;
        $this->slug = $this->analisis->slug;

        $this->editar = true;

    }

    public function update(){

        $this->resetErrorBag();

        $this->validate();

        $validador = Validator::make(['costo' => $this->costo], ['costo' => new ValidacionPrecioAnalisis]);
        if($validador->fails()) {
            $this->addError('costo', $validador->errors()->first());
            return;
        }

        $this->analisis = AnalisisImagenologico::find($this->analisis_id);


        $this->analisis->update([

            'nombre' => $this->nombre,
            'descripcion' => $this->descripcion,
            'clave' => $this->clave,
            'indicaciones' => $this->indicaciones,
            'costo' => $this->costo,
            'estimado_costo' => $this->estimado_costo,
            'tiempo_de_entrega' => $this->tiempo_de_entrega,
            'nombres_alternativos' => $this->nombres_alternativos,
            'contacto' => $this->contacto,
            'slug' => $this->slug,
            'tipoestudioimagelologicos_id' => $this->tipo_estudio
        ]);

        $this->emit('cerrarModal');

        $this->emit('refreshLivewireDatatable');

    }

    //termina update

    //cambio estatus
    public function cambioEstatus(AnalisisImagenologico $analisis)
    {
        $analisis->update([
            'estatus' => ! $analisis->estatus
        ]);
    }

    public function render()
    {
        return view('livewire.modal-analisis-imagenologico');
    }
}
