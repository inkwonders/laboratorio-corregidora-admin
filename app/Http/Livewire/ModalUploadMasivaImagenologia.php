<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AnalisisImport;
use App\Models\AnalisisImagenologico;

class ModalUploadMasivaImagenologia extends Component
{
    public $agregar_masiva = false;
    public $updateAnalisis;
    public $archivo = null;
    use WithFileUploads;

    protected $listeners = ['uploadMasivaModal'];

    protected $rules = [
        'archivo' => 'required',

    ];

    public function create() {

        $folios = Excel::toCollection(new AnalisisImport, $this->archivo->getRealPath())
        ->first() // Primera hoja
        ->skip(1) // Omitimos la primer fila
        ->mapWithKeys(function($row) {


            $folio=$row[0];//agregar al final una columna con autoincremental en el excel

            return [
                $folio => [
                    'd_id' => $row[0],
                    'd_nombre'=>$row[1],
                    'd_descripcion'=>$row[2],
                    'd_clave'  => $row[3],
                    'd_indicaciones'=>$row[4],
                    'd_costo'=>$row[5],
                    'd_estimado_costo'=>$row[6],
                    'd_tiempo_de_entrega'=>$row[7],
                    'd_nombres_alternativos'=>$row[8],
                    'd_contacto'=>$row[9],
                    'd_slug'=>$row[10],
                    'd_tipoTipoestudioImagelologicos_id'=>$row[11]
                ]
            ];


        })
        ->whereNotNull();

        foreach ($folios as $folio) {

            $id=$folio['d_id'];
            $busqueda=AnalisisImagenologico::find($id);

            $nombre=$folio['d_nombre'];
            $descripcion=$folio['d_descripcion'];
            $clave=$folio['d_clave'];
            $indicaciones=$folio['d_indicaciones'];
            $costo=$folio['d_costo'];
            $estimado_costo=$folio['d_estimado_costo'];
            $tiempo_de_entrega=$folio['d_tiempo_de_entrega'];
            $nombres_alternativos=$folio['d_nombres_alternativos'];
            $contacto=$folio['d_contacto'];
            $slug=$folio['d_slug'];
            $tipoTipoestudioImagelologico_id=$folio['d_tipoTipoestudioImagelologicos_id'];

            if($costo == null || $costo == ""){
                $costo = null;
            }

            if($estimado_costo == null || $estimado_costo == ""){
                $estimado_costo = null;
            }

            if($indicaciones == null || $indicaciones == ""){
                $indicaciones = null;
            }

            if($tiempo_de_entrega == null || $tiempo_de_entrega == ""){
                $tiempo_de_entrega = null;
            }

            if($nombres_alternativos == null || $nombres_alternativos == ""){
                $nombres_alternativos = null;
            }

            if($busqueda){

                $busqueda->update(
                    [
                        'nombre' => $nombre,
                        'descripcion' => $descripcion,
                        'clave' => $clave,
                        'indicaciones' => $indicaciones,
                        'costo' => $costo,
                        'estimado_costo' => $estimado_costo,
                        'tiempo_de_entrega' => $tiempo_de_entrega,
                        'nombres_alternativos' => $nombres_alternativos,
                        'contacto' => $contacto,
                        'slug' => $slug,
                        'tipoestudioimagelologicos_id' => $tipoTipoestudioImagelologico_id
                    ]

                );

            }else{

                AnalisisImagenologico::insert(
                    [
                        'nombre' => $nombre,
                        'descripcion' => $descripcion,
                        'clave' => $clave,
                        'indicaciones' => $indicaciones,
                        'costo' => $costo,
                        'estimado_costo' => $estimado_costo,
                        'tiempo_de_entrega' => $tiempo_de_entrega,
                        'nombres_alternativos' => $nombres_alternativos,
                        'contacto' => $contacto,
                        'slug' => $slug,
                        'tipoestudioimagelologicos_id' => $tipoTipoestudioImagelologico_id
                    ]

                );

            }

        }

        $this->emit('cerrarModal');
        $this->emit('refreshLivewireDatatable');
    }

    public function uploadMasivaAnalisis()
    {
        $this->agregar_masiva = true;
    }

    public function render()
    {
        return view('livewire.modal-upload-masiva-imagenologia');
    }
}
