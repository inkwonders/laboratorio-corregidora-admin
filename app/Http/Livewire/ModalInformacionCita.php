<?php

namespace App\Http\Livewire;

use App\Models\Cita;
use Livewire\Component;

class ModalInformacionCita extends Component
{
    public $cita, $asistencia, $noAsistencia;

    protected $listeners = ['mostrarCita', 'cerrarModal','asistenciaCita', 'noAsistencia', 'confirmarAsistencia'];

    public function mostrarCita(Cita $cita) {
        $this->cita = $cita;
        // dd($cita);
    }

    public function asistenciaCita(Cita $asistencia) {
        $this->asistencia = $asistencia;
    }

    public function noAsistencia(Cita $noAsistencia) {
        $this->noAsistencia = $noAsistencia;
    }

    public function confirmarAsistencia(Cita $cita) {
        if($cita->tipo_pago == 0){
            $cita->update([
                'status_asistencia' => 1,
                'pagado' => 1
            ]);
        }else{
            $cita->update([
                'status_asistencia' => 1
            ]);
        }

        $this->emit('cerrarModal');
        $this->emit('refreshLivewireDatatable');

    }

    public function quitarAsistencia(Cita $cita) {

        if($cita->tipo_pago == 0){
            $cita->update([
                'status_asistencia' => 0,
                'pagado' => 0
            ]);
        }else{
            $cita->update([
                'status_asistencia' => 0
            ]);
        }

        $this->emit('refreshLivewireDatatable');
    }

    public function cerrarModal() {
        $this->cita = false;
        $this->asistencia = false;
    }

    public function render()
    {
        return view('livewire.modal-informacion-cita');
    }
}
