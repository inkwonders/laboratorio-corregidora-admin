<?php

namespace App\Http\Livewire;

use App\Models\CarritoMedico;
use Livewire\Component;

class Paciente extends Component
{
    public $carrito;
    public $nombre, $email, $telefono;

    public function mount() {
        $this->nombre = $this->carrito->nombre_paciente;
        $this->email = $this->carrito->email_paciente;
        $this->telefono = $this->carrito->telefono_paciente;
    }

    public function update()
    {
        $this->validate([
            'nombre' => 'required',
            'email' => 'required|email',
            'telefono' => 'required|digits:10',
        ]);

        $this->carrito->update([
            'nombre_paciente' => $this->nombre,
            'email_paciente' => $this->email,
            'telefono_paciente' => $this->telefono
        ]);

        return redirect()->route('carrito.comprobante');
    }

    public function render()
    {
        return view('livewire.paciente');
    }
}
