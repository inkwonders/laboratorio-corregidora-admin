<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ModalEncuesta extends Component
{
    public $encuesta;
    public $preguntas;

    public function render()
    {
        return view('livewire.modal-encuesta');
    }
}
