<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AnalisisImport;
use Livewire\WithFileUploads;
use App\Models\Analisis;



class ModalUploadMasivaAnalisis extends Component
{
    public $agregar_masiva = false;
    public $updateAnalisis;
    public $archivo = null;
    use WithFileUploads;

    protected $listeners = ['uploadMasivaModal'];



    protected $rules = [
        'archivo' => 'required',

    ];

    public function create() {

        $folios = Excel::toCollection(new AnalisisImport, $this->archivo->getRealPath())
        ->first() // Primera hoja
        ->skip(1) // Omitimos la primer fila
        ->mapWithKeys(function($row) {


            $folio=$row[0];//agregar al final una columna con autoincremental en el excel

            return [
                $folio => [
                    'd_id' => $row[0],
                    'd_clave'  => $row[1],
                    'd_nombre'=>$row[2],
                    'd_estudios'=>$row[3],
                    'd_ayuno'=>$row[4],
                    'd_entrega'=>$row[5],
                    'd_indicaciones'=>$row[6],
                    'd_precio'=>$row[7],
                    // 'd_categorias'=>$row[8],

                ]
            ];


        })
        ->whereNotNull();

        //  dd($folios);
        // dd($this->archivo);
        foreach ($folios as $folio) {

            $id=$folio['d_id'];
            // $busqueda=Analisis::where('id',$id)->first();
            $busqueda=Analisis::find($id);
            // dd($busqueda);
            $clave=$folio['d_clave'];
            $nombre=$folio['d_nombre'];
            $estudios=$folio['d_estudios'];
            $ayuno=$folio['d_ayuno'];
            $entrega=$folio['d_entrega'];
            $indicaciones=$folio['d_indicaciones'];
            $precio=$folio['d_precio'];
            // $categorias=$folio['d_categorias'];
            if($precio == null || $precio == ""){
                $precio = "0";
            }
            // dd($busqueda);
            // try {
                // if(is_null($busqueda)){
                    $busqueda->update(
                        [
                            'clave' => $clave,
                            'nombre' => $nombre,
                            'estudios' => $estudios,
                            'ayuno' => $ayuno,
                            'entrega' => $entrega,
                            'indicaciones' => $indicaciones,
                            'precio' => $precio,
                            // 'categorias' => $categorias,
                        ]

                    );
                    // $id_municipio=$actualiza_analisis->id;
                // }else{ $id_municipio=$busqueda->id; }

        // }
        //     catch (\Throwable $th) {

        //         return response()->json(['success' => false, 'error' => $th->getMessage()], 503);
        //     }
        }

        $this->emit('cerrarModal');
        $this->emit('refreshLivewireDatatable');
    }

    public function uploadMasivaAnalisis()
    {
        $this->agregar_masiva = true;
    }



    // $this->emit('cerrarModal');

    public function render()
    {
        return view('livewire.modal-upload-masiva-analisis');
    }
}
