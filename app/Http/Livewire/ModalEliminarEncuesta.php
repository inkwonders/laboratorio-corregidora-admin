<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ModalEliminarEncuesta extends Component
{
    public $encuesta;

    public function render()
    {
        return view('livewire.modal-eliminar-encuesta');
    }

}
