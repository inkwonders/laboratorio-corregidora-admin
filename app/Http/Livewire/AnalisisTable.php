<?php

namespace App\Http\Livewire;

use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use App\Models\Analisis;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Illuminate\Support\Facades\Request;

class AnalisisTable extends LivewireDatatable
{

    public $exportable = false;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        return Analisis::query()
            ->leftJoin('categorias', 'analisis.categoria_id', '=', 'categorias.id');
    }


    public function columns()
    {
        return [

            Column::callback(['clave'], function ($clave) {
                return "<p class='font-bold text-blue-600'> $clave </p>";
            })
                ->label('Clave')
                ->searchable(),

            Column::callback(['nombre'], function ($nombre) {
                return "<p> $nombre </p>";
            })
                ->label('Nombre')
                ->searchable(),

            Column::callback(['estudios'], function ($estudios) {
                return "<p class='whitespace-pre-line '> $estudios </p>";
            })
                ->label('Estudios')
                ->searchable(),

            Column::callback(['precio'], function ($precio) {
                return "<p > $" . number_format($precio, 2) . " </p>";
            })
                ->label('Precio'),

            Column::callback(['categorias.nombre'], function ($categoria) {
                return "<p > " . $categoria . " </p>";
            })
                ->label('Categoria'),

            Column::callback(['estatus','id'], function ($estatus,$id) {
                return view('action-change-status-analisis', ['estatus' => $estatus,'id'=>$id]);
            })->label('Estatus')
            ,

            Column::callback(['id'], function ($id) {
                return view('table-actions-analisis', ['id' => $id]);
            })->label('Acciones'),




        ];
    }
}
