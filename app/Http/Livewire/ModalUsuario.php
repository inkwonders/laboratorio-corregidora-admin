<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Spatie\Permission\Models\Role;

use Spatie\Permission\Models\Permission;

class ModalUsuario extends Component
{
    public $nombre, $email, $password1, $password2, $tipo;
    public $editar = false;
    public $usuario;
    public $user;

    public $permisos;
    public $permisos_elegidos = [];
    public $permisos_usuario = [];
    public $roles;
    public $roles_elegidos = [];
    public $roles_usuario = [];
    public $rol_usuario;
    public $mostrar_admin;
    public $mostrar_medico;

    protected $listeners = ['editarModal','deleteUser'];

    protected $rules = [
        'nombre' => 'required',
        'email' => 'required|email',
        'password1' => 'required',
        'password2' => 'required|same:password1',
        'rol_usuario' => 'required|digits:1',
    ];


    public function mount()
    {
        $this->rol_usuario = Role::all();

        // $this->refrescaRolesUsuario();
    }
    public function change()
    {
       if($this->rol_usuario == "1"){
            $this->permisos = Permission::get()->unique()->whereNotIn('name',['update citas','list citas','show citas']);
            $this->permisos_usuario = $this->permisos->map(function ($permiso_base) {
                return [
                    'id' => $permiso_base->id,
                    'nombre' => $permiso_base->name,
                    // 'heredado' => $user->roles->pluck('permissions')->collapse()->contains('name', $permiso_base->name) ? 1 : 0,
                    'heredado' => 0,
                    'activo' => 0,
                    // 'activo' => $user->hasPermissionTo($permiso_base->name)
                ];
            })
                ->keyBy('id')
                ->toArray();
            $this->mostrar_admin = true;
        }
        else if($this->rol_usuario == "" || $this->rol_usuario == "2"){
            $this->mostrar_admin = false;
        }
        else {
            $this->mostrar_admin = false;
        }

    }
    
    public function refrescaRolesUsuario()
    {
        $this->permisos = Permission::get()->unique()->whereNotIn('name',['update citas','list citas','show citas']);
        $this->mostrar_admin = false;
        $this->mostrar_medico = true;

        if ($this->rol_usuario == "1") {
            $this->mostrar_admin = true;
        }

        if ($this->rol_usuario == "" ||  $this->rol_usuario == "") {
            $this->mostrar_medico = true;
        }

        $usuario = $this->usuario;
        $this->permisos_usuario = $this->permisos->map(function ($permiso_base) use ($usuario) {
            return [
                'id' => $permiso_base->id,
                'nombre' => $permiso_base->name,
                // 'heredado' => $user->roles->pluck('permissions')->collapse()->contains('name', $permiso_base->name) ? 1 : 0,
                'heredado' => 0,
                'activo' => $usuario->hasDirectPermission($permiso_base->name)
                // 'activo' => $user->hasPermissionTo($permiso_base->name)
            ];
        })
            ->keyBy('id')
            ->toArray();
    }


    public function create()
    {
        $this->validate();

        $crear_usuario = User::create([
            'name' => $this->nombre,
            'email' => $this->email,
            'password' => Hash::make($this->password1),
        ])->assignRole(
            Role::where('id', $this->rol_usuario)->first()->name
        );

        foreach ($this->permisos_usuario as $permiso_usuario) {
            if ($permiso_usuario['activo'])
            $crear_usuario->givePermissionTo($permiso_usuario['nombre']);
            else
            $crear_usuario->revokePermissionTo($permiso_usuario['nombre']);
        }

        $this->emit('cerrarModal');
        $this->emit('refreshLivewireDatatable');
    }

    public function editarModal($id){
        $this->user = $id;
        $this->editar = true;
        $this->usuario = User::find($id);
        $this->nombre = $this->usuario->name;
        $this->email = $this->usuario->email;
        $this->rol_usuario = $this->usuario->roles->first()->id;

        if($this->rol_usuario == "1"){
        $this->refrescaRolesUsuario();
        }
        else{

        }

    }

    public function update(){

        $this->validate([
            'nombre' => 'required',
            'email' => 'required|email',
            'password1' => 'sometimes',
            'password2' => 'sometimes|same:password1',
            'rol_usuario' => 'required|digits:1',
        ]);

        $this->usuario = User::find($this->user);

        $this->usuario->update([
            'name' => $this->nombre,
            'email' => $this->email,
        ]);

        if($this->password1 != '') {
            $this->usuario->update([
                'password' => Hash::make($this->password1),
            ]);
        }

        $this->usuario->removeRole($this->usuario->roles->first())->assignRole(
            Role::where('id', $this->rol_usuario)->first()->name
        );


        //  // Eliminamos o asignamos los permisos no heredados
         foreach ($this->permisos_usuario as $permiso_usuario) {
            if ($permiso_usuario['activo'])
            $this->usuario->givePermissionTo($permiso_usuario['nombre']);
            else
            $this->usuario->revokePermissionTo($permiso_usuario['nombre']);
        }

        $this->emit('cerrarModal');
        $this->emit('refreshLivewireDatatable');

    }



    public function deleteUser($id){
        $this->user = $id;
        $this->usuario = User::find($id);
    }

    public function render()
    {
        return view('livewire.modal-usuario');
    }
}
