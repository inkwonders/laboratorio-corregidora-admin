<?php

namespace App\Http\Livewire;

use App\Models\CarritoMedico;
use Livewire\Component;

class ModalRecibo extends Component
{

    public $carrito;
    public $modal = false;

    protected $listeners = ['mostrarLista'];

    public function mostrarLista(CarritoMedico $carrito){
        $this->carrito = $carrito;
        $this->modal = true;

    }

    public function cerrarModalRecibo(){
        $this->modal = false;
    }

    public function render()
    {
        return view('livewire.modal-recibo');
    }
}

