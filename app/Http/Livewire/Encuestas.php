<?php

namespace App\Http\Livewire;

use App\Models\Encuesta as ModelsEncuestas;
use App\Models\EncuestaUsuario;
use App\Models\Pregunta;
use App\Models\RespuestaUsuario;
use Livewire\Component;
class Encuestas extends Component
{

    public $operacion = true;
    public $titulo, $pregunta1, $pregunta2, $pregunta3, $pregunta4, $pregunta5;
    public $id_editar;
    public $modal = false;
    public $modalEnviar = false;
    public $form = null;
    public $asks = null;
    public $eliminar = false;
    public $modalEnviados = false;
    public $id_eliminar;
    public $modal_encuesta = false;
    public $modalEstadisticaEncuesta = false;
    public $id_encuesta;
    public $modal_carga_masiva_correos = false;

    protected $listeners = ['uploadMasivaModalCorreos','eliminar','editar', 'nuevo', 'abrirModal', 'cerrarModal', 'abrirModalEnviar','abrirModalEnviados', 'siEliminar', 'noEliminar','abrirModalEstadisticaEncuesta'];

    //validaciones
    protected $rules = [
        'titulo' => 'required|max:120',
        'pregunta1' => 'required|max:120',
        'pregunta2' => 'required|max:120',
        'pregunta3' => 'required|max:120',
        'pregunta4' => 'required|max:120',
        'pregunta5' => 'required|max:120',
    ];

    public function create(){

        $this->validate();

        $encuesta = ModelsEncuestas::create([
            'nombre' => $this->titulo,
        ]);
        $encuesta->preguntas()->createMany([
            ['descripcion' => $this->pregunta1],
            ['descripcion' => $this->pregunta2],
            ['descripcion' => $this->pregunta3],
            ['descripcion' => $this->pregunta4],
            ['descripcion' => $this->pregunta5]
        ]);

        $this->reset(['titulo', 'pregunta1', 'pregunta2', 'pregunta3', 'pregunta4', 'pregunta5']);
        $this->emit('refreshLivewireDatatable');

        $this->modal_encuesta = false;

    }

    public function editar($id){

        $this->modal_encuesta = true;

        $encuesta = ModelsEncuestas::find($id);
        $preguntas = $encuesta->preguntas()->get()->toArray();

        $this->titulo = $encuesta->nombre;
        $this->pregunta1 = $preguntas[0]['descripcion'];
        $this->pregunta2 = $preguntas[1]['descripcion'];
        $this->pregunta3 = $preguntas[2]['descripcion'];
        $this->pregunta4 = $preguntas[3]['descripcion'];
        $this->pregunta5 = $preguntas[4]['descripcion'];

        $this->operacion = false;
        $this->id_editar = $id;
    }

    public function update(){

        $this->validate();

        $encuesta = ModelsEncuestas::find($this->id_editar);

        $encuesta->update([
            'nombre' => $this->titulo
        ]);

        $preguntas = $encuesta->preguntas;
        $preguntas[0]->update(['descripcion' => $this->pregunta1]);
        $preguntas[1]->update(['descripcion' => $this->pregunta2]);
        $preguntas[2]->update(['descripcion' => $this->pregunta3]);
        $preguntas[3]->update(['descripcion' => $this->pregunta4]);
        $preguntas[4]->update(['descripcion' => $this->pregunta5]);

        $this->reset(['titulo', 'pregunta1', 'pregunta2', 'pregunta3', 'pregunta4', 'pregunta5']);
        $this->emit('refreshLivewireDatatable');
        $this->operacion = true;
        $this->id_editar = null;

        $this->modal_encuesta = false;

    }

    public function modalEncuesta(){
        $this->reset(['titulo', 'pregunta1', 'pregunta2', 'pregunta3', 'pregunta4', 'pregunta5']);
        $this->operacion = true;
        $this->modal_encuesta = true;
    }

     // funcion para abrir la modal para crear estudios
     public function uploadMasivaModalCorreos(){
        $this->modal_carga_masiva_correos = true;
        $this->emit('uploadMasivaCorreos');
    }

    public function nuevo(){
        $this->reset(['titulo', 'pregunta1', 'pregunta2', 'pregunta3', 'pregunta4', 'pregunta5']);
        $this->operacion = true;
    }

    public function eliminar($id){
        $this->id_eliminar = $id;
        $this->eliminar = true;
    }

    public function abrirModal($id){
        $this->form = ModelsEncuestas::find($id);
        $this->asks = ModelsEncuestas::find($id)->preguntas()->get();
        $this->modal = true;
    }

    public function abrirModalEnviar($id){
        $this->modalEnviar = true;
        $this->emit('recibir_id',$id);
    }


    public function abrirModalEnviados($id){
        $this->id_encuesta = $id;
        $this->modalEnviados = true;
        // $this->emit('recibir_id_enviados',$id);
    }

    public function cerrarModal(){
        $this->modalEnviados = false;
        $this->modalEstadisticaEncuesta = false;
        $this->modal_encuesta = false;
        $this->modal = false;
        $this->modalEnviar = false;
        $this->asks = false;
        $this->form = null;
        $this->modal_carga_masiva_correos = false;
    }

    public function siEliminar($id){
        RespuestaUsuario::where('encuesta_id',$id)->delete();
        Pregunta::where('encuesta_id',$id)->delete();
        EncuestaUsuario::where('encuesta_id',$id)->delete();
        ModelsEncuestas::find($id)->delete();
        $this->emit('refreshLivewireDatatable');
        $this->eliminar = false;
    }

    public function noEliminar(){
        $this->eliminar = false;
    }

    public function abrirModalEstadisticaEncuesta($id){
        $this->id_encuesta = $id;
        $this->modalEstadisticaEncuesta = true;
    }

    public function render()
    {
        return view('livewire.encuestas');
    }
}
