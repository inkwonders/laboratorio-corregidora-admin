<?php

namespace App\Http\Livewire;

use App\Models\Encuesta;
use App\Models\EncuestaUsuario;
use Illuminate\Support\Facades\DB;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Livewire\WithPagination;
use Mockery\Undefined;
use Illuminate\Support\Facades\Request;

class EncuestasTable extends LivewireDatatable
{

    public $exportable = false;
    use WithPagination;

    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        // return Encuesta::query()
        //     ->join('encuesta_usuario', 'encuestas.id', '=','encuesta_usuario.encuesta_id')
        //     ->orderBy('nombre','desc')
        //     ->distinct();

        return Encuesta::query()
            // ->distinct()
            ->orderBy('encuestas.id','desc');

    }

    public function columns()
    {
        return [

            NumberColumn::callback(['nombre'], function($nombre){
                return "<p class='font-bold text-blue-600'> $nombre </p>";
            })
            ->label('Título')
            ->searchable(),

            Column::callback(['enviado', 'id'], function($enviado, $id){

// dd($enviado, $id);
                $total_correos = EncuestaUsuario::all()->where('encuesta_id','=',$id)->toArray();
                $num_total = sizeof($total_correos);

                $enviados =  EncuestaUsuario::all()->where('enviado','=',1)->where('encuesta_id','=',$id)->toArray();

                $num_enviados = sizeof($enviados);

                if($enviado == 1){
                    return "<div class='flex items-center'><svg xmlns='http://www.w3.org/2000/svg' class='w-6 h-6 text-green-600' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
                        <path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z' />
                    </svg> <span class='pl-2 font-bold'>Enviados: ".$num_enviados." /".$num_total."</span></div>";
                }else{
                    return "<div class='flex items-center'><svg xmlns='http://www.w3.org/2000/svg' class='w-6 h-6 text-red-600' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
                    <path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z' />
                  </svg> <span class='pl-2 font-bold'>Sin enviar</span></div>";
                }
            })
            ->label('Enviado'),

            Column::callback(['created_at'], function($created_at){
                return date('d/m/Y h:i A', strtotime($created_at));
            })
            ->label('Fecha de creación')
            ->searchable(),

            Column::callback(['fecha_envio'], function($fecha_envio){
            if($fecha_envio == null){
                    return "Sin procesar...";
                }else{
                    return date('d/m/Y h:i A', strtotime($fecha_envio));
                }
            })
            ->label('Fecha de envio')
            ->searchable(),

            Column::callback(['id', 'enviado'], function ($id, $enviado) {
                return view('table-actions-encuestas', [
                    'id' => $id,
                    'enviado' => $enviado
                ]);
            })->label('Acciones')

        ];
    }
}
