<?php

namespace App\Http\Livewire;

use App\Models\Encuesta;
use App\Models\EncuestaUsuario;
use App\Models\Pregunta;
use App\Models\RespuestaUsuario;

use Livewire\Component;

class GraficaPieEncuestaSingle extends Component
{

    public $encuesta_id; // Recibimos este ID
    public $config;
    public $id_grafica = '';
    public $titulo;
    public $nombre;
    public $respuestas = [];
    public $preguntas = [];
    public $recuadros;
    public $respuestas_preguntas;
    public $respuestas_preguntas_uno, $respuestas_preguntas_dos, $respuestas_preguntas_tres, $respuestas_preguntas_cuatro, $respuestas_preguntas_cinco;
    public $id_preguntas;
    public $total_respuestas;
    public $contestado,$total_enviados;


    protected $listeners = ['loadChart'];

    public function mount()
    {

        $encuesta = Encuesta::find($this->encuesta_id);
        $this->nombre = $encuesta->nombre;

        $preguntas = ['Pregunta 1:','Pregunta 2:','Pregunta 3:','Pregunta 4:','Pregunta 5:'];

        // Encuesta::find($this->encuesta_id)->preguntas->map(function ($desc, $key) {
        //     // return "Pregunta " . ($key + 1) . ": " . $desc->descripcion;
        //     return "Pregunta " . ($key + 1) . ": " ;
        // })->toArray();
        if(RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)->sum('respuesta') >= 1){
            $this->total_respuestas = 100 / RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)->sum('respuesta') ;


        $this->respuestas = RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)
            ->get()
            ->groupBy('pregunta_id')
            ->map(function ($item) {
                $porcentaje_sin_dividir = $item -> sum('respuesta');
                $porcentaje =  $porcentaje_sin_dividir * $this->total_respuestas;
                return $porcentaje ;
            })->values();

        }

        $this->preguntas = Pregunta::where('encuesta_id', '=', $this->encuesta_id)
            ->get()
            ->map(function ($item) {
                return $item->descripcion;
            })->values();

        $this->id_preguntas = Pregunta::where('encuesta_id', '=', $this->encuesta_id)
            ->get()
            ->map(function ($item) {
                return $item->id;
            })->values();


        for ($i = 0; $i < 5; $i++) {

            $this->respuestas_preguntas_uno[] =  RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)
                ->where("pregunta_id", '=', $this->id_preguntas[0])
                ->where("respuesta", '=', $i+1)
                ->count();
        }

        for ($i = 0; $i < 5; $i++) {

            $this->respuestas_preguntas_dos[] =  RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)
                ->where("pregunta_id", '=', $this->id_preguntas[1])
                ->where("respuesta", '=', $i+1)
                ->count();
        }

        for ($i = 0; $i < 5; $i++) {

            $this->respuestas_preguntas_tres[] =  RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)
                ->where("pregunta_id", '=', $this->id_preguntas[2])
                ->where("respuesta", '=', $i+1)
                ->count();
        }

        for ($i = 0; $i < 5; $i++) {

            $this->respuestas_preguntas_cuatro[] =  RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)
                ->where("pregunta_id", '=', $this->id_preguntas[3])
                ->where("respuesta", '=', $i+1)
                ->count();
        }

        for ($i = 0; $i < 5; $i++) {

            $this->respuestas_preguntas_cinco[] =  RespuestaUsuario::where('encuesta_id', '=', $this->encuesta_id)
                ->where("pregunta_id", '=', $this->id_preguntas[4])
                ->where("respuesta", '=', $i+1)
                ->count();
        }


        $this->id_grafica = 'g-pie-' . uniqid();
        $this->titulo = "Respuestas de encuesta {$this->encuesta_id}";
        $this->config = collect([
            'etiquetas' => [
                [
                    'texto' => $preguntas[0],
                    'clase' => 'bg-blue-200'
                ],
                [
                    'texto' => $preguntas[1],
                    'clase' => 'bg-blue-300'
                ],
                [
                    'texto' => $preguntas[2],
                    'clase' => 'bg-blue-400'
                ],
                [
                    'texto' => $preguntas[3],
                    'clase' => 'bg-blue-500'
                ],
                [
                    'texto' => $preguntas[4],
                    'clase' => 'bg-blue-600'
                ]
            ],
            'dataset' => [
                [
                    'labels' => ['Pregunta 1', 'Pregunta 2', 'Pregunta 3', 'Pregunta 4', 'Pregunta 5'],
                    'data' => $this->respuestas,
                    'backgroundColor' => ['#c3ddfd', '#a4cafe', '#76a9fa', '#3f83f8', '#1c64f2'],
                ]
            ]
        ])->toArray();

        $this->total_enviados =  EncuestaUsuario::where('encuesta_id',$this->encuesta_id)
        ->count();

        $this->contestado =  EncuestaUsuario::where('encuesta_id',$this->encuesta_id)
        ->where('status','=','contestado')
        ->count();

    }

    public function loadChart()
    {
        $this->config['id'] = $this->id_grafica;
        $this->emit('drawChart', $this->config);
    }

    public function render()
    {
        return view('livewire.grafica-pie-encuesta-single')
            ->with(['nombre' => $this->nombre]);
    }
}
