<?php

namespace App\Http\Livewire;

use App\Models\Analisis;
use App\Models\Categoria;
use App\Rules\ValidacionPrecioAnalisis;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ViewErrorBag;
use Livewire\Component;

class ModalAnalisis extends Component
{

    // variables del formulario
    public $clave,$precio,$nombre,$estudios,$ayuno,$entrega,$indicaciones,$categoria, $identificador, $categorias,$analisis_id, $analisis, $estatus;

    // acciones de la modal
    public $agregar = false;
    public $eliminar = false;
    public $editar = false;

    //listeners enviados desde liveware.estudios
    protected $listeners = ['cambioEstatus','updateAnalisis','deleteAnalisis','createAnalisis'];

    //validaciones
    protected $rules = [
        'clave' => 'required|integer',
        'precio' => 'required|integer',
        'nombre' => 'required',
        'estudios' => 'required',
        'ayuno' => 'required',
        'entrega' => 'required',
        'indicaciones' => 'required',
        'categoria' => 'required',
    ];

    public function mount(){

        $this->categorias = Categoria::query()
        ->get();
        $this->identificador = rand();

     }


    //create
    //funcion para mostrar las opciones para crear un analisis en la modal
     public function createAnalisis()
    {
        $this->agregar = true;
    }

    public function create(){
        $this->resetErrorBag();

        $this->validate();

        $validador = Validator::make(['precio' => $this->precio], ['precio' => new ValidacionPrecioAnalisis]);
        if($validador->fails()) {
            $this->addError('precio', $validador->errors()->first());
            return;
        }

        Analisis::create([
            'clave' => $this->clave,
            'precio' => $this->precio,
            'nombre' => $this->nombre,
            'estudios' => $this->estudios,
            'ayuno' => $this->ayuno,
            'entrega' => $this->entrega,
            'indicaciones' => $this->indicaciones,
            'categoria_id' => $this->categoria
        ]);

        $this->emit('cerrarModal');

        $this->emit('refreshLivewireDatatable');

        $this->reset(['clave','precio','nombre','estudios','ayuno','entrega','indicaciones','categoria']);

    }

    //termina create

    //delete

    public function deleteAnalisis($id){

        $this->analisis_id = $id;
        $this->eliminar = true;

    }

    public function delete(){

        Analisis::find($this->analisis_id)->delete();

        $this->emit('refreshLivewireDatatable');
        $this->emit('cerrarModal');

    }

    //termina delete

    //update
    public function updateAnalisis($id){
        $this->analisis_id = $id;
        $this->analisis = Analisis::find($id);
        $this->clave = $this->analisis->clave;
        $this->precio = $this->analisis->precio;
        $this->nombre = $this->analisis->nombre;
        $this->estudios = $this->analisis->estudios;
        $this->ayuno = $this->analisis->ayuno;
        $this->entrega = $this->analisis->entrega;
        $this->indicaciones = $this->analisis->indicaciones;
        $this->categoria = $this->analisis->categoria_id;
        $this->editar = true;

    }

    public function update(){

        $this->resetErrorBag();

        $this->validate();

        $validador = Validator::make(['precio' => $this->precio], ['precio' => new ValidacionPrecioAnalisis]);
        if($validador->fails()) {
            $this->addError('precio', $validador->errors()->first());
            return;
        }

        $this->analisis = Analisis::find($this->analisis_id);


        $this->analisis->update([
            'clave' => $this->clave,
            'nombre' => $this->nombre,
            'estudios' => $this->estudios,
            'ayuno' => $this->ayuno,
            'entrega' => $this->entrega,
            'indicaciones' => $this->indicaciones,
            'precio' => $this->precio,
            // 'categoria_id' => $this->categoria,
        ]);

        $this->emit('cerrarModal');

        $this->emit('refreshLivewireDatatable');

    }

    //termina update

    //cambio estatus
    public function cambioEstatus(Analisis $analisis)
    {
        $analisis->update([
            'estatus' => ! $analisis->estatus
        ]);
    }

    public function render()
    {
        return view('livewire.modal-analisis');
    }
}
