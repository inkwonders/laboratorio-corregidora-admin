<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ModalEstadisticaEncuesta extends Component
{
    public $encuesta;

    public function render()
    {
        return view('livewire.modal-estadistica-encuesta');
    }
}
