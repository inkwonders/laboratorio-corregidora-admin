<?php

namespace App\Http\Livewire;

use App\Models\User;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Illuminate\Support\Facades\Request;

class UsuariosTable extends LivewireDatatable
{
    public $exportable = false;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        return User::query()
            ->leftJoin('model_has_roles','model_id', '=', 'users.id')
            ->join('roles','roles.id', '=', 'model_has_roles.role_id');
    }

    public function columns()
    {
        return [

            Column::callback(['name'], function($name) {
                return "{$name}";
            })
            ->label('Nombre')
            ->searchable(),

            Column::callback(['email'], function($email) {
                return "{$email}";
            })
            ->label('Email')
            ->searchable(),

            Column::name('roles.name')
            ->label('Tipo de usuario'),

            Column::callback(['id'], function ($id) {
                return view('table-actions-usuarios', ['id' => $id]);
            })->label('Acciones')

        ];

    }

    // public function getRolesProperty()
    // {
    //     return HasRoles::pluck('name');
    // }

}
