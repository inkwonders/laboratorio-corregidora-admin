<?php

namespace App\Http\Livewire;

use App\Models\Orden;
use Livewire\Component;

class Ordenes extends Component
{

    public $modal = false;
    public $orden;

    protected $listeners = ['modal','cerrarModal'];

    public function modal(Orden $orden){
        $this->modal = true;
        $this->orden = $orden;
    }

    public function cerrarModal(){
        $this->modal = false;
    }

    public function render()
    {
        return view('livewire.ordenes');
    }
}
