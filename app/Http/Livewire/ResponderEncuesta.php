<?php

namespace App\Http\Livewire;

use App\Models\Encuesta;
use App\Models\EncuestaUsuario;
use App\Models\RespuestaUsuario;
use Livewire\Component;

class ResponderEncuesta extends Component
{
    public $pruebas = 4;
    public $encuesta;
    public $datos_encuesta_usuario;
    public $encuesta_id;
    public $usuario_id;
    public $datos_encuesta;
    public $titulo_encuesta;
    public $preguntas = [];
    public $error;
    public $datos_guardar = [];
    public $contador = 0;
    public $contestado;

    protected $listeners = ['enviar_encuesta'];

    public function mount()
    {
          // Decodificar el JSON en un objeto
          $cadena = json_decode($this->encuesta);

          // Acceder al valor del campo "id"
          $id = $cadena->id;
        //   dd($id);

        $this->datos_encuesta_usuario = EncuestaUsuario::query()
        ->where('id', '=',  $id)
        ->get();
// dd($this->datos_encuesta_usuario);

        foreach ($this->datos_encuesta_usuario as $datos) {
            $this->encuesta_id = $datos->encuesta_id;
            $this->usuario_id = $datos->usuario_id;
        }

        $this->datos_encuesta = Encuesta::query()
            ->join('preguntas', 'preguntas.encuesta_id', '=', 'encuestas.id')
            ->where('encuestas.id', '=', $this->encuesta_id)
            ->get();

        foreach ($this->datos_encuesta as $datos_mostrar) {
            $this->titulo_encuesta = $datos_mostrar->nombre;
            $this->preguntas[] = ["id_pregunta" => $datos_mostrar->id, "descripcion_preguntas" =>  $datos_mostrar->descripcion];
        }
    }


    public function enviar_encuesta($arreglo)
    {
          // Decodificar el JSON en un objeto
          $cadena = json_decode($this->encuesta);

          // Acceder al valor del campo "id"
          $id = $cadena->id;
        //   dd($id);

        if (count($arreglo) == 5) {

            foreach ($this->preguntas as $pregunta) {
                $this->datos_guardar[] = ['encuesta_id' => $this->encuesta_id, "id_pregunta" => $pregunta['id_pregunta'], 'usuario_id' => $this->usuario_id, 'respuesta' => $arreglo[$this->contador]];
                $this->contador++;
            }

            $this->contestado = 'ok';

            foreach ($this->datos_guardar as $key => $value) {
                RespuestaUsuario::create([
                    'encuesta_id' => $value['encuesta_id'],
                    'pregunta_id' => $value['id_pregunta'],
                    'users_id' => $value['usuario_id'],
                    'respuesta' => $value['respuesta'],
                ]);
            }

            EncuestaUsuario::where('id', '=', $id)
                ->update(['status' => 'contestado']);

            $this->error = '';
        } else {

            $this->error = 'Todos los campos son obligatorios';
        }
    }

    public function render()
    {
        return view('livewire.responder-encuesta');
    }
}
