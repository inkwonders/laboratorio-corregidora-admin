<?php

namespace App\Http\Livewire;

use App\Models\Analisis;
use App\Models\CarritoMedico;
use App\Models\CarritoMedicoAnalisis;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Carrito extends Component
{
    public $user;
    public $carrito;
    public $carrito_medico_analisis;

    protected $listeners = ['incrementar', 'decrementar', 'eliminarAnalisis', 'vaciarCarrito'];

    public function mount()
    {
        $this->user = Auth::user();
        $this->refrescar();

    }

    public function refrescar()
    {
        $this->carrito = $this->user->carrito()->activo()->with('analisis')->firstOrCreate();
        $this->emit('refreshCarritoHeader');
    }

    public function incrementar($analisis_id, $cantidad)
    {
        $this->carrito->analisis()->updateExistingPivot($analisis_id, ['cantidad' => $cantidad + 1]);
        $this->refrescar();
    }

    public function decrementar($analisis_id, $cantidad)
    {
        if ($cantidad > 1) {
            $this->carrito->analisis()->updateExistingPivot($analisis_id, ['cantidad' => $cantidad - 1]);
            $this->refrescar();
        }
    }

    public function eliminarAnalisis($id)
    {
        $this->carrito->analisis()->detach($id);
        $this->refrescar();

        if ($this->carrito->analisis->isEmpty()) {
            $this->carrito->update([
                'nombre_paciente' => null,
                'email_paciente' => null,
                'telefono_paciente' => null
            ]);
        }
    }

    public function vaciarCarrito()
    {

        $this->carrito->update([
            'nombre_paciente' => null,
            'email_paciente' => null,
            'telefono_paciente' => null
        ]);

        $this->carrito->analisis()->each(function ($analisis) {
            $this->carrito->analisis()->detach($analisis->id);
        });
        $this->refrescar();
    }

    public function render()
    {
        return view('livewire.carrito');
    }
}
