<?php

namespace App\Http\Livewire;

use App\Models\Analisis;
use Livewire\Component;

class Estudios extends Component
{
    // modal
    public $modal = false;
    public $modal_carga_masiva = false;

    // id analisis
    public $analisis_id;

    // listeners
    protected $listeners = ['agregarModal','cerrarModal','modalEliminar','modalEditar','modalCambioEstatus','uploadMasivaModal'];

    // funcion para abrir la modal para crear estudios
    public function agregarModal(){
        $this->modal = true;
        $this->emit('createAnalisis');
    }

    // funcion para abrir la modal para crear estudios
    public function uploadMasivaModal(){
        $this->modal_carga_masiva = true;
        $this->emit('uploadMasivaAnalisis');
    }

    // funcion para abrir la modal para eliminar estudios
    public function modalEliminar($id){
        $this->modal = true;
        $this->emit('deleteAnalisis',$id);
    }

    // funcion para abrir la modal para editar estudios
    public function modalEditar($id){
        $this->modal = true;
        $this->emit('updateAnalisis',$id);
    }

    public function modalCambioEstatus($estatus){
        $this->modal = true;
        $this->emit('cambioEstatus',$estatus);
    }

    // funcion para cerrar las modales
    public function cerrarModal(){
        $this->modal = false;
        $this->eliminar = false;
        $this->editar = false;
        $this->modal_carga_masiva = false;
    }

    public function render()
    {
        return view('livewire.analisis');
    }
}
