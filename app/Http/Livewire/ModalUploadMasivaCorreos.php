<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ListaCorreoEncuestaImport;
use Livewire\WithFileUploads;
use App\Models\ListaCorreoEncuesta;

class ModalUploadMasivaCorreos extends Component
{
    public $agregar_masiva_correos = false;
    public $updateAnalisis;
    public $archivo = null;
    use WithFileUploads;


    protected $listeners = ['uploadMasivaModalCorreos'];



    protected $rules = [
        'archivo' => 'required',

    ];

    public function create() {

        $folios = Excel::toCollection(new ListaCorreoEncuestaImport, $this->archivo->getRealPath())
        ->first() // Primera hoja
        ->skip(1) // Omitimos la primer fila
        ->mapWithKeys(function($row) {


            $folio=$row[0];//agregar al final una columna con autoincremental en el excel

            return [
                $folio => [
                    // 'd_id' => $row[0],
                    'd_nombre'  => $row[1],
                    'd_correo'=>$row[2]

                ]
            ];


        })
        ->whereNotNull();

        // dd($folios);

        foreach ($folios as $folio) {

            $busqueda_si_el_correo_existe = ListaCorreoEncuesta::where('correo',$folio['d_correo'])->first();

            if( $busqueda_si_el_correo_existe == null ||  $busqueda_si_el_correo_existe =""){
                // $id=$folio['d_id'];
                // $busqueda=ListaCorreoEncuesta::where('id',$id)->first();

                $nombre=$folio['d_nombre'];
                $correo=$folio['d_correo'];

                ListaCorreoEncuesta::create(
                            [
                                'nombre' => $nombre,
                                'correo' => $correo

                            ]

                        );
            }
            else {

            }


        }


        $this->emit('cerrarModal');
        $this->emit('refreshLivewireDatatable');
    }


    public function uploadMasivaCorreos()
    {
        $this->agregar_masiva_correos = true;
    }

    public function render()
    {
        return view('livewire.modal-upload-masiva-correos');
    }
}
