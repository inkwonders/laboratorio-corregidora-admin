<?php

namespace App\Http\Livewire;

use App\Models\Orden;
use Livewire\Component;

class ModalOrdenes extends Component
{
    public $orden;

    public function render()
    {
        return view('livewire.modal-ordenes');
    }

}
