<?php

namespace App\Http\Livewire;

use App\Models\CitaImagenologico;
use Illuminate\Support\Facades\Request;

use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;

class CitasMananaImagenologiaTable extends LivewireDatatable
{

    public $ruta_actual;
    public $exportable = false;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        return CitaImagenologico::whereRaw('TO_DAYS(fecha_cita) = TO_DAYS(CURRENT_DATE + 1)')
            ->leftJoin('schedule_images', 'citas_imagenologicos.scheduleimage_id', '=', 'schedule_images.id')
            ->leftJoin('sucursales', 'citas_imagenologicos.sucursal', '=', 'sucursales.id');
    }

    public function columns()
    {
        return [

            Column::checkbox(),

            NumberColumn::callback(['id'], function($id){
                return "<p class='font-bold text-blue-600'> $id </p>";
            })
            ->label('Id.')
            ->searchable(),

            Column::callback(['nombre'], function($nombre) {
                return "{$nombre}";
            })
            ->label('Nombre Paciente')
            ->searchable(),

            Column::callback(['telefono'], function($telefono) {
                return "{$telefono}";
            })
            ->label('Teléfono')
            ->searchable(),

            Column::callback(['sucursales.sucursal'], function($sucursal) {
                return "{$sucursal}";
            })
            ->label('Sucursal')
            ->searchable(),


            Column::callback(['correo'], function($correo) {
                return "{$correo}";
            })
            ->label('Correo')
            ->searchable(),

            Column::callback(['fecha_cita'], function($fecha_cita) {
                return "{$fecha_cita}";
            })
            ->label('Fecha cita')
            ->searchable(),

            Column::callback(['schedule_images.horario_inicial', 'schedule_images.horario_final'], function($hora_inicial, $hora_final) {
                $hi = date('h:i A', strtotime($hora_inicial));
                $hf = date('h:i A', strtotime($hora_final));
                return "{$hi} - {$hf}";
            })
            ->label('Horario de cita')
            ->searchable(),

            Column::callback(['created_at'], function($created_at) {
                return "{$created_at}";
            })
            ->label('Fecha de registro')
            ->searchable()

        ];
    }
}
