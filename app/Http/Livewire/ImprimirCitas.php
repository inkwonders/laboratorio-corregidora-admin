<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Cita;

class ImprimirCitas extends Component
{

    public $modal = false;
    public $seleccionados = [];
    public $respuestas = null;

    protected $listeners = ['mostrarModalImprimir', 'cerrarModalImprimir'];

    public function query(){

        if(count($this->seleccionados) > 0){
            $consulta = cita::query()
            ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'paciente_email',  'tel_celular',  'fecha_seleccionada', 'costo', 'tipo_tarjeta', 'digitos')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');
            foreach($this->seleccionados as $id){
                $consulta->orWhere('citas.id','=',$id);
            }
        }else{
            $consulta = cita::query()
            ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'paciente_email',  'tel_celular',  'fecha_seleccionada', 'costo', 'tipo_tarjeta', 'digitos')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id')
            ->where('pagado', '=', 1);
        }
        $this->respuestas = $consulta->get()->toArray();
    }

    public function mostrarModalImprimir($datos){
        $this->seleccionados = $datos;
        $this->modal = true;
        $this->query();
    }

    public function cerrarModalImprimir(){
        $this->modal = false;
    }

    public function render()
    {
        return view('livewire.imprimir-citas');
    }
}
