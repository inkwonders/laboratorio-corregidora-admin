<?php

namespace App\Http\Livewire;

use Livewire\Component;

class GraficaLine extends Component
{
    public $config;
    public $titulo;
    public $etiquetas;
    public $data;
    public $id_grafica = '';

    protected $listeners = ['loadChartLine'];

    public function __construct()
    {
        $this->id_grafica = 'g-pie-' . uniqid();
    }

    public function mount() {
        if(isset($this->config) && $this->config != null) {
            $this->titulo = $this->config['titulo'];
            $this->etiquetas = $this->config['etiquetas'];
            $this->data= $this->config['data'];
        }
    }

    public function loadChartLine() {
        $this->emit('drawChartLine', $this->id_grafica, $this->data);
    }

    public function render()
    {
        return view('livewire.grafica-line');
    }
}
