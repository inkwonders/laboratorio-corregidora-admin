<?php

namespace App\Http\Livewire;

use App\Http\Controllers\SendMailController;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use App\Models\Analisis;

class CarritoHeader extends Component
{
    public $carrito;

    protected $listeners = ['refreshCarritoHeader','agregarCarrito', 'resetCarrito'];

    public function resetCarrito($id_reimprecion){


        $si_activo = $this->carrito->find($id_reimprecion);

        if($si_activo->activo == 1){
            $enviar_correo = new SendMailController;
            $enviar_correo->correo_comprobante($si_activo,$this->carrito->total);

            $si_activo->update([
                'activo' => 0
            ]);

            $this->refreshCarritoHeader();

            return redirect()->route('analisis.panel');

        }else{

            return redirect()->route('analisis.historicomedico');

        }

    }

    public function agregarCarrito(Analisis $estudio)
    {
        $estudio_existente = $this->carrito->analisis()->wherePivot('analisis_id', $estudio->id)->first();
        if($estudio_existente) {
            $this->incrementar($estudio->id, $estudio_existente->pivot->cantidad);
        }else{
            $this->carrito->analisis()->attach($estudio->id, ['cantidad' => 1 , 'created_at' => date('Y-m-d H:i:s')] );
        }
        $this->emit('refreshCarritoHeader');
        $this->emit('modalNotificarEstudioAgregado');
    }

    public function incrementar($analisis_id, $cantidad_actual) {
        $this->carrito->analisis()->updateExistingPivot($analisis_id, ['cantidad' => $cantidad_actual + 1, 'updated_at' => date('Y-m-d H:i:s')]);
    }

    public function refreshCarritoHeader() {
        $this->carrito = Auth::user()->carrito()->activo()->with('analisis')->firstOrCreate();
    }

    public function mount() {
        $this->refreshCarritoHeader();
    }

    public function render()
    {
        return view('livewire.carrito-header');
    }
}
