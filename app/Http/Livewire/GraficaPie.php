<?php

namespace App\Http\Livewire;

use Livewire\Component;

class GraficaPie extends Component
{
    public $config;
    public $titulo;
    public $etiquetas;
    public $dataset;
    public $id_grafica = '';

    protected $listeners = ['loadChart'];

    public function mount() {
        $this->id_grafica = 'g-pie-' . uniqid();
        if(isset($this->config) && $this->config != null) {
            $this->titulo = $this->config['titulo'];
            $this->etiquetas = $this->config['etiquetas'];
            $this->dataset = $this->config['dataset'];
        }
    }

    public function loadChart() {
        $this->config['id'] = $this->id_grafica;
        $this->emit('drawChart', $this->config);
    }

    public function render()
    {
        return view('livewire.grafica-pie');
    }
}
