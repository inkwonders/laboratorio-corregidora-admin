<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DatosAnalisis extends Component
{
    public $analisis;

    public function render()
    {
        return view('livewire.datos-analisis');
    }
}
