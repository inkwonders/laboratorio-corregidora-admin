<?php

namespace App\Http\Livewire;

use App\Models\CarritoMedico;
use App\Models\CarritoMedicoAnalisis;
use Illuminate\Support\Facades\Auth;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class HistoricoMedicoTable extends LivewireDatatable
{

    public $exportable = false;
    public $ruta_actual;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
    }

    public function builder()
    {
        return CarritoMedico::query()
            ->where('activo', 0)
            ->where('user_id', Auth::user()->id)
            // ->join('carrito_medico_analisis', 'carrito_medico_id', '=', 'carrito_medico_analisis.carrito_medico_id')
            // ->join('analisis', 'analisis.id', '=', 'carrito_medico_analisis.analisis_id')
            ->distinct();


    }

    public function columns()
    {

        return[

            NumberColumn::callback(['no_confirmacion'], function($no_confirmacion){
                return "<p class='font-bold text-blue-600'> $no_confirmacion </p>";
            })
            ->label('No. Confirmación')
            ->searchable(),

            Column::callback(['nombre_paciente'], function($nombre_paciente) {
                return "{$nombre_paciente}";
            })
            ->label('Nombre')
            ->searchable(),

            Column::callback(['telefono_paciente'], function($telefono_paciente) {
                return "{$telefono_paciente}";
            })
            ->label('Teléfono')
            ->searchable(),

            Column::callback(['email_paciente'], function($email_paciente) {
                return "{$email_paciente}";
            })
            ->label('Email')
            ->searchable(),

            Column::callback(['created_at'], function($created_at) {
                return date("d M Y", strtotime($created_at));
            })
            ->label('Fecha de creación')
            ->searchable(),

            Column::callback(['id'], function ($id) {
                return view('table-actions', [
                    'id' => $id,
                    'ruta_actual' => $this->ruta_actual
                ]);
            })->label('Acciones')

        ];
    }
}
