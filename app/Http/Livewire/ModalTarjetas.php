<?php

namespace App\Http\Livewire;

use App\Http\Controllers\AESCrypto;
use App\Models\ResponseTarjeta;
use App\Models\Cita;
use Livewire\Component;

class ModalTarjetas extends Component
{
    protected $listeners = ['verCadena'];

    public $key = "DD33CEF1A4A0DDF130996A2BD052486D";
    public $cadena;
    public $xml;

    public $numero_confirmacion_mostrar,$tipo_pago,$reference,$response,$time,$date,$foliocpagos,$auth,$cc_type,$amount,$email;

    // pago de citas
    public $concepto, $fecha_cita, $horario, $numero;

    // pago de analisis
    public $nombre, $telefono, $estudios_cantidad, $cantidad;



    public function verCadena($numero_confirmacion){
        $this->response = ResponseTarjeta::where('numero_confirmacion', $numero_confirmacion)->first();

        if (strlen($this->response->response) > 0) {
            $this->cadena = AESCrypto::desencriptar($this->response->response,$this->key);
            // $this->xml = simplexml_load_string($this->cadena);
            $this->xml = collect(simplexml_load_string($this->cadena));
            //if(isset($this->xml['reference']) == 'undefined'){
                //$this->reference = "no disponible";
            //}else{
            $this->reference =  (string)$this->xml['reference'];
            $this->response =   (string)$this->xml['response'];
            //}
    
            if ($this->response == 'approved') {
                $this->time =  (string)$this->xml['time'];
                $this->date =  (string)$this->xml['date'];
            } else {
                $this->time =  '';
                $this->date =  '';
            }
            $this->foliocpagos =  (string)$this->xml['foliocpagos'];
            $this->auth =  (string)$this->xml['auth'];
            $this->cc_type =  (string)$this->xml['cc_type'];
            $this->amount =  (int)$this->xml['amount'];
            $this->email =  isset($this->xml['email']) ? (string)$this->xml['email'] : '';
            $this->tipo_pago =  isset($this->xml['datos_adicionales']->data[5]->value) ? (string)$this->xml['datos_adicionales']->data[5]->value : '';
            $this->numero_confirmacion_mostrar = (string)$this->xml['datos_adicionales']->data[0]->value;

            if ($this->tipo_pago == 1) {
                $this->concepto = (string)$this->xml['datos_adicionales']->data[1]->value;
                $this->fecha_cita = (string)$this->xml['datos_adicionales']->data[2]->value;
                $this->horario = (string)$this->xml['datos_adicionales']->data[3]->value;
                $this->numero = (string)$this->xml['datos_adicionales']->data[4]->value;
            } else {
                $this->nombre = (string)$this->xml['datos_adicionales']->data[1]->value;
                $this->telefono = (string)$this->xml['datos_adicionales']->data[2]->value;
                $this->estudios_cantidad = (string)$this->xml['datos_adicionales']->data[3]->value;
                $this->estudios_cantidad = explode('@',$this->estudios_cantidad);
                $this->cantidad =  count(array_filter($this->estudios_cantidad));
            }
        } else {
            $this->reference = $this->response->numero_confirmacion;
            // $this->response =

            $cita = Cita::where("no_confirmacion", $this->response->numero_confirmacion)->first();

            $this->date = date(
                "d/m/Y",
                strtotime($this->response->fecha." ".$this->response->hora)
            );

            $this->time = (string)$this->response->hora;

            $this->foliocpagos = $this->response->foliocpagos;
            $this->auth = $this->response->autorizacion;
            // $this->cc_type =
            $this->amount = $this->response->importe;
            $this->email = $cita->paciente->paciente_email;
            $this->tipo_pago = $this->response->tipo;
            $this->numero_confirmacion_mostrar = $this->response->numero_confirmacion;

            $this->concepto = $cita->tipo_prueba == 1? "PRUEBA DE PCR PARA COVID-19 (SARS-Cov-2) EN MUESTRA NASO FARINGEA" : "PRUEBA DE ANTÍGENOS PARA COVID-19 (SARS-Cov-2)";

            $this->horario = date(
                "h:i A",
                strtotime($cita->fecha_seleccionada." ".$cita->horarios->hora_inicial)
            )." / ".date(
                "h:i A",
                strtotime($cita->fecha_seleccionada." ".$cita->horarios->hora_final)
            );

            $costo = $cita->tipo_prueba == 1? 2390 : 506;
            $importe = intval($this->response->importe);

            $this->numero = $importe / $costo;

            $this->response = $this->response->estatus;
        }
    }

    public function render()
    {
        return view('livewire.modal-tarjetas');
    }
}
