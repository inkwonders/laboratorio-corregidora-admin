<?php

namespace App\Http\Livewire;

use App\Http\Controllers\SendMailController;
use App\Models\Encuesta;
use App\Models\EncuestaUsuario;
use App\Models\ListaCorreoEncuesta;
use App\Models\Orden;
use App\Models\Paciente;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Spatie\Permission\Models\Role;


class ModalEnviarEncuesta extends Component
{
    public $paciente = '';
    public $citas = '';
    public $analisis = '';
    public $lista_de_correos = '';
    public $consulta = [];
    public $pacientes_encuesta = [];
    public $id_encuesta;
    public $validado = false;
    public $agregar_correo, $correo_repetido;
    protected $listeners = ['recibir_id'];

    public function recibir_id($id_encuesta)
    {
        return $this->id_encuesta = $id_encuesta;
    }

    public function updated($name, $value)
    {

        if ($name == "paciente") {

            if ($value == '') {
                $this->consulta = [];
            } else {
                $this->consulta = User::where("name", "LIKE", "%{$value}%")
                    ->orWhere("email", "LIKE", "%{$value}%")
                    ->take(5)
                    ->distinct('email')
                    ->get();
            }
        }

        if ($name == "citas") {

            if (!$value) {

                $this->consulta = Paciente::all()
                    ->unique('paciente_email');

                foreach ($this->consulta as $paciente) {
                    $id = array_search($paciente->paciente_email, $this->pacientes_encuesta);
                    $this->quitar($id);
                }

                $this->consulta = [];
            } else {

                $this->consulta = Paciente::all()
                    ->unique('paciente_email');

                foreach ($this->consulta as $paciente) {
                    $this->agregar($paciente->paciente_email, $paciente->id);
                }
            }
        }

        if ($name == "analisis") {

            if (!$value) {

                $this->consulta = Orden::all()
                    ->unique('email_paciente');

                foreach ($this->consulta as $paciente) {
                    $id = array_search($paciente->email_paciente, $this->pacientes_encuesta);
                    $this->quitar($id);
                }

                $this->consulta = [];
            } else {

                $this->consulta = Orden::all()
                    ->unique('email_paciente');

                foreach ($this->consulta as $paciente) {
                    $this->agregar($paciente->email_paciente, $paciente->id);
                }
            }
        }

        if ($name == "lista_de_correos") {

            if (!$value) {

                $this->consulta = ListaCorreoEncuesta::all()
                    ->unique('correo');


                foreach ($this->consulta as $paciente) {
                    $id = array_search($paciente->correo, $this->pacientes_encuesta);
                    $this->quitar($id);

                }

                $this->consulta = [];
            } else {

                $this->consulta = ListaCorreoEncuesta::all()
                    ->unique('correo');

                foreach ($this->consulta as $paciente) {
                    // dd($paciente);
                    $this->agregar($paciente->correo, $paciente->id);
                }
            }
        }


        $this->validar_array();
    }

    public function validar_array()
    {
        if (sizeof($this->pacientes_encuesta) != 0) {
            $this->validado = true;
        } else {
            $this->validado = false;
        }
    }


    public function quitar($id)
    {
        array_splice($this->pacientes_encuesta, $id, 1);

        if (!$this->pacientes_encuesta) {
            $this->citas = false;
            $this->analisis = false;
            $this->lista_de_correos = false;
        }

        $this->validar_array();
    }

    public function agregar($email, $id)
    {
        if (false === array_search($email, $this->pacientes_encuesta)) {
            array_push($this->pacientes_encuesta, ['id' => $id, 'email' => $email]);

            // $this->pacientes_encuesta[] = $email;

            $this->paciente = '';
            $this->consulta = [];
        }


        $this->validar_array();
    }

    public function enviar_correo()
    {
        $id = null;
        foreach ($this->pacientes_encuesta as $key => $value) {

            $id = $value['id'];
            EncuestaUsuario::create([
                'encuesta_id' => $this->id_encuesta,
                'usuario_id' => $value['id'],
                'paciente_email' => $value['email'],
                'enviado' => '0',
                'status' => 'NO CONTESTADO'
            ]);
        }

        // $encuesta = Encuesta::find($this->id_encuesta);


        // $encuesta->update([
        //     'enviado' => 1,
        //     'fecha_envio' => date('Y-m-d H:i:a')
        // ]);



        $this->emit('refreshLivewireDatatable');

        $this->emit('cerrarModal');


        $enviar_correo = new SendMailController;
        $enviar_correo->revisarCorreos($this->id_encuesta, $id);
    }


    public function agregar_correos()
    {
        $this->validate([
            'agregar_correo' => 'required|email',
        ]);


        if (in_array($this->agregar_correo, array_column($this->pacientes_encuesta, 'email')) != true) {

            $existe = User::where('email', '=', $this->agregar_correo)->get();
            if (count($existe) != 0) {
                $this->agregar($existe[0]->email, $existe[0]->id);
            } else {

                User::create([
                    'name' => $this->agregar_correo,
                    'email' => $this->agregar_correo,
                ])->assignRole(
                    Role::where('id', 3)->first()->name
                );
                $buscar = User::where('email', '=', $this->agregar_correo)->get();

                $this->agregar($buscar[0]->email, $buscar[0]->id);
            }
            $this->correo_repetido = '';
        } else {
            $this->correo_repetido = 'El correo ya se encuentra en la lista de remitentes';
        }
    }


    public function render()
    {
        return view('livewire.modal-enviar-encuesta');
    }
}
