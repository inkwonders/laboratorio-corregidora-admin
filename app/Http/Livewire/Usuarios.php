<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Usuarios extends Component
{
    public $modal = false;
    public $eliminar = false;
    public $user_id;

    protected $listeners = ['cerrarModal', 'editar','modalEliminar','eliminarUsuario'];

    public function abrirModal(){
        $this->modal = true;
    }

    public function cerrarModal(){
        $this->modal = false;
        $this->eliminar = false;
    }

    public function editar($id){
        $this->emit('editarModal', $id);
        $this->abrirModal();
    }

    public function modalEliminar($id){
        $this->eliminar = true;
        $this->user_id = $id;
    }

    public function eliminarUsuario(){
        User::find($this->user_id)->delete();
        $this->emit('refreshLivewireDatatable');
    }



    public function render()
    {
        return view('livewire.usuarios');
    }
}
