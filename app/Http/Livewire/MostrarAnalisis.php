<?php

namespace App\Http\Livewire;
use Livewire\Component;
use App\Models\Analisis;
use App\Models\Categoria;
use Illuminate\Support\Facades\Auth;

class MostrarAnalisis extends Component
{

    public $search;
    public $tipo = '1';
    public $datosEstudio = null;
    public $categorias = '1';
    public $analisis;
    public $estudios;
    public $carrito;

    public function mount(){
        $this->carrito = Auth::user()->carrito()->activo()->firstOrCreate();

        $this->estudios = collect();
        $this->categorias = Categoria::all();
        $this->analisis = Analisis::where('nombre', 'like', '%'.$this->search.'%')
        ->Where('categoria_id', '=', $this->tipo)
        ->Where('estatus','=','1')
        ->get();
    }

    public function updatedSearch(){
        $this->analisis = null;
        $this->analisis = Analisis::where('nombre', 'like', '%'.$this->search.'%')
        ->Where('categoria_id', '=', $this->tipo)
        ->get();
    }

    public function seleccionarCategoria(Categoria $categoria){
        $this->analisis = $categoria;
        $this->analisis = Analisis::where('nombre', 'like', '%'.$this->search.'%')
        ->Where('categoria_id', '=', $this->tipo)
        ->get();
    }

    public function mostrarEstudio(Analisis $datosEstudio) {
        $this->datosEstudio = $datosEstudio;
    }

    public function cerrarModalEstudio() {
        $this->datosEstudio = null;
    }

    public function render(){
        return view('livewire.mostrar-analisis');
    }

}
