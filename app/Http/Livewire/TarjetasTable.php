<?php

namespace App\Http\Livewire;

use App\Models\ResponseTarjeta;
use Illuminate\Support\Facades\DB;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Illuminate\Support\Facades\Request;
use Mediconesystems\LivewireDatatables\Http\Livewire\LabelColumn;

class TarjetasTable extends LivewireDatatable
{

    public $exportable = false;
    public $plataforma;
    public $fecha_start, $fecha_end;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {

        return ResponseTarjeta::query();
    }

    public function updatedFechaStart($value){
        $this->emit('fecha_inicio', $value);
    }

    public function updatedFechaEnd($value){
        $this->emit('fecha_fin', $value);
    }

    public function columns()
    {
        return [
            Column::callback(['foliocpagos'], function ($foliocpagos) {
                return "<p class='text-blue-600 whitespace-pre-line'> $foliocpagos </p>";
            })
                ->label('No. Operación')
                ->searchable(),

            Column::callback(['numero_confirmacion'], function ($numero_confirmacion) {
                    return "<p class='text-blue-600 whitespace-pre-line'> $numero_confirmacion </p>";
            })
                    ->label('REFERENCIA')
                    ->searchable(),

            // Column::callback(['tipo'], function ($tipo) {
            //     switch ($tipo) {
            //         case '1':
            //             $this->plataforma = 'Pago cita covid';
            //             break;
            //         case '2':
            //             $this->plataforma = 'Pago Análisis';
            //             break;
            //     }
            //     return "<p> $this->plataforma </p>";
            // })
            //     ->label('Plataforma pago'),

            Column::callback(['tipo_pago'], function ($tipo_pago) {
                return "<p class='whitespace-pre-line'> $tipo_pago </p>";
            })
                ->label('Tipo de pago'),

            Column::callback(['tarjeta_digitos'], function ($tarjeta_digitos) {
                return "<p class='whitespace-pre-line'> $tarjeta_digitos </p>";
            })
                ->label('# Tarjeta')
                ->searchable(),

            Column::callback(['autorizacion'], function ($autorizacion) {
                return "<p class='whitespace-pre-line'> $autorizacion </p>";
            })
                ->label('Autorización')
                ->searchable(),

            Column::callback(['tipo_transaccion'], function ($tipo_transaccion) {
                return "<p class='whitespace-pre-line'> $tipo_transaccion </p>";
            })
                ->label('Tipo de transacción'),

            Column::callback(['importe'], function ($importe) {
                return "<p class=''> $ " . number_format(floatval($importe), 2) . "</p>";
            })
                ->label('Importe'),

            DateColumn::name('fecha')
                ->filterable()
                ->label('Fecha'),

            Column::callback(['hora'], function ($hora) {
                return "<p class=''>" . date('h:i A', strtotime($hora)) . "</p>";
            })
                ->label('Hora'),

            Column::callback(['banco_emisor'], function ($banco_emisor) {
                    return "<p class=''>$banco_emisor</p>";
                })
                    ->label('Banco emisor'),

            Column::callback(['tarjeta'], function ($tarjeta) {
                        return "<p class=''>$tarjeta</p>";
                    })
                        ->label('Tarjeta'),

            Column::callback(['tipo_tarjeta'], function ($tipo_tarjeta) {
                return "<p class=''>$tipo_tarjeta</p>";
            })
                ->label('Tipo de Tarjeta'),


            Column::callback(['estatus'], function ($estatus) {
                return "<p class=''>$estatus</p>";
            })
                ->label('Estatus'),

          

            Column::callback(['created_at'], function ($created_at) {
                return "<p>" . date('d M Y', strtotime($created_at)) . "</p>";
            })

                ->label('Fecha de creacion')
                ->searchable(),


            Column::callback(['response', 'numero_confirmacion'], function ($response, $numero_confirmacion) {
                return view('action-tarjetas', [
                    'response' => $response,
                    'numero_confirmacion' => $numero_confirmacion
                ]);
            })->label('Ver cadena')


        ];
    }
}
