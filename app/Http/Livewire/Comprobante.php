<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Comprobante extends Component
{
    public $carrito;

    protected $listeners = ['imprimir'];

    public function imprimir(){

    }

    public function render()
    {
        return view('livewire.comprobante');
    }

}
