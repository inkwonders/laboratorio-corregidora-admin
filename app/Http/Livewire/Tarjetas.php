<?php

namespace App\Http\Livewire;

use App\Models\ResponseTarjeta;
use Livewire\Component;
class Tarjetas extends Component
{
    public $modal = false;
    public $numero_confirmacion;
    public $total;

    public $fecha_start = null;
    public $fecha_end = null;

    protected $listeners = ['openModal', 'cerrarModal', 'fecha_inicio', 'fecha_fin'];

    public function fecha_inicio($value){
        $this->fecha_start = $value;
    }

    public function fecha_fin($value){
        $this->fecha_end = $value;
    }

    public function mount(){
        $this->total = ResponseTarjeta::sum('importe');
    }

    public function openModal($numero_confirmacion)
    {
        $this->modal = true;
        $this->emit('verCadena', $numero_confirmacion);
    }

    public function cerrarModal()
    {

        $this->modal = false;
    }

    public function render()
    {
        return view('livewire.tarjetas');
    }
}
