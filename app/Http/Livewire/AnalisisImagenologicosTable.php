<?php

namespace App\Http\Livewire;

use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use App\Models\AnalisisImagenologico;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Illuminate\Support\Facades\Request;

class AnalisisImagenologicosTable extends LivewireDatatable
{
    public $exportable = false;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        return AnalisisImagenologico::query()
            ->leftJoin('tipoestudio_images', 'analisis_imagenologicos.tipoestudioimagelologicos_id', '=', 'tipoestudio_images.id');
    }


    public function columns()
    {
        return [

            Column::callback(['clave'], function ($clave) {
                return "<p class='font-bold text-blue-600'> $clave </p>";
            })
                ->label('Clave')
                ->searchable(),

            Column::callback(['nombre'], function ($nombre) {
                return "<p> $nombre </p>";
            })
                ->label('Nombre')
                ->searchable(),

            // Column::callback(['descripcion'], function ($descripcion) {
            //     return "<p class='overflow-hidden truncate whitespace-pre-line text-clip'> $descripcion </p>";
            // })
            //     ->label('Descripcion')
            //     ->searchable(),

            Column::callback(['indicaciones'], function ($indicaciones) {
                return "<p class='truncate whitespace-pre-line '> $indicaciones </p>";
            })
                ->label('Indicaciones')
                ->searchable(),

            Column::callback(['costo', 'estimado_costo'], function ($costo, $estimado_costo) {
                if($costo == null){
                    return "<p > " . $estimado_costo . " </p>";
                }else{
                    return "<p > $" . number_format((float)$costo, 2) . " </p>";
                }
            })
                ->label('Costo'),

            // Column::callback(['tiempo_de_entrega'], function ($tiempo_de_entrega) {
            //     return "<p class='truncate whitespace-pre-line'> $tiempo_de_entrega </p>";
            // })
            //     ->label('Tiempo de entrega')
            //     ->searchable(),

            // Column::callback(['nombres_alternativos'], function ($nombres_alternativos) {
            //     return "<p class='whitespace-pre-line '> $nombres_alternativos </p>";
            // })
            //     ->label('Nombres alternativos')
            //     ->searchable(),

            // Column::callback(['contacto'], function ($contacto) {
            //     return "<p class='overflow-hidden truncate whitespace-pre-line text-ellipsis'> $contacto </p>";
            // })
            //     ->label('Contacto')
            //     ->searchable(),

            // Column::callback(['slug'], function ($slug) {
            //     return "<p class='whitespace-pre-line '> $slug </p>";
            // })
            //     ->label('Slug')
            //     ->searchable(),

            Column::callback(['created_at'], function ($created_at) {
                return "<p class='whitespace-pre-line '> $created_at </p>";
            })
                ->label('Fecha de creación')
                ->searchable(),

            Column::callback(['id'], function ($id) {
                return view('table-actions-analisis_imagenologicos', ['id' => $id]);
            })->label('Acciones')

        ];
    }
}
