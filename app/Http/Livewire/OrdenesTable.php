<?php

namespace App\Http\Livewire;

use App\Models\Orden;
use Mediconesystems\LivewireDatatables\Column;

use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Illuminate\Support\Facades\Request;

class OrdenesTable extends LivewireDatatable
{
    public $exportable = false;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        return Orden::query()->where('estatus',1);
    }

    public function columns()
    {
        return [

            Column::callback(['no_confirmacion'], function ($numero_confirmacion) {
                return "<p class='font-bold text-blue-600 whitespace-pre-line'> $numero_confirmacion </p>";
            })
                ->label('Numero de confirmación')
                ->searchable(),

            Column::callback(['nombre_paciente'], function ($nombre_paciente) {
                return "<p class='whitespace-pre-line'> $nombre_paciente </p>";
            })
                ->label('Paciente')
                ->searchable(),

            Column::callback(['tel_paciente'], function ($tel_paciente) {
                return "<p class='whitespace-pre-line'> $tel_paciente </p>";
            })
                ->label('Teléfono')
                ->searchable(),

            Column::callback(['email_paciente'], function ($email_paciente) {
                return "<p class='whitespace-pre-line'> $email_paciente </p>";
            })
                ->label('Email')
                ->searchable(),

            Column::callback(['tipo_tarjeta','digitos'], function ($tipo_tarjeta, $dijitos) {
                return "<p class='whitespace-pre-line'> $tipo_tarjeta - $dijitos </p>";
            })
                ->label('Tarjeta')
                ->searchable(),

            Column::callback(['total'], function ($total) {
                return "<p class='whitespace-pre-line'> $".number_format($total,2)." </p>";
            })
                ->label('Total')
                ->searchable(),

            Column::callback(['updated_at'], function ($updated_at) {
                return "<p class='whitespace-pre-line'> " . date('d M Y', strtotime($updated_at)) ." </p>";
            })
                ->label('Fecha de pago')
                ->searchable(),

            Column::callback(['id'], function ($id) {
                return view('table-actions-ordenes', ['id' => $id]);
            })->label('Ver cadena')

        ];
    }
}
