<?php

namespace App\Http\Livewire;

use App\Models\Cita;
use App\Models\Paciente;
use App\Models\Horario;
use Illuminate\Support\Carbon;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Illuminate\Support\Facades\Request;

class TomorrowTable extends LivewireDatatable
{

    public $exportable = false;
    public $ruta_actual;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        // return Cita::query()
        // ->leftJoin('horarios_citas', 'citas.horario_id', '=', 'horarios_citas.id')
        // ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');

        return Cita::whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE + 1)')
            ->where('pagado','=',1)
            ->leftJoin('horarios_citas', 'citas.horario_id', '=', 'horarios_citas.id')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');
    }

    public function columns()
    {
        return [

            Column::checkbox(),

            NumberColumn::callback(['no_confirmacion'], function($no_confirmacion){
                return "<p class='font-bold text-blue-600'> $no_confirmacion </p>";
            })
            ->label('No. Confirmación')
            ->searchable(),

            Column::callback(['paciente.nombre', 'paciente.apellido_paterno','paciente.apellido_materno'], function($nombre, $apellido_paterno, $apellido_materno) {
                return "{$nombre} {$apellido_paterno} {$apellido_materno}";
            })
            ->label('Nombre Paciente')
            ->searchable(),

            Column::callback(['horarios_citas.hora_inicial', 'horarios_citas.hora_final'], function($hora_inicial, $hora_final) {
                $hi = date('h:i A', strtotime($hora_inicial));
                $hf = date('h:i A', strtotime($hora_final));
                return "{$hi} - {$hf}";
            })
            ->label('Horario de cita')
            ->searchable(),

            Column::name('pacientes.tel_celular')
            ->label('Tél. movil')
            ->searchable(),

            // Column::name('pacientes.paciente_email')
            // ->label('Email')
            // ->searchable(),

            Column::callback(['tipo_prueba'], function($tipo_prueba) {
                switch($tipo_prueba) {
                    case 1: return 'PCR (SARS-Cov-2) '; break;
                    case 2: return 'ANTÍGENOS COVID-19'; break;
                };
            })
            ->label('Prueba')
            ->searchable(),

            Column::callback(['tipo_pago'], function($tipo_pago) {
                switch($tipo_pago) {
                    case 0: return '<div class="flex items-center"><svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"   stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                        </svg> Sucursal</div>'; break;
                        case 1: return '<div class="flex"><svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 10h18M7 15h1m4 0h1m-7 4h12a3 3 0 003-3V8a3 3 0 00-3-3H6a3 3 0 00-3 3v8a3 3 0 003 3z" />
                          </svg>Online</div>'; break;
                };
            })
            ->label('Lugar de pago')
            ->searchable(),

            Column::callback(['costo'], function($costo) {
                return '$'.number_format($costo,2);
            })
            ->label('Costo')
            ->searchable(),

            Column::callback(['pagado','fecha_seleccionada','status_asistencia', 'horarios_citas.hora_inicial'], function($pagado,$fecha_seleccionada, $status_asistencia, $hora_inicial) {
                switch($pagado) {
                    case 0:

                        if(strtotime(now()) > strtotime($fecha_seleccionada." ".$hora_inicial)){

                            return '<div class="flex items-center text-red-600"><svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                </svg>No pagado</div>';

                        }else{

                            if(strtotime(date('G:i:s') > strtotime($hora_inicial))){

                                return '<div class="flex items-center text-red-600"><svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                                    </svg>No pagado</div>';

                            }else{

                                return '<div class="flex items-center text-blue-600"><svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                </svg>Pago en la muestra</div>';

                            }

                        }

                        break;

                    case 1: return '<div class="flex text-green-600"><svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                      <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                    </svg> Pagado</div>'; break;
                };
            })
            ->label('Pagado')
            ->searchable(),

            Column::callback(['tipo_tarjeta', 'digitos', 'tipo_pago'], function($tipo_tarjeta, $digitos, $tipo_pago) {
                if($tipo_pago == 1){
                    return "{$tipo_tarjeta} - {$digitos}";
                }else{
                    return "No aplica";
                }
            })
            ->label('Tipo tarjeta - ultimos 4 digitos')
            ->searchable(),

            Column::callback(['id', 'status_asistencia'], function ($id, $status_asistencia) {
                return view('table-actions', [
                    'id' => $id,
                    'status_asistencia' => $status_asistencia,
                    'ruta_actual' => $this->ruta_actual
                ]);
            })->label('Acciones')

        ];

    }

    public function getPacientesProperty()
    {
        return Paciente::pluck('nombre');
    }

    public function getHorarioCitaProperty() {
        return Horario::pluck('horario_cita');
    }

}
