<?php

namespace App\Http\Livewire;

use App\Models\CitaImagenologico;
use Illuminate\Support\Facades\Request;

use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\NumberColumn;

class CitasHistoricoImagenologiaTable extends LivewireDatatable
{

    public $ruta_actual;
    public $exportable = false;
    public $ruta;

    public function __construct()
    {
        $this->ruta_actual = request()->route()->uri;
        $this->ruta = Request::route()->getName();
    }

    public function builder()
    {
        // $resltado = CitaImagenologico::query()
        // ->leftJoin('tipoestudio_images', 'citas_imagenologicos.analisisimagelologico_id', '=', 'tipoestudio_images.id')
        // ->leftJoin('schedule_images', 'citas_imagenologicos.scheduleimage_id', '=', 'schedule_images.id')
        // ->leftJoin('sucursales', 'citas_imagenologicos.sucursal', '=', 'sucursales.id')
        // ->get();
        // dd($resltado);
        return CitaImagenologico::query()
        ->leftJoin('tipoestudio_images', 'citas_imagenologicos.analisisimagelologico_id', '=', 'tipoestudio_images.id')
        ->leftJoin('schedule_images', 'citas_imagenologicos.scheduleimage_id', '=', 'schedule_images.id')
        ->leftJoin('sucursales', 'citas_imagenologicos.sucursal', '=', 'sucursales.id');

    }

    public function columns()
    {
        return [

            Column::checkbox(),

            NumberColumn::callback(['id'], function($id){
                return "<p class='font-bold text-blue-600'> $id </p>";
            })
            ->label('Id.')
            ->searchable(),

            Column::callback(['tipoestudio_images.nombre'], function($tipo) {
                return "{$tipo}";
            })
            ->label('Tipo Analisis')
            ->searchable(),

            Column::callback(['nombre'], function($nombre) {
                return "{$nombre}";
            })
            ->label('Nombre Paciente')
            ->searchable(),

            Column::callback(['telefono'], function($telefono) {
                return "{$telefono}";
            })
            ->label('Teléfono')
            ->searchable(),

            Column::callback(['correo'], function($correo) {
                return "{$correo}";
            })
            ->label('Correo')
            ->searchable(),

            Column::callback(['sucursales.sucursal'], function($sucursal) {
                return "{$sucursal}";
            })
            ->label('Sucursal')
            ->searchable(),

            Column::callback(['fecha_cita'], function($fecha_cita) {
                if($fecha_cita == null || $fecha_cita =="")
                {$fecha_cita ="<p class='font-bold text-red-500'>N/A</p>"; }
                else
                {$fecha_cita = $fecha_cita;}
                return "{$fecha_cita}";
            })
            ->label('Fecha cita')
            ->searchable(),

            Column::callback(['schedule_images.horario_inicial', 'schedule_images.horario_final'], function($hora_inicial, $hora_final) {

                $hi = date('h:i A', strtotime($hora_inicial));
                $hf = date('h:i A', strtotime($hora_final));
                if($hi == "12:00 AM" &&  $hf== "12:00 AM")
                {$hunidos = "<p class='font-bold text-red-500'>N/A</p>"; }
                else
                {$hunidos = $hi." - ".$hf;}
                return "{$hunidos}";
            })
            ->label('Horario de cita')
            ->searchable(),
            // [auxiliar == 2  ? '' : auxiliar == 4 ? '' : 'hidden'
  // return "<p class='font-bold text-blue-600'> $id </p>";
//   $fecha_cita == "0000-00-00 00:00:00" ? "<p class='font-bold text-blue-600'>0000-00-00</p>": $fecha_cita
            Column::callback(['created_at'], function($created_at) {
                return "{$created_at}";
            })
            ->label('Fecha de registro')
            ->searchable()

        ];
    }
}
