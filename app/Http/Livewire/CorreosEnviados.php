<?php

namespace App\Http\Livewire;

use App\Models\EncuestaUsuario;
use Livewire\Component;

class CorreosEnviados extends Component
{
    public $correos;
    public $id_encuesta;
    public $correos_buscador;

    public function mount($encuesta)
    {
        $this->id_encuesta = $encuesta;
    }

    public function render()
    {
        $array_correos = EncuestaUsuario::where('paciente_email', 'like', '%' . $this->correos_buscador . '%')
            ->where('encuesta_id', $this->id_encuesta)
            ->get();

        $this->correos = $array_correos;

        return view('livewire.correos-enviados');
    }
}
