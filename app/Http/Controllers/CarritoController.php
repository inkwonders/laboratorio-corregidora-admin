<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class CarritoController extends Controller
{
    public function index(){
        return view('lab.carrito.index');
    }

    public function paciente(){
        $tiene_analisis = Auth::user()->carrito->where("activo",1)->first()->analisis->count();

        if($tiene_analisis > 0){
            return view("lab.carrito.paciente")->with(["carrito" => Auth::user()->carrito->where("activo",1)->first()]);
        }else{
            return redirect()->to("carrito");
        }
    }

    public function comprobante() {
        $tiene_analisis = Auth::user()->carrito->where("activo",1)->first()->analisis->count();
        if($tiene_analisis > 0){
            return view('lab.carrito.comprobante')->with(["carrito" => Auth::user()->carrito->where("activo",1)->first()]);
        }else{
            return redirect()->to("carrito");
        }

    }
}
