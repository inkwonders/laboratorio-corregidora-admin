<?php

namespace App\Http\Controllers;

use App\Jobs\sendComprobante;
use App\Jobs\sendEncuestas;
use App\Mail\correoEncuesta;
use App\Models\CarritoMedicoAnalisis;
use App\Models\Encuesta;
use App\Models\EncuestaUsuario;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Notifications\EncuestasNotification;
use Carbon\Carbon;


class SendMailController extends Controller
{

    public function revisarCorreos($encuesta, $usuario)
    {

        $encuestas = EncuestaUsuario::select('id', 'paciente_email')->where('enviado', 0)->where('encuesta_id', '=', $encuesta)->get();
        $tiempo = 1;

        foreach ($encuestas as $encuesta) {
            // $enviar_correo = new SendMailController;
            // $enviar_correo->enviar($encuesta->paciente_email, $encuesta->id, $tiempo);
            // DB::table('encuesta_usuario')->where('id', '=', $this->encuesta);
            // $encuesta_usuario = EncuestaUsuario::where('id', '=', $encuesta);
            // $encuesta_usuario->user->SendEncuestasNotification($encuesta->paciente_email, $encuesta->id, $tiempo);

            // $email = $encuesta->paciente_email;
            // Mail::to($email)->send(new EncuestasNotification($encuesta));
            // $encuesta->update(['enviado' => 1]);

            $email = $encuesta->paciente_email;

            // Enviar correo electrónico utilizando la clase Mailable
            Mail::to($email)->send(new correoEncuesta($encuesta));

            // Actualizar el estado de enviado
            $update_encuesta_usario = EncuestaUsuario::where('id', $encuesta->id)->first();
            $update_encuesta_usario->enviado = 1;
            $update_encuesta_usario->save();

            $update_encuesta = Encuesta::where('id', $update_encuesta_usario->encuesta_id)->first();

            // dd($update_encuesta);
            $update_encuesta->enviado = 1;
            $update_encuesta->fecha_envio =  Carbon::now();
            $update_encuesta->save();


            $tiempo++;
        }

    }

    public function enviar($email, $encuesta, $tiempo)
    {
        // sendEncuestas::dispatch($email, $encuesta)->delay(now()->addMinutes($tiempo));;
        sendEncuestas::dispatch($email, $encuesta);
        // dd("entra");


    }

    public function correo_comprobante($datos_envio,$total)
    {
        $enviar_id = $datos_envio->id;
        $enviar_confirmacion = $datos_envio->no_confirmacion;
        $enviar_email = $datos_envio->email_paciente;
        $enviar_analisis[]  = CarritoMedicoAnalisis::select('clave', 'nombre', 'cantidad','precio')
        ->where('carrito_medico_id', $enviar_id)
        ->join('analisis', 'analisis_id', '=', 'analisis.id')
        ->get();
        $enviar_correo = new SendMailController;
        $enviar_correo->enviar_comprobante($total, $enviar_confirmacion, $enviar_email, $enviar_analisis);
    }
    public function enviar_comprobante($total, $enviar_confirmacion, $enviar_email, $enviar_analisis)
    {
        // sendEncuestas::dispatch($email, $encuesta)->delay(now()->addMinutes($tiempo));;
        sendComprobante::dispatch($total, $enviar_confirmacion, $enviar_email, $enviar_analisis);
    }
}
