<?php

namespace App\Http\Controllers;

use App\Models\EncuestaUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class responderEncuestaController extends Controller
{
    public function show($encuesta)
    {

        $cadenaDesencriptada = Crypt::decryptString($encuesta);
        // dd($cadenaDesencriptada);


        // Decodificar el JSON en un objeto
        $cadena = json_decode($cadenaDesencriptada);

        // Acceder al valor del campo "id"
        $id = $cadena->id;


        $existe_activa = EncuestaUsuario::where('id', '=', $id)
            ->where('status', '=', 'NO CONTESTADO')
            ->count();
// dd($existe_activa);
        $existe_eliminada = EncuestaUsuario::where('id', '=', $id)
            ->count();

// dd($cadenaDesencriptada);
// $cadenaDesencriptada = json_encode($cadenaDesencriptada);

        if ($existe_activa != 0) {
            return view('lab.encuestas.form')
                ->with('encuesta', $cadenaDesencriptada);
        } else {
            if ($existe_eliminada != 0) {
                return view('lab.encuestas.contestada');
            }else{
                return view('lab.encuestas.eliminada');
            }
        }
    }
}
