<?php

namespace App\Http\Controllers;

use App\Models\ListaCorreoEncuesta;
use Illuminate\Http\Request;
use App\Exports\correosExport;

class ListaCorreoEncuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListaCorreoEncuesta  $listaCorreoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function show(ListaCorreoEncuesta $listaCorreoEncuesta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListaCorreoEncuesta  $listaCorreoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function edit(ListaCorreoEncuesta $listaCorreoEncuesta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ListaCorreoEncuesta  $listaCorreoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListaCorreoEncuesta $listaCorreoEncuesta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListaCorreoEncuesta  $listaCorreoEncuesta
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListaCorreoEncuesta $listaCorreoEncuesta)
    {
        //
    }

    public function exportLista()
    {
        return (new correosExport)->download('Encuestas - Listado de correos para encuesta.xlsx');
    }

}
