<?php

namespace App\Http\Controllers;

use App\Exports\usuariosExport;
use App\Models\CarritoMedico;
use App\Models\Cita;
use App\Models\Orden;
use App\Models\User;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\ToArray;

class CovidController extends Controller
{
    public function getPanelCovid()
    {
        $total = Cita::count();

        $asistidas = Cita::where('status_asistencia', 1)->count();
        $no_asistidas = Cita::where('status_asistencia', 0)
            ->where('pagado',1)
            ->count();

        $ingreso_online_general = Cita::where('tipo_pago', 1)
            ->where('pagado', 1)->sum('costo');

        $ingreso_total = Cita::where('pagado', 1)->sum('costo');

        $ingreso_sucursal_general_estimado = Cita::where('tipo_pago', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) <= TO_DAYS(CURRENT_DATE)')
            ->sum('costo');

        $ingreso_sucursal_general_real = Cita::where('status_asistencia', 1)
            ->where('tipo_pago', 0)
            ->where('pagado', 1)->sum('costo');

        $restantes = Cita::where('status_asistencia', 0)
            // ->whereHas('horarios', function ($query) {
            //     $query->whereRaw('UNIX_TIMESTAMP(hora_inicial) > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)');
            // })
            ->whereRaw('TO_DAYS(fecha_seleccionada) > TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)
            ->count();

        $asistidas_hoy = Cita::where('status_asistencia', 1)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->count();

        $restantes_hoy = Cita::where('status_asistencia', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->whereHas('horarios', function ($query) {
                $query->whereRaw('UNIX_TIMESTAMP(hora_inicial) > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)');
            })->count();

        $citas_agrupadas = Cita::query()
            ->select('tipo_prueba', DB::raw('count(*) as total'))
            ->groupBy('tipo_prueba')
            ->get();

        $citas_agrupadas_line = Cita::query()
            ->select(
                'tipo_prueba',
                DB::raw('count(*) as total'),
                DB::raw('month(fecha_seleccionada) as mes')
            )
            ->groupBy('tipo_prueba', 'mes')
            ->get()->groupBy('tipo_prueba')
            ->map(function ($tipo_prueba) {
                return $tipo_prueba->keyBy('mes');
            });

        $pagos_agrupados_line = Cita::query()
            ->select(
                DB::raw('count(*) as sum'),
                DB::raw('sum(costo) as total'),
                'tipo_pago',
                DB::raw('month(fecha_seleccionada) as mes')
            )
            ->groupBy('mes', 'tipo_pago')
            ->where('pagado', 1)
            ->get()
            ->keyBy('tipo_pago');

        $dataset = collect();
        $datasetIngresos = collect();

        foreach ($citas_agrupadas_line as $tipo_prueba => $cita) {

            $meses = collect();

            for ($i = 1; $i <= 12; $i++) {
                if (isset($citas_agrupadas_line[$tipo_prueba][$i]))
                    $meses[$i] = $citas_agrupadas_line[$tipo_prueba][$i]->total;
                else
                    $meses[$i] = 0;
            }

            $dataset[] =  [
                'label' => $tipo_prueba == 1 ? 'PCR' : 'Antigenos',
                'backgroundColor' => $tipo_prueba == 1 ? '#0694a2' : '#1c64f2',
                'borderColor' =>  $tipo_prueba == 1 ? '#0694a2' : '#1c64f2',
                'data' => $meses->values()->toArray(),
                'fill' => false,
            ];
        }

        foreach ($pagos_agrupados_line as $tipo_pago => $pago) {
            $meses = collect();

            for ($i = 1; $i <= 12; $i++) {
                if (isset($pagos_agrupados_line[$tipo_pago]) && $pagos_agrupados_line[$tipo_pago]->mes == $i) {
                    $meses[$i] = $pagos_agrupados_line[$tipo_pago]->total;
                } else
                    $meses[$i] = 0;
            }

            $datasetIngresos[] =  [
                'label' => $tipo_pago == 1 ? 'Tarjeta de debito / credito' : 'Sucursal',
                'backgroundColor' => $tipo_pago == 1 ? '#9061f9' : '#ff5a1f',
                'borderColor' =>  $tipo_pago == 1 ? '#9061f9' : '#ff5a1f',
                'data' => $meses->values()->toArray(),
                'fill' => false,
            ];
        }

        $grafica_pruebas_pie = [
            'titulo' => 'Tipos de pruebas',
            'etiquetas' => collect([
                [
                    'texto' => 'PCR',
                    'clase' => 'bg-green-500'
                ],
                [
                    'texto' => 'Antigenos',
                    'clase' => 'bg-blue-500'
                ]
            ]),
            'dataset' => [[
                'data' => $citas_agrupadas->pluck('total'),
                'backgroundColor' => ['#0694a2', '#1c64f2'],
            ]]

        ];

        $grafica_pruebas_line =  [
            'titulo' => 'Tipos de pruebas',
            'etiquetas' => collect([
                [
                    'texto' => 'PCR',
                    'clase' => 'bg-green-500'
                ],
                [
                    'texto' => 'Antigenos',
                    'clase' => 'bg-blue-500'
                ]
            ]),
            'data' => [
                'datasets' => $dataset,
                'labels' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            ]
        ];

        /** ingresos grafica pie */

        $grafica_pruebas_pie_ingresos = [
            'titulo' => 'Tipos de pago',
            'etiquetas' => collect([
                [
                    'texto' => 'Tarjeta de debito/credito',
                    'clase' => 'bg-purple-500'
                ],
                [
                    'texto' => 'Sucursal',
                    'clase' => 'bg-orange-500'
                ]
            ]),
            'dataset' => [[
                'data' => Cita::get()->where('pagado', 1)->partition(function ($cita) {
                    return $cita->tipo_pago == 1;
                })->map(function ($el) {
                    return $el->count();
                }),
                'backgroundColor' => ['#9061f9', '#ff5a1f'],
            ]]
        ];

        /* ingresos grafica line */
        $grafica_pruebas_line_ingresos = [
            'titulo' => 'Tipos de pruebas',
            'etiquetas' => collect([
                [
                    'texto' => 'Tarjeta de debito/credito',
                    'clase' => 'bg-purple-500'
                ],
                [
                    'texto' => 'Sucursal',
                    'clase' => 'bg-orange-500'
                ]
            ]),
            'data' => [
                'datasets' => $datasetIngresos,
                'labels' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            ]
        ];

        return view('lab.covidgeneral')
            ->with(compact([
                'total',
                'asistidas',
                'no_asistidas',
                'restantes',
                'ingreso_online_general',
                'ingreso_sucursal_general_real',
                'ingreso_total',
                'ingreso_sucursal_general_estimado',
                'grafica_pruebas_pie',
                'grafica_pruebas_line',
                'grafica_pruebas_pie_ingresos',
                'grafica_pruebas_line_ingresos'
            ]));
    }

    public function getPanelCovidHoy()
    {

        $total_hoy = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')->where('pagado', 1)->count();

        $ingreso_sucursal_dia_real = Cita::where('tipo_pago', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)->sum('costo');

        $ingreso_sucursal_dia_estimado =  Cita::where('tipo_pago', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->sum('costo');

        $ingreso_online_dia = Cita::where('tipo_pago', 1)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)->sum('costo');

        $ingreso_total_dia = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)->sum('costo');

        $no_asistidas_hoy = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('status_asistencia', 0)
            ->where('pagado', 1)
            ->whereHas('horarios', function ($query) {
                $query->whereRaw('UNIX_TIMESTAMP(hora_inicial) < UNIX_TIMESTAMP(CURRENT_TIMESTAMP)');
            })->count();

        $asistidas_hoy = Cita::where('status_asistencia', 1)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->count();

        $restantes_hoy = Cita::where('status_asistencia', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)
            ->whereHas('horarios', function ($query) {
                $query->whereRaw('UNIX_TIMESTAMP(hora_inicial) > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)');
            })->count();


        $citas_agrupadas = Cita::query()
            ->select('tipo_prueba', DB::raw('count(*) as total'))
            ->groupBy('tipo_prueba')
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->get();


        $citas_agrupadas_line = Cita::query()
            ->select(
                'tipo_prueba',
                DB::raw('count(*) as total'),
                DB::raw('month(fecha_seleccionada) as mes')
            )
            ->groupBy('tipo_prueba', 'mes')
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->get()->groupBy('tipo_prueba')
            ->map(function ($tipo_prueba) {
                return $tipo_prueba->keyBy('mes');
            });

        $grafica_pruebas_pie = [
            'titulo' => 'Tipos de pruebas',
            'etiquetas' => collect([
                [
                    'texto' => 'PCR',
                    'clase' => 'bg-green-500'
                ],
                [
                    'texto' => 'Antigenos',
                    'clase' => 'bg-blue-500'
                ]
            ]),
            'dataset' => [[
                'data' => $citas_agrupadas->pluck('total'),
                'backgroundColor' => ['#0694a2', '#1c64f2'],
            ]]

        ];

        if (count($citas_agrupadas_line) != 0) {
            foreach ($citas_agrupadas_line as $tipo_prueba => $cita) {

                $meses = collect();

                for ($i = 1; $i <= 12; $i++) {
                    if (isset($citas_agrupadas_line[$tipo_prueba][$i]))
                        $meses[$i] = $citas_agrupadas_line[$tipo_prueba][$i]->total;
                    else
                        $meses[$i] = 0;
                }

                $dataset[] =  [
                    'label' => $tipo_prueba == 1 ? 'PCR' : 'Antigenos',
                    'backgroundColor' => $tipo_prueba == 1 ? '#0694a2' : '#1c64f2',
                    'borderColor' =>  $tipo_prueba == 1 ? '#0694a2' : '#1c64f2',
                    'data' => $meses->values()->toArray(),
                    'fill' => false,
                ];
            }
        } else {

            $dataset[] =  [
                'label' =>  'PCR',
                'backgroundColor' => '#0694a2',
                'borderColor' =>  '#0694a2',
                'data' => 0,
                'fill' => false,

                'label' =>  'Antigenos',
                'backgroundColor' => '#1c64f2',
                'borderColor' =>   '#1c64f2',
                'data' => 0,
                'fill' => false,
            ];
        }
        $grafica_pruebas_line =  [
            'titulo' => 'Tipos de pruebas',
            'etiquetas' => collect([
                [
                    'texto' => 'PCR',
                    'clase' => 'bg-green-500'
                ],
                [
                    'texto' => 'Antigenos',
                    'clase' => 'bg-blue-500'
                ]
            ]),
            'data' => [
                'datasets' => $dataset,
                'labels' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            ]
        ];

        return view('lab.covidhoy')
            ->with(compact([
                'total_hoy',
                'no_asistidas_hoy',
                'asistidas_hoy',
                'restantes_hoy',
                'ingreso_sucursal_dia_real',
                'ingreso_sucursal_dia_estimado',
                'ingreso_online_dia',
                'ingreso_total_dia',
                'grafica_pruebas_pie',
                'grafica_pruebas_line'
            ]));
    }

    public function getPanelCovidTomorrow()
    {

        $total_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')->where('pagado', 1)->count();

        $ingreso_online_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')
            ->where('tipo_pago', 1)
            ->where('pagado', 1)->sum('costo');

        $ingreso_sucursal_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')
            ->where('tipo_pago', 0)
            ->sum('costo');

        $ingreso_total_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')
            ->where('pagado', 1)
            ->sum('costo');

        return view('lab.covidtomorrow')
            ->with(compact([
                'ingreso_online_siguiente',
                'ingreso_sucursal_siguiente',
                'ingreso_total_siguiente',
                'total_siguiente'
            ]));
    }

    public function getPanelAnalisis()
    {

        $orden = Orden::where('estatus', 1)->count();

        $orden_no = Orden::where('estatus', 0)->count();

        $ingreso_orden_total = Orden::where('estatus', 1)->sum('total');

        $analisis_vendidos_hoy = Orden::whereRaw('TO_DAYS(created_at) = TO_DAYS(CURRENT_DATE)')
            ->where('estatus', 1)->count();

        $total_analisis_vendidos_hoy = Orden::whereRaw('TO_DAYS(created_at) = TO_DAYS(CURRENT_DATE)')
            ->where('estatus', 1)->sum('total');

        $ordenes_agrupadas_line = Orden::query()
            ->select(
                'estatus',
                DB::raw('count(*) as total'),
                DB::raw('month(created_at) as mes')
            )
            ->groupBy('estatus', 'mes')
            ->get()->groupBy('estatus')
            ->map(function ($pagado) {
                return $pagado->keyBy('mes');
            });


        if (count($ordenes_agrupadas_line) != 0) {


            foreach ($ordenes_agrupadas_line as $pagado => $orden_pago) {

                $meses = collect();

                for ($i = 1; $i <= 12; $i++) {
                    if (isset($ordenes_agrupadas_line[$pagado][$i]))
                        $meses[$i] = $ordenes_agrupadas_line[$pagado][$i]->total;
                    else
                        $meses[$i] = 0;
                }
                $dataset[] =  [
                    'label' => $pagado == 1 ? 'Pagado' : 'No Pagado',
                    'backgroundColor' => $pagado == 1 ? '#31c48d' : '#e02424',
                    'borderColor' =>  $pagado == 1 ? '#31c48d' : '#e02424',
                    'data' => $meses->values()->toArray(),
                    'fill' => false,
                ];
            }
        } else {
            $dataset[] =  [
                'label' => 'Pagado',
                'backgroundColor' => '#31c48d' ,
                'borderColor' =>  '#31c48d' ,
                'data' => 0,
                'fill' => false,
            ];

            $dataset[] =  [
                'label' => 'No Pagado',
                'backgroundColor' =>  '#e02424',
                'borderColor' =>  '#e02424',
                'data' => 0,
                'fill' => false,
            ];

        }

        $grafica_pruebas_line =  [
            'titulo' => 'Pago analísis',
            'etiquetas' => collect([
                [
                    'texto' => 'Pagado',
                    'clase' => 'bg-green-400'
                ],
                [
                    'texto' => 'No Pagado',
                    'clase' => 'bg-red-600'
                ]
            ]),
            'data' => [
                'datasets' => $dataset,
                'labels' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            ]
        ];


        $grafica_pruebas_pie = [
            'titulo' => 'Pago analísis',
            'etiquetas' => collect([
                [
                    'texto' => 'Pagado',
                    'clase' => 'bg-green-400'
                ],
                [
                    'texto' => 'No Pagado',
                    'clase' => 'bg-red-600'
                ]
            ]),
            'dataset' => [[
                'data' => Orden::get()->partition(function ($orden) {
                    return $orden->estatus == 1;
                })->map(function ($el) {
                    return $el->count();
                }),
                'backgroundColor' => ['#31c48d', '#e02424'],
            ]]

        ];


        return view('lab.analisisgeneral')
            ->with(compact([
                'analisis_vendidos_hoy',
                'total_analisis_vendidos_hoy',
                'ingreso_orden_total',
                'orden',
                'orden_no',
                'grafica_pruebas_pie',
                'grafica_pruebas_line',

            ]));
    }

    public function getPanelListasAnalisis()
    {

        $notas_impresas = CarritoMedico::where('activo', '=', '0')
            ->count();

        $total_medicos = User::query()
            ->leftJoin('model_has_roles', 'model_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '2')
            ->count();

        return view('lab.listasanalisis')
            ->with(compact([
                'total_medicos',
                'notas_impresas'
            ]));
    }

    public function export_usuarios()
    {
        return (new usuariosExport)->download('Laboratorio Corregidora - Usuarios.xlsx');
    }
}
