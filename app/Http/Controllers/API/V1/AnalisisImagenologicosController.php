<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\AnalisisImagenologico;
use App\Models\CitaImagenologico;
use App\Models\Scheduleimage;
use App\Models\TipoestudioImage;
use App\Models\Sucursales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\Mail;

use function PHPUnit\Framework\isEmpty;

class AnalisisImagenologicosController extends Controller
{

    // obtenemos todos los estudios
    public function getEstudios(Request $request){

        // validacionde datos y guardado
        Log::info('AnalisisImagenologicosController@getEstudios', $request->all());

        $mensajes = [
            'required' => 'Se esperaba :attribute'
        ];

        $validator = Validator::make(
            $request->all(),
            [
                "tipo" => "required",
            ], $mensajes
        );

        // dd($mensajes);

        if($validator->fails()) {
            return response()->json(['success' => false, 'msg' => 'Error, fallaron las validaciones', 'data' => $validator->errors()->all()], 400);
        }

        if((int)$request->tipo == 0){
            $listaAnalisis = AnalisisImagenologico::all()->toArray();
        }else{
            $listaAnalisis = AnalisisImagenologico::where('tipoestudioimagelologicos_id', (int)$request->tipo)->get()->toArray();
        }

        return response()->json(['status' => 200, 'success' => true, 'msg' => 'Colección: Analisis generados con éxito', 'data' => $listaAnalisis], 200);

    }

    // obtenemos los tipos de estudio
    public function getTipos(){
        // $listaTipos = TipoestudioImage::where('activo','=', 1)->get()->toArray();
        $listaTipos = TipoestudioImage::all()->toArray();
        // dd($listaTipos);
        return response()->json(['status' => 200, 'success' => true, 'msg' => 'Colección: Tipo Analisis generados con éxito', 'data' => $listaTipos], 200);
    }

        // Obtenemos las sucursales
        // Solo requerimos el tipo de estudio

        public function getSucursales(Request $request){

            Log::info('AnalisisImagenologicosController@getSucursales', $request->all());

            $mensajes = [
                'required' => 'Se esperaba el atributo tipo'
            ];

            $validator = Validator::make(
                $request->all(),
                [
                    "tipo" => "required",
                ], $mensajes
            );

            if ($validator->fails()) {
                return response()->json(['status' => 400, 'success' => false, 'msg' => 'Colección: Posibles suscursales', 'data' => $validator->errors()->all()],400);
            }

            $tipo = $request->tipo;
            $sucursales = Sucursales::whereHas('sucursalesEstudio', function ($query) use ($tipo){
                $query->where('tipoestudio_image_id', '=', $tipo);
                $query->where('estatus',1);
            })
            ->select('id','sucursal', 'ubicacion')
            ->get();

            if($sucursales->isEmpty()){
                return response()->json(['status' => 300, 'success' => false, 'msg' => 'Colección: Posibles suscursales', 'data' => 'No se encontraron resultados'],300);
            }else{
                return response()->json(['status' => 200, 'success' => true, 'msg' => 'Colección: Posibles suscursales', 'data' => $sucursales],200);
            }

        }


    // obtenemos datos del estudio
    public function getEstudioEspecifico(Request $request){
        $datosAnalisis = AnalisisImagenologico::where("slug", $request->slug)->get()->toArray();
        return response()->json(['status' => 200, 'success' => true, 'msg' => 'Colección: Datos de analisis generados con éxito', 'data' => $datosAnalisis], 200);
    }

    // obtenemos los horarios
    //Se pide la fecha, fin (fin de semana) y sucursal
    public function getHorarios(Request $request){

        Log::info('AnalisisImagenologicosController@getHorarios', $request->all());

        $hora_server = date("H:i:s");

        // dd($request);
        if(
            $request->fecha == '2023-02-06' ||
            $request->fecha == '2023-03-20' ||
            $request->fecha == '2023-05-01' ||
            $request->fecha == '2023-09-16' ||
            $request->fecha == '2023-11-20' ||
            $request->fecha == '2022-12-25' ||
            $request->fecha == '2023-12-25'

        ){

            Log::info('no hay citas', $request->all());
            $listaHorarios = "No hay citas en este dia.";

        /*}else if(
            ($request->fecha == '2023-03-09' ||
            $request->fecha == '2023-03-10' ||
            $request->fecha == '2023-03-11' ||
            $request->fecha == '2023-03-12' ||
            $request->fecha == '2023-03-13' ||
            $request->fecha == '2023-03-14' ||
            $request->fecha == '2023-03-15' ||
            $request->fecha == '2023-03-16' ||
            $request->fecha == '2023-03-17') &&
            $request->tipo == 3 && ($request->sucursal == 2 || $request->sucursal == 4)
        ){

            if($request->fin == 1 || $request->fin == 2){
                $listaHorarios = "No hay citas en este dia.";
            }else{

                $lista = array(
                    "id" => 15,
                    "horario_inicial" => "17:00:00",
                    "horario_final" => "18:00:00",
                    "numero_pacientes" => 1000,
                    "dia_semana" => 1,
                    "orden" => 9
                );

                $listaHorarios = [$lista];

            }*/


        }else{

            Log::info('entro else', $request->all());
            // SABADO

            if($request->fin == 1){

                if(strtotime($request->fecha) == strtotime(date("Y-m-d"))){

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            $query->where('dia_semana', 4)
                                ->orwhere('dia_semana', 5)
                                ->orwhere('dia_semana', 9);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->whereTime('horario_inicial','>',$hora_server)
                    ->orderBy("orden")
                    ->get()
                    ->toArray();
                    // return dd($listaHorarios);
                }else{

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                        $query->where('dia_semana', 4)
                            ->orwhere('dia_semana', 5)
                            ->orwhere('dia_semana', 9);
                    })
                    // ->whereHas("horarios.sucursalesEstudio",function ($query){
                    //         $query->where('tipoestudio_image_id','=', 3);
                    // })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->orderBy("orden")
                    ->get()
                    ->toArray();
                    // dd("Valor del sabado: ",$listaHorarios);
                }

            }

            // LUNES

            if($request->fin == 3){

                if(strtotime($request->fecha) == strtotime(date("Y-m-d"))){

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            $query->where('dia_semana', 2)
                                ->orwhere('dia_semana', 1)
                                ->orwhere('dia_semana', 5)
                                ->orwhere('dia_semana', 6);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->whereTime('horario_inicial','>',$hora_server)
                    ->orderBy("orden")
                    ->get()
                    ->toArray();
                    // $listaHorarios = Scheduleimage::where("dia_semana", 2)
                    // ->whereTime('horario_inicial','>',$hora_server)
                    // ->orderBy("orden")
                    // ->get()
                    // ->toArray();

                }else{

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            $query->where('dia_semana', 2)
                                ->orwhere('dia_semana', 1)
                                ->orwhere('dia_semana', 5)
                                ->orwhere('dia_semana', 6);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }

            }

            // MARTES

            if($request->fin == 4){

                if(strtotime($request->fecha) == strtotime(date("Y-m-d"))){

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            // $query->where('dia_semana', 3)
                            //     ->orwhere('dia_semana', 1)
                            //     ->orwhere('dia_semana', 5);
                            $query->where('dia_semana', 1)
                                ->orwhere('dia_semana', 5)
                                ->orwhere('dia_semana', 7);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->whereTime('horario_inicial','>',$hora_server)
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }else{

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            // $query->where('dia_semana', 3)
                            //     ->orwhere('dia_semana', 1)
                            //     ->orwhere('dia_semana', 5);
                            $query
                            ->where('dia_semana', 1)
                            ->orwhere('dia_semana', 5)
                            ->orwhere('dia_semana', 7);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }

            }

            // MIERCOLES

            if($request->fin == 5){

                if(strtotime($request->fecha) == strtotime(date("Y-m-d"))){

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            $query->where('dia_semana', 1)
                                ->orwhere('dia_semana', 3)
                                ->orwhere('dia_semana', 5)
                                ->orwhere('dia_semana', 8);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->whereTime('horario_inicial','>',$hora_server)
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }else{

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            $query->where('dia_semana', 1)
                            ->orwhere('dia_semana', 3)
                                ->orwhere('dia_semana', 5)
                                ->orwhere('dia_semana', 8);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }

            }

            // JUEVES

            if($request->fin == 6){

                if(strtotime($request->fecha) == strtotime(date("Y-m-d"))){

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            $query->where('dia_semana', 2)
                                ->orwhere('dia_semana', 1)
                                ->orwhere('dia_semana', 5)
                                ->orwhere('dia_semana', 6);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->whereTime('horario_inicial','>',$hora_server)
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }else{

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                        $query->where('dia_semana', 2)
                            ->orwhere('dia_semana', 1)
                            ->orwhere('dia_semana', 5)
                            ->orwhere('dia_semana', 6);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }

            }


            // VIERNES

            if($request->fin == 7){

                if(strtotime($request->fecha) == strtotime(date("Y-m-d"))){

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                        // $query->where('dia_semana', 3)
                        //     ->orwhere('dia_semana', 1)
                        //     ->orwhere('dia_semana', 5);
                        $query->where('dia_semana', 1)
                            ->orwhere('dia_semana', 5)->orwhere('dia_semana', 7);
                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->whereTime('horario_inicial','>',$hora_server)
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }else{

                    $listaHorarios = Scheduleimage::whereHas("horarios", function ($query) use ($request){
                        $query->where('sucursales_id', '=', $request->sucursal)
                        ->where('id_tipo_estudio','=',$request->tipo);
                    })
                    ->where(function ($query){
                            // $query->where('dia_semana', 3)
                            //     ->orwhere('dia_semana', 1)
                            //     ->orwhere('dia_semana', 5);
                            $query->where('dia_semana', 1)
                            ->orwhere('dia_semana', 5)->orwhere('dia_semana', 7);

                    })
                    ->select('id','horario_inicial','horario_final','numero_pacientes','dia_semana','orden')
                    ->orderBy("orden")
                    ->get()
                    ->toArray();

                }

            }

            // DOMINGO

            if($request->fin == 2){

                return response()->json(['status' => 200, 'success' => false, 'msg' => 'El domingo no hay citas.', 'data' => 'El domingo no hay citas.', 'horaserver' => 'No hay hora disponible'], 200);


            }

            // FECHA INVALIDA (NO EXISTE EN EL CALENDARIO)

            if($request->fin == 0){

                return response()->json(['status' => 302, 'success' => false, 'msg' => 'No es un horario valido', 'data' => $listaHorarios, 'horaserver' => 'No hay hora disponible'], 302);

            }

            // FECHA INVALIDA (NO EXISTE EN EL CALENDARIO)
            if($request->fin > 7){

                return response()->json(['status' => 302, 'success' => false, 'msg' => 'No es un horario valido', 'data' => $listaHorarios, 'horaserver' => 'No hay hora disponible'], 302);

            }

            // No hay horarios disponibles
            if (count($listaHorarios) == 0) {

                return response()->json(['status' => 200, 'success' => false, 'msg' => 'No se encontraron horarios disponibles', 'data' => 'No se encontraron horarios disponibles', 'horaserver' => 'No hay hora disponible'], 200);

            }

        }

        // return dd($request->fin, strtotime($request->fecha), strtotime(date("Y-m-d")), $hora_server);
        return response()->json(['status' => 200, 'success' => true, 'msg' => 'Colección: lista de horarios obtenida cone exito', 'data' => $listaHorarios, 'horaserver' => $hora_server], 200);
    }

    // mandar correo de contacto
    public function mandarCorreoContacto(Request $request){

        // validacionde datos y guardado
        Log::info('AnalisisImagenologicosController@mandarCorreoContacto', $request->all());

        $mensajes = [
            'required' => 'Se esperaba :attribute'
        ];

        $validator = Validator::make(
            $request->all(),
            [
                "nombre"           => "required",
                "telefono"         => "required|numeric",
                "correo"           => "required|email",
                "mensaje"          => "required"
            ], $mensajes
        );

        if($validator->fails()) {
            return response()->json(['success' => false, 'msg' => 'Error, fallaron las validaciones', 'data' => $validator->errors()->all()], 400);
        }

        $to = 'imagen@laboratoriocorregidora.com.mx';
        // $to = 'oswaldo@inkwonders.com';

        Mail::send('emails.formContacto',['datos' => $request->all()], function($message) use ($to) {
            $message->from('noreply@laboratoriocorregidora.com.mx');
            $message->to($to)
            ->subject('Contacto desde imagenologia.laboratoriocorregidora.com.mx')
            // ->cc('oswaldoferral@gmail.com');
            ->bcc('eddy@inkwonders.com');
        });

        return response()->json(['success' => true, 'msg' => 'Se mando el correo correctamente'], 200);

    }

    // creamos la cita con los datos, fecha y horario
    public function guardarCita(Request $request){

         // validacionde datos y guardado
         Log::info('AnalisisImagenologicosController@guardarCita', $request->all());

         $mensajes = [
             'required' => 'Se esperaba :attribute'
         ];

            if($request->tipo == 2 || $request->tipo == 4 || $request->tipo == 5){

                $validator = Validator::make(
                    $request->all(),
                    [
                        "tipo"               => "required",
                        // "fecha_cita"         => "required|date",
                        // "horario"            => "required",
                        "nombre"             => "required",
                        "telefono"           => "required|digits:10",
                        "correo"             => "required|email",
                        "fecha_nacimiento"   => "required|date"
                    ], $mensajes
                );
            }
            else{
                $validator = Validator::make(
                    $request->all(),
                    [
                        "tipo"               => "required",
                        // "fecha_cita"         => "required|date",
                        // "horario"            => "required",
                        "nombre"             => "required",
                        "telefono"           => "required|digits:10",
                        "correo"             => "required|email",
                        "fecha_nacimiento"   => "required|date"
                    ], $mensajes
                );
            }




        if($validator->fails()) {
            return response()->json(['success' => false, 'msg' => 'Error, fallaron las validaciones', 'data' => $validator->errors()->all()], 400);
        }

        DB::beginTransaction();

        try{


            if($request->fecha_cita == null){
                // $request->fecha_cita = date('Y-m-d h:i:s');
                $request->fecha_cita = null;
            }

            if($request->horario == null){
                $request->horario = "24";
            }

            $cita = new CitaImagenologico();

            $cita->analisisimagelologico_id      = $request->tipo;
            $cita->fecha_cita                    = $request->fecha_cita;
            $cita->scheduleimage_id              = $request->horario;
            $cita->nombre                        = $request->nombre;
            $cita->telefono                      = $request->telefono;
            $cita->correo                        = $request->correo;
            $cita->sucursal                      = $request->sucursal;
            $cita->fecha_nacimiento              = $request->fecha_nacimiento;

            $horario = Scheduleimage::find($request->horario);

            //guardar cita en bd

            $cita->save();

            DB::commit();

            // dd($cita);

            if($request->tipo == 1 || $request->tipo == 3){
                $to = $request->correo;

                Mail::send('emails.form',['datos' => $request->all(), 'horario' => $horario], function($message) use ($to) {
                    $message->from('noreply@laboratoriocorregidora.com.mx');
                    $message->to($to)
                    ->subject('Se completo el registro de cita')
                    // ->cc('mario@inkwonders.com');
                    ->bcc('eddy@inkwonders.com');
                });

                $to_admin = "imagen@laboratoriocorregidora.com.mx";
                // $to_admin = "mario@inkwonders.com";

                Mail::send('emails.form_admin',['datos' => $request->all(), 'horario' => $horario], function($message) use ($to_admin) {
                    $message->from('noreply@laboratoriocorregidora.com.mx');
                    $message->to($to_admin)
                    ->subject('Nuevo registro de cita en Imagenología')
                    // ->cc('oswaldo@inkwonders.com');
                    ->bcc('eddy@inkwonders.com');
                });
            }
            else{
                $to = $request->correo;

                Mail::send('emails.formRadioBiop',['datos' => $request->all(), 'horario' => $horario], function($message) use ($to) {
                    $message->from('noreply@laboratoriocorregidora.com.mx');
                    $message->to($to)
                    ->subject('Se completo el registro de cita')
                    // ->cc('mario@inkwonders.com');
                    ->bcc('eddy@inkwonders.com');
                });

                $to_admin = "imagen@laboratoriocorregidora.com.mx";
                // $to_admin = "mario@inkwonders.com";

                Mail::send('emails.formRadioBiop_admin',['datos' => $request->all(), 'horario' => $horario], function($message) use ($to_admin) {
                    $message->from('noreply@laboratoriocorregidora.com.mx');
                    $message->to($to_admin)
                    ->subject('Nuevo registro de cita en Imagenología')
                    // ->cc('mario@inkwonders.com');
                    ->bcc('eddy@inkwonders.com');
                });
            }


            return response()->json(['success' => true, 'msg' => 'Registro de cita imagenologica exitosa'], 200);

        }catch(Exception $e) {
            DB::rollBack();
            Log::error("Error al resgistrar la cita", ['message' => $e->getMessage()]);
            return response()->json(['success' => false, 'msg' => 'Error al registrar la cita', 'error' => $e->getMessage(), 'datos' => $request->all()], 400);
        }

    }

}
