<?php

namespace App\Http\Controllers;

use App\Exports\citasHoyExport;
use App\Exports\citasMananaExport;
use App\Exports\historicoExport;
use App\Exports\HistoricoExportImagenologia;
use App\Exports\HoyExportImagenologia;
use App\Exports\MananaExportImagenologia;
use App\Models\CarritoMedico;
use App\Models\Cita;
use App\Models\CitaImagenologico;
use App\Models\Orden;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CitasController extends Controller
{
    public function getPanel()
    {
        $total = Cita::count();
        $orden = Orden::where('estatus', 1)->count();
        $orden_no = Orden::where('estatus', 0)->count();
        $asistidas = Cita::where('status_asistencia', 1)->count();
        $no_asistidas = Cita::where('status_asistencia', 0)
            ->where('pagado', 1)->count();

        // ordenes ingresos total
        $ingreso_orden_total = Orden::where('estatus', 1)->sum('total');


        $ingreso_online_general = Cita::where('status_asistencia', 1)
            ->where('tipo_pago', 1)
            ->where('pagado', 1)->sum('costo');

        $ingreso_total = Cita::where('pagado', 1)->sum('costo');

        $ingreso_sucursal_general_estimado = Cita::where('tipo_pago', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) <= TO_DAYS(CURRENT_DATE)')
            ->sum('costo');

        $ingreso_sucursal_general_real = Cita::where('status_asistencia', 1)
            ->where('tipo_pago', 0)
            ->where('pagado', 1)->sum('costo');

        $restantes = Cita::where('status_asistencia', 0)
            ->where('pagado', 1)
            ->whereHas('horarios', function ($query) {
                $query->whereRaw('UNIX_TIMESTAMP(hora_inicial) > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)');
            })
            ->count();

        $total_hoy = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)
            ->count();

        $total_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')
            ->where('pagado', 1)
            ->count();

        $ingreso_sucursal_dia_real = Cita::where('tipo_pago', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)
            ->sum('costo');

        $ingreso_sucursal_dia_estimado =  Cita::where('tipo_pago', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->sum('costo');

        $ingreso_online_dia = Cita::where('tipo_pago', 1)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)->sum('costo');

        $ingreso_total_dia = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('pagado', 1)->sum('costo');

        $no_asistidas_hoy = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->where('status_asistencia', 0)
            ->where('pagado', 1)
            ->whereHas('horarios', function ($query) {
                $query->whereRaw('UNIX_TIMESTAMP(hora_inicial) < UNIX_TIMESTAMP(CURRENT_TIMESTAMP)');
            })->count();

        $ingreso_online_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')
            ->where('tipo_pago', 1)
            ->where('pagado', 1)->sum('costo');

        $ingreso_sucursal_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')
            ->where('tipo_pago', 0)
            ->sum('costo');

        $ingreso_total_siguiente = Cita::whereRaw('TO_DAYS(fecha_seleccionada) = (TO_DAYS(CURRENT_DATE) + 1)')
            ->where('pagado', 1)
            ->sum('costo');

        $asistidas_hoy = Cita::where('status_asistencia', 1)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->count();

        $restantes_hoy = Cita::where('status_asistencia', 0)
            ->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)')
            ->whereHas('horarios', function ($query) {
                $query->whereRaw('UNIX_TIMESTAMP(hora_inicial) > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)');
            })->count();

        $total_medicos = User::query()
            ->leftJoin('model_has_roles', 'model_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '2')
            ->count();

        $notas_impresas = CarritoMedico::where('activo', '=', '0')
            ->count();

        $citas_agrupadas = Cita::query()
            ->select('tipo_prueba', DB::raw('count(*) as total'))
            ->groupBy('tipo_prueba')
            ->get();

        $citas_agrupadas_line = Cita::query()
            ->select(
                'tipo_prueba',
                DB::raw('count(*) as total'),
                DB::raw('month(fecha_seleccionada) as mes')
            )
            ->groupBy('tipo_prueba', 'mes')
            ->get()->groupBy('tipo_prueba')
            ->map(function ($tipo_prueba) {
                return $tipo_prueba->keyBy('mes');
            });

        $pagos_agrupados_line = Cita::query()
            ->select(
                DB::raw('count(*) as sum'),
                DB::raw('sum(costo) as total'),
                'tipo_pago',
                DB::raw('month(fecha_seleccionada) as mes')
            )
            ->groupBy('mes', 'tipo_pago')
            ->where('pagado', 1)
            ->get()
            ->keyBy('tipo_pago');

        $dataset = collect();
        $datasetIngresos = collect();

        foreach ($citas_agrupadas_line as $tipo_prueba => $cita) {

            $meses = collect();

            for ($i = 1; $i <= 12; $i++) {
                if (isset($citas_agrupadas_line[$tipo_prueba][$i]))
                    $meses[$i] = $citas_agrupadas_line[$tipo_prueba][$i]->total;
                else
                    $meses[$i] = 0;
            }

            $dataset[] =  [
                'label' => $tipo_prueba == 1 ? 'PCR' : 'Antigenos',
                'backgroundColor' => $tipo_prueba == 1 ? '#0694a2' : '#1c64f2',
                'borderColor' =>  $tipo_prueba == 1 ? '#0694a2' : '#1c64f2',
                'data' => $meses->values()->toArray(),
                'fill' => false,
            ];
        }

        foreach ($pagos_agrupados_line as $tipo_pago => $pago) {
            $meses = collect();

            for ($i = 1; $i <= 12; $i++) {
                if (isset($pagos_agrupados_line[$tipo_pago]) && $pagos_agrupados_line[$tipo_pago]->mes == $i) {
                    $meses[$i] = $pagos_agrupados_line[$tipo_pago]->total;
                } else
                    $meses[$i] = 0;
            }

            $datasetIngresos[] =  [
                'label' => $tipo_pago == 1 ? 'Tarjeta de debito / credito' : 'Sucursal',
                'backgroundColor' => $tipo_pago == 1 ? '#9061f9' : '#ff5a1f',
                'borderColor' =>  $tipo_pago == 1 ? '#9061f9' : '#ff5a1f',
                'data' => $meses->values()->toArray(),
                'fill' => false,
            ];
        }

        $grafica_pruebas_pie = [
            'titulo' => 'Tipos de pruebas',
            'etiquetas' => collect([
                [
                    'texto' => 'PCR',
                    'clase' => 'bg-green-500'
                ],
                [
                    'texto' => 'Antigenos',
                    'clase' => 'bg-blue-500'
                ]
            ]),
            'dataset' => [[
                'data' => $citas_agrupadas->pluck('total'),
                'backgroundColor' => ['#0694a2', '#1c64f2'],
            ]]

        ];

        $grafica_pruebas_line =  [
            'titulo' => 'Tipos de pruebas',
            'etiquetas' => collect([
                [
                    'texto' => 'PCR',
                    'clase' => 'bg-green-500'
                ],
                [
                    'texto' => 'Antigenos',
                    'clase' => 'bg-blue-500'
                ]
            ]),
            'data' => [
                'datasets' => $dataset,
                'labels' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            ]
        ];

        /** ingresos grafica pie */

        $grafica_pruebas_pie_ingresos = [
            'titulo' => 'Tipos de pago',
            'etiquetas' => collect([
                [
                    'texto' => 'Tarjeta de debito/credito',
                    'clase' => 'bg-purple-500'
                ],
                [
                    'texto' => 'Sucursal',
                    'clase' => 'bg-orange-500'
                ]
            ]),
            'dataset' => [[
                'data' => Cita::get()->where('pagado', 1)->partition(function ($cita) {
                    return $cita->tipo_pago == 1;
                })->map(function ($el) {
                    return $el->count();
                }),
                'backgroundColor' => ['#9061f9', '#ff5a1f'],
            ]]
        ];

        /* ingresos grafica line */
        $grafica_pruebas_line_ingresos = [
            'titulo' => 'Tipos de pago',
            'etiquetas' => collect([
                [
                    'texto' => 'Tarjeta de debito/credito',
                    'clase' => 'bg-purple-500'
                ],
                [
                    'texto' => 'Sucursal',
                    'clase' => 'bg-orange-500'
                ]
            ]),
            'data' => [
                'datasets' => $datasetIngresos,
                'labels' => ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            ]
        ];

        $analisis_vendidos_hoy = Orden::whereRaw('TO_DAYS(created_at) = TO_DAYS(CURRENT_DATE)')
            ->where('estatus', 1)->count();


        $total_analisis_vendidos_hoy = Orden::whereRaw('TO_DAYS(created_at) = TO_DAYS(CURRENT_DATE)')
            ->where('estatus', 1)->sum('total');

        return view('lab.panel')
            ->with(compact([
                'total',
                'asistidas',
                'no_asistidas',
                'restantes',
                'total_hoy',
                'no_asistidas_hoy',
                'asistidas_hoy',
                'restantes_hoy',
                'ingreso_online_general',
                'ingreso_sucursal_general_real',
                'ingreso_total',
                'ingreso_sucursal_general_estimado',
                'ingreso_sucursal_dia_real',
                'ingreso_sucursal_dia_estimado',
                'ingreso_online_dia',
                'ingreso_total_dia',
                'ingreso_online_siguiente',
                'ingreso_sucursal_siguiente',
                'ingreso_total_siguiente',
                'total_siguiente',
                'total_medicos',
                'notas_impresas',
                'grafica_pruebas_pie',
                'grafica_pruebas_line',
                'grafica_pruebas_pie_ingresos',
                'grafica_pruebas_line_ingresos',
                'orden',
                'ingreso_orden_total',
                'analisis_vendidos_hoy',
                'total_analisis_vendidos_hoy',
                'orden_no'
            ]));
    }

    public function export(Request $request)
    {
        // return (new historicoExport)->download('Laboratorio Corregidora - historico.xlsx');
        return Excel::download(new historicoExport(
            $request
        ), 'Laboratorio Corregidora - historico.xlsx');
    }


    public function export_hoy(Request $request)
    {
        // return (new citasHoyExport)->download('Laboratorio Corregidora - Citas Hoy.xlsx');
        return Excel::download(new citasHoyExport(
            $request
        ), 'Laboratorio Corregidora - Citas Hoy.xlsx');
    }


    public function export_manana(Request $request)
    {
        // return (new citasMananaExport)->download('Laboratorio Corregidora - Citas Mañana.xlsx');
        return Excel::download(new citasMananaExport(
            $request
        ), 'Laboratorio Corregidora - Citas Mañana.xlsx');
    }

    // expor imagenologia historico
    public function exportImagenologiaHistorico(Request $request)
    {
        return Excel::download(new HistoricoExportImagenologia(
            $request
        ), 'Laboratorio Corregidora - historicoImagenologia.xlsx');
    }

    // expor imagenologia hoy
    public function exportImagenologiaHoy(Request $request)
    {
        return Excel::download(new HoyExportImagenologia(
            $request
        ), 'Laboratorio Corregidora - hoyImagenologia.xlsx');
    }

    // expor imagenologia manana
    public function exportImagenologiaManana(Request $request)
    {
        return Excel::download(new MananaExportImagenologia(
            $request
        ), 'Laboratorio Corregidora - mananaImagenologia.xlsx');
    }

    public function imprimir(Request $request){

        // dd($request->toArray());

        $arreglo = null;

        foreach($request->toArray() as $key =>$value){
            if($key == 'arreglo'){
                $arreglo = $value;
            }
            if($key == 'tipo'){
                $tipo = $value;
            }
        }

        if(isset($arreglo)){
            $consulta = cita::query()
            ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'paciente_email',  'tel_celular',  'fecha_seleccionada', 'costo', 'tipo_tarjeta', 'digitos')
            ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');
            foreach($arreglo as $key => $value){
                $consulta->orWhere('citas.id','=',$value);
            }

            $respuestas = $consulta->get()->toArray();
            return view('lab.imprimir')->with('respuestas', $respuestas);

        }else{
            if($tipo != 0){

                $consulta = cita::query()
                ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'paciente_email',  'tel_celular',  'fecha_seleccionada', 'costo', 'tipo_tarjeta', 'digitos')
                ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');

                if($tipo == 1){
                    $consulta->where('pagado', '=', 1)->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)');
                }

                if($tipo == 2){
                    $consulta->where('pagado', '=', 1)->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE) + 1');
                }

                $respuestas = $consulta->get()->toArray();
                return view('lab.imprimir')->with('respuestas', $respuestas);

            }else{
                $consulta = cita::query()
                ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'paciente_email',  'tel_celular',  'fecha_seleccionada', 'costo', 'tipo_tarjeta', 'digitos')
                ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id')
                ->where('pagado', '=', 1);

                $respuestas = $consulta->get()->toArray();
                return view('lab.imprimir')->with('respuestas', $respuestas);
            }
        }

        // $respuestas = $consulta->get()->toArray();
        // return view('lab.imprimir')->with('respuestas', $respuestas);

    }

    public function imprimirHistoricoImagenologia(Request $request){

        $arreglo = null;

        foreach($request->toArray() as $key =>$value){
            if($key == 'arreglo'){
                $arreglo = $value;
            }
        }

        if(isset($arreglo)){
            $consulta = CitaImagenologico::query()
            ->select('analisis_imagenologicos.nombre AS tipo', 'citas_imagenologicos.nombre', 'telefono', 'correo', 'fecha_nacimiento', 'fecha_cita', 'schedule_images.horario_inicial', 'schedule_images.horario_final', 'citas_imagenologicos.created_at')
            ->leftJoin('analisis_imagenologicos', 'citas_imagenologicos.analisisimagelologico_id', '=', 'analisis_imagenologicos.id')
            ->leftJoin('schedule_images', 'citas_imagenologicos.horariosimagelologico_id', '=', 'schedule_images.id');
            // foreach($this->request as $id){
            //     $consulta->orWhere('citas_imagenologicos.id','=',$id);
            // }

            $respuestas = $consulta->get()->toArray();
            return view('lab.imprimirImagenologia')->with('respuestas', $respuestas);

        }else{
            // if($tipo != 0){

                // $consulta = cita::query()
                // ->select('no_confirmacion', 'nombre', 'apellido_paterno', 'apellido_materno', 'paciente_email',  'tel_celular',  'fecha_seleccionada', 'costo', 'tipo_tarjeta', 'digitos')
                // ->leftJoin('pacientes', 'citas.paciente_id', '=', 'pacientes.id');

                // if($tipo == 1){
                //     $consulta->where('pagado', '=', 1)->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE)');
                // }

                // if($tipo == 2){
                //     $consulta->where('pagado', '=', 1)->whereRaw('TO_DAYS(fecha_seleccionada) = TO_DAYS(CURRENT_DATE) + 1');
                // }

                // $respuestas = $consulta->get()->toArray();
                // return view('lab.imprimir')->with('respuestas', $respuestas);

            // }else{
                $consulta = CitaImagenologico::query()
                ->select('analisis_imagenologicos.nombre AS tipo', 'citas_imagenologicos.nombre', 'telefono', 'correo', 'fecha_nacimiento', 'fecha_cita', 'schedule_images.horario_inicial', 'schedule_images.horario_final', 'citas_imagenologicos.created_at')
                ->leftJoin('analisis_imagenologicos', 'citas_imagenologicos.analisisimagelologico_id', '=', 'analisis_imagenologicos.id')
                ->leftJoin('schedule_images', 'citas_imagenologicos.horariosimagelologico_id', '=', 'schedule_images.id');

                $respuestas = $consulta->get()->toArray();
                return view('lab.imprimirImagenologia')->with('respuestas', $respuestas);
            // }
        }

    }

    public function imprimirHoyImagenologia(Request $request){

        $arreglo = null;

        foreach($request->toArray() as $key =>$value){
            if($key == 'arreglo'){
                $arreglo = $value;
            }
        }

        if(isset($arreglo)){
            $consulta = CitaImagenologico::query()
            ->select('analisis_imagenologicos.nombre AS tipo', 'citas_imagenologicos.nombre', 'telefono', 'correo', 'fecha_nacimiento', 'fecha_cita', 'schedule_images.horario_inicial', 'schedule_images.horario_final', 'citas_imagenologicos.created_at')
            ->leftJoin('analisis_imagenologicos', 'citas_imagenologicos.analisisimagelologico_id', '=', 'analisis_imagenologicos.id')
            ->leftJoin('schedule_images', 'citas_imagenologicos.horariosimagelologico_id', '=', 'schedule_images.id');
            foreach($this->request as $id){
                $consulta->orWhere('citas_imagenologicos.id','=',$id);
            }
            $consulta->whereRaw('TO_DAYS(fecha_cita) = TO_DAYS(CURRENT_DATE)');

            $respuestas = $consulta->get()->toArray();
            return view('lab.imprimirImagenologia')->with('respuestas', $respuestas);

        }else{

            $consulta = CitaImagenologico::query()
            ->select('analisis_imagenologicos.nombre AS tipo', 'citas_imagenologicos.nombre', 'telefono', 'correo', 'fecha_nacimiento', 'fecha_cita', 'schedule_images.horario_inicial', 'schedule_images.horario_final', 'citas_imagenologicos.created_at')
            ->leftJoin('analisis_imagenologicos', 'citas_imagenologicos.analisisimagelologico_id', '=', 'analisis_imagenologicos.id')
            ->leftJoin('schedule_images', 'citas_imagenologicos.horariosimagelologico_id', '=', 'schedule_images.id')
            ->whereRaw('TO_DAYS(fecha_cita) = TO_DAYS(CURRENT_DATE)');

            $respuestas = $consulta->get()->toArray();
            return view('lab.imprimirImagenologia')->with('respuestas', $respuestas);

        }

    }

    public function imprimirMananaImagenologia(Request $request){

        $arreglo = null;

        foreach($request->toArray() as $key =>$value){
            if($key == 'arreglo'){
                $arreglo = $value;
            }
        }

        if(isset($arreglo)){
            $consulta = CitaImagenologico::query()
            ->select('analisis_imagenologicos.nombre AS tipo', 'citas_imagenologicos.nombre', 'telefono', 'correo', 'fecha_nacimiento', 'fecha_cita', 'schedule_images.horario_inicial', 'schedule_images.horario_final', 'citas_imagenologicos.created_at')
            ->leftJoin('analisis_imagenologicos', 'citas_imagenologicos.analisisimagelologico_id', '=', 'analisis_imagenologicos.id')
            ->leftJoin('schedule_images', 'citas_imagenologicos.horariosimagelologico_id', '=', 'schedule_images.id');
            foreach($this->request as $id){
                $consulta->orWhere('citas_imagenologicos.id','=',$id);
            }
            $consulta->whereRaw('TO_DAYS(fecha_cita) = TO_DAYS(CURRENT_DATE) + 1');

            $respuestas = $consulta->get()->toArray();
            return view('lab.imprimirImagenologia')->with('respuestas', $respuestas);

        }else{

            $consulta = CitaImagenologico::query()
            ->select('analisis_imagenologicos.nombre AS tipo', 'citas_imagenologicos.nombre', 'telefono', 'correo', 'fecha_nacimiento', 'fecha_cita', 'schedule_images.horario_inicial', 'schedule_images.horario_final', 'citas_imagenologicos.created_at')
            ->leftJoin('analisis_imagenologicos', 'citas_imagenologicos.analisisimagelologico_id', '=', 'analisis_imagenologicos.id')
            ->leftJoin('schedule_images', 'citas_imagenologicos.horariosimagelologico_id', '=', 'schedule_images.id')
            ->whereRaw('TO_DAYS(fecha_cita) = TO_DAYS(CURRENT_DATE) + 1');

            $respuestas = $consulta->get()->toArray();
            return view('lab.imprimirImagenologia')->with('respuestas', $respuestas);

        }

    }

}
