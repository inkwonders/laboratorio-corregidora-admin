<?php

namespace App\Http\Controllers;
use App\Exports\AnalisisImagenologiaExport;

class AnalisisImagenologiaController extends Controller
{
    public function export_analisis_imagenologia(){
        return (new AnalisisImagenologiaExport)->download('Laboratorio Corregidora - Listado Analisis Imagenologia.xlsx');
    }
}
