<?php

namespace App\Http\Controllers;

use App\Exports\analisisExport;
use App\Exports\historicoAnalisisExport;
use App\Exports\ordenExport;
use App\Models\Analisis;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AnalisisImport;

class AnalisisController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $analisis = Analisis::get();

        return view('lab.analisis.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function show(Analisis $analisis)
    {
        return view('lab.analisis.show')
            ->with('analisis',$analisis);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function edit(Analisis $analisis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Analisis $analisis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Analisis  $analisis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Analisis $analisis)
    {
        //
    }

    public function export_analisis()
    {
        return (new analisisExport)->download('Laboratorio Corregidora - Listado Analisis.xlsx');
    }

    public function export_ordenes()
    {
        return (new ordenExport)->download('Laboratorio Corregidora - Listado Ordenes.xlsx');
    }

    public function export_historico()
    {
        return (new historicoAnalisisExport)->download('Laboratorio Corregidora - Listado Historico Análisis.xlsx');
    }

    public function showImportAnalisis(Request $request)
    {
        return view('lab.analisis.uploadAnalisis');
    }

    public function importAnalisis(Request $request)
    {

        // $request->validate([
        //     'archivo'       => 'required|file|mimes:xlsx,xls,csv'
        // ]);

        //  dd($request->archivo);

        $folios = Excel::toCollection(new AnalisisImport, $request->file('archivo'))
        ->first() // Primera hoja
        ->skip(1) // Omitimos la primer fila
        ->mapWithKeys(function($row) {



            $folio=$row[7];//agregar al final una columna con autoincremental en el excel

            return [
                $folio => [
                    'd_codigo' => $row[0],
                    'd_asenta'  => $row[1],
                    'd_tipo_asenta'=>$row[2],
                    'D_mnpio'=>$row[3],
                    'd_estado'=>$row[4],
                    'd_ciudad'=>$row[5],
                    'd_CP'=>$row[6],

                ]
            ];


        })
        ->whereNotNull();

        //  dd($folios);


        foreach ($folios as $folio) {

            $municipios=$folio['D_mnpio'];

            $busqueda=Municipality::where('description',$municipios)->first();




            // dd($folio['d_estado']);

            $estados=$folio['d_estado'];
            $neighborhood=$folio['d_asenta'];
            $zip_code=$folio['d_codigo'];
            $type=$folio['d_tipo_asenta'];


            $busqueda_estado=State::where('description',$estados)->first();



            try {


                if(is_null($busqueda)){

                    $inserta_mun=Municipality::create(
                        [
                            'description' => $municipios,
                            'latitude' => '0.00000000',
                            'longitude' => '0.00000000',
                            'state_id' => $busqueda_estado->id

                        ],
                        true // Recursivo
                    );


                    $id_municipio=$inserta_mun->id;

                }else{ $id_municipio=$busqueda->id; }



            $inserta_col=Neighborhood::create(
                [
                    'neighborhood' => $neighborhood,
                    'zip_code' => $zip_code,
                    'type' => $type,
                    'municipality_id' => $id_municipio

                ],
                true // Recursivo
            );

        } catch (\Throwable $th) {


            return response()->json(['success' => false, 'error' => $th->getMessage()], 503);
        }
        }





    }

}
