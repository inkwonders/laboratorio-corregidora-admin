<?php

namespace App\Http\Controllers;

use App\Exports\tarjetaExport;
use Illuminate\Http\Request;

class ResponseTarjetaController extends Controller
{
    public function export_tarjetas(Request $request)
    {
        // dd($request->fs,$request->fe);
        // dd($request->fe);

        if($request->fs == null || $request->fe == null){
            return (new tarjetaExport('vacio', 'vacio'))->download('Laboratorio Corregidora - Tarjeta pagos.xlsx');
        }else{
            return (new tarjetaExport($request->fs, $request->fe))->download('Laboratorio Corregidora - Tarjeta pagos.xlsx');
        }

    }
}
