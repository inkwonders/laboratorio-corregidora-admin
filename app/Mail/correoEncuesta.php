<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class correoEncuesta extends Mailable
{
    use Queueable, SerializesModels;

    private $encuesta;

    public function __construct($encuesta)
    {
        $this->encuesta = Crypt::encryptString($encuesta);
        // $this->encuesta = $encuesta;
    }

    public function build()
    {
        return $this->
        from('noreply@laboratoriocorregidora.com.mx','Laboratorios Corregidora')
        ->view('lab.encuestas.correo')->with('encuesta',$this->encuesta);
    }
}
