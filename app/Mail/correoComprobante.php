<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class correoComprobante extends Mailable
{
    use Queueable, SerializesModels;

    private $total, $enviar_confirmacion, $enviar_email, $enviar_analisis;


    public function __construct($total, $enviar_confirmacion, $enviar_email, $enviar_analisis)
    {
        $this->total = $total;
        $this->enviar_confirmacion = $enviar_confirmacion;
        $this->enviar_email = $enviar_email;
        $this->enviar_analisis = $enviar_analisis;
    }


    public function build()
    {
        return $this->from('noreply@laboratoriocorregidora.com.mx', 'Laboratorios Corregidora')
            ->view('lab.analisis.comprobante')->with([
                'confirmacion' => $this->enviar_confirmacion,
                'analisis' =>  $this->enviar_analisis,
                'total' =>  $this->total
            ]);
    }
}
