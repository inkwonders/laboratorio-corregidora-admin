<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class EncuestasNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

     protected  $encuesta;

    public function __construct($encuesta)
    {
        //
        $this->encuesta = $encuesta;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        // ->subject('Asunto del Correo')
        // ->view('lab.encuestas.correo', [
        //     'encuesta' => $this->encuesta,
        // ]);
        ->subject(Lang::get('Encuesta enviada'))
        // ->bcc('pedidosweb@booktrain.com.mx')
        // ->bcc('pedidosweb@provesa.mx')
        // ->bcc('coordinacionsac1@provesa.mx')
        // ->bcc($this->pedido->email_pedido)
        ->view('lab.encuestas.correo', [
            'encuesta' => $this->encuesta,
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
