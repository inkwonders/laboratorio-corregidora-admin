<?php

namespace App\Jobs;

use App\Mail\correoComprobante;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class sendComprobante implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $total, $enviar_confirmacion, $enviar_email, $enviar_analisis;

    public function __construct($total, $enviar_confirmacion, $enviar_email, $enviar_analisis)
    {
        $this->total = $total;
        $this->enviar_confirmacion = $enviar_confirmacion;
        $this->enviar_email = $enviar_email;
        $this->enviar_analisis = $enviar_analisis;

    }


    public function handle()
    {


        $email = new correoComprobante($this->total, $this->enviar_confirmacion, $this->enviar_email, $this->enviar_analisis);

        try {

            Mail::to($this->enviar_email)->queue($email);


            logger('correo enviado' . $email);
        } catch (\Throwable $th) {

            logger('error de envio de correos ' . $th->getMessage());
        }

    }
}
