<?php

namespace App\Jobs;

use App\Mail\correoEncuesta;
use App\Models\EncuestaUsuario;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable as MailMailable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class sendEncuestas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email;
    private $encuesta;

    public function __construct($email, $encuesta)
    {
        $this->email = $email;
        $this->encuesta = $encuesta;
    }

    public function handle()
    {
        // dd("entra");
        $email = new correoEncuesta($this->encuesta);
        // dd($email);

        try {

            Mail::to($this->email)->queue($email);

            DB::table('encuesta_usuario')->where('id', '=', $this->encuesta)->update(['enviado' => 1]);

            logger('correo enviado' . $email);
        } catch (\Throwable $th) {

            logger('encuesta ' . $this->encuesta . ' / error de envio de correos ' . $th->getMessage());
        }
    }
}
