<?php

use App\Http\Controllers\AnalisisController;
use App\Http\Controllers\CitasController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $usuario=Auth::User();

    if (is_null($usuario)) {
        return redirect('login');
    }

    if ($usuario->hasRole('Administrador')) {
        return redirect('panel');
    }

    if ($usuario->hasRole('Medico')) {
        return redirect('analisis');
    }

});

Route::group(['middleware' => ['auth:sanctum', 'verified', 'role:Administrador']], function () {
    Route::get('/panel', [App\Http\Controllers\CitasController::class, 'getPanel'])->name('panel');
    // rutas citas covid
    Route::view('historico', 'lab.historico')->name('historico');
    Route::view('citashoy', 'lab.citashoy')->name('citashoy');
    Route::view('citasmañana', 'lab.citasmañana')->name('citasmañana');
    // rutas citas imagenologia
    Route::view('historicoImagenologia', 'lab.historicoImagenologia')->name('historicoImagenologia');
    Route::view('hoyImagenologia', 'lab.hoyImagenologia')->name('hoyImagenologia');
    Route::view('mananaImagenologia', 'lab.mananaImagenologia')->name('mananaImagenologia');

    Route::get('/citas/{id}', [App\Http\Controllers\CitasController::class, 'show'])->name('citas.show');
    Route::view('usuarios', 'lab.usuarios')->name('usuarios');
    Route::view('pruebas', 'lab.pruebas')->name('pruebas');
    Route::view('tarjetas', 'lab.tarjetas')->name('Pago tarjetas');
    Route::view('ordenes', 'lab.ordenes')->name('ordenes');
    Route::get('covidgeneral', [App\Http\Controllers\CovidController::class, 'getPanelCovid'])->name('covid-general');
    Route::get('covidhoy', [App\Http\Controllers\CovidController::class, 'getPanelCovidHoy'])->name('covid-hoy');
    Route::get('covidtomorrow', [App\Http\Controllers\CovidController::class, 'getPanelCovidTomorrow'])->name('covid-tomorrow');
    Route::get('analisisgeneral', [App\Http\Controllers\CovidController::class, 'getPanelAnalisis'])->name('analisis-general');
    Route::get('listasanalisis', [App\Http\Controllers\CovidController::class, 'getPanelListasAnalisis'])->name('listas-analisis');
    Route::view('encuestas', 'lab.encuestas.index')->name('encuestas');
    Route::get('historico/export/', 'CitasController@export')->name('export_historico');
    Route::get('citashoy/export/', 'CitasController@export_hoy')->name('export_hoy');
    Route::get('citasmanana/export/', 'CitasController@export_manana')->name('export_manana');
    Route::get('analisis/export/', 'AnalisisController@export_analisis')->name('export_analisis');
    Route::get('ordenes/export/', 'AnalisisController@export_ordenes')->name('export_ordenes');
    Route::get('tarjeta/export/', 'ResponseTarjetaController@export_tarjetas')->name('export_tarjetas');
    Route::get('usuarios/export/', 'CovidController@export_usuarios')->name('export_usuarios');

    // export imagenolgia
    Route::get('analisisImagenologia/export/', 'AnalisisImagenologiaController@export_analisis_imagenologia')->name('export_analisis_imagenologicos');
    Route::get('historicoImagenologia/export/', 'CitasController@exportImagenologiaHistorico')->name('export_historico_imagenologia');
    Route::get('hoyImagenologia/export/', 'CitasController@exportImagenologiaHoy')->name('export_hoy_imagenologia');
    Route::get('mananaImagenologia/export/', 'CitasController@exportImagenologiaManana')->name('export_manana_imagenologia');

    Route::post('importAnalisis', [App\Http\Controllers\AnalisisController::class, 'importAnalisis'])->name('importAnalisis');
    Route::get('showUpdateAnalisis', [App\Http\Controllers\AnalisisController::class, 'showImportAnalisis'])->name('showUpdateAnalisis');
    Route::get('listado_correos_cuesta/export/', 'ListaCorreoEncuestaController@exportLista')->name('export_correos_encuesta');

    Route::get('/imprimir', 'CitasController@imprimir')->name('imprimir');
    Route::get('/imprimirHistoricoImagenologia', 'CitasController@imprimirHistoricoImagenologia')->name('imprimir-historico-imagenologia');
    Route::get('/imprimirHoyImagenologia', 'CitasController@imprimirHoyImagenologia')->name('imprimir-hoy-imagenologia');
    Route::get('/imprimirMananaImagenologia', 'CitasController@imprimirMananaImagenologia')->name('imprimir-manana-imagenologia');

    // lista analsis imagenologia
    Route::view('analisisImagenologia', 'lab.pruebasImagenologia')->name('analsis-imagenologia');
});



Route::group(['middleware' => ['auth:sanctum', 'verified', 'role:Medico']], function () {
    Route::get('analisis', [App\Http\Controllers\AnalisisController::class, 'index'])->name('analisis.panel');
    Route::get('/analisis/{analisis}', [App\Http\Controllers\AnalisisController::class, 'show'])->name('analisis.show');
    Route::get('/carrito', [App\Http\Controllers\CarritoController::class, 'index'])->name('carrito');
    Route::get('/carrito/paciente', [App\Http\Controllers\CarritoController::class, 'paciente'])->name('carrito.paciente');
    Route::get('/carrito/comprobante', [App\Http\Controllers\CarritoController::class, 'comprobante'])->name('carrito.comprobante');
    Route::view('historico-medico', 'lab.analisis.historicoMedico')->name('analisis.historicomedico');
    Route::get('historico_analisis/export/', 'AnalisisController@export_historico')->name('export_historico_analisis');


});

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('responder_encuesta/{encuesta}', 'responderEncuestaController@show')->name('form_encuesta');

// Route::view('correo', 'emails.form')->name('email-form');


