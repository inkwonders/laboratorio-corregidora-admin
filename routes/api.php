<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\AnalisisImagenologicosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::apiResource('v1/posts', App\Http\Controllers\Api\V1\PostController::class)->middleware('api');

Route::prefix('v1/analisis-imagenologicos')->group(function () {

    //Contacto
    Route::post('/contacto', [AnalisisImagenologicosController::class, 'mandarCorreoContacto']);
    // crea cita
    Route::post('/guardar-cita', [AnalisisImagenologicosController::class, 'guardarCita']);
    //Analisis imagenologicos
    Route::post('/', [AnalisisImagenologicosController::class, 'getEstudios']);
    //tipos de analisis imagenologicos
    Route::get('/tipos', [AnalisisImagenologicosController::class, 'getTipos']);
    //Sucursales
    Route::post('/sucursales', [AnalisisImagenologicosController::class, 'getSucursales']);
    //lista de horarios
    Route::post('/horarios', [AnalisisImagenologicosController::class, 'getHorarios']);
    //Analisis especifico
    Route::post('/{slug}', [AnalisisImagenologicosController::class, 'getEstudioEspecifico']);

});


